class test{
  public static void main(String args[]) {
    cell_matrix_channel pc,cc,pc_cc,brk;

    String msg="Test sequence for CylconeIV-based emulator";
    cell_matrix_interface cmi=new cell_matrix_interface("test.seq",msg);

/* Start a wire */
    wire_build w=new wire_build(cmi,"e",0,0,"PC,CC");
    w.init();
    w.extend(4);
    w.turn("cw");
    w.turn("ccw");
    w.extend(1);

    cmi.close();
  }
}
