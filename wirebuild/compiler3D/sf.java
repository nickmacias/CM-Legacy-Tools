/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class sf{
  public static void main(String args[]) {

    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("sf.seq",msg);

/* Read the binary grid file */
    binary_file_reader bfr_e=new binary_file_reader("../loader/sbir/sf.bin");

    space_filler sf=new space_filler(cmi,63);

// Build a sequence to the East
    cmi.comment("Building in parallel to east");
    sf.init(bfr_e);
    sf.build(bfr_e,0);
    sf.build(bfr_e,0);

/* Finish up */
    cmi.comment("Finished");
    cmi.close();
  }
}
