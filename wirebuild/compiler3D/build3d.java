/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class build3d{
  static cell_matrix_channel pc,cc,pc_cc,cc_cc,sel,sel2;
  static cell_matrix_3d_interface cmi;

  public static void main(String args[]) {
    int i;

    init();
    initial();
    for (i=0;i<7;i++){
      top();
      east();
    }
    deinit();
  }

  static void init()
  {
    String msg="Copyright (C) 2002 Cell Matrix Corporation. All Rights Reserved";
    cmi=new cell_matrix_3d_interface("c:/nick/javasim/3dbuild.seq",msg);
    sel=cmi.assign_channel(0,0,0,"DW");
    pc=cmi.assign_channel(1,0,0,"DW");
    pc_cc=cmi.assign_channel(1,0,0,"CW");
    cc=cmi.assign_channel(2,0,0,"DW");
    cc_cc=cmi.assign_channel(2,0,0,"CW");
    sel2=cmi.assign_channel(3,0,0,"DW");
  }

  static void deinit()
  {
    cmi.close();
  }

  static void east()
  {
/* Extend to east */
    sel.set(false);sel2.set(false);cmi.cycle();
    pc.set("ds=w;cs=1"); cc.set(true); cmi.cycle(); 
    pc.set("ds=sn+;cs=1"); cc.set(false); cmi.cycle();
    pc.set("ds=w");cc.set(true);cmi.cycle();
// Configure SEL2 cell
    pc.set("de=w;dn=w");cc.set(false);cmi.cycle();
    pc.set("ds=w;cs=1");cc.set(true); cmi.cycle(); 
    cc.set(true);pc.set("ds=w;cs=1");cmi.cycle();
// Configure CC
    pc.set("dt=ws&;dw=ns~&ts&+;de=we&s~& ws& +;dn=we~&s~& wt~&s& +;"); cc.set(false); cmi.cycle();
    pc.set("dn=w;cn=1");cc.set(true);cmi.cycle();
// Set SEL cell
    pc.set("ds=w;de=w");cc.set(false);cmi.cycle();
// Configure PC
    pc.set("dt=wn&;de=wn&;ds=1n~&;db=1n&;dt=wn&;de=w;ct=sn&;ce=sn~&");cc.set(true);cmi.cycle();
    cc.set(false);pc.set(false);cmi.cycle();
  }

  static void top()
  {
/* Build a row on TOP */
    sel.set(true);sel2.set(true);cmi.cycle();
    cc.set(true);pc.set("ds=b;cs=1");cmi.cycle();
    pc.set("db=n;dn=bt~&;dt=bt&");cc.set(false);cmi.cycle();
    pc.set("dt=b;ct=s;ds=1");cc.set(true);cmi.cycle();
    pc.set(false);cc.set(false);cmi.cycle();
  }
  static void initial()
  {
// Configure the initial PC and CC cells, plus the SEL blocks
    pc.set("ds=w;cs=1"); pc_cc.set(true); cmi.cycle(); 
    pc.set("ds=sn+;cs=1"); pc_cc.set(false); cmi.cycle();
    pc.set("ds=w");pc_cc.set(true);cmi.cycle();
// Configure SEL2 cell
    pc.set("de=w;dn=w");pc_cc.set(false);cmi.cycle();
    pc.set("ds=w;cs=1"); pc_cc.set(true); cmi.cycle(); 
// Configure CC cell
    pc.set("dt=ws&;dw=ns~&ts&+;de=we&s~& ws& +;dn=we~&s~& wt~&s& +;"); pc_cc.set(false); cmi.cycle();
    pc.set("dn=w;cn=1");pc_cc.set(true);cmi.cycle();
// Set SEL cell
    pc.set("ds=w;de=w");pc_cc.set(false);cmi.cycle();

// Set PC
    pc.set("dt=wn&;de=wn&;ds=1n~&;db=1n&;dt=wn&;de=w;ct=sn&;ce=sn~&");pc_cc.set(true);cmi.cycle();
    pc.set(false);pc_cc.set(false);cmi.cycle();
  }
}
