class test{
  static cell_matrix_channel pc,cc,pc_cc,brk;
  static cell_matrix_3d_interface cmi;

  public static void main(String args[])
  {
    cmi=new cell_matrix_3d_interface("test.seq","test");

    pc=cmi.assign_channel(0,0,0,"DW");
    pc_cc=cmi.assign_channel(0,0,0,"CW");
    cc=cmi.assign_channel(1,0,0,"DW");
    brk=cmi.assign_channel(2,0,0,"DW");

// configure PC/CC at Z=0
    pc.set("ds=w;cs=1");pc_cc.set(true);cmi.cycle();
    pc.set("dt=wt&;dn=wt~&");pc_cc.set(false);cmi.cycle();
    pc.set("dt=w;dw=t;ct=s");pc_cc.set(true);cmi.cycle();
    pc.set(false);pc_cc.set(false);cmi.cycle();

// build initial PC/CC at Z=1
    pc.set("cs=1;ds=b");cc.set(true);cmi.cycle();
    pc.set("dn=be~&;de=be&;db=n");cc.set(false);cmi.cycle();
    pc.set("de=b;ce=s;ds=1");cc.set(true);cmi.cycle();
    pc.set(false);cc.set(false);cmi.cycle();

    cmi.close();
  }
}
