/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class nick{
  public static void main(String args[]) {
    int i,c,r;
    cell_matrix_channel pc,cc,pc_cc,cc_cc;

    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("boot.seq",msg);

    if (args.length != 1){
      System.out.println("Usage: boot00 filename.bin");
      System.exit(1);
    }

/* Read the binary grid file */
    binary_file_reader bfr=new binary_file_reader(args[0]);

/* Start a wire */
    wire_build w=new wire_build(cmi,"s",0,15,"CC,PC,BRK");
    //w.preclear(true);
      w.init();
      w.extend_break(true); /* Include breakline in extend() */
      w.extend(5);
      w.extend_break(false);
      w.end_break(); /* Add the action end to the breakline */
      w.extend(5);
    cmi.close();
  }
}
