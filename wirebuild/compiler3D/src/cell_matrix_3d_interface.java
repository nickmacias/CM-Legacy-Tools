/*
 * Copyright (C) 1992-2002 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

import java.io.*;
import java.lang.*;
import java.util.*;

/*
 * Main class for 3D Cell Matrix interface
 *
 * Main classes are:
 *  cmi=new cell_matrix_3d_interface("filename","legal-info-text");
 *    This creates an interface object, which will write configuration
 *    strings into "filename"
 *
 *  chan=new cell_matrix_channel(row,col,layer,"side");
 *    where side is DN, DS, DW, DE, DT, DB,
 *                  CN, CS, CW, CE, CT or CB
 *
 * Methods are as follows:
 *  chan=cmi.assign_channel(row,col,layer,"side");  which creates a channel
 *  chan.set(true/false);
 *  chan.set(int);
 *  chan.set("256-hex-char string");
 *  chan.set("DW=NE&,CE=1"); where argument can be any set of RPN
 *      logical equations, separated by ,;: or \
 *  The set() methods indicate a 768-bit pattern which should be sent to
 *  the corresponding channel during the next 768 clock ticks
 *
 *  cmi.cycle()  requests another 768 clock ticks
 *
 *  cmi.close()  Closes the output file
 *
 *  cmi.rotate("cw" or "ccw" or "reset");  Sets the rotation for future TTs
 *      reset returns the absolute rotation to 0.
 *      For now, rotation occurs only in the Row/Col plane (single layer)
 *
 *  cmi.flip(boolean flag)  Flip cell about E-W axis if true
 *
 *  boolean cmi.flip() - returns flip state
 *
 *  int cmi.rotate() - returns rotate state
 *
 *  cmi.push(); cmi.pop(); Save and restore current state (one level)
 *
 *  cmi.comment(String text); Add a comment to .SEQ file
 *      Can be used as breakpoint in simulator
 *
 *
 * In general, truth table bitpatterns are stored internally as 256 hex
 * characters. The TT organization is:
 *
 *   64 rows of C bits, followed by 64 rows of D bits
 *   Each row uses two hex characters (MSB/LSB)
 *   Input order is BTWSEN
 *   Output order is also BTWSEN
 *   Therefore, the upper right bit is CN out, etc.
 *
 * Bits are sent from the lower-left corner of the TT (DNout when all Din=1)
 * Shifting is from Right to Left, Top to Bottom.
 *
 * The corresponding 256 character string for a truth table would be:
 *   First character=Hex character corresponding to WSEN
 *   8*W + 4*S + 2*E + N
 *   Second character is [00BT]
 * and so on.
 * 
 * ----------------------------------------------
 * The above may or may not be true lol. But here's the final
 * bit-sequence for configuring a TT inside the Matrix itself:
 *
 * C-Mode Bit Ordering is:
 * 
 * B T W S E N |  CB   CT   CW   CS   CE   CN   DB   DT   DW   DS   DE   DN
 * 0 0 0 0 0 0 | 762  763  764  765  766  767  378  379  380  381  382  383
 * ...
 * 
 * 1 1 1 1 1 0 | 390  391  392  393  394  395    6    7    8    9   10   11
 * 1 1 1 1 1 1 | 384  385  386  387  388  389    0    1    2    3    4    5
 * ----------------------------------------------
 *
 * Each row of the output file is terminated by a platform-specific
 * newline separator. Additionally, each row is surrounded by quotes.
 *
 * Output file format is:
 *  #SEQUENCE FILE         standard header
 *  Legal Info Text          any text not containing \n
 *  Flags                    reserved for future use, i.e., encryption, etc.
 *  Number of channels
 *  row,col,layer,side       specifies location of first channel
 *  row,col,layer,side       specifies location of second channel
 *     ...
 *  row,col,layer,side       specifies location of last channel
 *  pattern 0                First pattern for channel 0
 *  pattern 1                First pattern for channel 1
 *     ...
 *  pattern m                First pattern for channel m
 *  pattern 0                Second pattern for channel 0
 *  pattern 1                Second pattern for channel 1
 *     ...
 *  pattern m                Second pattern for channel m
 *     ...
 *  pattern 0                Last pattern for channel 0
 *  pattern 1                Last pattern for channel 1
 *     ...
 *  pattern m                Last pattern for channel m
 *  #EOF                     Standard trailer
 *
 */

class cmstate{
  boolean nsflip;
  int rotation;
};

class cell_matrix_3d_interface{
  Vector channel_vector=new Vector(10); /* Remember all channels */
  boolean first_cycle=false; /* Set after first call to cycle() */
  BufferedWriter bw;
  String legal=""; /* Store customer info in header */
  int rotation=0; /* # of CW steps (0-3) */
  boolean nsflip=false; /* Set to do flip */
  Stack xstack=new Stack<Integer>(); /* Record rotations and flips */
   

  public void push(){
    xstack.push(new Integer(rotation));
    xstack.push(new Boolean(nsflip));
  }

  public void pop(){
    nsflip=((Boolean)xstack.pop()).booleanValue();
    rotation=((Integer)xstack.pop()).intValue();
  }

// sometimes, we just want to save/restore a specific state

  public cmstate state()
  {
    cmstate r=new cmstate();
    r.nsflip=nsflip;
    r.rotation=rotation;
    return(r);
  }

  public void state(cmstate s)
  {
    nsflip=s.nsflip;
    rotation=s.rotation;
  }

/* Constructors */

  public cell_matrix_3d_interface(String fname, String text)
  {
    this(fname);
    legal=text;
  }

  public cell_matrix_3d_interface(String fname) /* Output to a file */
  {
    try {
      FileWriter fw=new FileWriter(fname);
      bw=new BufferedWriter(fw);
    } catch (Exception e){
      System.out.println("ERROR OPENING OUTPUT FILE " + fname + ":" + e);
      System.exit(1);
    }
  }

/* Method to set rotation */
  public void rotate(String dir)
  {
    if (dir.toUpperCase().equals("CW")) rotation=(++rotation)%4;
    else if (dir.toUpperCase().equals("CCW")) rotation=(4+(--rotation))%4;
    else if (dir.toUpperCase().equals("RESET")) rotation=0; /* reset */
    else System.out.println("ERROR: Unknown rotation direction " + dir);
  }

/* FLIP method */
  public void flip(boolean flag){nsflip=flag;}

// readbacks
  public boolean flip(){return(nsflip);}
  public int rotate(){return(rotation);}

/* Method to create a new input channel to the matrix */
  public cell_matrix_channel assign_channel(int row, int col, int layer,
                                            String side)
  {
    if (first_cycle){
      System.out.println("ERROR: Cannot create new channels after first call to cycle()");
      return null;
    }

/* Create a channel */
    cell_matrix_channel cmc=new cell_matrix_channel(this,row,col,layer,side.toUpperCase());

/* record this channel */
    channel_vector.addElement(cmc);
/* and return the channel */
    return(cmc);
  }

/* Method to write a line of text to the file */
  void output(String text)
  {
    try{
      String t;
      int i;
      t="";
      for (i=0;i<text.length();i++){
        if ((text.charAt(i) != '"')  &&
            (text.charAt(i) != '\n')  &&
            (text.charAt(i) != '\r'))
          t=t+text.charAt(i);
      } /* This removes all " from the text */
      bw.write("\""+t+"\"");bw.newLine();
    } catch (Exception e){System.out.println("ERROR:" + e);}
  }

/* Comment method */
  public void comment(String text)
  {
    if (!first_cycle){
      System.out.println("Warning: Can not place comments before first cycle()");
      return;
    }
    output("#"+text);
  }

/* Method to output the channel information */
  public void cycle()
  {
    cell_matrix_channel cmc;
    int i,j;
    String TTout;

    if (!first_cycle){ /* Output header */
      output("#SEQUENCE FILE"); /* Stupid header */
      output(legal);
      output("V3TT"); /* Misc Flags */
      output("3"); /* # of dimensions */
      output("NESWTB"); /* Ordering of sides */
      output(""+channel_vector.size());

      for (i=0;i<channel_vector.size();i++){
        cmc=(cell_matrix_channel) channel_vector.elementAt(i);
/* Now output the data for this channel */
        output(cmc.row + "," + cmc.col + "," + cmc.layer + "," + cmc.side);
      } /* End of all channels */
    } /* Header done */

    first_cycle=true; /* Remember we've called this method at least once */

    for (i=0;i<channel_vector.size();i++){
      cmc=(cell_matrix_channel) channel_vector.elementAt(i);
/* Now output the data for this channel */
/* First rotate it */
      TTout=cmc.TT; /* Natural truth table */
/* Compress TTout before writing */
      TTout=compress(TTout);
      output(TTout);
    } /* End of all channels */
    return;
  }

/* Close method */
  public void close()
  {
    output("#EOF");
    try{bw.close();} catch(Exception e){};
  }

/* Remove spaces from output TT */
  String compress(String TT)
  {
    String out;
    int i,a,b,c,d;

    out="";
    for (i=0;i<256;i+=8){
      a=Integer.parseInt(TT.substring(i  ,i+2),16);
      b=Integer.parseInt(TT.substring(i+2,i+4),16);
      c=Integer.parseInt(TT.substring(i+4,i+6),16);
      d=Integer.parseInt(TT.substring(i+6,i+8),16);
// Now pack abcd into abc
      a=a | ((b&3)<<6);
      b=((b>>2)&15) | ((c&15)<<4);
      c=((c>>4)&3) | (d<<2);
      out=Integer.toHexString(a>>4)+Integer.toHexString(a&15)+out;
      out=Integer.toHexString(b>>4)+Integer.toHexString(b&15)+out;
      out=Integer.toHexString(c>>4)+Integer.toHexString(c&15)+out;
    }
    return(out);
  }
}

/* cell_matrix_channel class */
class cell_matrix_channel{
  cell_matrix_3d_interface cmi;
  int row;
  int col;
  int layer;
  String side; /* Where the input feeds the matrix */
  String TT; /* Stores the current truth table string */

/* The constructor */
  public cell_matrix_channel(cell_matrix_3d_interface cmi_in,int r, int c, int l, String s)
  {
    cmi=cmi_in;
    row=r;col=c;layer=l;side=s;
    TT="00000000000000000000000000000000" +
       "00000000000000000000000000000000" +
       "00000000000000000000000000000000" +
       "00000000000000000000000000000000" +
       "00000000000000000000000000000000" +
       "00000000000000000000000000000000" +
       "00000000000000000000000000000000" +
       "00000000000000000000000000000000";
  }

/* Methods for setting the channel's value */
  public void set(boolean b)
  {
    if (b){
      TT="3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F" +
         "3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F" +
         "3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F" +
         "3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F" +
         "3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F" +
         "3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F" +
         "3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F" +
         "3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F";
    } else {
      TT="00000000000000000000000000000000" +
         "00000000000000000000000000000000" +
         "00000000000000000000000000000000" +
         "00000000000000000000000000000000" +
         "00000000000000000000000000000000" +
         "00000000000000000000000000000000" +
         "00000000000000000000000000000000" +
         "00000000000000000000000000000000";
    }
  }

  cell_exchange ce=new cell_exchange(3,6);

  public void set(String s2)
  {
// We'll do flips and rotates on the source string:
    String s,s_save;
    int i,r;
    char thischar;

    s2=s2.toUpperCase();
    s_save=s2;

// NS flip first
    if (cmi.flip()){
      s="";
      for (i=0;i<s2.length();i++){
        thischar=s2.charAt(i);
        switch (thischar){
          case 'N':s=s+"S";break;
          case 'S':s=s+"N";break;
          default:s=s+thischar;break;
        }
      }
      s2=s; // we've flipped it
    }

// Now do any rotations
    for (r=0;r<cmi.rotate();r++){
      s="";
      for (i=0;i<s2.length();i++){
        thischar=s2.charAt(i);
        switch (thischar){
          case 'N':s=s+"E";break;
          case 'E':s=s+"S";break;
          case 'S':s=s+"W";break;
          case 'W':s=s+"N";break;
          default:s=s+thischar;break;
        }
      }
      s2=s;
    }

    ce.tt_compile(s2);
// If compile fails, this was probably a hex string
    if (ce.error) TT=s_save; else {
// Set TT bytes based on ce.tt[]
      TT="";
      for (i=0;i<128;i++){
        TT=TT+Integer.toHexString(ce.tt[i]>>4);
        TT=TT+Integer.toHexString(ce.tt[i] & 15);
      }
    }
  }

} /* End of class */
