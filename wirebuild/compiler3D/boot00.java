/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class boot00{
  public static void main(String args[]) {
    int i,c,r;
    cell_matrix_channel pc,cc,pc_cc,brk;

    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";

    if (args.length != 1){
      System.out.println("Usage: boot00 filename");
      System.exit(1);
    }

    cell_matrix_interface cmi=new cell_matrix_interface(args[0]+".seq",msg);

/* Read the binary grid file */
    binary_file_reader bfr=new binary_file_reader(args[0]+".bin");

/* Start a wire */
    wire_build w=new wire_build(cmi,"e",0,0,"BRK,PC,CC");
    brk=w.get_channel("brk"); /* Access wire builder class' break line */
    cmi.push();  /* Save current state */

/* SuperSequence 1-Do All except the leftmost 3 columns */

    for (c=bfr.cols()-1;c>2;c--){ /* Do most columns */
      w.preclear(true); w.extend_break(true);
      cmi.pop();cmi.push();
      w.init();
      w.extend(c-3);
      w.extend_break(false);
      w.end_break();
      w.turn("cw");
      for (r=3;r<=bfr.rows()-3;r++){ /* For most rows */
        w.transfer(bfr.readTT(r,c),"ccw"); w.extend();
      }
      w.preclear(false);
      if (bfr.rows() > 4){
        w.transfer(bfr.readTT(bfr.rows()-2,c),"ccw");
        w.extend();
      }
      if (bfr.rows() > 3) w.transfer(bfr.readTT(bfr.rows()-1,c),"ccw");

      brk.set(true);cmi.cycle();brk.set(false);  /* Go back to corner */
      cmi.pop();cmi.push(); /* Back to base rotation */

      w.preclear(true); w.extend();w.extend();
      w.transfer(bfr.readTT(0,c),"ccw");
      w.transfer(bfr.readTT(2,c),"cw");
      w.transfer(bfr.readTT(1,c));
    }

/* SuperSequence 2-Do all but Upperleft 3x3 corner */
    w.extend_break(false);
    w.preclear(true);
    for (r=bfr.rows()-1;r>=3;r--){
      cmi.pop();cmi.push();
      w.init("cw");
      w.extend(r-3);
      w.transfer(bfr.readTT(r,0),"cw");
      w.transfer(bfr.readTT(r,2),"ccw");
      w.transfer(bfr.readTT(r,1));
    }

/* SuperSequence 3-Upper left 3x3 corner */
    cmi.pop();cmi.push();
    w.preclear(true);

    w.init();
    w.extend_wire(false);
cmi.comment("Ready to extend");
    w.extend();
cmi.comment("Extended");
    w.transfer(bfr.readTT(2,2),"cw");
    w.transfer(bfr.readTT(0,2),"ccw");
    w.transfer(bfr.readTT(1,2));

    w.init();
    w.transfer(bfr.readTT(2,1),"cw");
    w.transfer(bfr.readTT(0,1),"ccw");
    w.transfer(bfr.readTT(1,1));

    w.init("x",bfr.readTT(2,0),"cw");
    w.init("x",bfr.readTT(0,0),"ccw");
    w.init("x",bfr.readTT(1,0),"");

    cmi.pop();
    cmi.close();
  }
}
