class test2{
  static cell_matrix_channel pc,cc,brk;
  static cell_matrix_3d_interface cmi;

  public static void main(String args[])
  {
    cmi=new cell_matrix_3d_interface("test2.seq","test");

    pc=cmi.assign_channel(0,0,0,"DW");
    cc=cmi.assign_channel(1,0,0,"DW");
    brk=cmi.assign_channel(2,0,0,"DW");

// Standard W->E sequence
    pc.set("cs=1;ds=w");cc.set(true);cmi.cycle();
    pc.set("dw=n;dn=we~&;de=we&");cc.set(false);cmi.cycle();
    pc.set("dw=e;de=w;ce=s;ds=1");cc.set(true);cmi.cycle();
    pc.set(false);cc.set(false);cmi.cycle();

    cmi.close();
  }
}
