/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

/*
 * class for space-filling supersequences.
 *
 * space_filler sf=new space_filler(cmi,bfr,delta)-Constructor
 * sf.init(binary_file_reader bfr) - Build initial supercell on an edge
 * sf.build(binary_file_reader bfr,int rotate) - Build normal supercell
 *    using existing head cells
 *    bfr is the binary file reader for the subgrid to build
 *    rotate is # of turns CW for current build direction
 *           This affects the read-order, but doesn't rotate BFR TTs
 *           It also affects the wire-building directions
 *
 */

class space_filler{
  cell_matrix_interface cmi;  // Main interface object
  int delta;		// How far down the main wire begins
  binary_file_reader bfr;	// Binary file to build
  cell_matrix_channel brk;
  wire_build w;
  cell_matrix_channel link,m1,m0; /* Other inputs */

// Constructor
  space_filler(cell_matrix_interface cmi_in,int delta_in)
  {
    cmi=cmi_in;
    delta=delta_in; 
    link=cmi.assign_channel(delta-3,0,"DW");
    m0=cmi.assign_channel(delta-2,0,"DW");
    m1=cmi.assign_channel(delta-1,0,"DW");

/* Activate the first LINK input */
   link.set(true);

/* Start a wire */
    w=new wire_build(cmi,"e",delta,0,"BRK,PC,CC");
    brk=w.get_channel("brk"); /* Access wire builder class' break line */
  }

// Rotation routine
  void rotate(int n)
  {
    int i;

/* Rotate the cell matrix interface */
    cmi.rotate("reset");
    for (i=0;i<n;i++) cmi.rotate("cw");

/* Tell the binary file reader to read from a rotated grid */
    bfr.rotate(n);

/* Set the S1/S0 select lines */

    switch (n%4){
      case 0:m0.set(true);m1.set(true);break;
      case 1:m0.set(true);m1.set(false);break;
      case 2:m0.set(false);m1.set(true);break;
      case 3:m0.set(false);m1.set(false);break;
    }
    cmi.cycle();
  }

// Build routine for initial cell on edge
  public void init(binary_file_reader bfr_in)
  {
    bfr=bfr_in;
    rotate(0);
    do_build(0); // 0 means init
  }

// Normal build routine
  public void build(binary_file_reader bfr_in,int r)
  {
    bfr=bfr_in;
    rotate(r);
    do_build(1);
  }

// main build routine
// flag=0 means one-time init along edge (need to build head cells)
// flag=1 means normal build using pre-existing head cells
  public void do_build(int flag)
  {
    int i,c,r,n;
    int del; /* Displ. w/in supercell */

    cmi.push();  /* Save current state */
    w.preclear(true);
    w.extend_break(false);
    w.extend_wire(true);
    w.rotate_transfer(false);

    del=delta % bfr.rows();

/* create the initial break cell */
    if (flag==1) w.end_break();

/* SuperSequence 1-Do All except the leftmost 5 columns */

    for (c=bfr.cols()-1;c>4;c--){ /* Do most columns */
      cmi.pop();cmi.push();
      if (flag==0) w.init(); else w.extend();
      w.turn("ccw");w.extend(del-1);w.turn("cw");
      w.extend(c-5);w.turn("cw");
      for (r=2;r<=bfr.rows()-2;r++){
// Check for optional preclear here...
        w.cond_preclear(bfr.readTT(r+1,c),"ccw");
        w.transfer(bfr.readTT(r,c),"ccw");
        w.extend();
      }
      w.transfer(bfr.readTT(bfr.rows()-1,c),"ccw");

      if (flag==1) {brk.set(true);cmi.cycle();brk.set(false);}
      cmi.pop();cmi.push();

      if (flag==0) w.init(); else w.extend();
      w.turn("ccw");w.extend(del-1);w.turn("cw");
      w.extend(c-4);w.extend_wire(false);w.extend();
/* Make head cells non-extendible, so cells we're
   building don't interfere with new wire */
      w.transfer(bfr.readTT(1,c),"cw");
      w.transfer(bfr.readTT(0,c),"");
      w.extend_wire(true);

      if (flag==1) {brk.set(true);cmi.cycle();brk.set(false);}
    }

/* SuperSequence 2 */

    for (n=del;n>=0;n--){
      if (flag==1){brk.set(true);cmi.cycle();brk.set(false);}
      cmi.pop();cmi.push();
      if (flag==0) w.init(); else w.extend();
      w.extend(2);
      if (n==0) w.extend_wire(false); // corner will be end of wire
      w.turn("ccw");
      w.extend(n-1);
      if (n > 0){w.extend_wire(false);w.extend();}
      w.transfer(bfr.readTT(del-n,4),"cw");
      w.transfer(bfr.readTT(del-n,3),"");
      w.extend_wire(true);
    }

    for (n=del;n>=1;n--){
      if (flag==1){brk.set(true);cmi.cycle();brk.set(false);}
      cmi.pop();cmi.push();
      if (flag==0) w.init(); else w.extend(1);
      w.turn("ccw");
      w.extend(n-1);w.extend_wire(false);w.extend();
// This works fine if n=1, which is smallest n value
      w.transfer(bfr.readTT(del-n,0),"ccw");
      w.transfer(bfr.readTT(del-n,2),"cw");
      w.transfer(bfr.readTT(del-n,1),"");
      w.extend_wire(true);
    }

// Supersequence 3

    for (r=bfr.rows()-1;r>=del+3;r--){
      w.extend_wire(true);
      if (flag==1){brk.set(true);cmi.cycle();brk.set(false);}
      cmi.pop();cmi.push();
      if (flag==0) w.init(); else w.extend();
      w.extend(1);
      if (r==del+3) w.extend_wire(false); // CW turn is end of wire
      w.turn("cw");
      w.extend((r-(del+3))-1);w.extend_wire(false);
      if (r > del+3) w.extend();
      w.transfer(bfr.readTT(r,4),"ccw");
      w.transfer(bfr.readTT(r,2),"cw");
      w.transfer(bfr.readTT(r,3),"");
    }

    w.extend_wire(true);
    if (flag==1) {brk.set(true);cmi.cycle();brk.set(false);}
    cmi.pop();cmi.push();
    if (flag==0) w.init(); else w.extend();
    w.extend(2);w.extend_wire(false);w.extend();
    w.transfer(bfr.readTT(del+2,4),"cw");
    w.transfer(bfr.readTT(del+1,4),"");

    w.extend_wire(true);
    if (flag==1) {brk.set(true);cmi.cycle();brk.set(false);}
    cmi.pop();cmi.push();
    if (flag==0) w.init(); else w.extend();
    w.extend(1);w.extend_wire(false);w.extend();
    w.transfer(bfr.readTT(del+2,3),"cw");
    w.transfer(bfr.readTT(del+1,3),"");

// Supersequence 4

    for (r=bfr.rows()-1;r>=del+3;r--){
      w.extend_wire(true);
      if (flag==1){brk.set(true);cmi.cycle();brk.set(false);}
      cmi.pop();cmi.push();
      if (r==del+3) w.extend_wire(false); // CW turn is end of wire
      if (flag==0) w.init("cw"); else w.turn("cw");
      w.extend((r-(del+3))-1);w.extend_wire(false);
      if (r > del+3) w.extend();
      w.transfer(bfr.readTT(r,0),"cw");
      w.transfer(bfr.readTT(r,1),"");
    }

    w.extend_wire(true);
    if (flag==1){brk.set(true);cmi.cycle();brk.set(false);}
    cmi.pop();cmi.push();
    if (flag==0) w.init(); else w.extend();
    w.transfer("","ccw");
    w.extend_wire(false);w.extend();
    w.transfer(bfr.readTT(del,2),"ccw");
    w.transfer(bfr.readTT(del+2,2),"cw");
    w.transfer(bfr.readTT(del+1,2),"");

    w.extend_wire(true);
    if (flag==1){brk.set(true);cmi.cycle();brk.set(false);}
    cmi.pop();cmi.push();
    w.extend_wire(false);if (flag==0) w.init(); else w.extend();
    w.transfer(bfr.readTT(del,1),"ccw");
    w.transfer(bfr.readTT(del+2,1),"cw");
    w.transfer(bfr.readTT(del+1,1),"");

    if (flag==1){brk.set(true);cmi.cycle();brk.set(false);}
    cmi.pop();cmi.push();
    w.extend_wire(false);
    if (flag==1){
      w.transfer(bfr.readTT(del,0),"ccw");
      w.transfer(bfr.readTT(del+2,0),"cw");
      w.transfer(bfr.readTT(del+1,0),"");
    } else {
      w.init("X",bfr.readTT(del,0),"ccw");
      w.init("X",bfr.readTT(del+2,0),"cw");
      w.init("X",bfr.readTT(del+1,0),"");
    }

    cmi.pop();
  }
}
