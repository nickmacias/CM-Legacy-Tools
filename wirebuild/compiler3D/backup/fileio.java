  public void writebinarygrid(String name)
  {
    int r,c,i;
    Vector cell_vector;
    Enumeration en;
    singlecellclass singlecell;
    char inputs,output;
    String desc,cellTT,key;

    try{
      FileWriter fw=new FileWriter(name);
      BufferedWriter out=new BufferedWriter(fw);
      String temp;

/* Write header */
      temp="Generate by Java Loader V1.0                                                                        ";
      for (i=0;i<80;i++) writechar(out,temp.charAt(i));

      writeint(out,ROWS);
      writeint(out,COLS);
  
/* Write cells */
      for (r=0;r<ROWS;r++){
        for(c=0;c<COLS;c++){
          cellTT="00000000000000000000000000000000";
          desc=""; /* Default NOP  values */

          writecell(out,'\000','\000',cellTT,desc);
        } /* End of c */
      } /* End of r */
      out.close();	/* Done with file */
    } catch (Exception e){
      parent.msg("Failure inside writegrid():"+e);
    }

  }

/* I/O routines */

  void writeint(BufferedWriter out,int i)
  {
    writechar(out,(char) (i%256)); /* LSB */
    writechar(out,(char) (i/256)); /* MSB */
  }

  inr readint(BufferedWriter in)
  {
    String c1,c2;
    int i;
    c1=readchar(in); /* LSB */
    c2=readchar(in); /* MSB */
    i=Byte.decode(c1) | (Byte.decode(c2)<<8);
    return(i);
  }



  void writechar(BufferedWriter out,char c)
  {
    try{
      out.write((char)(32+(int)(c/16))); /* MSN */
      out.write((char)(32+(int)(c%16))); /* LSN */
    } catch (Exception e){
      parent.msg("Failure inside writechar():"+e);
    }
  }

  void writecell(BufferedWriter out,char in,char outs,String tt, String desc)
  {
    int i;

    writechar(out,in);
    writechar(out,outs);
    for (i=0;i<32;i+=2) writechar(out,(char)Integer.parseInt(tt.substring(i,i+2),16));
    writeint(out,desc.length());
    for (i=0;i<desc.length();i++) writechar(out,desc.charAt(i));
  }

