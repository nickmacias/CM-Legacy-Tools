/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class ctest{
  public static void main(String args[]) {
    int i;
    cell_matrix_channel pc,cc,pc_cc,cc_cc;

    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("test.seq",msg);

    wire_build w=new wire_build(cmi,"e",7,0,"CC,PC");
    w.preclear(true);
    w.init();
//    w.extend(5);
    w.turn("cw");
    w.extend(1);
    w.transfer("dn=s;ds=n;dw=e;de=w","CCW");
    w.transfer("ds=1;dw=1;de=1","CW");
    w.transfer("ds=1","");
    cmi.close();

  }

}
