/* Truth Table Manipulation Routines */
  String ROTATE(String TT,int num)
  {
    int i,row;
    String NEWTT;
    int rowperm[]={0,8,4,12,1,9,5,13,2,10,6,14,3,11,7,15}; /* Huh? */
    int colperm[]={0,4,8,12,2,6,10,14,1,5,9,13,3,7,11,15}; /* Whatever */

    if (num==0) return(TT);

/* Convert TT to integer array */
    int tt[]=new int[32];
    int newtt[]=new int[32];

    for (i=0;i<32;i++)
      tt[i]=Integer.parseInt(TT.substring(i,i+1),16); /* Convert to number */

    for (i=0;i<num;i++){ /* do whole rotation scheme here */
      for (row=0;row<16;row++){
        newtt[2*row]=colperm[tt[2*rowperm[row]]];
        newtt[1+2*row]=colperm[tt[1+2*rowperm[row]]];
      }
      for (row=0;row<32;row++) tt[row]=newtt[row];
    }

/* Now convert back to string */
    NEWTT="";
    for (i=0;i<32;i++)
      NEWTT=NEWTT+Integer.toHexString(newtt[i]);

    return(NEWTT); /* Return the result */
  } /* End of ROTATE() */

  String OR(String TT1,String TT2,int r, int c)
  {
    int i,t1,t2;
    String NEWTT;

    NEWTT="";
    for (i=0;i<32;i++){
      t1=Integer.parseInt(TT1.substring(i,i+1),16); /* Integer value */
      t2=Integer.parseInt(TT2.substring(i,i+1),16); /* Integer value */
      if ((t1&t2) != 0) parent.msg("WARNING: Possible cell contention at [" + r + "," + c + "]");
      NEWTT=NEWTT+Integer.toHexString(t1|t2);
    }
    return(NEWTT);
  }
