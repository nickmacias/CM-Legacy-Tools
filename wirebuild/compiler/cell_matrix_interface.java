/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

import java.io.*;
import java.lang.*;
import java.util.*;

/*
 * Main class for cell matrix interface
 *
 * Main classes are:
 *  cmi=new cell_matrix_interface("filename","legal-info-text");
 *    This creates an interface object, which will write configuration
 *    strings into "filename"
 *
 *  chan=new cell_matrix_channel(row,col,"side");
 *    where side is DN, DS, DW, DE, CN, CS, CW or CE
 *
 * Methods are as follows:
 *  chan=cmi.assign_channel(row,col,"side");  which creates a channel
 *  cmi.setversion(2 or 3);  one-time asignment of version number (def=2) 
 *  chan.set(true/false);
 *  chan.set(int);
 *  chan.set("32-hex-char string");
 *  chan.set("DW=NE&,CE=1"); where argument can be any set of RPN
 *      logical equations, separated by ,;: or \
 *  The set() methods indicate a 128-bit pattern which should be sent to
 *  the corresponding channel during the next 128 clock ticks
 *
 *  cmi.cycle()  requests another 128 clock ticks
 *
 *  cmi.close()  Closes the output file
 *
 *  cmi.rotate("cw" or "ccw" or "reset");  Sets the rotation for future TTs
 *      reset returns the absolute rotation to 0.
 *
 *  cmi.flip(boolean flag)  Flip cell about E-W axis if true
 *
 *  boolean cmi.flip() - returns flip state
 *
 *  int cmi.rotate() - returns rotate state
 *
 *  cmi.push(); cmi.pop(); Save and restore current state (one level)
 *
 *  cmi.comment(String text); Add a comment to .SEQ file
 *      Can be used as breakpoint in simulator
 *
 *
 * In general, truth table bit-patterns are stored internally as 32 hex
 * chanacters. The TT organization is:
 *
 * INPUTS  |      OUTPUTS     
 *  DATA   |   CODE    DATA  
 * S N E W | N S W E N S W E
 * --------+----------------
 * 0 0 0 0 | 120   ...   127
 * 0 0 0 1 | 112   ...   119
 *  ...    |       ...
 * 1 1 1 0 | 8 9 10 ...14 15
 * 1 1 1 1 | 0 1 2 3 4 5 6 7
 * --------+----------------
 *
 * where the number in the right hand side of the table indicates the
 * order in which that bit will be sent to an input channel of the matrix.
 *
 * The corresponding 32 character string for the above table would be:
 *   First character=Hex character corresponding to [0123], i.e.,
 *   8*0 + 4*1 + 2*2 + 3
 *   Second character is [4567]
 * and so on, the last character being [124 125 126 127]
 *
 * Each row of the output file is terminated by a platform-specific
 * newline separator. Additionally, each row is surrounded by quotes.
 *
 * Output file format is:
 *  #SEQUENCE FILE     standard header
 *  Legal Info Text    any text not containing \n
 *  Flags              reserved for future use, i.e., encryption, etc.
 *  Number of channels
 *  row,col,side       specifies location of first channel
 *  row,col,side       specifies location of second channel
 *     ...
 *  row,col,side       specifies location of last channel
 *  pattern 0          First pattern for channel 0
 *  pattern 1          First pattern for channel 1
 *     ...
 *  pattern m          First pattern for channel m
 *  pattern 0          Second pattern for channel 0
 *  pattern 1          Second pattern for channel 1
 *     ...
 *  pattern m          Second pattern for channel m
 *     ...
 *  pattern 0          Last pattern for channel 0
 *  pattern 1          Last pattern for channel 1
 *     ...
 *  pattern m          Last pattern for channel m
 *  #EOF               Standard trailer
 *
 */

class cmstate{
  boolean nsflip;
  int rotation;
};

class cell_matrix_interface{
  Vector channel_vector=new Vector(10); /* Remember all channels */
  boolean first_cycle=false; /* Set after first call to cycle() */
  BufferedWriter bw;
  String legal=""; /* Store customer info in header */
  int rotation=0; /* # of CW steps (0-3) */
  boolean nsflip=false; /* Set to do flip */
  Stack xstack=new Stack(); /* Record rotations and flips */
  int version=3; /* 2=old TT, 3=new, general purpose table */
   
  public void setversion(int x){
    version=x;
  }

  public int getversion(){
    return(version);
  }

  public void push(){
    xstack.push(new Integer(rotation));
    xstack.push(new Boolean(nsflip));
  }

  public void pop(){
    nsflip=((Boolean)xstack.pop()).booleanValue();
    rotation=((Integer)xstack.pop()).intValue();
  }

// sometimes, we just want to save/restore a specific state

  public cmstate state()
  {
    cmstate r=new cmstate();
    r.nsflip=nsflip;
    r.rotation=rotation;
    return(r);
  }

  public void state(cmstate s)
  {
    nsflip=s.nsflip;
    rotation=s.rotation;
  }

/* Constructors */

  public cell_matrix_interface(String fname, String text)
  {
    this(fname);
    legal=text;
  }

  public cell_matrix_interface(String fname) /* Output to a file */
  {
    try {
      FileWriter fw=new FileWriter(fname);
      bw=new BufferedWriter(fw);
    } catch (Exception e){
      System.out.println("ERROR OPENING OUTPUT FILE " + fname + ":" + e);
      System.exit(1);
    }
  }

/* Method to set rotation */
  public void rotate(String dir)
  {
    if (dir.toUpperCase().equals("CW")) rotation=(++rotation)%4;
    else if (dir.toUpperCase().equals("CCW")) rotation=(4+(--rotation))%4;
    else if (dir.toUpperCase().equals("RESET")) rotation=0; /* reset */
    else System.out.println("ERROR: Unknown rotation direction " + dir);
  }

/* FLIP method */
  public void flip(boolean flag){nsflip=flag;}

// readbacks
  public boolean flip(){return(nsflip);}
  public int rotate(){return(rotation);}

/* Method to create a new input channel to the matrix */
  public cell_matrix_channel assign_channel(int row, int col, String side)
  {
    side=side.toUpperCase();
    if (first_cycle){
      System.out.println("ERROR: Cannot create new channels after first call to cycle()");
      return null;
    }

      cell_matrix_channel cmc;
// See if this channel already exists somewhere in out channel_vector
      for (int i=0;i<channel_vector.size();i++){
        cmc=(cell_matrix_channel) channel_vector.elementAt(i);
//System.out.println("Want " + row + " " + col + " " + side + "   found " + cmc.row + " " + cmc.col + " " + cmc.side);
        if ((cmc.row==row)&&(cmc.col==col)&&
            (cmc.side.equals(side))){
//System.out.println("Found " + row +"," + col + " side " + side);
          return(cmc);
        }
      } /* End of all channels */

// otherwise, create a channel
    cmc=new cell_matrix_channel(this,row,col,side.toUpperCase());
// record this channel
    channel_vector.addElement(cmc);
// and return the channel
    return(cmc);
  }

/* Method to write a line of text to the file */
  void output(String text)
  {
    try{
      String t;
      int i;
      t="";
      for (i=0;i<text.length();i++){
        if ((text.charAt(i) != '"')  &&
            (text.charAt(i) != '\n')  &&
            (text.charAt(i) != '\r'))
          t=t+text.charAt(i);
      } /* This removes all " from the text */
      bw.write("\""+t+"\"");bw.newLine();
    } catch (Exception e){System.out.println("ERROR:" + e);}
  }

/* Comment method */
  public void comment(String text)
  {
    if (!first_cycle){
      System.out.println("Warning: Cannot place comments before first cycle()");
      return;
    }
    output("#"+text);
  }

/* Method to output the channel information */
  public void cycle()
  {
    cell_matrix_channel cmc;
    int i,j;
    String TTout;

    if (!first_cycle){ /* Output header */
      output("#SEQUENCE FILE"); /* Stupid header */
      output(legal);
      if (version==2){
        output("V2TT"); /* 2 dim, default side ordering */
      } else {
        output("V3TT"); /* Misc Flags */
        output("2"); /* # of dimensions */
        output("NESW"); /* Ordering of sides */
      }
      output(""+channel_vector.size());

      for (i=0;i<channel_vector.size();i++){
        cmc=(cell_matrix_channel) channel_vector.elementAt(i);
/* Now output the data for this channel */
        output(cmc.row + "," + cmc.col + "," + cmc.side);
      } /* End of all channels */
    } /* Header done */

    first_cycle=true; /* Remember we've called this method at least once */

    for (i=0;i<channel_vector.size();i++){
      cmc=(cell_matrix_channel) channel_vector.elementAt(i);
/* Now output the data for this channel */
/* First rotate it */
      TTout=cmc.TT; /* Natural truth table */
      //if (nsflip) TTout=do_nsflip(TTout);
      //for (j=0;j<rotation;j++) TTout=do_rotate(TTout);
      output(TTout);
    } /* End of all channels */
    return;
  }

/* Close method */
  public void close()
  {
    output("#EOF");
    try{bw.close();} catch(Exception e){};
  }

/* Truth Table Manipulation Routines */
  String XXXdo_rotate(String TT)
  {
    int rowperm[]={0,8,4,12,1,9,5,13,2,10,6,14,3,11,7,15}; /* Huh? */
    int colperm[]={0,4,8,12,2,6,10,14,1,5,9,13,3,7,11,15}; /* Whatever */
    int colperm3[]={0,2,4,6,8,10,12,14,1,3,5,7,9,11,13,15};
    int rowperm3[]={0,8,1,9,2,10,3,11,4,12,5,13,6,14,7,15};
    if (version==2){
      return(do_perm(TT,rowperm,colperm));
    }
    return(do_perm(TT,rowperm3,colperm3));

  }

  String XXXdo_nsflip(String TT)
  {
    int rowperm[]={0,1,2,3,8,9,10,11,4,5,6,7,12,13,14,15};
    int colperm[]={0,1,2,3,8,9,10,11,4,5,6,7,12,13,14,15};
    int rowperm3[]={0,4,2,6,1,5,3,7,8,12,10,14,9,13,11,15};
    int colperm3[]={0,4,2,6,1,5,3,7,8,12,10,14,9,13,11,15};
    if (version==2){
      return(do_perm(TT,rowperm,colperm));
    }
    return(do_perm(TT,rowperm3,colperm3));
  }

  String do_perm(String TT,int rowperm[], int colperm[]) /* Performs a single Clockwise rotation */
  {
    int i,row;
    String NEWTT;

/* Convert TT to integer array */
    int tt[]=new int[32];
    int newtt[]=new int[32];

    for (i=0;i<32;i++)
      tt[i]=Integer.parseInt(TT.substring(i,i+1),16); /* Convert to number */

      for (row=0;row<16;row++){
        if (version==2){
          newtt[2*row]=colperm[tt[2*rowperm[row]]];
          newtt[1+2*row]=colperm[tt[1+2*rowperm[row]]];
        } else {
          newtt[row]=colperm[tt[rowperm[row]]];
          newtt[16+row]=colperm[tt[16+rowperm[row]]];
        }
      }
      for (row=0;row<32;row++) tt[row]=newtt[row];

/* Now convert back to string */
    NEWTT="";
    for (i=0;i<32;i++)
      NEWTT=NEWTT+Integer.toHexString(newtt[i]);

    return(NEWTT); /* Return the result */
  } /* End of do_perm() */

  String do_or(String TT1,String TT2)
  {
    int i,t1,t2;
    String NEWTT;

    NEWTT="";
    for (i=0;i<32;i++){
      t1=Integer.parseInt(TT1.substring(i,i+1),16); /* Integer value */
      t2=Integer.parseInt(TT2.substring(i,i+1),16); /* Integer value */
      if ((t1&t2) != 0) System.out.println("WARNING: Possible cell contention");
      NEWTT=NEWTT+Integer.toHexString(t1|t2);
    }
    return(NEWTT);
  }
}

/* cell_matrix_channel class */
class cell_matrix_channel{
  cell_matrix_interface cmi;
  int row;
  int col;
  int version=3;
  String side; /* Where the input feeds the matrix */
  String TT; /* Stores the current truth table string */

/* The constructor */
  public cell_matrix_channel(cell_matrix_interface cmi_in,int r, int c, String s)
  {
    cmi=cmi_in;
    row=r;col=c;side=s;TT="00000000000000000000000000000000";
  }

/* Methods for setting the channel's value */
  public void set(boolean b)
  {
    if (b) TT="FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
    else TT="00000000000000000000000000000000";
  }

  public void set(int i)
  {
    TT="000000000000000000000000"; /* First 24 nibbles (12 bytes) */
    TT=TT + Integer.toHexString((i>>28) & 15);
    TT=TT + Integer.toHexString((i>>24) & 15);
    TT=TT + Integer.toHexString((i>>20) & 15);
    TT=TT + Integer.toHexString((i>>16) & 15);
    TT=TT + Integer.toHexString((i>>12) & 15);
    TT=TT + Integer.toHexString((i>>8) & 15);
    TT=TT + Integer.toHexString((i>>4) & 15);
    TT=TT + Integer.toHexString(i & 15);
  }

  public void set(String s2)
  {
// We'll do flips and rotates on the source string:
    String s,s_save;
    int i,r;
    char thischar;

    s2=s2.toUpperCase();
    s_save=s2;

// NS flip first
    if (cmi.flip()){
      s="";
      for (i=0;i<s2.length();i++){
        thischar=s2.charAt(i);
        switch (thischar){
          case 'N':s=s+"S";break;
          case 'S':s=s+"N";break;
          default:s=s+thischar;break;
        }
      }
      s2=s; // we've flipped it
    }

// Now do any rotations
    for (r=0;r<cmi.rotate();r++){
      s="";
      for (i=0;i<s2.length();i++){
        thischar=s2.charAt(i);
        switch (thischar){
          case 'N':s=s+"E";break;
          case 'E':s=s+"S";break;
          case 'S':s=s+"W";break;
          case 'W':s=s+"N";break;
          default:s=s+thischar;break;
        }
      }
      s2=s;
    }

    TT=ttcompile.compile(s2,version);
    if (TT.length() == 0) TT=s_save; /* Compile error-assume input is hex string */
  }

} /* End of class */
