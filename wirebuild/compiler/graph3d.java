import java.io.*;
import java.util.*;
import java.awt.*;

class graph3d{
/* 2D vars */
  static double WINXMIN, WINXMAX, WINYMIN, WINYMAX; /* ABS */
  static double XMIN, XMAX, YMIN, YMAX; /* REL */

/* 3D vars */
  static double XA, XB, YA, YB, ZA, ZB; /* 3D bounding box */
  static double XCENT, YCENT, ZCENT; /* Centerpoint of view */
  static double ANG1, ANG2, RAD; /* Angles and distance to viewpoint */

/* Define the 2D physical coordinate limits */
  static public void setwin(double xmin, double xmax, double ymin, double ymax)
  {
    WINXMIN=xmin;WINXMAX=xmax;WINYMIN=ymin;WINYMAX=ymax;
  }

/* Define the relative coordinate scale */
  static public void scale(double xmin, double xmax, double ymin, double ymax)
  {
    XMIN=xmin;XMAX=xmax;YMIN=ymin;YMAX=ymax;
  }

/* Draw a 2D line with relatvie coordinates */
  static public void line(Graphics g, double x1, double y1, double x2, double y2)
  {
    double X1,Y1,X2,Y2;
    X1=WINXMIN + ((x1-XMIN)*(WINXMAX-WINXMIN))/(XMAX-XMIN);
    X2=WINXMIN + ((x2-XMIN)*(WINXMAX-WINXMIN))/(XMAX-XMIN);
    Y1=WINYMIN + ((y1-YMIN)*(WINYMAX-WINYMIN))/(YMAX-YMIN);
    Y2=WINYMIN + ((y2-YMIN)*(WINYMAX-WINYMIN))/(YMAX-YMIN);
    Y1=WINYMAX-Y1;
    Y2=WINYMAX-Y2;
    g.drawLine((int) X1,(int) Y1,(int) X2,(int) Y2);
  }

  static int[] r=new int[2];
  static public int[] preline(double x1, double y1)
  {
    double X1,Y1;
    X1=WINXMIN + ((x1-XMIN)*(WINXMAX-WINXMIN))/(XMAX-XMIN);
    Y1=WINYMIN + ((y1-YMIN)*(WINYMAX-WINYMIN))/(YMAX-YMIN);
    Y1=WINYMAX-Y1;
    r[0]=(int)X1;r[1]=(int)Y1;
    return(r);
  }

/* Establish 3D relative coordinate boundaries */
  static public void setup(double x1, double x2,
                           double y1, double y2,
                           double z1, double z2,
                           double xv, double yv, double zv)
  {
    XA=x1;XB=x2;YA=y1;YB=y2;ZA=z1;ZB=z2;
    XCENT=(x1+x2)/2.; YCENT=(y1+y2)/2.; ZCENT=(z1+z2)/2.;

/* Setup the viewpoint */
    view(xv,yv,zv);

/* adjust the 2D scale to make bounding box visible */
    adjust();
  }

  public static void box(Graphics g)
  {
    g.setColor(new Color(0,255,255));
    plot(g,XA,YA,ZA,XB,YA,ZA);
    plot(g,XB,YA,ZA,XB,YB,ZA);
    plot(g,XB,YB,ZA,XA,YB,ZA);
    plot(g,XA,YB,ZA,XA,YA,ZA);

    plot(g,XA,YA,ZB,XB,YA,ZB);
    plot(g,XB,YA,ZB,XB,YB,ZB);
    plot(g,XB,YB,ZB,XA,YB,ZB);
    plot(g,XA,YB,ZB,XA,YA,ZB);

    plot(g,XA,YA,ZA,XA,YA,ZB);
    plot(g,XB,YA,ZA,XB,YA,ZB);
    plot(g,XB,YB,ZA,XB,YB,ZB);
    plot(g,XA,YB,ZA,XA,YB,ZB);
  }

/* Translate all corners of 3D bounding box, and set 2D scale accordingly */
  static public void adjust()
  {
    double p[][]=new double[8][3];
    double x,y,z; /* Center */
    double xmin, xmax, ymin, ymax;
    double t;
    int i;

    x=XCENT;y=YCENT;z=ZCENT+RAD;

    p[0]=convert(XA,YA,ZA);
    p[1]=convert(XA,YA,ZB);
    p[2]=convert(XA,YB,ZA);
    p[3]=convert(XA,YB,ZB);
    p[4]=convert(XB,YA,ZA);
    p[5]=convert(XB,YA,ZB);
    p[6]=convert(XB,YB,ZA);
    p[7]=convert(XB,YB,ZB); /* These are the 8 bounding box corners */

/* Now convert the X coordinates and find max and min */
    xmin=xmax=-(x-p[0][0])/(z-p[0][2]);
    ymin=ymax=-(y-p[0][1])/(z-p[0][2]);

    for (i=1;i<8;i++){
      t=-(x-p[i][0])/(z-p[i][2]);
      if (t > xmax) xmax=t;
      if (t < xmin) xmin=t;
      t=-(y-p[i][1])/(z-p[i][2]);
      if (t > ymax) ymax=t;
      if (t < ymin) ymin=t;
    } /* xmin, xmax, ymin and myax should be set up now */
    scale(xmin, xmax, ymin, ymax);
  }

/* Set up radius and rotation angles for a given viewpoint */
  static public void view(double x, double y, double z)
  {
    double x1, y1, z1;
    x1=x-XCENT;y1=y-YCENT;z1=z-ZCENT;
    if (z1 == 0) z1=.0001*(ZB-ZA); /* Avoid degenerate case */
    ANG1=Math.atan(x1/z1);
    if (z1 < 0) ANG1+=3.1415927;
    ANG2=Math.atan(y1/Math.sqrt(x1*x1+z1*z1));
    RAD=Math.sqrt(x1*x1+y1*y1+z1*z1);
  }

/* Convert (x,y,z)->(x',y',z') based on ANG1 and ANG2 */
  static public double[] convert(double x, double y, double z)
  {
    double ret[],x1,y1,z1,theta1,theta2,r;

    x1=x-XCENT;y1=y-YCENT;z1=z-ZCENT;
    if (z1==0) z1=.0001*(ZB-ZA);
    theta1=Math.atan(x1/z1);
    if (z1 < 0) theta1+=3.1415927;
    theta1=theta1-ANG1;

    r=Math.sqrt(x1*x1+z1*z1);
    x1=r*Math.sin(theta1);
    z1=r*Math.cos(theta1);
    if (z1==0) z1=.0001*(ZB-ZA);
    theta2=Math.atan(y1/z1);
    if (z1 < 0) theta2+=3.1415927;
    theta2=theta2-ANG2;

    r=Math.sqrt(y1*y1+z1*z1);
    y1=r*Math.sin(theta2);
    z1=r*Math.cos(theta2);

    ret=new double[3];
    ret[0]=x1+XCENT;
    ret[1]=y1+YCENT;
    ret[2]=z1+ZCENT;
    return(ret);
  }


  public static void rotate(double a1, double a2)
  {
    ANG1=ANG1-a1;
    ANG2=ANG2-a2;
  }

  public static void plot(Graphics g,double x1, double y1, double z1,
                          double x2, double y2, double z2)
  {
    double p[],xloc,yloc,zloc,xnew1,ynew1,xnew2,ynew2;

    xloc=XCENT;yloc=YCENT;zloc=ZCENT+RAD; /* Focal point */

    p=convert(x1,y1,z1); /* Rotate point */
    if (p[2]==zloc) p[2]+=.0001*(ZB-ZA);
    xnew1=-(xloc-p[0])/(zloc-p[2]);
    ynew1=-(yloc-p[1])/(zloc-p[2]);

    p=convert(x2,y2,z2); /* Second point */
    if (p[2]==zloc) p[2]+=.0001*(ZB-ZA);
    xnew2=-(xloc-p[0])/(zloc-p[2]);
    ynew2=-(yloc-p[1])/(zloc-p[2]);

    line(g,xnew1,ynew1,xnew2,ynew2);
  }


  static double[] preplot(double x1, double y1, double z1)
  {
    double p[],xloc,yloc,zloc,xnew1,ynew1,xnew2,ynew2;
    double r[]=new double[2]; /* Return value */

    xloc=XCENT;yloc=YCENT;zloc=ZCENT+RAD; /* Focal point */

    p=convert(x1,y1,z1); /* Rotate point */
    if (p[2]==zloc) p[2]+=.0001*(ZB-ZA);
    r[0]=-(xloc-p[0])/(zloc-p[2]);
    r[1]=-(yloc-p[1])/(zloc-p[2]);

    return(r);
  }

  public static void rect(Graphics g,
    double x1,double y1, double z1,
    double x2,double y2, double z2,
    double x3,double y3, double z3,
    double x4,double y4, double z4)
  {
    double fr[]=new double[2];
    int ir[]=new int[2];
    int x[]=new int[4];
    int y[]=new int[4];

    fr=preplot(x1,y1,z1);ir=preline(fr[0],fr[1]);x[0]=ir[0];y[0]=ir[1];
    fr=preplot(x2,y2,z2);ir=preline(fr[0],fr[1]);x[1]=ir[0];y[1]=ir[1];
    fr=preplot(x3,y3,z3);ir=preline(fr[0],fr[1]);x[2]=ir[0];y[2]=ir[1];
    fr=preplot(x4,y4,z4);ir=preline(fr[0],fr[1]);x[3]=ir[0];y[3]=ir[1];

    g.fillPolygon(x,y,4);
  }
}
