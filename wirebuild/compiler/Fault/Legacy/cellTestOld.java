/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

/* this is a first shot at a method for testing the cells inside a supercell for faults. */

class cellTest{

// these two are static only so that they can be used by the public static void main()
  static cell_matrix_channel PC,CC,BRK,COMPARE,GO;
  static String zeros="00000000000000000000000000000000";

  public static void setupPatterns(String Pin[], String Pout[])
  {
  Pin[0] = "DW=N;DN=WE~&;DE=WE&";	
  Pout[0] = Pin[0];
  
             //0101 repeated 32 times, encoded in hex
  Pin[1]="55555555555555555555555555555555";
             //1010 repeated 32 times, encoded in hex
  Pout[1]="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
  	
  }

  public static void main(String args[]) {
    int i,c,r;
    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("faultFinder.seq",msg);

    int rowsAbove = 4; // Number of rows above the BRK line. about half the height of supercell, the number of cells above the row where the PC signal comes in
    int rowsBelow = 6; // Number of rows below the CC line. the rest of the height of the supercell
    int cols = 4;  // width of supercell. Make it small for now. 
    
    // using knowledge external to this file to set these numbers: see fflogic.com and the width of the other supercell replicated there. 
    int startCol = 0; // physical position on grid edge where outside world will hook up to this sequence. Should correspond with position in fflogic.com, digit after the colon (e.g. 0 in "L[9:0]fffail~1.bin")
    int startRow = 9; // physical position on grid edge where outside world will hook up to this sequence. Should correspond with position in fflogic.com, first digit after L (e.g. 9 in "L[9:0]fffail~1.bin")
    // region of grid representing the neighboring supercell on the left is 4 cells wide.
        
/* Start a wire. The code I'm calling assumes wires start from the middle of the supercell. */
    wire_build w=new wire_build(cmi,"e",startRow, startCol,"BRK,PC,CC");
    BRK=w.get_channel("brk"); /* Access wire builder class' break line */
    PC=w.get_channel("pc"); /* Access wire builder class' break line */
    CC=w.get_channel("cc"); /* Access wire builder class' break line */
    COMPARE=cmi.assign_channel(startRow+3,startCol,"DW");
    GO=cmi.assign_channel(startRow+4,startCol,"DW");

    w.preclear(true); 

    String Pin[] = new String[3]; 
    String Pout[] = new String[3];
    setupPatterns(Pin, Pout);

/* SuperSequence 1- traverse the row, testing cells at the head of the wire
    at each wire extension.*/
    /* wire orientation (assuming it is being extended horizontally, from the left) is:
      BRK
      PC
      CC
      */
    int colsEatenByTurning = 2; // I keep count of how many columns have gotten eaten up by the turns in the wire. I use this to stop at the right edge of the cell.
    int rowsEatenByTurning = 2;  // 2 rows will be eaten by the w.turn("CW") statement, and I should reduce the wire extend command by this much
    int goUpAmt = 0;
    
// SuperSequence #1. Test most edges of most cells.
    for (r=0; r<rowsAbove-1; r++)  // can't test the topmost row, hence the subtraction
      {
      cmi.push();  /* Save current state (orientation of head of wire) */
      w.extend_break(false);  // going to always break wire at head, since that's the way wire_build.java class works best right now.
      w.end_break();  // causes BRK line to do something useful
      w.extend(); // need to go forward by one so that the break line will work
      cmi.comment("had to extend wire one column outward so that the break line will operate");

      w.turn("CCW"); // turn North
      cmi.comment("turned North");
      goUpAmt = (rowsAbove-r)-rowsEatenByTurning;
      if (goUpAmt > 0)
         {
         w.extend(goUpAmt); // this actually extends it one shy of the upper edge, which is perfect. Why? because it's relative to the PC line, whereas rowsAbove is counted from the BRK line, one above.
         String s = "extended wire by " + goUpAmt + " cells Northward.";
         cmi.comment(s);
         }
      else cmi.comment("no wire extension. There is at least one row that can be accessed by turning north but not extending north.");
      w.turn("CW"); 	// turn East. Now facing the right direction for testing. Doesn't eat any columns.
      String ss = "aligned to test row with PC at supercell row " + (r+1);
      cmi.comment(ss);
      for (c=0; c<cols-colsEatenByTurning; c++)
         {
         // LATER: try more patterns for both C-mode and D-mode tests. 
         // PC-cell-to-be: test the cell directly right of the PC head cell
         PCTest1(cmi, Pin[0], Pout[0]);      // test 1: make sure truth tables load in properly
         PCTest2(cmi, Pin[1], Pout[1], w);   //test 2: make sure data in and data out lines work properly

         // BRK-cell-to-be: test the cell to the right and above the PC head cell, the to-be-BRK cell
         BRKTest1(cmi, Pin[0], Pout[0], w);      // test 1: make sure truth tables load in properly
         BRKTest2(cmi, Pin[1], Pout[1], w);      //test 2: make sure data in and data out lines work properly
       //  BRKTest3(cmi, Pin[0], Pout[0]);          //test3: make sure truth tables load in properly from the side of the cell opposite the wire, here the East side of the cell.
      //   BRKTest4(cmi, Pin[1], Pout[1], w);      //test4: make sure data in and data out lines work properly from the side of the cell opposite the wire, here the East side of the cell.
      
         // CC-cell-to-be: test the cell to the right and below the PC head cell, the to-be-CC cell
         CCTest1(cmi, Pin[0], Pout[0], w);       // test 1: make sure truth tables load in properly
         CCTest2(cmi, Pin[1], Pout[1], w);       //test 2: make sure data in and data out lines work properly
       //  CCTest3(cmi, Pin[0], Pout[0]);          //test3: make sure truth tables load in properly from the side of the cell opposite the wire, here the East side of the cell.
      //   CCTest4(cmi, Pin[1], Pout[1], w);      //test4: make sure data in and data out lines work properly from the side of the cell opposite the wire, here the East side of the cell.
       
         // go to next rung of wire, if not already at cell edge.
         if(c<cols-colsEatenByTurning-1) 
           {
            w.extend();
            cmi.comment("extended wire by one column. Testing next rung of wire.");
           }
        else 
           cmi.comment("finished with column.");
        } // end column

     BRK.set(true);cmi.cycle();BRK.set(false);  /* Go back to corner */
     cmi.pop(); /* Back to base rotation */
     cmi.comment("done with row. On to next row.");

     } // end row

// End SuperSequence #1. 

    cmi.comment("done testing Supercell");
      
    cmi.pop();
    cmi.close();
   
    } // end main

public static void PCTest1(cell_matrix_interface cmi, String Pin, String Pout) 
// test 1: make sure truth tables load in properly
     {
      // test 1: make sure truth tables load in properly
      CC.set(true);
      PC.set(Pin);
      cmi.cycle();
      cmi.comment("Beginning PCTest1, C-mode test of next PC cell from West side of cell");
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      PC.set(zeros);
      cmi.cycle();
      CC.set(false);  // not sure why I don't set it false earlier in the test. This test used to have no CC.set(false) at all. 
      cmi.comment("finished PCTest1 and cleanup");
      } // end PCTest1

public static void PCTest2(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test 2: make sure data in and data out lines work properly
      w.transfer("DW=W~");  // feedback loop that inverts the input
      cmi.comment("Beginning PCTest2, D-mode test of next PC cell from West side of cell");
      PC.set(Pin);
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      w.transfer(zeros); // blank out PC cell
      cmi.comment("finished PCTest2 and cleanup");
      } // end PCTest2

public static void BRKTest1(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      // test 1: make sure truth tables load in properly
      cmi.comment("beginning BRKTest1, C-mode test for next BRK cell from South side of cell");
      w.transfer("CN=1;DN=W;DW=N"); // redirect stuff up to this cell
      PC.set(Pin);
      cmi.cycle();
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      PC.set(zeros);  // blank out BRK cell
      cmi.cycle();  
      w.transfer(zeros); // clean up PC cell
      cmi.comment("finished BRKTest1 and cleanup");
      // LATER: try more patterns
      } // end BRKTest1

public static void BRKTest2(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test 2: make sure data in and data out lines work properly
      cmi.comment("beginning BRKTest2, D-mode test for next BRK cell from South side of cell");
      w.transfer("DS=S~", "CCW");  // feedback loop that inverts the input
      w.transfer("DW=N;DN=W"); // reroute the output to BRK cell and back 
      PC.set(Pin);
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      w.transfer(zeros, "CCW");  // clear out BRK cell
      w.transfer(zeros);  // clear out PC cell
      cmi.comment("finished BRKTest2 and cleanup");
      } // end BRKTest2

public static void BRKTest3(cell_matrix_interface cmi, String Pin, String Pout) 
      {
      //test3: make sure truth tables load in properly from the side of the cell opposite the wire, here the East side of the cell.
      CC.set(true);                           //step1: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
       cmi.comment("Beginning BRKTest3, to test C-mode from East side of next BRK cell");
//      cmi.comment("step1: configured cell 1 to gain access to cell 4");
     cmi.cycle();
      CC.set(false);                          //step2: configure cell 4 so that it can configure cell 5
      PC.set("DN=WN+;CN=1"); 
//      cmi.comment("step2: configure cell 4 in order to configure cell 5 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step3: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("step3: remove c=1 in cell 1");
     cmi.cycle(); 
      CC.set(false);                          //step4: configure cell 5 to configure cell 2 and pass truth table to/from cell 2.
      PC.set("DW=S;DS=W;CW=1");
//      cmi.comment("step4: configure cell 5 to control cell 2 and pass a truth table to and from cell 2");
      cmi.cycle();
      CC.set(true);                           //step5: regain control of cell 4
      PC.set("DE=W;CE=1"); 
 //      cmi.comment("step5: regain control of cell 4");
     cmi.cycle();
      CC.set(false);                         //step6: give cell 4 its final configuration: it passes data to/from cell 5.
      PC.set("DN=W;DW=N");
//      cmi.comment("step6: give cell 4 its final configuration, passing data to and from cell 5");
      cmi.cycle();
      CC.set(true);                          //step7: reconfigure cell 1 to remove C=1, and to pass the output of the feedback loop to the bidirectional PC line for comparison.
      PC.set("DE=W;DW=E");
 //      cmi.comment("step7: give cell 1 its final configuration");
      cmi.cycle();
      CC.set(false);                         //step8: set cc line low, then use the PC line to drive the feedback loop and to test it.
      PC.set(Pin);                        
      cmi.cycle(); // program cell 2
      COMPARE.set(Pout);          // compare result to inverted input pattern (held in Pout[1])
      GO.set(true);
//      cmi.comment("step8: pass in the Pin[0], truth table config. test, to cell 2 via cells 1, 4, and 5");
      cmi.cycle();
      GO.set(false);
      cmi.comment("finished BRKTest3. Beginning cleanup.");
      // steps 9 - 15 undo the C=1 line in cell 5.
      PC.set(zeros);                          // step 8 and a half: blank out cell 2
 //      cmi.comment("step 8 and a half: cleanup: use connection to cell 2 to blank it out");
     cmi.cycle();
      CC.set(true);                           //step9: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
//      cmi.comment("step9: cleanup: configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //step10: configure cell 4 so that it can configure cell 5
      PC.set("DN=W;CN=1"); 
//      cmi.comment("step10: cleanup: configure cell 4 in order to configure cell 5 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step11: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("step11: cleanup: remove c=1 in cell 1");
      cmi.cycle(); 
      CC.set(false);                          //step12: configure cell 5 to configure cell 2 and pass truth table to/from cell 2.
      PC.set(zeros);
//      cmi.comment("step12: cleanup: clear cell 5");
      cmi.cycle();
      CC.set(true);                           //step13: regain control of cell 4
      PC.set("DE=W;CE=1"); 
 //      cmi.comment("step12: cleanup: regain control of cell 4");
      cmi.cycle();
      CC.set(false);                         //step13: give cell 4 its final configuration: it passes data to/from cell 5.
      PC.set(zeros);
//      cmi.comment("step13: cleanup: clear cell 4");
      cmi.cycle();
      cmi.comment("finished cleanup from BRKTest3");
     // end undo
     } // end BRKTest3


public static void BRKTest4(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test4: make sure data in and data out lines work properly from the side of the cell opposite the wire, here the East side of the cell.
      w.transfer("DE=E~", "CCW");  // feedback loop that inverts the input
      CC.set(true);                           //step3: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
       cmi.comment("beginning BRKTest4, to test D-mode of East side of next BRK cell");
//      cmi.comment("step3: set up feedback loop in target cell, configured cell 1 to gain access to cell 4");
     cmi.cycle();
      CC.set(false);                          //step4: configure cell 4 so that it can configure cell 5
      PC.set("DN=WN+;CN=1"); 
 //      cmi.comment("step4: configure cell 4 in order to configure cell 5 (include feedback loop to prevent config string loss in step 7)");
     cmi.cycle();
      CC.set(true);                           //step5: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
 //      cmi.comment("step5: remove c=1 in cell 1");
     cmi.cycle(); 
      CC.set(false);                          //step6: configure cell 5 to pass PC data to cell 2's inverted feedback loop.
      PC.set("DW=S;DS=W");
//      cmi.comment("step6: configure cell 5 to pass PC data to and from cell 2's feedback loop");
      cmi.cycle();
      CC.set(true);                           //step7: regain control of cell 4
      PC.set("DE=W;CE=1"); 
//      cmi.comment("step7: regain control of cell 4");
      cmi.cycle();
      CC.set(false);                         //step8: give cell 4 its final configuration: it passes data to/from cell 5.
      PC.set("DN=W;DW=N");
//      cmi.comment("step8: give cell 4 its final configuration, passing data to and from cell 5");
      cmi.cycle();
      CC.set(true);                          //step9: reconfigure cell 1 to remove C=1, and to pass the output of the feedback loop to the bidirectional PC line for comparison.
      PC.set("DE=W;DW=E");
//      cmi.comment("step9: give cell 1 its final configuration");
      cmi.cycle();
      CC.set(false);                         //step10: set cc line low, then use the PC line to drive the feedback loop and to test it.
      PC.set(Pin);                        
      COMPARE.set(Pout);          // compare result to inverted input pattern (held in Pout[1])
      GO.set(true);
//      cmi.comment("step10: pass in the Pin[1], feedback loop test, to cell 2 via cells 1, 4, and 5");
      cmi.cycle();
      GO.set(false);
      cmi.comment("finished BRKTest4");
      } // end BRKTest4


public static void CCTest1(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      // test 1: make sure truth tables load in properly
      w.transfer("CS=1;DS=W;DW=S"); // redirect stuff down to this cell
      PC.set(Pin);
      cmi.comment("beginning CCTest1, to test C-mode for the next CC line cell from North side of cell"); 
      cmi.cycle();
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      PC.set(zeros); // clear out CC cell so that this cell can't affect any other tests down the road
      cmi.cycle();
      w.transfer(zeros); // clear out PC cell so that this cell can't affect any other tests down the road
      cmi.comment("finished CCTest1 and cleanup");
      } // end CCTest1 


public static void CCTest2(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test 2: make sure data in and data out lines work properly
      cmi.comment("beginning CCTest2, to test D-mode for the next CC line cell from North side of cell");
      w.transfer("DN=N~", "CW");  // feedback loop that inverts the input
      w.transfer("DW=S;DS=W"); // reroute the output to BRK cell and back 
      PC.set(Pin);
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      w.transfer(zeros, "CW"); // clear out CC cell
      w.transfer(zeros);  // clear out PC cell 
      cmi.comment("finished CCTest2 and cleanup");
      } // end CCTest2 
 
public static void CCTest3(cell_matrix_interface cmi, String Pin, String Pout) 
      {
      //test3: make sure truth tables load in properly from the side of the cell opposite the wire, here the East side of the cell.
      CC.set(true);                           //step1: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
       cmi.comment("Beginning CCTest3, to test C-mode from East side of next CC cell");
//      cmi.comment("step1: configured cell 1 to gain access to cell 4");
     cmi.cycle();
      CC.set(false);                          //step2: configure cell 4 so that it can configure cell 6
      PC.set("DS=WS+;CS=1"); 
//      cmi.comment("step2: configure cell 4 in order to configure cell 6 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step3: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("step3: remove c=1 in cell 1");
     cmi.cycle(); 
      CC.set(false);                          //step4: configure cell 6 to configure cell 3 and pass truth table to/from cell 3.
      PC.set("DW=N;DN=W;CW=1");
//      cmi.comment("step4: configure cell 6 to control cell 3 and pass a truth table to and from cell 3");
      cmi.cycle();
      CC.set(true);                           //step5: regain control of cell 4
      PC.set("DE=W;CE=1"); 
 //      cmi.comment("step5: regain control of cell 4");
     cmi.cycle();
      CC.set(false);                         //step6: give cell 4 its final configuration: it passes data to/from cell 6.
      PC.set("DS=W;DW=S");
//      cmi.comment("step6: give cell 4 its final configuration, passing data to and from cell 6");
      cmi.cycle();
      CC.set(true);                          //step7: reconfigure cell 1 to remove C=1, and to pass the output of the feedback loop to the bidirectional PC line for comparison.
      PC.set("DE=W;DW=E");
 //      cmi.comment("step7: give cell 1 its final configuration");
     cmi.cycle();
      CC.set(false);                         //step8: set cc line low, then use the PC line to drive the feedback loop and to test it.
      PC.set(Pin);                        
      cmi.cycle(); // program cell 2
      COMPARE.set(Pout);          // compare result to inverted input pattern (held in Pout[1])
      GO.set(true);
//      cmi.comment("step8: pass in the Pin[0], truth table config. test, to cell 2 via cells 1, 4, and 5");
      cmi.comment("finishing CCTest3. Beginning cleanup.");
      cmi.cycle();
      GO.set(false);
      // steps 9 - 15 undo the C=1 line in cell 5.
      PC.set(zeros);                          // step 8 and a half: blank out cell 2
 //      cmi.comment("step 8 and a half: cleanup: use connection to cell 2 to blank it out");
     cmi.cycle();
      CC.set(true);                           //step9: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
//      cmi.comment("step9: cleanup: configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //step10: configure cell 4 so that it can configure cell 6
      PC.set("DS=W;CS=1"); 
//      cmi.comment("step10: cleanup: configure cell 4 in order to configure cell 6 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step11: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("step11: cleanup: remove c=1 in cell 1");
      cmi.cycle(); 
      CC.set(false);                          //step12: configure cell 6 to configure cell 3 and pass truth table to/from cell 3.
      PC.set(zeros);
//      cmi.comment("step12: cleanup: clear cell 6");
      cmi.cycle();
      CC.set(true);                           //step13: regain control of cell 4
      PC.set("DE=W;CE=1"); 
 //      cmi.comment("step12: cleanup: regain control of cell 4");
      cmi.cycle();
      CC.set(false);                         //step13: give cell 4 its final configuration: all zeros
      PC.set(zeros);
//      cmi.comment("step13: cleanup: clear cell 4");
      cmi.cycle();
      cmi.comment("finished cleanup from CCTest3");
      // end undo
     } // end BRKTest3


public static void CCTest4(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test4: make sure data in and data out lines work properly from the side of the cell opposite the wire, here the East side of the cell.
      w.transfer("DE=E~", "CW");  // feedback loop that inverts the input
      CC.set(true);                           //step3: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
       cmi.comment("beginning CCTest4, to test D-mode of East side of next CC cell");
//      cmi.comment("step3: set up feedback loop in target cell, configured cell 1 to gain access to cell 4");
     cmi.cycle();
      CC.set(false);                          //step4: configure cell 4 so that it can configure cell 6
      PC.set("DS=WS+;CS=1"); 
 //      cmi.comment("step4: configure cell 4 in order to configure cell 6 (include feedback loop to prevent config string loss in step 7)");
     cmi.cycle();
      CC.set(true);                           //step5: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
 //      cmi.comment("step5: remove c=1 in cell 1");
     cmi.cycle(); 
      CC.set(false);                          //step6: configure cell 6 to pass PC data to cell 3's inverted feedback loop.
      PC.set("DW=N;DN=W");
//      cmi.comment("step6: configure cell 6 to pass PC data to and from cell 3's feedback loop");
      cmi.cycle();
      CC.set(true);                           //step7: regain control of cell 4
      PC.set("DE=W;CE=1"); 
//      cmi.comment("step7: regain control of cell 4");
      cmi.cycle();
      CC.set(false);                         //step8: give cell 4 its final configuration: it passes data to/from cell 6.
      PC.set("DS=W;DW=S");
//      cmi.comment("step8: give cell 4 its final configuration, passing data to and from cell 6");
      cmi.cycle();
      CC.set(true);                          //step9: reconfigure cell 1 to remove C=1, and to pass the output of the feedback loop to the bidirectional PC line for comparison.
      PC.set("DE=W;DW=E");
//      cmi.comment("step9: give cell 1 its final configuration");
      cmi.cycle();
      CC.set(false);                         //step10: set cc line low, then use the PC line to drive the feedback loop and to test it.
      PC.set(Pin);                        
      COMPARE.set(Pout);          // compare result to inverted input pattern (held in Pout[1])
      GO.set(true);
//      cmi.comment("step10: pass in the Pin[1], feedback loop test, to cell 3 via cells 1, 4, and 6");
      cmi.cycle();
      GO.set(false);
      cmi.comment("finished CCTest4");
      } // end CCTest4
 
} // end cellTest class
