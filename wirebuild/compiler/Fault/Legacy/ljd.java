/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

/* this is a first shot at a method for testing the cells inside a supercell for faults. */

class ljd{
  public static void main(String args[]) {
    int i,c,r;
    cell_matrix_channel pc,cc,pc_cc,brk;
    int rows = 10; // length/height of supercell
    int cols = 10;  // width of supercell. Make it small for now. 
    
    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("faultFinder.seq",msg);

/* Start a wire. The code I'm calling assumes wires start from the upper left 
    corner of a matrix. */
    int startCol = 0; 
    int startRow = 0;

    wire_build w=new wire_build(cmi,"e",startRow, startCol,"BRK,PC,CC");
    brk=w.get_channel("brk"); /* Access wire builder class' break line */
    cmi.push();  /* Save current state */

/* SuperSequence 1- traverse the row, testing cells at the head of the wire
    at each wire extension.*/
    /* wire orientation (assuming it is being extended horizontally, from the left) is:
      BRK
      PC
      CC
      */

   w.preclear(true); w.extend_break(true);

   for (r=startRow; r<rows; r++) 
     { 
      w.init();
      for (c=startCol;c<cols; c++ )
        { 
         //      cmi.pop();cmi.push();
         /* one: test the cell that is the next PC line cell. This cell actually gets tested twice, once by previous iteration, but it makes the concept easier.*/
        w.transfer(FeedbackLoopW);
        // insert d-mode tests here. 
        w.transfer(ConfigureTest1);
        w.transfer(ConfigureTest2);
        /* two: test the cell that is the next BRK line cell. */
        w.transfer(FeedbackLoopS, "ccw");
        // insert d-mode tests here. 
        w.transfer(ConfigureTest1, "ccw"); // above
        w.transfer(ConfigureTest2, "ccw"); // above
        /* three: test the cell that is the next CC line cell. */
        w.transfer(FeedbackLoopN, "cw");
        // insert d-mode tests here. 
        w.transfer(ConfigureTest1, "cw"); // below
        w.transfer(ConfigureTest2, "cw"); // below
        /* four: test the cell that is the next-next PC line cell. (i.e. not next time the wire is extended, but the time after that)*/
        w.extend(); // seems like the easiest way to get there, and also causes this loop to make progress.
        w.transfer(FeedbackLoopW);
        // insert d-mode tests here. 
        w.transfer(ConfigureTest1);
        w.transfer(ConfigureTest2);
        }
     /* is this how you break a wire? */
     brk.set(true);cmi.cycle();brk.set(false);  /* Go back to corner */

     }

/* SuperSequence 2- test the western edges of all cells  */
// by starting from right side of grid: flip the above statements to achieve this.

    cmi.pop();
    cmi.close();
  }
}
