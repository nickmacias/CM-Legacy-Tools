/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

/* this is a first shot at a method for testing the cells inside a supercell for faults. */

class cellTest{

// these two are static only so that they can be used by the public static void main()
  static cell_matrix_channel PC,CC,BRK,COMPARE,GO;
  static String zeros="00000000000000000000000000000000";
  static String Pin_0 = "DW=N;DN=WE~&;DE=WE&";	// I did not test the case where Pin_0 has any control lines set.
  static String Pout_0 = Pin_0;
                                //0101 repeated 32 times, encoded in hex
  static String Pin_1="55555555555555555555555555555555";
                                 //1010 repeated 32 times, encoded in hex
  static String Pout_1="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

  public static void main(String args[]) {
    
    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("faultFinder.seq",msg);
    
    // using knowledge external to this file to set these numbers: see fflogic.com and the width of the other supercell replicated there. 
    int startCol = 0; // physical position on grid edge where outside world will hook up to this sequence. Should correspond with position in fflogic.com, digit after the colon (e.g. 0 in "L[9:0]fffail~1.bin")
    int startRow = 9; // physical position on grid edge where outside world will hook up to this sequence. Should correspond with position in fflogic.com, first digit after L (e.g. 9 in "L[9:0]fffail~1.bin")
    // region of grid representing the neighboring supercell on the left is 4 cells wide.
        
/* Start a wire. The code I'm calling assumes wires start from the middle of the supercell. */
    wire_build w=new wire_build(cmi,"e",startRow, startCol,"BRK,PC,CC");
    BRK=w.get_channel("brk"); /* Access wire builder class' break line */
    PC=w.get_channel("pc"); /* Access wire builder class' break line */
    CC=w.get_channel("cc"); /* Access wire builder class' break line */
    COMPARE=cmi.assign_channel(startRow+3,startCol,"DW");
    GO=cmi.assign_channel(startRow+4,startCol,"DW");

    w.preclear(true); 
    
/* SuperSequence 1- traverse the row, testing cells at the head of the wire
    at each wire extension.*/
    /* wire orientation (assuming it is being extended horizontally, from the left) is:
      BRK
      PC
      CC
      */

    // see picture below. Dimensions of supercell.
    int m = 10;    // columns
    int n = 10;     // rows
    int i = 5;       // position along one side where the BRK line feeds in
    int j = 6;       // position along one side where the PC line feeds in
    int k = 7;      // position along one side where the CC line feeds in
//           (0,0)                                   (0, m)
//               --------------------------------
//              |                                          |
//              |                                          |
//              |                                          |
//   BRK  i  |                                          |
//   PC    j  |                                          |
//   CC   k  |                                          |
//              |                                          |
//              |                                          |
//              |                                          |
//               --------------------------------
//           (n,0)                                   (n, m)

    // iterators for looping
    int currentTestRow = 0;
    int it = 0;  // stands for column or row
    int d = 0;
   
//  Supersequence: has 5 parts. Test most edges of most cells.
    cmi.push(); // establish base rotation. I'm not sure whether this is needed, but Nick does it.
    setUpBreakLine(cmi, w);
    
// subsequence #1: run a wire straight out horizontally, testing all the cells along the way. Do this because it covers the initial cells used in a wire, and it doesn't get covered in the test below.
    for (it = 2; it <= (m-1); it++) // test starts at column 2, hence it = 2. 
        { // run a wire straight out horizontally, testing all the cells along the way.
           testCellsAtHeadOfWire(cmi, w, j, it, true);
           w.extend(); cmi.comment("extended wire...");
          }  // end for c
    breakWire(cmi); w.extend();  // need to recreate wire after break, hence the extend().


// subsequence #2: run a wire straight up vertically, testing all the cells along the way. After this, those cells can be assumed to be good for future wire building.
      cmi.push(); // base rotation to return to after this. 
      cmi.comment("------testing vertically, upward-------");
      w.turn("CCW");  // go up
      for (it = i; it > 0; it--)  // go up from row i to 0 i(i = BRK row)
          { 
           if (it < i ) // don't ruin BRK wire end by overwriting it in a test. 
              {
               w.rotate_transfer(true);
               testCellsAtHeadOfWire(cmi, w, it, 2, true);
               w.rotate_transfer(false);
               }
           w.extend();	 cmi.comment("extended wire...");
         }
      cmi.pop();  // back to base rotation
      breakWire(cmi); w.extend(); // need to recreate wire after break, hence the extend().

     
    // run a wire straight down vertically, testing all the cells along the way. After this, those cells can be assumed to be good for future wire building.
      cmi.push(); // base rotation to return to after this
      cmi.comment("------testing vertically, downward-------");
      w.turn("CW");  // go down
      for (it = k; it < n; it++)  // go up from CC row to bottom of supercell.
          { 
           w.rotate_transfer(true);
           testCellsAtHeadOfWire(cmi, w, it, 2, true);
           w.rotate_transfer(false);
           w.extend();	 cmi.comment("extended wire...");
         }
      cmi.pop(); 
      breakWire(cmi); w.extend(); // need to recreate wire after break, hence the extend().


 // subsequence #3:  go to each row of cell except the topmost and bottommost (hence the minus 2), extend a wire along it, and test each cell this intersects.
    // Go along in the same axis as the incoming BRK, PC, CC lines.
    // I start with currentTestRow = 2 because this is the topmost possible location for the PC line (the BRK line is above that, taking up one row, and I want to skip the topmost row, so go down to the 3rd row.)

    for (currentTestRow = 2; currentTestRow <= (n-2); currentTestRow++)
       {
       if ((currentTestRow < i) || (currentTestRow > j))
          {
          cmi.push();  // Save current state (orientation of head of wire) 
          cmi.comment("Testing a new row.");
          if (currentTestRow < i)  w.turn("CCW"); // go up
          if (currentTestRow > j)  w.turn("CW");  // go down
          if (currentTestRow < i)   d = i - currentTestRow;  // calculate amount to extend wire
          if (currentTestRow > j)  d = currentTestRow - k;  // calculate amount to extend wire
          w.extend(d);
          
          if (currentTestRow < i)  w.turn("CW"); // turn East
          if (currentTestRow > j)  w.turn("CCW");  // turn East
          for (it = 4; it <= m-1; it++)
              { // the 4 is because the wire starts off with one rung, the initial BRK, PC, and CC signals; turning the wire eats up 2 columns; and setting up the break line eats up 1 column.
              // the m -1 is because I don't intend to test the last column of cells at all: they should only be involved in the tests from the East side of the previous rung, m-1
              testCellsAtHeadOfWire(cmi, w, currentTestRow, it, true);
              if (it < m-1) 
                 w.extend();	 cmi.comment("extended wire...");
              }
          breakWire(cmi); w.extend(); // need to recreate wire after break, hence the extend().
          cmi.pop();  // Back to a known rotation 
          } // end if above or below straight across.
        } // end for currentTestRow 

// test the outer square of cells, 2 columns total on the West side of supercell and once column total on the other 3 sides.

// subsequence #4: run a wire up vertically, in column 1 (as opposed to column 0), testing all the cells along the way. After this, those cells can be assumed to be good for future wire building.
      cmi.push(); // base rotation to return to after this. 
      cmi.comment("testing upward, column 1 that is skipped in previous tests.");
      w.extend();  // go out the minimal amount needed to double back to column 1.
      w.turn("CCW");  // go up
      w.extend(); // go up one row to pass the BRK line.
      w.turn("CCW"); // double back
      w.turn("CW"); // in the right position now.
      cmi.comment("beginning to test column 1 upward.");
      for (it = i-3; it > 0; it--)  // go up from the bend in the wire to the top edge of the supercell.
          { 
               w.rotate_transfer(true);
               testCellsAtHeadOfWire(cmi, w, it, 1, true);
               w.rotate_transfer(false);
               if (it > 1) w.extend();  // don't tread into zero'th row of supercell.
         }
      cmi.pop();  // back to base rotation
      breakWire(cmi); w.extend(); // need to recreate wire after break, hence the extend().
      
// NOTE: SOME SORT OF PROBLEM HERE< RESULTING IN GRID INSTABLILITY>
// subsequence #5: go around the border of the cell, testing the outer row of edge cells. 
// go clockwise around the cell. 
      cmi.push(); // base rotation to return to after this. 
      cmi.comment("testing the edge cells on the border of the supercell");
      w.turn("CCW");  // go up
      w.extend(); // go up one row to pass the BRK line
      w.turn("CCW"); // double back
      w.turn("CW"); // in the right position now.
      cmi.comment("beginning edge test, column 0, upward.");
      for (it = i-3; it > 0; it--)  // go up from bend in wire to 2 cells from the edge of the cell.
          { 
               w.rotate_transfer(true);
               testCellsAtHeadOfWire(cmi, w, it, 0, false);
               w.rotate_transfer(false);
               if (it > 1) w.extend();  //don't tread into edge row of supercell.
         }
      cmi.pop();  // back to base rotation
      breakWire(cmi); w.extend(); // need to recreate wire after break, hence the extend().
    


// End SuperSequence 
    cmi.comment("done testing Supercell");
//    cmi.pop();
    cmi.close();
   
    } // end main

public static void breakWire(cell_matrix_interface cmi)
    {
     // code segment 3.
     BRK.set(true);cmi.cycle();BRK.set(false);  /* Go back to corner */
     cmi.comment("Just broke wire. ");
     // end code segment 3.
    } // end breakWire

public static void setUpBreakLine(cell_matrix_interface cmi, wire_build w)
{  // extend wire one column and put the action end on the break line
       // code segment 1. 
       w.extend_break(false);  // going to always break wire at head, since that's the way wire_build.java class works best right now.
       w.end_break();  // causes BRK line to do something useful
       w.extend(); // need to go forward by one so that the break line will work
       cmi.comment("had to extend wire one column outward so that the break line will operate");
       // end code segment 1.
} // end setUpBreakLine

public static void testCellsAtHeadOfWire(cell_matrix_interface cmi, wire_build w, int row, int column, boolean testBRK) 
// test the 3 cells at the head of the current wire. 
// row and column are used just to record which cells have undergone which tests.
// they are the coordinates of the main cell, the one occupying the PC-to-be position.
       {
      // code segment 2. 
      // LATER: try more patterns for both C-mode and D-mode tests. 
      // PC-cell-to-be: test the cell directly right of the PC head cell
      PCTest1(cmi, Pin_0, Pout_0);      // test 1: make sure truth tables load in properly
      PCTest2(cmi, Pin_1, Pout_1, w);   //test 2: make sure data in and data out lines work properly

      if (testBRK)  // false for testing the cells at the edge of the supercell
         {
          // BRK-cell-to-be: test the cell to the right and above the PC head cell, the to-be-BRK cell
          BRKTest1(cmi, Pin_0, Pout_0, w);      // test 1: make sure truth tables load in properly
          BRKTest2(cmi, Pin_1, Pout_1, w);      //test 2: make sure data in and data out lines work properly
          BRKTest3(cmi, Pin_0, Pout_0);          //test3: make sure truth tables load in properly from the side of the cell opposite the wire, here the East side of the cell.
          BRKTest4(cmi, Pin_1, Pout_1, w);      //test4: make sure data in and data out lines work properly from the side of the cell opposite the wire, here the East side of the cell.
          }
        
      // CC-cell-to-be: test the cell to the right and below the PC head cell, the to-be-CC cell
      CCTest1(cmi, Pin_0, Pout_0, w);       // test 1: make sure truth tables load in properly
      CCTest2(cmi, Pin_1, Pout_1, w);       //test 2: make sure data in and data out lines work properly
      CCTest3(cmi, Pin_0, Pout_0);          //test3: make sure truth tables load in properly from the side of the cell opposite the wire, here the East side of the cell.
      CCTest4(cmi, Pin_1, Pout_1, w);      //test4: make sure data in and data out lines work properly from the side of the cell opposite the wire, here the East side of the cell.
      // end code segment 2.
 } // end testCellsAtHeadOfWire        
         
     
public static void PCTest1(cell_matrix_interface cmi, String Pin, String Pout) 
// test 1: make sure truth tables load in properly
     {
      // test 1: make sure truth tables load in properly
      CC.set(true);
      PC.set(Pin);
      cmi.cycle();
      cmi.comment("Beginning PCTest1, C-mode test of next PC cell from West side of cell");
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      PC.set(zeros);
      cmi.cycle();
      CC.set(false);  // not sure why I don't set it false earlier in the test. This test used to have no CC.set(false) at all. 
      cmi.comment("finished PCTest1 and cleanup");
      } // end PCTest1

public static void PCTest2(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test 2: make sure data in and data out lines work properly
      w.transfer("DW=W~");  // feedback loop that inverts the input
      cmi.comment("Beginning PCTest2, D-mode test of next PC cell from West side of cell");
      PC.set(Pin);
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      w.transfer(zeros); // blank out PC cell
      cmi.comment("finished PCTest2 and cleanup");
      } // end PCTest2

public static void BRKTest1(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      // test 1: make sure truth tables load in properly
      cmi.comment("beginning BRKTest1, C-mode test for next BRK cell from South side of cell");
      w.transfer("CN=1;DN=W;DW=N"); // redirect stuff up to this cell
      PC.set(Pin);
      cmi.cycle();
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      PC.set(zeros);  // blank out BRK cell
      cmi.cycle();  
      w.transfer(zeros); // clean up PC cell
      cmi.comment("finished BRKTest1 and cleanup");
      // LATER: try more patterns
      } // end BRKTest1

public static void BRKTest2(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test 2: make sure data in and data out lines work properly
      cmi.comment("beginning BRKTest2, D-mode test for next BRK cell from South side of cell");
      w.transfer("DS=S~", "CCW");  // feedback loop that inverts the input
      w.transfer("DW=N;DN=W"); // reroute the output to BRK cell and back 
      PC.set(Pin);
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      w.transfer(zeros, "CCW");  // clear out BRK cell
      w.transfer(zeros);  // clear out PC cell
      cmi.comment("finished BRKTest2 and cleanup");
      } // end BRKTest2

/*
*              ______________________
*              |                  |                    |
*  (BRK)   |  cell 2        |    cell 5        |
*              |                  |                    |
*              --------------- ------------------
*              |                  |                    |
*  PC ->   |  cell 1        |    cell 4        |
*              |                  |                    |
*              --------------- ------------------
*              |                  |                    |
*  CC ->   |  cell 3        |    cell 6        |
*              |                  |                    |
*              --------------- ------------------
*   
*
*
*/
public static void BRKTest3(cell_matrix_interface cmi, String Pin, String Pout) 
      {
      //test3: make sure truth tables load in properly from the side of the cell opposite the wire, here the East side of the cell.
 
 // gain access to and clear out cell 5. This is necessary before allowing an OR operation in step 4.
       CC.set(true);                           //pre-step1: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
       cmi.comment("Beginning BRKTest3, to test C-mode from East side of next BRK cell");
//      cmi.comment("pre-step1: configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //pre-step2: configure cell 4 so that it can configure cell 5
      PC.set("DN=W;CN=1"); 
//      cmi.comment("pre-step2: configure cell 4 in order to configure cell 5 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //pre-step3: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("pre-step3: remove c=1 in cell 1");
     cmi.cycle(); 
      CC.set(false);                          //pre-step4: configure cell 5 to configure cell 2 and pass truth table to/from cell 2.
      PC.set(zeros);
//      cmi.comment("pre-step4: configure cell 5 to control cell 2 and pass a truth table to and from cell 2");
      cmi.cycle();

// NOW I can begin the BRK test sequence. 
      CC.set(true);                           //step1: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
       cmi.comment("Beginning BRKTest3 for real now.");
//      cmi.comment("step1: configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //step2 & 3/4: configure cell 4 so that it can configure cell 5
      PC.set("DN=WN+;CN=1"); 
//      cmi.comment("step2 & 3/4: configure cell 4 in order to configure cell 5 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step3: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("step3: remove c=1 in cell 1");
     cmi.cycle(); 
      CC.set(false);                          //step4: configure cell 5 to configure cell 2 and pass truth table to/from cell 2.
      PC.set("DW=S;DS=W;CW=1");
//      cmi.comment("step4: configure cell 5 to control cell 2 and pass a truth table to and from cell 2");
      cmi.cycle();
      CC.set(true);                           //step5: regain control of cell 4
      PC.set("DE=W;CE=1"); 
 //      cmi.comment("step5: regain control of cell 4");
     cmi.cycle();
      CC.set(false);                         //step6: give cell 4 its final configuration: it passes data to/from cell 5.
      PC.set("DN=W;DW=N");
//      cmi.comment("step6: give cell 4 its final configuration, passing data to and from cell 5");
      cmi.cycle();
      CC.set(true);                          //step7: reconfigure cell 1 to remove C=1, and to pass the output of the feedback loop to the bidirectional PC line for comparison.
      PC.set("DE=W;DW=E");
 //      cmi.comment("step7: give cell 1 its final configuration");
      cmi.cycle();
      CC.set(false);                         //step8: set cc line low, then use the PC line to drive the feedback loop and to test it.
      PC.set(Pin);                        
      cmi.cycle(); // program cell 2
      COMPARE.set(Pout);          // compare result to inverted input pattern (held in Pout_1)
      GO.set(true);
//      cmi.comment("step8: pass in the Pin_0, truth table config. test, to cell 2 via cells 1, 4, and 5");
      cmi.cycle();
      GO.set(false);
      cmi.comment("finished BRKTest3. Beginning cleanup.");
      // steps 9 - 15 undo the C=1 line in cell 5.
      PC.set(zeros);                          // step 8 and a half: blank out cell 2
 //      cmi.comment("step 8 and a half: cleanup: use connection to cell 2 to blank it out");
     cmi.cycle();
      CC.set(true);                           //step9: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
//      cmi.comment("step9: cleanup: configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //step10: configure cell 4 so that it can configure cell 5
      PC.set("DN=W;CN=1"); 
//      cmi.comment("step10: cleanup: configure cell 4 in order to configure cell 5 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step11: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("step11: cleanup: remove c=1 in cell 1");
      cmi.cycle(); 
      CC.set(false);                          //step12: configure cell 5 to configure cell 2 and pass truth table to/from cell 2.
      PC.set(zeros);
//      cmi.comment("step12: cleanup: clear cell 5");
      cmi.cycle();
      CC.set(true);                           //step13: regain control of cell 4
      PC.set("DE=W;CE=1"); 
 //      cmi.comment("step12: cleanup: regain control of cell 4");
      cmi.cycle();
      CC.set(false);                         //step13: give cell 4 its final configuration: it passes data to/from cell 5.
      PC.set(zeros);
//      cmi.comment("step13: cleanup: clear cell 4");
      cmi.cycle();
      CC.set(true);                           //step13: regain control of cell 1
      PC.set(zeros);                          // clear out cell 1
//      cmi.comment("step14: cleanup: clear cell 1");
      cmi.cycle();
      cmi.comment("finished cleanup from BRKTest3");
     // end undo
     } // end BRKTest3


public static void BRKTest4(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test4: make sure data in and data out lines work properly from the side of the cell opposite the wire, here the East side of the cell.
      w.transfer("DE=E~", "CCW");  // feedback loop that inverts the input
 
 // gain access to and clear out cell 5. This is necessary before allowing an OR operation in step 4.
       CC.set(true);                           //step3: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
       cmi.comment("beginning BRKTest4, to test D-mode of East side of next BRK cell");
//      cmi.comment("pre-step3: set up feedback loop in target cell, configured cell 1 to gain access to cell 4");
     cmi.cycle();
      CC.set(false);                          //step4: configure cell 4 so that it can configure cell 5
      PC.set("DN=W;CN=1"); 
 //      cmi.comment("pre-step4: configure cell 4 in order to configure cell 5 (include feedback loop to prevent config string loss in step 7)");
     cmi.cycle();
      CC.set(true);                           //step5: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
 //      cmi.comment("pre-step5: remove c=1 in cell 1");
     cmi.cycle(); 
      CC.set(false);                          //step6: clear cell 5.
      PC.set(zeros);
//      cmi.comment("pre-step6: clear cell 5");
      cmi.cycle();
 
// NOW I can begin the BRK test sequence. 
      CC.set(true);                           //step3: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
       cmi.comment("beginning BRKTest4, to test D-mode of East side of next BRK cell");
//      cmi.comment("step3: set up feedback loop in target cell, configured cell 1 to gain access to cell 4");
     cmi.cycle();
      CC.set(false);                          //step4: configure cell 4 so that it can configure cell 5
      PC.set("DN=WN+;CN=1"); 
 //      cmi.comment("step4: configure cell 4 in order to configure cell 5 (include feedback loop to prevent config string loss in step 7)");
     cmi.cycle();
      CC.set(true);                           //step5: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
 //      cmi.comment("step5: remove c=1 in cell 1");
     cmi.cycle(); 
      CC.set(false);                          //step6: configure cell 5 to pass PC data to cell 2's inverted feedback loop.
      PC.set("DW=S;DS=W");
//      cmi.comment("step6: configure cell 5 to pass PC data to and from cell 2's feedback loop");
      cmi.cycle();
      CC.set(true);                           //step7: regain control of cell 4
      PC.set("DE=W;CE=1"); 
//      cmi.comment("step7: regain control of cell 4");
      cmi.cycle();
      CC.set(false);                         //step8: give cell 4 its final configuration: it passes data to/from cell 5.
      PC.set("DN=W;DW=N");
//      cmi.comment("step8: give cell 4 its final configuration, passing data to and from cell 5");
      cmi.cycle();
      CC.set(true);                          //step9: reconfigure cell 1 to remove C=1, and to pass the output of the feedback loop to the bidirectional PC line for comparison.
      PC.set("DE=W;DW=E");
//      cmi.comment("step9: give cell 1 its final configuration");
      cmi.cycle();
      CC.set(false);                         //step10: set cc line low, then use the PC line to drive the feedback loop and to test it.
      PC.set(Pin);                        
      COMPARE.set(Pout);          // compare result to inverted input pattern (held in Pout_1)
      GO.set(true);
//      cmi.comment("step10: pass in the Pin_1, feedback loop test, to cell 2 via cells 1, 4, and 5");
      cmi.cycle();
      GO.set(false);
      cmi.comment("finished BRKTest4");
      } // end BRKTest4


public static void CCTest1(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      // test 1: make sure truth tables load in properly
      w.transfer("CS=1;DS=W;DW=S"); // redirect stuff down to this cell
      PC.set(Pin);
      cmi.comment("beginning CCTest1, to test C-mode for the next CC line cell from North side of cell"); 
      cmi.cycle();
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      PC.set(zeros); // clear out CC cell so that this cell can't affect any other tests down the road
      cmi.cycle();
      w.transfer(zeros); // clear out PC cell so that this cell can't affect any other tests down the road
      cmi.comment("finished CCTest1 and cleanup");
      } // end CCTest1 


public static void CCTest2(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test 2: make sure data in and data out lines work properly
      cmi.comment("beginning CCTest2, to test D-mode for the next CC line cell from North side of cell");
      w.transfer("DN=N~", "CW");  // feedback loop that inverts the input
      w.transfer("DW=S;DS=W"); // reroute the output to BRK cell and back 
      PC.set(Pin);
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      w.transfer(zeros, "CW"); // clear out CC cell
      w.transfer(zeros);  // clear out PC cell 
      cmi.comment("finished CCTest2 and cleanup");
      } // end CCTest2 
 
public static void CCTest3(cell_matrix_interface cmi, String Pin, String Pout) 
      {
      //test3: make sure truth tables load in properly from the side of the cell opposite the wire, here the East side of the cell.


 // gain access to and clear out cell 6. This is necessary before allowing an OR operation in step 4.
      CC.set(true);                           //pre-step1: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
       cmi.comment("Beginning CCTest3, to test C-mode from East side of next CC cell");
//      cmi.comment("pre-step1: configured cell 1 to gain access to cell 4");
     cmi.cycle();
      CC.set(false);                          //pre-step2: configure cell 4 so that it can configure cell 6
      PC.set("DS=W;CS=1"); 
//      cmi.comment("pre-step2: configure cell 4 in order to configure cell 6 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //pre-step3: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("pre-step3: remove c=1 in cell 1");
     cmi.cycle(); 
      CC.set(false);                          //pre-step4: configure cell 6 to configure cell 3 and pass truth table to/from cell 3.
      PC.set(zeros);
//      cmi.comment("pre-step4: configure cell 6 to control cell 3 and pass a truth table to and from cell 3");
      cmi.cycle();


// NOW I can begin the CC test sequence. 
      CC.set(true);                           //step1: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
       cmi.comment("Beginning CCTest3, to test C-mode from East side of next CC cell");
//      cmi.comment("step1: configured cell 1 to gain access to cell 4");
     cmi.cycle();
      CC.set(false);                          //step2: configure cell 4 so that it can configure cell 6
      PC.set("DS=WS+;CS=1"); 
//      cmi.comment("step2: configure cell 4 in order to configure cell 6 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step3: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("step3: remove c=1 in cell 1");
     cmi.cycle(); 
      CC.set(false);                          //step4: configure cell 6 to configure cell 3 and pass truth table to/from cell 3.
      PC.set("DW=N;DN=W;CW=1");
//      cmi.comment("step4: configure cell 6 to control cell 3 and pass a truth table to and from cell 3");
      cmi.cycle();
      CC.set(true);                           //step5: regain control of cell 4
      PC.set("DE=W;CE=1"); 
 //      cmi.comment("step5: regain control of cell 4");
     cmi.cycle();
      CC.set(false);                         //step6: give cell 4 its final configuration: it passes data to/from cell 6.
      PC.set("DS=W;DW=S");
//      cmi.comment("step6: give cell 4 its final configuration, passing data to and from cell 6");
      cmi.cycle();
      CC.set(true);                          //step7: reconfigure cell 1 to remove C=1, and to pass the output of the feedback loop to the bidirectional PC line for comparison.
      PC.set("DE=W;DW=E");
 //      cmi.comment("step7: give cell 1 its final configuration");
     cmi.cycle();
      CC.set(false);                         //step8: set cc line low, then use the PC line to drive the feedback loop and to test it.
      PC.set(Pin);                        
      cmi.cycle(); // program cell 2
      COMPARE.set(Pout);          // compare result to inverted input pattern (held in Pout_1)
      GO.set(true);
//      cmi.comment("step8: pass in the Pin_0, truth table config. test, to cell 2 via cells 1, 4, and 5");
      cmi.comment("finishing CCTest3. Beginning cleanup.");
      cmi.cycle();
      GO.set(false);
      // steps 9 - 15 undo the C=1 line in cell 5.
      PC.set(zeros);                          // step 8 and a half: blank out cell 2
 //      cmi.comment("step 8 and a half: cleanup: use connection to cell 2 to blank it out");
     cmi.cycle();
      CC.set(true);                           //step9: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
//      cmi.comment("step9: cleanup: configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //step10: configure cell 4 so that it can configure cell 6
      PC.set("DS=W;CS=1"); 
//      cmi.comment("step10: cleanup: configure cell 4 in order to configure cell 6 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step11: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("step11: cleanup: remove c=1 in cell 1");
      cmi.cycle(); 
      CC.set(false);                          //step12: configure cell 6 to configure cell 3 and pass truth table to/from cell 3.
      PC.set(zeros);
//      cmi.comment("step12: cleanup: clear cell 6");
      cmi.cycle();
      CC.set(true);                           //step13: regain control of cell 4
      PC.set("DE=W;CE=1"); 
 //      cmi.comment("step12: cleanup: regain control of cell 4");
      cmi.cycle();
      CC.set(false);                         //step13: give cell 4 its final configuration: all zeros
      PC.set(zeros);
//      cmi.comment("step13: cleanup: clear cell 4");
      cmi.cycle();
      cmi.comment("finished cleanup from CCTest3");
      // end undo
     } // end BRKTest3


public static void CCTest4(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test4: make sure data in and data out lines work properly from the side of the cell opposite the wire, here the East side of the cell.
      w.transfer("DE=E~", "CW");  // feedback loop that inverts the input
 
 // gain access to and clear out cell 6. This is necessary before allowing an OR operation in step 4.
       CC.set(true);                           //pre-step3: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
       cmi.comment("beginning CCTest4, to test D-mode of East side of next CC cell");
//      cmi.comment("pre-step3: set up feedback loop in target cell, configured cell 1 to gain access to cell 4");
     cmi.cycle();
      CC.set(false);                          //pre-step4: configure cell 4 so that it can configure cell 6
      PC.set("DS=W;CS=1"); 
 //      cmi.comment("pre-step4: configure cell 4 in order to configure cell 6 (include feedback loop to prevent config string loss in step 7)");
     cmi.cycle();
      CC.set(true);                           //pre-step5: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
 //      cmi.comment("pre-step5: remove c=1 in cell 1");
     cmi.cycle(); 
      CC.set(false);                          //pre-step6: clear out any garbage in cell 6.
      PC.set(zeros);
//      cmi.comment("pre-step6: clearing cell 6 in case it's not already clear");
      cmi.cycle();
 
 
// NOW I can begin the CC test sequence. 
      CC.set(true);                           //step3: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
       cmi.comment("beginning CCTest4, to test D-mode of East side of next CC cell");
//      cmi.comment("step3: set up feedback loop in target cell, configured cell 1 to gain access to cell 4");
     cmi.cycle();
      CC.set(false);                          //step4: configure cell 4 so that it can configure cell 6
      PC.set("DS=WS+;CS=1"); 
 //      cmi.comment("step4: configure cell 4 in order to configure cell 6 (include feedback loop to prevent config string loss in step 7)");
     cmi.cycle();
      CC.set(true);                           //step5: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
 //      cmi.comment("step5: remove c=1 in cell 1");
     cmi.cycle(); 
      CC.set(false);                          //step6: configure cell 6 to pass PC data to cell 3's inverted feedback loop.
      PC.set("DW=N;DN=W");
//      cmi.comment("step6: configure cell 6 to pass PC data to and from cell 3's feedback loop");
      cmi.cycle();
      CC.set(true);                           //step7: regain control of cell 4
      PC.set("DE=W;CE=1"); 
//      cmi.comment("step7: regain control of cell 4");
      cmi.cycle();
      CC.set(false);                         //step8: give cell 4 its final configuration: it passes data to/from cell 6.
      PC.set("DS=W;DW=S");
//      cmi.comment("step8: give cell 4 its final configuration, passing data to and from cell 6");
      cmi.cycle();
      CC.set(true);                          //step9: reconfigure cell 1 to remove C=1, and to pass the output of the feedback loop to the bidirectional PC line for comparison.
      PC.set("DE=W;DW=E");
//      cmi.comment("step9: give cell 1 its final configuration");
      cmi.cycle();
      CC.set(false);                         //step10: set cc line low, then use the PC line to drive the feedback loop and to test it.
      PC.set(Pin);                        
      COMPARE.set(Pout);          // compare result to inverted input pattern (held in Pout_1)
      GO.set(true);
//      cmi.comment("step10: pass in the Pin_1, feedback loop test, to cell 3 via cells 1, 4, and 6");
      cmi.cycle();
      GO.set(false);
      cmi.comment("finished CCTest4");
      } // end CCTest4
 
} // end cellTest class
