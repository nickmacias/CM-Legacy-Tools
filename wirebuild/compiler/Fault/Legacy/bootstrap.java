/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class bootstrap{
  public static void main(String args[]) {
    int i,c,r;
    cell_matrix_channel pc,cc,pc_cc,cc_cc;

    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("boot.seq",msg);

    if (args.length != 1){
      System.out.println("Usage: bootstrap filename.bin");
      System.exit(1);
    }

/* Read the binary grid file */
    binary_file_reader bfr=new binary_file_reader(args[0]);

/* Start a wire */
    wire_build w=new wire_build(cmi,"e",0,0,"PC,CC");
    w.preclear(true);

/* Loop through all columns */
    for (c=bfr.cols()-1;c>=0;c--){
      w.init();
      w.extend(c);
      w.turn("CW");
      for (r=0;r<bfr.rows();r++){
        w.extend();
        w.transfer(bfr.readTT(r,c),"CCW");
      }
    }
    cmi.close();
  }
}
