/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

import java.lang.*;
import java.io.*;

/*
 * binary_file_reader - main class for accessing binary files
 * suitable for loading into standard cell matrix simulator
 *
 * Constructor: binary_file_reader(filename);
 *  Opens the file and reads it into the created object
 *
 * Methods:
 *  int rows() - returns # of rows in grid
 *  int cols() - returns # of columns in grid
 *  boolean error() - return error status
 *  String readTT(int row, int col) - returns 32 hex character TT at [r,c]
 *
 */

class binary_file_reader{
  String grid[][];
  int ROWS,COLS;
  boolean ERROR=false;
  String fname="<No File Opened>";  /* Save Filename */

/* Instantiator */
  public binary_file_reader(String name)
  {
    fname=name;
    readbinarygrid(name);
    if (ERROR) fname="<No File Opened>";
  }

/* read dims */
  public int rows(){return(ROWS);}
  public int cols(){return(COLS);}
  public boolean error(){return(ERROR);}

/* Read a truth table */
  public String readTT(int r,int c){
    if ((r < 0) || (r >= ROWS) || (c < 0) || (c >= COLS)){
      System.out.println("No cell [" + r + "," + c + "] in " + fname);
      return("00000000000000000000000000000000");
    }

    return(grid[r][c]);
  }

  public void readbinarygrid(String name)
  {
    int r,c,i,j,k;
    String TT;
    String temp;
    FileReader fr;
    BufferedReader in;

    try{
      fr=new FileReader(name);
      in=new BufferedReader(fr);

/* read header */
      for (i=0;i<80;i++) j=readchar(in);

/* Read dims */
      
      ROWS=readint(in);
      COLS=readint(in);

/* Make space for the grid */
      grid=new String[ROWS][COLS];
  
/* read the cells */
      for (r=0;r<ROWS;r++){
        for(c=0;c<COLS;c++){
          i=readchar(in); /* IN */
          i=readchar(in); /* OUT */
          TT="";
          for (i=0;i<16;i++){ /* TT */
            j=readchar(in);
            TT=Integer.toHexString((j/16)&15) + Integer.toHexString((j&15)) + TT;
          }
          grid[r][c]=TT;
          j=readint(in); /* # chars in desc */
          for (i=0;i<j;i++) k=readchar(in); /* Flush descriptor */
       } /* End of c */
      } /* End of r */
      in.close();	/* Done with file */
    } catch (Exception e){
      ERROR=true;
      System.out.println("File error: " + e);
    }

  }

/* I/O routines */

  public int readint(BufferedReader in)
  {
    int i1,i2,i;
    i1=readchar(in); /* Actually returns an int from 0-255 */
    i2=readchar(in);
    i=i1 | (i2 << 8);
    return(i);
  }

  public int readchar(BufferedReader in)
  {
    char c1,c2;
    char buffer[] = new char[1];
    int i,i1,i2;

    try{
      i=in.read(buffer);c1=buffer[0];
      i1=(" !\"#$%&'()*+,-.//").indexOf(c1); /* MSN + 32 */
      i=in.read(buffer);c2=buffer[0];
      i2=(" !\"#$%&'()*+,-.//").indexOf(c2); /* LSN + 32 */
      i=(i1<<4)|i2;
      return(i);
    } catch (Exception e){
      ERROR=true;
      System.out.println("Read error: " + e);
      return(0);
    }
  }
}
