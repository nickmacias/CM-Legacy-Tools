/* ljkd's TABULATE.C                                                     */
/*     -  REVERSE POLISH BITWISE CALCULATOR FOR NICK'S TRUTH TABLES      */
/* Polish calculator from K&R modified to do bitwise operations          */
/* Reverse Notation required: write DN := ~E N|S  as DN := E! NS|        */
/*     ( or (E!)(NS|) ). Any spaces are ignored.                         */
/* INPUT: takes the symbolic RHS of the statement above -----^           */
/* as subprocedure, takes this RHS as a one-line argument "input"        */
/* OUTPUT: the boolean value of the RHS for all possible values of E, N, &S */
/*         in the form of an array called result.                        */
/* Program can be used interactively. To see partial results, put parens */
/* around subexpressions.                                                */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define MAXOP 100
#define ENSW  '9'   /* signal that an operand was found. */
#define NUMBER '0'   /* for a current of 1 or 0 on RHS */
#define BUFSIZE 100

unsigned int * tabulate();
int getop();
void push();
unsigned int pop();
unsigned int masked();

char input[MAXOP];         /* 1 line of input from stdin */
char * position;        /* current pos for reading input line */

unsigned int * tabulate(input)
char * input;
{
  int type, i, return_val;
  unsigned int d1, d2;     /* space for temp variables */
  char s[MAXOP];
  static unsigned int result[16];/* 1-dim array for return values for 0 - 15 */

  for (i = 0; i < 16; ++i) {  
    position = &input[0];
    while ((type = getop(s)) != '\0') {
      switch(type) {
      case ENSW:
	push(masked(i,atoi(s)));
	break;
      case '&':          /* AND *//* all operators are boolean instead of */
	d1 = pop();
	d2 = pop();
	push(d1 && d2);   /* bitwise since I divided i into 1 or 2 */
	break;                  /* numbers = mask of all but 1 bit */
      case '+':          /* OR */
      case '|':
	d1 = pop();
	d2 = pop();
	push(d1 || d2); 
	break;
      case '~':          /* NOT */
      case '!':
	push(!(pop()));
	break;
      case '^':         /* XOR */
	d1 = pop();
	d2 = pop();
	push( (!d1 && d2) || (d1 && !d2) );
	break;
      case NUMBER:          /* 0 or 1, i.e. CN = 1 */
	return_val = atoi(s);         /* for the return array 0 --> 15 */
	push((unsigned int)return_val);   /* put return val T or F on stack: if you get */
	break;            /* bugs, remove this special case from switch */
      case '(':          /* ignore opening parenthesis */
	break;          
      case ')':         /* print answer for each subexpression */
	d1 = pop();
	push(d1);            /* but keep the value in stack */
	continue;         /* get out of loop? */
      default:
	fprintf(stderr,"error: unknown command %s\n", s);
	return NULL;
      }
    }   /* WHILE NOT EOL */
    /*  case '\n':  */       /* print answer for each expression, each line */
    d1 = pop();
    d1 = d1 > 0? 1 : 0;
    result[i] = d1;
  }  /* end for all i */
  return(result);       
}

      

#define STACKEND 100         /* max depth of stack */

int sp = 0;
unsigned int val[STACKEND];

/* PUSH VALUE ONTO STACK */

void push(f)
unsigned int f;
{
  if (sp < STACKEND)
    val[sp++] = f;
  else
    fprintf(stderr,"error: stack full, can't push %g\n", f);
  return;
}


/* POP VALUE OFF STACKEND AND RETURN IT */

unsigned int pop()
{
  if (sp > 0)
    return val[--sp];
  else {
    fprintf(stderr,"error: stack empty\n");
    return (unsigned) -1;
  }
}
    

unsigned int masked(template, bit)
unsigned int template;
unsigned int bit;
{
  return (template & bit);     /* returns all zero's but what's at that bit */
}



/* GETOP: get next operator (or 0 or 1) or numeric operand */
/* RETURN the TYPE of that value */

int getop(s)
char s[];
{
  int c;

  while ((s[0] = c = *position++) == ' ' || c == '\t')
    ;
  s[1] = '\0';
  if (isalpha(c)) {  /* should be either N, E, S, or W */
    if (c == 'S') {
      s[0] = '8';
      s[1] = '\0';
      return ENSW;
    }
    else if (c == 'N') {
      s[0] =  '4';    /* map N to the 3rd bit */
      s[1] = '\0';
      return ENSW;
    }
    else if (c == 'E') {
      s[0] = '2';
      s[1] = '\0';
      return ENSW;    /* map E to the 2nd bit */
    }
    else if (c == 'W'){
      s[0] = '1';
      s[1] = '\0';
      return ENSW;
    }

  } 
    
  if (!isdigit(c))
    return c;            /* not a number: hopefully an operator */
  if (isdigit(c)) {      /* if digit, it'll be a 1 or 0  */
    if (c == '0' || c == '1') {
      s[0] = c;
      s[1] = '\0';
    }
    else {
        if (c == -1)      /* -1 is EOF */
	  fprintf(stderr,"EOF encountered\n");
	else
	  fprintf(stderr,"input # out-of-bounds \n");
      }
  }

  return NUMBER;
}
