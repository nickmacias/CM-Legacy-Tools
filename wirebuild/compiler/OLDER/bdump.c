#include <stdio.h> 
#include <string.h>

void bcompute(char *,char[8][80]);
int pget(char *,int);
int try(int,int,int,int,int[4][4],int[4][4]);
void output(int,int,int,int);
void km(char *,int *,char *);

bdump(ii,jj,program)	/* display boolean functions */
int ii,jj;
char *program;
{
	int i;
	char output[8][80];

	printf("BOOLEAN DUMP OF CELL[%d,%d]\n",ii,jj);
	bcompute(program,output);	/* load the output strings */
	for (i=0;i<8;i++)
	if (0!=strlen(output[i]))
		printf("%s\n",output[i]);
	return(0);
}

static char label[8][3]={"DE","DW","DS","DN","CE","CW","CS","CN"};

void bcompute(program,output)	/* create the output strings */
char *program;
char output[8][80];
{
	int pass[16];	/* program to pass to km */
	int i,j;

	for (i=0;i<8;i++){	/* do output column #i */
		for (j=0;j<16;j++) pass[j]=pget(program,i+(j<<3));
		km(label[i],pass,output[i]);	/* load output[i] */
	}
}

int pget(program,ind)
char *program;
int ind;
{
  return((program[ind>>3]>>(ind%8))&1);
}


static int disp_rbase=0,disp_cbase=0;	/* the ones displayed */
static int dindex[16]={0,1,3,2,4,5,7,6,12,13,15,14,8,9,11,10};
char buf[120];
static int new=1;

void km(out,program,retbuf)
char *out,*retbuf;
int *program;
{
	int map[4][4];	/* Karnaugh map (Damn you GW!) */
	int omap[4][4];	/* entries we've covered alredy */
	int i,j;

/* Load the map in the proper order */
	for (i=0;i<4;i++)
	for (j=0;j<4;j++){	/* load the map */
		omap[i][j]=map[i][j]=program[dindex[4*i+j]];
	}

	sprintf(buf,"%s=",out);
	new=1;

/* Sweep the map, try to cover rectangles */
	for (i=0;i<4;i++)
	for (j=0;j<4;j++)
	if (map[i][j]){	/* find a rect */
		if (try(i,j,4,4,map,omap)) continue;
		if (try(i,j,2,4,map,omap)) continue;
		if (try(i+1,j,2,4,map,omap)) continue;
		if (try(i,j,4,2,map,omap)) continue;
		if (try(i,j-1,4,2,map,omap)) continue;
		if (try(i,j,2,2,map,omap)) continue;
		if (try(i+1,j,2,2,map,omap)) continue;
		if (try(i,j-1,2,2,map,omap)) continue;
		if (try(i+1,j-1,2,2,map,omap)) continue;
		if (try(i,j,1,4,map,omap)) continue;
		if (try(i,j,4,1,map,omap)) continue;
		if (try(i,j,1,2,map,omap)) continue;
		if (try(i,j-1,1,2,map,omap)) continue;
		if (try(i,j,2,1,map,omap)) continue;
		if (try(i+1,j,2,1,map,omap)) continue;
		if (try(i,j,1,1,map,omap)) continue;
	}
	if (new) strcpy(buf,"");
	strcpy(retbuf,buf);
	/*while (strlen(retbuf) < 72) strcat(retbuf," ");*/
}

int try(i,j,h,w,map,omap)	/* try to cover rect */
int i,j,h,w;		/* If you can, clear map & record it */
int map[4][4],omap[4][4];
{
	int ii,jj;
	for (ii=0;ii<h;ii++)
	for (jj=0;jj<w;jj++)
		if (!omap[(8+i-ii)%4][(4+j+jj)%4]) return(0);
/* Got it covered! */
	if (!new) strcat(buf,"+");
	new=0;
	output((i+4)%4,(j+4)%4,h,w);
	for (ii=0;ii<h;ii++)
	for (jj=0;jj<w;jj++)
		map[(8+i-ii)%4][(4+j+jj)%4]=0;
	return(1);
}

void output(i,j,h,w)	/* tack cool stuff onto buf */
int i,j,h,w;
{
	switch(h){
		case 4:break;	/* all E and N */
		case 2:switch(i){
			case 0:strcat(buf,"~N");break;
			case 1:strcat(buf,"~S");break;
			case 2:strcat(buf,"N");break;
			case 3:strcat(buf,"S");break;
		} break;
		case 1:switch(i){
			case 0:strcat(buf,"~S~N");break;
			case 1:strcat(buf,"~SN");break;
			case 2:strcat(buf,"SN");break;
			case 3:strcat(buf,"S~N");break;
		}
	}

	switch(w){
		case 4:if (h==4) strcat(buf,"1");
			break;	/* all S and W */
		case 2:switch(j){
			case 0:strcat(buf,"~E");break;
			case 1:strcat(buf,"W");break;
			case 2:strcat(buf,"E");break;
			case 3:strcat(buf,"~W");break;
		} break;
		case 1:switch(j){
			case 0:strcat(buf,"~E~W");break;
			case 1:strcat(buf,"~EW");break;
			case 2:strcat(buf,"EW");break;
			case 3:strcat(buf,"E~W");break;
		}
	}
}
