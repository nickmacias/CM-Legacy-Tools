#include <stdio.h>
/*#include <windows.h>*/
#include "engine.h"
#include "eng_def.h"
#include <string.h>
#include <fcntl.h> 
#include <io.h>                           
/*#include <vbapi.h>*/
#include <malloc.h>

/*
 * engine support code - mainly file I/O and wrapping around engine.c code
 *
 * Most of the wrapping is cosmetic (name changes, etc).
 * read_cell has to return a variable-length description, so it needs to
 * actually do something (VBSetHlstr/etc).
 *
 * The file I/O code is for reading binary cell/grid files and loading them
 * into the grid (once loaded, the caller can use read_cell to examine them).
 * Having these routines available in C frees the VB code from having
 * to deal with the <CR><LF> oddities of C write calls (which add <CR>s
 * even when writing binary data). 
 *
 */
 
int far pascal pigsFail(int r,int c,int mode)
{ 
  switch (mode){
    case 0:fail(r,c,'0');break;
    case 5:fail(r,c,'I');break;
  }
  return(0);
}

int init_grid(int,int,int,int,int);
/*int GetPrivateProfileInt(char *,char *,int,char *); */
void bcompute(char *,char *,char*,char*,char*,char*,char*,char*,char*);
 
int far pascal LibMain(HANDLE hInstance,WORD wDataSeg,WORD wHeapSize,LPSTR lpszCmdLine)
{  
   int height,width,qsize,chgstacksize,carrsize;
    /*FILE *fp;*/
   
   if (wHeapSize > 0) UnlockData(0);
/* Read array dims from .ini file */
   
   height=GetPrivateProfileInt("Engine","Height",50,"pigs.ini");
   width=GetPrivateProfileInt("Engine","Width",50,"pigs.ini");
   qsize=GetPrivateProfileInt("Engine","QSize",7500,"pigs.ini");
   carrsize=GetPrivateProfileInt("Engine","CArrSize",15000,"pigs.ini");
   chgstacksize=GetPrivateProfileInt("Engine","ChgStackSize",15000,"pigs.ini");
   
   if (init_grid(height,width,qsize,chgstacksize,carrsize)) return(0); /* Out of memory */

 /***
 fp=fopen("c:\\test.txt","w+");
 fprintf(fp,"HAHAHA\n");
 fclose(fp);
 ***/
   return(1); /* succesful! */
}

/**************************************/
/*   binary cell reader/loader        */
/*   returns 0(success) or 1(error)   */
/**************************************/
        
int far pascal pigsQSize()
{
  return (q_size());        
}

int far pascal pigsReInit()
{
  return(reinit());
}

int far pascal pigsLoadCellFile(char *file,int i,int j)
{
  char program[16];
  int ifp,desc_len;
  char desc[1024];

   ifp=open(file,0);
   if (ifp <= 0) return(1); /* error */

/* read program */
   rread(ifp,program,16);

/* read description */
   rread(ifp,(char *) &desc_len,sizeof(int));
   if (desc_len > 1023) desc_len=1023;
   if (desc_len > 0) rread(ifp,desc,desc_len);
   else strcpy(desc,"");
   desc[1023]='\0';
   close(ifp);

/* Now load into the grid */
   if (write_cell(i,j,program,desc)){
      return(1);
   }
   
   return(0);
}

int far pascal pigsLoadGridFile(char *fname,int r,int c,int *h,int *w)
{
  int i,j,ifp,desc_len;
  char in,out,program[16],buffer[80],desc[1024];

  ifp=open(fname,O_RDONLY);
  if (ifp < 0){
    return(1); /* open failure */
  }

  rread(ifp,buffer,80); /* read comment */

  rread(ifp,(char *)h,sizeof(int));
  rread(ifp,(char *)w,sizeof(int)); /* read dims */

  for (i=r;i<r+(*h);i++){
    for(j=c;j<c+(*w);j++){
      rread(ifp,&in,1);
      rread(ifp,&out,1);
      rread(ifp,program,16);
      rread(ifp,(char *) &desc_len,sizeof(int)); /* read # bytes in desc */
      if (desc_len > 1023) desc_len=1023;
      if (desc_len > 0) rread(ifp,desc,desc_len); /* and read description */
      else strcpy(desc,"");
      desc[1023]='\0';                                                       
      if (write_cell(i,j,program,desc)) return(1); /* No in/out recording! */
    }
  }
  close(ifp);
  return(0);
}

int far pascal pigsReadCell(int row,int col,char *in,char *out,char prog[16],LPVOID desc_out)
{
  char desc[1024];

  if (read_cell(row,col,in,out,prog,desc)) return(1); /* read-failure */
  
/* Want to pass back the description... */ 
   return(VBSetHlstr((HLSTR far*) &desc_out,(LPVOID) desc,(USHORT) strlen(desc)));
}

int far pascal pigsProcessEvent()
{
 int status;
 status=process_queue_head(); /* 0=OK, 1=error, 99=Done */
 return(status);
}

int far pascal pigsSetInput(int row,int col,int mask,int value)
{
  return(user_set_input(row,col,mask,value));
}

int far pascal pigsWriteCell(int row,int col, char *prog,char *desc)
{
  return(write_cell(row,col,prog,desc));
}

int far pascal pigsUnloadChange(int *row,int *col,int *in,int *out)
{
  return(unload_chg(row,col,in,out));
}

static int lastclock=0; /* state of system clock */
int far pascal pigsTickClock()
{
  lastclock=1-lastclock;
  if (do_tick(lastclock)) return(-1);
  return(lastclock);
}

void far pascal pigsBdump(char *program,char *o0,char *o1,char *o2,char *o3,char *o4,char *o5,char *o6,char *o7)
{
  bcompute(program,o0,o1,o2,o3,o4,o5,o6,o7);
}

int far pascal pigsTurbo(int ticks)
{
/*
 * Execute ticks-number of clock cycles FAST...flush event queue after
 * each tick, and let changes pile up
 */   
 int i,status;
 for (i=0;i<2*ticks;i++){
   while (0==(status=pigsProcessEvent()));
   if (status==1) return(1); /* Failure */
   if (pigsTickClock() < 0) return(1); /* Bump the clock */
 }
 return(0); /* Success */ 
}
