/*
 * bdump class is used to convert a truth table into a set of Boolean
 * equations.
 */

class bdump
{
/* dindex shows ordering of cells in 4x4 Karnaugh map */
  static int dindex[]={0,1,3,2,4,5,7,6,12,13,15,14,8,9,11,10};
  static StringBuffer retbuf; /* Loaded by cover() method */
  static String labels="NESW"; /* char(0)=bit0=LSB */
  static boolean map[][]=new boolean[4][4];/* Entries we still need to cover */
  static boolean omap[][]=new boolean[4][4];/* Original map */
  static StringBuffer buffer; /* This stores final equation */

/* decomp decompiles a cell_exchange structure into a String[] array */
  static public String[] decomp(cell_exchange ce)
  {
    String s[]=new String[8];
    String sout[]=new String[8];
    int temp[]=new int[16];
    int numStr; /* # of strings in output */
    int i,j;

    numStr=0;
    for (i=0;i<8;i++){
      s[i]="";
/* Load up temp[] array */
      for (j=0;j<16;j++){
        temp[j]=((ce.tt[j+((i>3)?16:0)]&(1<<(i%4)))==0)?0:1;
/* That just picks tt[] rows based on i and j and checks bit based on i */
      }
      s[numStr]=((i<4)?"C":"D") + (labels.charAt(i%4)) + "=" + 
                one_decomp(temp);
      if (s[numStr].length() > 3){
        ++numStr;
      }
    } /* All columns finished */
    if (numStr < 8) s[numStr]="";
    return(s);
  }

/* one_decomp reads an int[] of 1s and 0s and returns RHS */
  static String one_decomp(int program[])
  {
    int i,j;

/* Load the map in the proper order */
    for (i=0;i<4;i++){
      for (j=0;j<4;j++){  /* load the map */
        omap[i][j]=map[i][j]=(program[dindex[4*i+j]]==0)?false:true;
      }
    }

    buffer=new StringBuffer(""); /* Final output buffer (RHS) */

/* Sweep the map, try to cover rectangles */
    for (i=0;i<4;i++){
      for (j=0;j<4;j++){
    retbuf=new StringBuffer(""); /* Returned by output() */
        if (map[i][j]){  /* find a rect */
/* Successful cover() loads retbuf */
          if (cover(i,j,4,4)) continue;
          if (cover(i,j,2,4)) continue;
          if (cover(i+1,j,2,4)) continue;
          if (cover(i,j,4,2)) continue;
          if (cover(i,j-1,4,2)) continue;
          if (cover(i,j,2,2)) continue;
          if (cover(i+1,j,2,2)) continue;
          if (cover(i,j-1,2,2)) continue;
          if (cover(i+1,j-1,2,2)) continue;
          if (cover(i,j,1,4)) continue;
          if (cover(i,j,4,1)) continue;
          if (cover(i,j,1,2)) continue;
          if (cover(i,j-1,1,2)) continue;
          if (cover(i,j,2,1)) continue;
          if (cover(i+1,j,2,1)) continue;
          if (cover(i,j,1,1)) continue;
        }
      }
    }
    return(buffer.toString());
  }

/* Try to cover a rectangle. If you can, then clear from map. */
  static boolean cover(int i,int j,int h,int w)
  {
    int ii,jj;
    for (ii=0;ii<h;ii++){
      for (jj=0;jj<w;jj++){
/* indicies are done mod 4 */
        if (!omap[(8+i-ii)%4][(4+j+jj)%4]) return(false);
      }
    }

/* Got it covered! */
    output((i+4)%4,(j+4)%4,h,w); /* Load retbuf with this product */

/* Add retbuf to buffer */
    if (buffer.length() != 0) buffer.append("+");
    buffer.append(retbuf.toString()); /* Add latest piece of equation */

/* Now clear these bits from map[][] so we don't try to cover them again */
    for (ii=0;ii<h;ii++){
      for (jj=0;jj<w;jj++){
        map[(8+i-ii)%4][(4+j+jj)%4]=false;
      }
    }
    return(true);
  }

  static void output(int i,int j,int h,int w)  /* tack cool stuff onto retbuf */
/* Coverage starts at [i,j] (Lower Left?), size=hxw */
  {
    retbuf=new StringBuffer(""); /* Start with clear buffer */
    switch(h){
      case 4:break;  /* all E and N */
      case 2:switch(i){
        case 0:retbuf.append("~"+labels.charAt(2));break;
        case 1:retbuf.append("~"+labels.charAt(3));break;
        case 2:retbuf.append(""+labels.charAt(2));break;
        case 3:retbuf.append(""+labels.charAt(3));break;
      } break;
      case 1:switch(i){
        case 0:retbuf.append("~"+labels.charAt(3)+"~"+labels.charAt(2));break;
        case 1:retbuf.append("~"+labels.charAt(3)+labels.charAt(2));break;
        case 2:retbuf.append(""+labels.charAt(3)+labels.charAt(2));break;
        case 3:retbuf.append(labels.charAt(3)+"~"+labels.charAt(2));break;
      }
    }
  
    switch(w){
      case 4:if (h==4) retbuf.append("1"); break;  /* all S and W */
      case 2:switch(j){
        case 0:retbuf.append("~"+labels.charAt(1));break;
        case 1:retbuf.append(""+labels.charAt(0));break;
        case 2:retbuf.append(""+labels.charAt(1));break;
        case 3:retbuf.append("~"+labels.charAt(0));break;
      } break;
      case 1:switch(j){
        case 0:retbuf.append("~"+labels.charAt(1)+"~"+labels.charAt(0));break;
        case 1:retbuf.append("~"+labels.charAt(1)+labels.charAt(0));break;
        case 2:retbuf.append(""+labels.charAt(1)+labels.charAt(0));break;
        case 3:retbuf.append(labels.charAt(1)+"~"+labels.charAt(0));break;
      }
    }
  }
}
