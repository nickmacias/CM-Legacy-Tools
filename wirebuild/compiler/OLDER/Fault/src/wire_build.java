/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

/*
 * wire-building class. Defines objects and methods for
 * cell matrix wire building
 *
 * Constructer:
 *  wire_build w=wire_build(cell_matrix_interface cmi,
 *                          String dir, int row, int col,
 *                          String config);
 *    where
 *         dir is N, S, W or E, the primary direction of the wire
 *         row and col locate the cell furthest in the CCW direction from dir
 *         config is "PC,CC" or "CC,PC" and is ordered starting with [row,col]
 *         config can also be "BRK,PC,CC" or "CC,PC,BRK" i.e., BRK must
 *         be adjacent to PC for now.
 *
 * Methods for w are as follows:
 *  w.init(); Hardwires the initial head cells
 *  w.init("CW","CCW"); Build initial cells in corner config
 *  w.init("X",TT,"CW/CCW/"); Build initial transfer cell and do transfer
 *  w.extend(); Extends wire in current direction
 *  w.extend(int n); Extends wire "n" steps
 *  w.extend_break(boolean flag); To include break line in extend() calls
 *  w.extend_wire(boolean flag); Should extend() be extendible?
 *  w.end_break();  Add the action end to the break line
 *  w.turn("CW" or "CCW"); Turns the wire accordingly
 *  w.preclear(boolean flag); Pre-clear on extend and turn
 *  w.transfer(String tt, String dir); Transfers TT to head of wire
 *      dir is "CW" or "CCW" or "" (@head)
 *  w.transfer(String tt);  Same as (tt,"")
 *  w.cond_preclear(String TT,"CW" or "CCW" or "");
 *    Check the truth table TT to see if any C outputs are ever asserted.
 *    If so, then preclear the cell one-spot ahead and in the indicated
 *    direction. This allows live cells to be guarded by an
 *    active guard cell which re-circulates the lvie cell's TT, but
 *    requires the live cell to be initially cleared
 *  w.rotate_transfer(boolean flag);
 *  cell_matrix_channel chan=w.get_channel("PC,CC,BRK");
 *      returns the channel allocated by the wire_build() class
 *
 */

class wire_build
{
  cell_matrix_channel pc,cc,pc_cc,cc_cc; /* We'll always have these */
  cell_matrix_channel brk,brk_cc; /* may not use these... */

  cell_matrix_interface cmi; /* This is passed in via constructor */

  int order=0; /* PC/CC Ordering. 0 means PC north of CC for W->E wire */
  boolean preclear_flag=false; /* Do we preclear on extend/corner? */
  boolean brk_flag=false; /* Set if breakline present */
  boolean brk_extend_flag=false; /* Set to include break line in extend() */
  boolean wire_extend_flag=true; /* extend() builds extendible wire */
  boolean turn_transfer=false;	/* Default is no turn on xfer */

  public void rotate_transfer(boolean f){turn_transfer=f;}

/* Send back a channel to the user */
  public cell_matrix_channel get_channel(String name)
  {
    if (name.toUpperCase().equals("BRK")) return(brk);
    else if (name.toUpperCase().equals("PC")) return(pc);
    else if (name.toUpperCase().equals("CC")) return(cc);
    else{
      System.out.println("Unknown channel: " + name);
      return(null);
    }
  }

/* Here's the constructor */
  public wire_build(cell_matrix_interface cmi_in,String dir_in, int row, int col, String config_in)
  {
    int r1,c1,r2,c2,r3,c3; /* [r,c] of each head cell */
    String input_side; /* Which side do we feed the wire from */

    cmi=cmi_in; /* Need to record the interface */

    cmi.rotate("RESET"); /* Clear out all rotation (?) */

/* Parse the remaining arguments to set up channels and rotations */
    switch (dir_in.toUpperCase().charAt(0)){
      case 'E':input_side="W";
               r1=row;c1=col;r2=row+1;c2=col;
               r3=row+2;c3=col;break; /* No rotation necessary */
      case 'S':input_side="N";cmi.rotate("CW");
               r1=row;c1=col;r2=row;c2=col-1;
               r3=row;c3=col-2;break;
      case 'W':input_side="E";cmi.rotate("CW");cmi.rotate("CW");
               r1=row;c1=col;r2=row-1;c2=col;
               r3=row-2;c3=col;break;
      case 'N':input_side="S";cmi.rotate("CCW");
               r1=row;c1=col;r2=row;c2=col+1;
               r3=row;c3=col+2;break;
      default: System.out.println("ERROR: Illegal direction: " + dir_in);
               return;
    }

/* Parse the configuration string */
    if (config_in.equals("PC,CC")){
      order=0;cmi.flip(false);brk_flag=false;
    } else if (config_in.equals("BRK,PC,CC")){
      order=0;cmi.flip(false);brk_flag=true;
    } else if (config_in.equals("CC,PC")){
      order=1;cmi.flip(true);brk_flag=false;
    } else if (config_in.equals("CC,PC,BRK")){
      order=1;cmi.flip(true);brk_flag=true;
    } else {
      System.out.println("ERROR: Unknown config: " + config_in);
      return;
    }

/* Now assign channels for the PC and the CC */
    if (order==0){ /* PC then CC */
      if (brk_flag){ /* Do break first */
        brk=cmi.assign_channel(r1,c1,"D"+input_side);
//        brk_cc=cmi.assign_channel(r1,c1,"C"+input_side);
        r1=r2;r2=r3;c1=c2;c2=c3; /* Shift channels */
      }
      pc=cmi.assign_channel(r1,c1,"D"+input_side);
      pc_cc=cmi.assign_channel(r1,c1,"C"+input_side);
//      cc_cc=cmi.assign_channel(r2,c2,"C"+input_side);
      cc=cmi.assign_channel(r2,c2,"D"+input_side);
    } else if (order==1){
      cc=cmi.assign_channel(r1,c1,"D"+input_side);
//      cc_cc=cmi.assign_channel(r1,c1,"C"+input_side);
      pc=cmi.assign_channel(r2,c2,"D"+input_side);
      pc_cc=cmi.assign_channel(r2,c2,"C"+input_side);
      if (brk_flag){ /* Do break first */
        brk=cmi.assign_channel(r3,c3,"D"+input_side);
//        brk_cc=cmi.assign_channel(r3,c3,"C"+input_side);
      }
    }
  }

/* Methods to initialize first head cells */
  public void init(String cmd, String TT, String dir)
  {
    if (!cmd.toUpperCase().equals("X")){
      System.out.println("Expecting X as first argument");
      return;
    }

    if (dir.toUpperCase().equals("CW")){
      cmi.push();
      pc_cc.set(true);pc.set("ds=w;cs=1");cmi.cycle();
      pc_cc.set(false);pc.set(TT);cmi.cycle();
      pc_cc.set(true);pc.set(false);cmi.cycle();
      pc_cc.set(false);cmi.cycle();
      cmi.pop();
    } else if (dir.toUpperCase().equals("CCW")){
      cmi.push();
      pc_cc.set(true);pc.set("dn=w;cn=1");cmi.cycle();
      pc_cc.set(false);pc.set(TT);cmi.cycle();
      pc_cc.set(true);pc.set(false);cmi.cycle();
      pc_cc.set(false);cmi.cycle();
      cmi.pop();
    } else if (dir.toUpperCase().equals("")){
      cmi.push();
      pc_cc.set(true);pc.set(TT);cmi.cycle();
      pc_cc.set(false);pc.set(false);cmi.cycle();
      cmi.pop();
    } else {
      System.out.println("Unknown direction:" + dir);
      return;
    }
  }

  public void init()
  {
    if (preclear_flag){
      pc_cc.set(true);
      pc.set("CE=1;");
      cmi.cycle();
      pc_cc.set(false);
      cmi.cycle(); /* This does the preclear */
    }

      pc_cc.set(true);
      pc.set("cs=1;ds=w");
      cmi.cycle();
      pc_cc.set(false);
      pc.set("DW=N;DN=WE~&;DE=WE&");
      cmi.cycle();   /* This configures CC head cell */

      if (brk_flag){
        pc_cc.set(true);
        pc.set("DN=W;CN=1");
        cmi.cycle();
        pc_cc.set(false);
        pc.set("DE=W"); /* Normal break line */
        cmi.cycle();
      }

      pc_cc.set(true);
      pc.set("DW=E;DE=W;DS=1;CE=S");   /* This configures PC head cell */
      cmi.cycle();

/* We're done directly controlling head cells */
    pc_cc.set(false);
    pc.set(false);
    cmi.cycle();
  }

/* Corner init */
  public void init(String dir)
  {
    if (((dir.toUpperCase().equals("CW")) && (order==0)) ||
        ((dir.toUpperCase().equals("CCW")) && (order==1))){
      if (preclear_flag){ /* clear future target cell */
        pc_cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
        pc_cc.set(false);pc.set("cs=1");cmi.cycle(); /* prepare for OR build */
        pc_cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
        pc_cc.set(false);pc.set("ds=sw+;cs=1");cmi.cycle();
        pc_cc.set(true);pc.set("de=w");cmi.cycle();
        pc_cc.set(false);pc.set("cs=1");cmi.cycle(); /* Clear target cell */
      } /* Will actually clear in 2 more steps */

      pc_cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
      pc_cc.set(false);pc.set("cs=1");cmi.cycle(); /* prepare for OR build */
      pc_cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
      pc_cc.set(false);pc.set("ds=sw+;cs=1");cmi.cycle();
      pc_cc.set(true);pc.set("de=w");cmi.cycle();
      pc_cc.set(false);pc.set("cs=w;ds=n;dn=s");cmi.cycle(); /* Finishes remote corner */
      pc_cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
      pc_cc.set(false);pc.set("ds=w;dw=s");cmi.cycle(); /* Finishes outer corner */
      pc_cc.set(true);pc.set("ds=w;cs=1");cmi.cycle();
      pc_cc.set(false);pc.set("ds=sw&;de=s~w&;dw=n");cmi.cycle();
      pc_cc.set(true);pc.set("dw=e;de=w;ds=1");cmi.cycle();
      pc_cc.set(false);pc.set(false);cmi.cycle();
      cmi.rotate(dir.toUpperCase());
    } else if (((dir.toUpperCase().equals("CCW")) && (order==0)) ||
               ((dir.toUpperCase().equals("CW")) && (order==1))){
      if (preclear_flag){
        pc_cc.set(true);pc.set("de=w;ce=1;cn=1");cmi.cycle();
      } else {
        pc_cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
      }
      pc_cc.set(false);pc.set("cs=1");cmi.cycle(); /* Clear corner */
      pc_cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
      pc_cc.set(false);pc.set("ds=sw+;cs=1");cmi.cycle();
      pc_cc.set(true);pc.set("de=w");cmi.cycle();
      pc_cc.set(false);pc.set("dn=w");cmi.cycle(); /* Finishes corner */
      pc_cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
      pc_cc.set(false);pc.set("dn=ns&;dw=n~s&");cmi.cycle();

      pc_cc.set(true);pc.set("ds=w;cs=1");cmi.cycle();
      pc_cc.set(false);pc.set("dw=n;de=w");cmi.cycle();
      pc_cc.set(true);pc.set("dn=w;dw=n;ds=1;cn=e");cmi.cycle();
      pc_cc.set(false);pc.set(false);cmi.cycle();
      cmi.rotate(dir.toUpperCase());
    } else System.out.println("ERROR: Unknown direction:" + dir);
  }

  public void preclear(boolean flag){preclear_flag=flag;}

  public void extend(int n)
  {
    int i;
    for (i=0;i<n;i++) extend();
  }

  public void extend_break(boolean f){brk_extend_flag=f;}
  public void extend_wire(boolean f){wire_extend_flag=f;}

  public void end_break()
  {
    cc.set(true);
    pc.set("dn=w;cn=1");
    cmi.cycle();
    cc.set(false);
    pc.set("cs=w");
    cmi.cycle();
    cc.set(true);pc.set(false);cmi.cycle();
    cc.set(false);cmi.cycle();
  }

  public void extend()
  {
    if (brk_extend_flag){
      cc.set(true);
      pc.set("dn=w;cn=1");
      cmi.cycle();
      cc.set(false);
      pc.set("de=w");
      cmi.cycle();
    }  /* That's the break line */

    cc.set(true);
    if (preclear_flag){
      pc.set("DS=W;CS=1;CE=1"); /* Includes pre-clear */
    } else {
      pc.set("DS=W;CS=1"); /* No pre-clear! */
    }
    cmi.cycle();

    cc.set(false);
    if (wire_extend_flag) {
      pc.set("DW=N;DN=WE~&;DE=WE&");
    } else { /* Build non-extendible head cells */
      pc.set("dn=w;dw=n");
    }
    cmi.cycle();  /* New CC head cell */

    cc.set(true);
    pc.set("DW=E;DE=W;DS=1;CE=S");
    cmi.cycle();  /* New PC head cell */

    cc.set(false);pc.set(false);cmi.cycle();
  }

/* Method to turn a corner... */
/* Exclude BRK line for now */
  public void turn(String dir)
  {
/* If order==1, then flip() has been called, so this does CCW */
    if (((dir.toUpperCase().equals("CW")) && (order==0)) ||
        ((dir.toUpperCase().equals("CCW")) && (order==1))){
      if (preclear_flag){ /* clear future target cell */
        cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
        cc.set(false);pc.set("cs=1");cmi.cycle(); /* prepare for OR build */
        cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
        cc.set(false);pc.set("ds=sw+;cs=1");cmi.cycle();
        cc.set(true);pc.set("de=w");cmi.cycle();
        cc.set(false);pc.set("cs=1");cmi.cycle(); /* Clear target cell */
      } /* Will actually clear in 2 more steps */

      cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
      cc.set(false);pc.set("cs=1");cmi.cycle(); /* prepare for OR build */
      cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
      cc.set(false);pc.set("ds=sw+;cs=1");cmi.cycle();
      cc.set(true);pc.set("de=w");cmi.cycle();
      cc.set(false);pc.set("cs=w;ds=n;dn=s");cmi.cycle(); /* Finishes remote corner */
      cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
      cc.set(false);pc.set("ds=w;dw=s");cmi.cycle(); /* Finishes outer corner */
      cc.set(true);pc.set("ds=w;cs=1");cmi.cycle();

      if (wire_extend_flag){
        cc.set(false);pc.set("ds=sw&;de=s~w&;dw=n");cmi.cycle();
      } else {
        cc.set(false);pc.set("de=w;dw=n");cmi.cycle();
      }

      cc.set(true);pc.set("dw=e;de=w;ds=1");cmi.cycle();
      cc.set(false);pc.set(false);cmi.cycle();
      cmi.rotate(dir.toUpperCase());
    } else if (((dir.toUpperCase().equals("CCW")) && (order==0)) ||
               ((dir.toUpperCase().equals("CW")) && (order==1))){
      if (preclear_flag){
        cc.set(true);pc.set("de=w;ce=1;cn=1");cmi.cycle();
      } else {
        cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
      }
      cc.set(false);pc.set("cs=1");cmi.cycle(); /* Clear corner */
      cc.set(true);pc.set("de=w;ce=1");cmi.cycle();
      cc.set(false);pc.set("ds=sw+;cs=1");cmi.cycle();
      cc.set(true);pc.set("de=w");cmi.cycle();
      cc.set(false);pc.set("dn=w");cmi.cycle(); /* Finishes corner */
      cc.set(true);pc.set("de=w;ce=1");cmi.cycle();

      if (wire_extend_flag){
        cc.set(false);pc.set("dn=ns&;dw=n~s&");cmi.cycle();
      } else {
        cc.set(false);pc.set("dw=s");cmi.cycle();
      }

      cc.set(true);pc.set("ds=w;cs=1");cmi.cycle();
      cc.set(false);pc.set("dw=n;de=w");cmi.cycle();
      cc.set(true);pc.set("dn=w;dw=n;ds=1;cn=e");cmi.cycle();
      cc.set(false);pc.set(false);cmi.cycle();
      cmi.rotate(dir.toUpperCase());
    } else System.out.println("ERROR: Unknown direction:" + dir);
  }

/* Method for conditional pre-clearing */
  public void cond_preclear(String TT, String dir)
  {
// See if TT has any C outputs
    int i,C;
    C=0;
    for (i=0;i<31;i+=2){
      if (!TT.substring(i,i+1).equals("0")) C=1;
    }
    if (C==0) return;

// Now we want to pre-clear
cmi.comment("Pre-clearing to " + dir + " direction");
    pc.set("de=w;ce=1");cc.set(true);cmi.cycle();

    if (dir.toUpperCase().equals("CW")) pc.set("cs=1");
    else if (dir.toUpperCase().equals("CCW")) pc.set("cn=1");
    else if (dir.toUpperCase().equals("")) pc.set("ce=1");
    else System.out.println("ERROR: Unknown direction:" + dir);
    cc.set(false);cmi.cycle();

    pc.set(false);cc.set(true);cmi.cycle();

    cc.set(false);cmi.cycle();

    return;
}


/* Method to transfer a truth table to a cell near the target cell */
  public void transfer(String TT)
  {
    transfer(TT,"");
  }

  public void transfer(String TT, String dir)
  {
    cmi.push(); /* To preserve nsflip flag */
    cmi.flip(false); /* Don't flip N/S for now */
    if (dir.toUpperCase().equals("CW")){ /* Copy to nominal South */
      cc.set(true);pc.set("ds=w;cs=1");cmi.cycle();
      cmi.push();if (!turn_transfer) cmi.rotate("RESET");
      cc.set(false);pc.set(TT);cmi.cycle();
      cmi.pop();
      cc.set(true);pc.set(false);cmi.cycle();
      cc.set(false);cmi.cycle();
    } else if (dir.toUpperCase().equals("CCW")){ /* Nominal North */
      cc.set(true);pc.set("dn=w;cn=1");cmi.cycle();
      cmi.push();if (!turn_transfer) cmi.rotate("RESET");
      cc.set(false);pc.set(TT);cmi.cycle();
      cmi.pop();
      cc.set(true);pc.set(false);cmi.cycle();
      cc.set(false);cmi.cycle();
    } else if (dir.equals("")){ /* copy @ head */
      cmi.push();if (!turn_transfer) cmi.rotate("RESET");
      cc.set(true);pc.set(TT);cmi.cycle();
      cmi.pop();
      cc.set(false);pc.set(false);cmi.cycle();
    } else System.out.println("ERROR: Unknown transfer direction:" + dir);

    cmi.pop(); /* Restore old nsflip flag */
  }

}
