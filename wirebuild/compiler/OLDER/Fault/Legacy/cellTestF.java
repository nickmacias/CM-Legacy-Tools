/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

/* this is a first shot at a method for testing the cells inside a supercell for faults. */

class cellTestF{

// these two are static only so that they can be used by the public static void main()
  static cell_matrix_channel PC,CC,BRK,COMPARE,GO;
  static String zeros="00000000000000000000000000000000";
  static String Pin_0 = "DW=N;DN=WE~&;DE=WE&";	// I did not test the case where Pin_0 has any control lines set.
  static String Pout_0 = Pin_0;
                                //0101 repeated 32 times, encoded in hex
  static String Pin_1="55555555555555555555555555555555";
                                 //1010 repeated 32 times, encoded in hex
  static String Pout_1="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

  public static void main(String args[]) {
    
    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("faultFinder.seq",msg);
    
    // using knowledge external to this file to set these numbers: see fflogic.com and the width of the other supercell replicated there. 
    int startCol = 0; // physical position on grid edge where outside world will hook up to this sequence. Should correspond with position in fflogic.com, digit after the colon (e.g. 0 in "L[9:0]fffail~1.bin")
    int startRow = 9; // physical position on grid edge where outside world will hook up to this sequence. Should correspond with position in fflogic.com, first digit after L (e.g. 9 in "L[9:0]fffail~1.bin")
    // region of grid representing the neighboring supercell on the left is 4 cells wide.
        
/* Start a wire. The code I'm calling assumes wires start from the middle of the supercell. */
    wire_build w=new wire_build(cmi,"e",startRow, startCol,"BRK,PC,CC");
    BRK=w.get_channel("brk"); /* Access wire builder class' break line */
    PC=w.get_channel("pc"); /* Access wire builder class' break line */
    CC=w.get_channel("cc"); /* Access wire builder class' break line */
    COMPARE=cmi.assign_channel(startRow+3,startCol,"DW");
    GO=cmi.assign_channel(startRow+4,startCol,"DW");

    w.preclear(true); 
    
/* SuperSequence - traverse the columns of the supercell, testing cells at the head of the wire
    at each wire extension.*/

    // see picture below. Dimensions of supercell.
    int m = 40;    // columns
    int n = 40;     // rows
    int i = 24;       // position along one side where the BRK line feeds in
    int j = 25;       // position along one side where the PC line feeds in
    int k = 26;      // position along one side where the CC line feeds in
//           (0,0)                                   (0, m)
//               --------------------------------
//              |                                          |
//              |                                          |
//              |                                          |
//   BRK  i  |                                          |
//   PC    j  |                                          |
//   CC   k  |                                          |
//              |                                          |
//              |                                          |
//              |                                          |
//               --------------------------------
//           (n,0)                                   (n, m)
    /* wire orientation (assuming it is being originated from the left side of the cell) is:
      BRK
      PC
      CC
      */

    // iterators for looping
    int currentTestRow = 0;
    int it = 0;  // stands for column or row
    int itt=0; // stands for column or row
    int d = 0;
   

// FIRST, TEST THE INITIAL pc AND cc CELLS AT THE ORIGIN OF THE WIRE.

  // subsequence #1: test just the initial PC and CC cell, in column 0
      cmi.push(); // base rotation to return to after this. 
      // set up break line as close to the bone as possible.
      w.extend_break(false); 
      w.end_break();
      cmi.comment("test the initial PC cell and initial CC cell. Can't test initial break cell: need it to break the wire!");
      testCellsAtHeadOfWire(cmi, w, j, 0, false, true, true);
      cmi.pop();  // back to base rotation
      breakWire(cmi); //w.extend(); // need to recreate wire after break, hence the extend().

// DOWNWARD SEQUENCES. DO THIS COLUMN 0 ONE FIRST, BEFORE the CALL TO setUpBreakLine()
  
  // subsequence #2: run a wire down vertically, in column 0. 
      cmi.push(); // base rotation to return to after this. 
      cmi.comment("column 0, downward. Testing the edge cells on the border of the supercell");
      // set up break line as close to the bone as possible.
      w.extend_break(false); 
      w.end_break();
      w.turn("CW");  // go down
      for (it = k+1; it < n; it++)  // go up from bend in wire to 1 cell from the edge of the cell.
          { 
               w.rotate_transfer(true);
               testCellsAtHeadOfWire(cmi, w, it, 0, true, true, true);
               w.rotate_transfer(false);
               w.extend();  
         }
      // test cells at row 0. Uses a subset of the tests.
      w.rotate_transfer(true);
      testCellsAtHeadOfWire(cmi, w, it, 0, true, false, false);
      w.rotate_transfer(false);
      //
      cmi.pop();  // back to base rotation
      breakWire(cmi); //w.extend(); // need to recreate wire after break, hence the extend().


//------------------------------------
// THE REST OF THE TESTS NEED THE SAME BREAK LINE IMPLEMENTATION, EMBEDDED IN setUpBreakLine().
//    cmi.push(); // establish base rotation. I'm not sure whether this is needed, but Nick does it.
    setUpBreakLine(cmi, w);


// HORIZONTAL SEQUENCE: TEST THE MOST-OFTEN USED CELLS ONLY ONCE, FIRST-OFF!

// subsequence #1: run a wire straight out horizontally, testing all the cells along the way. Do this because it covers the initial cells used in a wire, and it doesn't get covered in the test below.
    for (it = 1; it <= (m-1); it++) // test starts at column 1, hence it = 1. 
        { // run a wire straight out horizontally, testing all the cells along the way.
           testCellsAtHeadOfWire(cmi, w, j, it, true, true, true);
           w.extend(); cmi.comment("extended wire...");
          }  // end for c
    // at column m, edge of supercell. Use a subset of the tests.      
    testCellsAtHeadOfWire(cmi, w, j, it, true, false, false);
    //
    breakWire(cmi); w.extend();  // need to recreate wire after break, hence the extend().

// DOWNWARD SEQUENCES
  
  // subsequence #2: run a wire down vertically, in column 0. 
      cmi.push(); // base rotation to return to after this. 
      cmi.comment("column 0, downward. Testing the edge cells on the border of the supercell");
      w.extend(); // align so that it'll end up in column 1 after all the turns
      w.turn("CW");  // go down
      w.turn("CW"); // double back
      w.turn("CCW"); // in the right position now.
      cmi.comment("beginning edge test, column 0, downward.");
      for (it = k+3; it < n; it++)  // go up from bend in wire to 1 cell from the edge of the cell.
          { 
               w.rotate_transfer(true);
               testCellsAtHeadOfWire(cmi, w, it, 0, true, true, false);
               w.rotate_transfer(false);
               w.extend();  
         }
      // test cells at row 0. Uses a subset of the tests.
      w.rotate_transfer(true);
      testCellsAtHeadOfWire(cmi, w, it, 0, true, false, false);
      w.rotate_transfer(false);
      //
      cmi.pop();  // back to base rotation
      breakWire(cmi); w.extend(); // need to recreate wire after break, hence the extend().


   // subsequence #3: run a wire straight down vertically, column 1, testing all the cells along the way. After this, those cells can be assumed to be good for future wire building.
      cmi.push(); // base rotation to return to after this
      cmi.comment("column 1, downward testing");
      w.turn("CW");  // go down
      for (it = k+1; it < n; it++)  // go from row one below the CC row to bottom of supercell.
          { 
           w.rotate_transfer(true);
           testCellsAtHeadOfWire(cmi, w, it, 2, true, true, false);
           w.rotate_transfer(false);
           w.extend();	 // cmi.comment("extended wire...");
         }
      // the edge row (row n) gets tested differently. Use a subset of the tests.
      w.rotate_transfer(true);
      testCellsAtHeadOfWire(cmi, w, it, 2, true, false, false);
      w.rotate_transfer(false);
      //     
      cmi.pop(); 
      breakWire(cmi); w.extend(); // need to recreate wire after break, hence the extend().


// subsequence #4: column 2 to m-2: For each, run a wire straight down vertically, testing all the cells along the way. After this, those cells can be assumed to be good for future wire building.
    for (itt = 2, d=1; itt <= m-2; itt++, d++)
      {
      cmi.push(); // base rotation to return to after this. 
      w.extend(d);
      String s = "column " + itt + ", downward testing";
      cmi.comment(s);
      w.turn("CW");  // go down
      for (it = k+1; it < n; it++)  // go from row one below the CC row to bottom of supercell.
          { 
           w.rotate_transfer(true);
           testCellsAtHeadOfWire(cmi, w, it, itt, true, true, false);
           w.rotate_transfer(false);
           w.extend();	 // cmi.comment("extended wire...");
          }
      // at row 0. Use a subset of the tests.
      w.rotate_transfer(true);
      testCellsAtHeadOfWire(cmi, w, it, itt, true, false, false);
      w.rotate_transfer(false);
      //
      cmi.pop();  // back to base rotation
      breakWire(cmi); w.extend(); // need to recreate wire after break, hence the extend().
      } // end columns 3 to m-1


// subsequence #5: column m, testing downward. Skip BRK test. 
      cmi.push(); // base rotation to return to after this. 
      w.extend(m-1-1); //  subtract one for the column 0 that the head of the wire takes up; subtract another one because the turn will advance the wire by two more columns in itself. 
      String ss = "column " + m + ", downward testing";
      cmi.comment(ss);
      w.turn("CW");  // go down
      for (it = k+1; it < n; it++)  // go from row one below the CC row to bottom of supercell.
          { 
           w.rotate_transfer(true);
           testCellsAtHeadOfWire(cmi, w, it, itt, false, true, false); // don't test BRK cell, since that would override the edge of the supercell.
           w.rotate_transfer(false);
           w.extend();	 // cmi.comment("extended wire...");
          }
      // at row 0. Use a subset of the tests.
      w.rotate_transfer(true);
      testCellsAtHeadOfWire(cmi, w, it, itt, false, false, false);
      w.rotate_transfer(false);
      //
      cmi.pop();  // back to base rotation
      breakWire(cmi); w.extend(); // need to recreate wire after break, hence the extend().
      

// UPWARD SEQUENCES
 

// subsequence #6: run a wire up vertically, in column 0, testing all the cells along the way. After this, those cells can be assumed to be good for future wire building.
      cmi.push(); // base rotation to return to after this. 
      cmi.comment("column 1, testing upward.");
      w.extend();  // go out the minimal amount needed to double back to column 1.
      w.turn("CCW");  // go up
      w.extend(); // go up one row to pass the BRK line.
      w.turn("CCW"); // double back
      w.turn("CW"); // in the right position now.
      cmi.comment("beginning to test column 1 upward.");
      for (it = i-3; it >= 1; it--)  // go up from the bend in the wire to the top edge of the supercell.
          { 
           w.rotate_transfer(true);
           testCellsAtHeadOfWire(cmi, w, it, 1, false, true, false);  // skip the BRk tests, since the BRK cells would fall outside the supercell.
           w.rotate_transfer(false);
           w.extend();  
           }
      // at row 0: 
      w.rotate_transfer(true);
      testCellsAtHeadOfWire(cmi, w, it, 1, false, false, false);
      w.rotate_transfer(false);
      //
      cmi.pop();  // back to base rotation
      breakWire(cmi); w.extend(); // need to recreate wire after break, hence the extend().


// subsequence #7: column 1. run a wire straight up vertically, testing all the cells along the way. After this, those cells can be assumed to be good for future wire building.
      cmi.push(); // base rotation to return to after this. 
      cmi.comment("column 2, upward testing. Preclearing one critical cell before turning upward.");
      PC.set("DE=W; CE=1");  // gain control of cell one past head cell.
      CC.set(true);
      cmi.cycle();
      PC.set("CN=1");
      CC.set(false);
      cmi.cycle();
      cmi.comment("turning upward...");   
      w.turn("CCW");  // go up
      for (it = i; it >= 1; it--)  // go up from row i to 0 i(i = BRK row)
          { 
          if (it == i)  // don't clobber the cell that breaks the wire, in BRK row.
               {
               w.rotate_transfer(true);
               testCellsAtHeadOfWire(cmi, w, it, 2, false, true, false);
               w.rotate_transfer(false);
               }
          else  // run BRK test as well.
               {
              	w.rotate_transfer(true);
               testCellsAtHeadOfWire(cmi, w, it, 2, true, true, false);
               w.rotate_transfer(false);
               }
           w.extend();	 // cmi.comment("extended wire...");
          }  
                 
      // test edge cells differently, at row 0
      w.rotate_transfer(true);
      testCellsAtHeadOfWire(cmi, w, it, 2, true, false, false);
      w.rotate_transfer(false);
      //
      cmi.pop();  // back to base rotation
      breakWire(cmi); w.extend(); // need to recreate wire after break, hence the extend().


// subsequence #8: column 2 to m-1: For each, run a wire straight up vertically, testing all the cells along the way. After this, those cells can be assumed to be good for future wire building.
   for (itt = 2; itt <= m-1; itt++)
      {
      cmi.push(); // base rotation to return to after this. 
      w.extend(itt-1);
      String s = "column " + itt + ", upward testing";
      cmi.comment(s);
      w.turn("CCW");  // go up
      for (it = i; it > 0; it--)  // go up from row i to 0 i(i = BRK row)
          { 
           w.rotate_transfer(true);
           if (itt == m-1) // do special extra CC tests
              testCellsAtHeadOfWire(cmi, w, it, itt, true, true, true);
           else
              testCellsAtHeadOfWire(cmi, w, it, itt, true, true, false);
           w.rotate_transfer(false);
           w.extend();	 // cmi.comment("extended wire...");
          }
      // at row 0. Use a subset of the tests.
      w.rotate_transfer(true);
      testCellsAtHeadOfWire(cmi, w, it, itt, true, false, false);
      w.rotate_transfer(false);
      //
      cmi.pop();  // back to base rotation
      breakWire(cmi); w.extend(); // need to recreate wire after break, hence the extend().
      } // end columns 3 to m-1

  


// End SuperSequence 
    cmi.comment("done testing Supercell");
//    cmi.pop();
    cmi.close();
   
    } // end main

public static void breakWire(cell_matrix_interface cmi)
    {
     // code segment 3.
     BRK.set(true);cmi.cycle();BRK.set(false);  /* Go back to corner */
     cmi.comment("Just broke wire. ");
     // end code segment 3.
    } // end breakWire

public static void setUpBreakLine(cell_matrix_interface cmi, wire_build w)
{  // extend wire one column and put the action end on the break line
       // code segment 1. 
       w.extend_break(false);  // going to always break wire at head, since that's the way wire_build.java class works best right now.
       w.end_break();  // causes BRK line to do something useful
       w.extend(); // need to go forward by one so that the break line will work. This may not actually be necessary: see sequence 1, where somehow the wire manages to break despite the fact that there is no wire beneath it.
       cmi.comment("had to extend wire one column outward so that the break line will operate");
       // end code segment 1.
} // end setUpBreakLine

public static void testCellsAtHeadOfWire(cell_matrix_interface cmi, wire_build w, int row, int column, boolean testBRK, boolean testCC4, boolean testCC5) 
// test the 3 cells at the head of the current wire. 
// row and column are used just to record which cells have undergone which tests.
// they are the coordinates of the main cell, the one occupying the PC-to-be position.
       {
      // code segment 2. 
      // LATER: try more patterns for both C-mode and D-mode tests. 
      // PC-cell-to-be: test the cell directly right of the PC head cell
      PCTest1(cmi, Pin_0, Pout_0);      // test 1: make sure truth tables load in properly
      PCTest2(cmi, Pin_1, Pout_1, w);   //test 2: make sure data in and data out lines work properly

      if (testBRK)  // false for testing the cells at the edge of the supercell
         {
          // BRK-cell-to-be: test the cell to the right and above the PC head cell, the to-be-BRK cell
          BRKTest1(cmi, Pin_0, Pout_0, w);      // test 1: make sure truth tables load in properly
          BRKTest2(cmi, Pin_1, Pout_1, w);      //test 2: make sure data in and data out lines work properly
          if (testCC4) // this will be false if the cells being tested are on an edge of the supercell
             {
              BRKTest3(cmi, Pin_0, Pout_0);          //test3: make sure truth tables load in properly from the side of the cell opposite the wire, here the East side of the cell.
              BRKTest4(cmi, Pin_1, Pout_1, w);      //test4: make sure data in and data out lines work properly from the side of the cell opposite the wire, here the East side of the cell.
              }
          }
        
      // CC-cell-to-be: test the cell to the right and below the PC head cell, the to-be-CC cell
      CCTest1(cmi, Pin_0, Pout_0, w);       // test 1: make sure truth tables load in properly
      CCTest2(cmi, Pin_1, Pout_1, w);       //test 2: make sure data in and data out lines work properly
      if (testCC4) // this will be false if the cells being tested are on an edge of the supercell
         {
         CCTest3(cmi, Pin_0, Pout_0);          //test3: make sure truth tables load in properly from the side of the cell opposite the wire, here the East side of the cell.
         CCTest4(cmi, Pin_1, Pout_1, w);      //test4: make sure data in and data out lines work properly from the side of the cell opposite the wire, here the East side of the cell.
         }
      // end code segment 2.
      if (testCC5) // true only for those cells I can't get to on a few edges of the supercell.
         {
         CCTest5(cmi, Pin_0, Pout_0, w);      //test3: make sure truth tables load in properly from the wire-side of the cell, here the West side. 
         CCTest6(cmi, Pin_1, Pout_1, w);      //test4: make sure data in and data out lines work properly from wire-side of the cell, here the West side. 
         }
 } // end testCellsAtHeadOfWire        
         
     
public static void PCTest1(cell_matrix_interface cmi, String Pin, String Pout) 
// test 1: make sure truth tables load in properly
     {
      // test 1: make sure truth tables load in properly
      CC.set(true);
      PC.set(Pin);
      cmi.cycle();
      cmi.comment("Beginning PCTest1, C-mode test of next PC cell from West side of cell");
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      PC.set(zeros);
      cmi.cycle();
      CC.set(false);  // not sure why I don't set it false earlier in the test. This test used to have no CC.set(false) at all. 
      //cmi.comment("finished PCTest1 and cleanup");
      } // end PCTest1

public static void PCTest2(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test 2: make sure data in and data out lines work properly
      w.transfer("DW=W~");  // feedback loop that inverts the input
      cmi.comment("Beginning PCTest2, D-mode test of next PC cell from West side of cell");
      PC.set(Pin);
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      w.transfer(zeros); // blank out PC cell
      //cmi.comment("finished PCTest2 and cleanup");
      } // end PCTest2

public static void BRKTest1(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      // test 1: make sure truth tables load in properly
      cmi.comment("beginning BRKTest1, C-mode test for next BRK cell from South side of cell");
      w.transfer("CN=1;DN=W;DW=N"); // redirect stuff up to this cell
      PC.set(Pin);
      cmi.cycle();
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      PC.set(zeros);  // blank out BRK cell
      cmi.cycle();  
      w.transfer(zeros); // clean up PC cell
      //cmi.comment("finished BRKTest1 and cleanup");
      // LATER: try more patterns
      } // end BRKTest1

public static void BRKTest2(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test 2: make sure data in and data out lines work properly
      cmi.comment("beginning BRKTest2, D-mode test for next BRK cell from South side of cell");
      w.transfer("DS=S~", "CCW");  // feedback loop that inverts the input
      w.transfer("DW=N;DN=W"); // reroute the output to BRK cell and back 
      PC.set(Pin);
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      w.transfer(zeros, "CCW");  // clear out BRK cell
      w.transfer(zeros);  // clear out PC cell
      //cmi.comment("finished BRKTest2 and cleanup");
      } // end BRKTest2

/*
*              ______________________
*              |                  |                    |
*  (BRK)   |  cell 2        |    cell 5        |
*              |                  |                    |
*              --------------- ------------------
*              |                  |                    |
*  PC ->   |  cell 1        |    cell 4        |
*              |                  |                    |
*              --------------- ------------------
*              |                  |                    |
*  CC ->   |  cell 3        |    cell 6        |
*              |                  |                    |
*              --------------- ------------------
*   
*
*
*/
public static void BRKTest3(cell_matrix_interface cmi, String Pin, String Pout) 
      {
      //test3: make sure truth tables load in properly from the side of the cell opposite the wire, here the East side of the cell.
 
 // gain access to and clear out cell 5. This is necessary before allowing an OR operation in step 4.
       CC.set(true);                           //pre-step1: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
       cmi.comment("Beginning BRKTest3, to test C-mode from East side of next BRK cell");
//      cmi.comment("pre-step1: configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //pre-step2: configure cell 4 so that it can configure cell 5
      PC.set("DN=W;CN=1"); 
//      cmi.comment("pre-step2: configure cell 4 in order to configure cell 5 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //pre-step3: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("pre-step3: remove c=1 in cell 1");
      cmi.cycle(); 
      CC.set(false);                          //pre-step4: configure cell 5 to configure cell 2 and pass truth table to/from cell 2.
      PC.set(zeros);
//      cmi.comment("pre-step4: configure cell 5 to control cell 2 and pass a truth table to and from cell 2");
      cmi.cycle();

// NOW I can begin the BRK test sequence. 
      CC.set(true);                           //step1: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
 //      cmi.comment("Beginning BRKTest3 for real now.");
//      cmi.comment("step1: configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //step2 & 3/4: configure cell 4 so that it can configure cell 5
      PC.set("DN=WN+;CN=1"); 
//      cmi.comment("step2 & 3/4: configure cell 4 in order to configure cell 5 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step3: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("step3: remove c=1 in cell 1");
      cmi.cycle(); 
      CC.set(false);                          //step4: configure cell 5 to configure cell 2 and pass truth table to/from cell 2.
      PC.set("DW=S;DS=W;CW=1");
//      cmi.comment("step4: configure cell 5 to control cell 2 and pass a truth table to and from cell 2");
      cmi.cycle();
      CC.set(true);                           //step5: regain control of cell 4
      PC.set("DE=W;CE=1"); 
 //      cmi.comment("step5: regain control of cell 4");
      cmi.cycle();
      CC.set(false);                         //step6: give cell 4 its final configuration: it passes data to/from cell 5.
      PC.set("DN=W;DW=N");
//      cmi.comment("step6: give cell 4 its final configuration, passing data to and from cell 5");
      cmi.cycle();
      CC.set(true);                          //step7: reconfigure cell 1 to remove C=1, and to pass the output of the feedback loop to the bidirectional PC line for comparison.
      PC.set("DE=W;DW=E");
 //      cmi.comment("step7: give cell 1 its final configuration");
      cmi.cycle();
      CC.set(false);                         //step8: set cc line low, then use the PC line to drive the feedback loop and to test it.
      PC.set(Pin);                        
      cmi.cycle(); // program cell 2
      COMPARE.set(Pout);          // compare result to inverted input pattern (held in Pout_1)
      GO.set(true);
//      cmi.comment("step8: pass in the Pin_0, truth table config. test, to cell 2 via cells 1, 4, and 5");
      cmi.cycle();
      GO.set(false);
      //cmi.comment("finished BRKTest3. Beginning cleanup.");
      // steps 9 - 15 undo the C=1 line in cell 5.
      PC.set(zeros);                          // step 8 and a half: blank out cell 2
 //      cmi.comment("step 8 and a half: cleanup: use connection to cell 2 to blank it out");
      cmi.cycle();
      CC.set(true);                           //step9: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
//      cmi.comment("step9: cleanup: configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //step10: configure cell 4 so that it can configure cell 5
      PC.set("DN=W;CN=1"); 
//      cmi.comment("step10: cleanup: configure cell 4 in order to configure cell 5 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step11: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("step11: cleanup: remove c=1 in cell 1");
      cmi.cycle(); 
      CC.set(false);                          //step12: configure cell 5 to configure cell 2 and pass truth table to/from cell 2.
      PC.set(zeros);
//      cmi.comment("step12: cleanup: clear cell 5");
      cmi.cycle();
      CC.set(true);                           //step13: regain control of cell 4
      PC.set("DE=W;CE=1"); 
 //      cmi.comment("step12: cleanup: regain control of cell 4");
      cmi.cycle();
      CC.set(false);                         //step13: give cell 4 its final configuration: it passes data to/from cell 5.
      PC.set(zeros);
//      cmi.comment("step13: cleanup: clear cell 4");
      cmi.cycle();
      CC.set(true);                           //step13: regain control of cell 1
      PC.set(zeros);                          // clear out cell 1
//      cmi.comment("step14: cleanup: clear cell 1");
      cmi.cycle();
      //cmi.comment("finished cleanup from BRKTest3");
     // end undo
     } // end BRKTest3


public static void BRKTest4(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test4: make sure data in and data out lines work properly from the side of the cell opposite the wire, here the East side of the cell.
      w.transfer("DE=E~", "CCW");  // feedback loop that inverts the input
 
 // gain access to and clear out cell 5. This is necessary before allowing an OR operation in step 4.
      CC.set(true);                           //step3: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
      cmi.comment("beginning BRKTest4, to test D-mode of East side of next BRK cell");
//      cmi.comment("pre-step3: set up feedback loop in target cell, configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //step4: configure cell 4 so that it can configure cell 5
      PC.set("DN=W;CN=1"); 
 //      cmi.comment("pre-step4: configure cell 4 in order to configure cell 5 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step5: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
 //      cmi.comment("pre-step5: remove c=1 in cell 1");
      cmi.cycle(); 
      CC.set(false);                          //step6: clear cell 5.
      PC.set(zeros);
//      cmi.comment("pre-step6: clear cell 5");
      cmi.cycle();
 
// NOW I can begin the BRK test sequence. 
      CC.set(true);                           //step3: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
      // cmi.comment("beginning BRKTest4, to test D-mode of East side of next BRK cell");
//      cmi.comment("step3: set up feedback loop in target cell, configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //step4: configure cell 4 so that it can configure cell 5
      PC.set("DN=WN+;CN=1"); 
 //      cmi.comment("step4: configure cell 4 in order to configure cell 5 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step5: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
 //      cmi.comment("step5: remove c=1 in cell 1");
      cmi.cycle(); 
      CC.set(false);                          //step6: configure cell 5 to pass PC data to cell 2's inverted feedback loop.
      PC.set("DW=S;DS=W");
//      cmi.comment("step6: configure cell 5 to pass PC data to and from cell 2's feedback loop");
      cmi.cycle();
      CC.set(true);                           //step7: regain control of cell 4
      PC.set("DE=W;CE=1"); 
//      cmi.comment("step7: regain control of cell 4");
      cmi.cycle();
      CC.set(false);                         //step8: give cell 4 its final configuration: it passes data to/from cell 5.
      PC.set("DN=W;DW=N");
//      cmi.comment("step8: give cell 4 its final configuration, passing data to and from cell 5");
      cmi.cycle();
      CC.set(true);                          //step9: reconfigure cell 1 to remove C=1, and to pass the output of the feedback loop to the bidirectional PC line for comparison.
      PC.set("DE=W;DW=E");
//      cmi.comment("step9: give cell 1 its final configuration");
      cmi.cycle();
      CC.set(false);                         //step10: set cc line low, then use the PC line to drive the feedback loop and to test it.
      PC.set(Pin);                        
      COMPARE.set(Pout);          // compare result to inverted input pattern (held in Pout_1)
      GO.set(true);
//      cmi.comment("step10: pass in the Pin_1, feedback loop test, to cell 2 via cells 1, 4, and 5");
      cmi.cycle();
      GO.set(false);
      //cmi.comment("finished BRKTest4");
      } // end BRKTest4


public static void CCTest1(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      // test 1: make sure truth tables load in properly
      w.transfer("CS=1;DS=W;DW=S"); // redirect stuff down to this cell
      PC.set(Pin);
      cmi.comment("beginning CCTest1, to test C-mode for the next CC line cell from North side of cell"); 
      cmi.cycle();
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      PC.set(zeros); // clear out CC cell so that this cell can't affect any other tests down the road
      cmi.cycle();
      w.transfer(zeros); // clear out PC cell so that this cell can't affect any other tests down the road
      //cmi.comment("finished CCTest1 and cleanup");
      } // end CCTest1 


public static void CCTest2(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test 2: make sure data in and data out lines work properly
      cmi.comment("beginning CCTest2, to test D-mode for the next CC line cell from North side of cell");
      w.transfer("DN=N~", "CW");  // feedback loop that inverts the input
      w.transfer("DW=S;DS=W"); // reroute the output to BRK cell and back 
      PC.set(Pin);
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      w.transfer(zeros, "CW"); // clear out CC cell
      w.transfer(zeros);  // clear out PC cell 
      //cmi.comment("finished CCTest2 and cleanup");
      } // end CCTest2 
 
public static void CCTest3(cell_matrix_interface cmi, String Pin, String Pout) 
      {
      //test3: make sure truth tables load in properly from the side of the cell opposite the wire, here the East side of the cell.


 // gain access to and clear out cell 6. This is necessary before allowing an OR operation in step 4.
      CC.set(true);                           //pre-step1: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
      cmi.comment("Beginning CCTest3, to test C-mode from East side of next CC cell");
//      cmi.comment("pre-step1: configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //pre-step2: configure cell 4 so that it can configure cell 6
      PC.set("DS=W;CS=1"); 
//      cmi.comment("pre-step2: configure cell 4 in order to configure cell 6 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //pre-step3: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("pre-step3: remove c=1 in cell 1");
      cmi.cycle(); 
      CC.set(false);                          //pre-step4: configure cell 6 to configure cell 3 and pass truth table to/from cell 3.
      PC.set(zeros);
//      cmi.comment("pre-step4: configure cell 6 to control cell 3 and pass a truth table to and from cell 3");
      cmi.cycle();


// NOW I can begin the CC test sequence. 
      CC.set(true);                           //step1: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
      // cmi.comment("Beginning CCTest3, to test C-mode from East side of next CC cell");
//      cmi.comment("step1: configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //step2: configure cell 4 so that it can configure cell 6
      PC.set("DS=WS+;CS=1"); 
//      cmi.comment("step2: configure cell 4 in order to configure cell 6 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step3: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("step3: remove c=1 in cell 1");
      cmi.cycle(); 
      CC.set(false);                          //step4: configure cell 6 to configure cell 3 and pass truth table to/from cell 3.
      PC.set("DW=N;DN=W;CW=1");
//      cmi.comment("step4: configure cell 6 to control cell 3 and pass a truth table to and from cell 3");
      cmi.cycle();
      CC.set(true);                           //step5: regain control of cell 4
      PC.set("DE=W;CE=1"); 
 //      cmi.comment("step5: regain control of cell 4");
      cmi.cycle();
      CC.set(false);                         //step6: give cell 4 its final configuration: it passes data to/from cell 6.
      PC.set("DS=W;DW=S");
//      cmi.comment("step6: give cell 4 its final configuration, passing data to and from cell 6");
      cmi.cycle();
      CC.set(true);                          //step7: reconfigure cell 1 to remove C=1, and to pass the output of the feedback loop to the bidirectional PC line for comparison.
      PC.set("DE=W;DW=E");
 //      cmi.comment("step7: give cell 1 its final configuration");
      cmi.cycle();
      CC.set(false);                         //step8: set cc line low, then use the PC line to drive the feedback loop and to test it.
      PC.set(Pin);                        
      cmi.cycle(); // program cell 2
      COMPARE.set(Pout);          // compare result to inverted input pattern (held in Pout_1)
      GO.set(true);
//      cmi.comment("step8: pass in the Pin_0, truth table config. test, to cell 2 via cells 1, 4, and 5");
      //cmi.comment("finishing CCTest3. Beginning cleanup.");
      cmi.cycle();
      GO.set(false);
      // steps 9 - 15 undo the C=1 line in cell 5.
      PC.set(zeros);                          // step 8 and a half: blank out cell 2
 //      cmi.comment("step 8 and a half: cleanup: use connection to cell 2 to blank it out");
      cmi.cycle();
      CC.set(true);                           //step9: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
//      cmi.comment("step9: cleanup: configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //step10: configure cell 4 so that it can configure cell 6
      PC.set("DS=W;CS=1"); 
//      cmi.comment("step10: cleanup: configure cell 4 in order to configure cell 6 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step11: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
//      cmi.comment("step11: cleanup: remove c=1 in cell 1");
      cmi.cycle(); 
      CC.set(false);                          //step12: configure cell 6 to configure cell 3 and pass truth table to/from cell 3.
      PC.set(zeros);
//      cmi.comment("step12: cleanup: clear cell 6");
      cmi.cycle();
      CC.set(true);                           //step13: regain control of cell 4
      PC.set("DE=W;CE=1"); 
 //      cmi.comment("step12: cleanup: regain control of cell 4");
      cmi.cycle();
      CC.set(false);                         //step13: give cell 4 its final configuration: all zeros
      PC.set(zeros);
//      cmi.comment("step13: cleanup: clear cell 4");
      cmi.cycle();
      //cmi.comment("finished cleanup from CCTest3");
      // end undo
     } // end BRKTest3


public static void CCTest4(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test4: make sure data in and data out lines work properly from the side of the cell opposite the wire, here the East side of the cell.
      w.transfer("DE=E~", "CW");  // feedback loop that inverts the input
 
 // gain access to and clear out cell 6. This is necessary before allowing an OR operation in step 4.
       CC.set(true);                           //pre-step3: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
       PC.set("DE=W;CE=1");
       cmi.comment("beginning CCTest4, to test D-mode of East side of next CC cell");
//      cmi.comment("pre-step3: set up feedback loop in target cell, configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //pre-step4: configure cell 4 so that it can configure cell 6
      PC.set("DS=W;CS=1"); 
 //      cmi.comment("pre-step4: configure cell 4 in order to configure cell 6 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //pre-step5: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
 //      cmi.comment("pre-step5: remove c=1 in cell 1");
      cmi.cycle(); 
      CC.set(false);                          //pre-step6: clear out any garbage in cell 6.
      PC.set(zeros);
//      cmi.comment("pre-step6: clearing cell 6 in case it's not already clear");
      cmi.cycle();
 
 
// NOW I can begin the CC test sequence. 
      CC.set(true);                           //step3: configure center cell, cell 1,  to gain access to cell 4 (see notebook)
      PC.set("DE=W;CE=1");
       //cmi.comment("beginning CCTest4, to test D-mode of East side of next CC cell");
//      cmi.comment("step3: set up feedback loop in target cell, configured cell 1 to gain access to cell 4");
      cmi.cycle();
      CC.set(false);                          //step4: configure cell 4 so that it can configure cell 6
      PC.set("DS=WS+;CS=1"); 
 //      cmi.comment("step4: configure cell 4 in order to configure cell 6 (include feedback loop to prevent config string loss in step 7)");
      cmi.cycle();
      CC.set(true);                           //step5: reconfigure cell 1 to remove c=1
      PC.set("DE=W");         
 //      cmi.comment("step5: remove c=1 in cell 1");
      cmi.cycle(); 
      CC.set(false);                          //step6: configure cell 6 to pass PC data to cell 3's inverted feedback loop.
      PC.set("DW=N;DN=W");
//      cmi.comment("step6: configure cell 6 to pass PC data to and from cell 3's feedback loop");
      cmi.cycle();
      CC.set(true);                           //step7: regain control of cell 4
      PC.set("DE=W;CE=1"); 
//      cmi.comment("step7: regain control of cell 4");
      cmi.cycle();
      CC.set(false);                         //step8: give cell 4 its final configuration: it passes data to/from cell 6.
      PC.set("DS=W;DW=S");
//      cmi.comment("step8: give cell 4 its final configuration, passing data to and from cell 6");
      cmi.cycle();
      CC.set(true);                          //step9: reconfigure cell 1 to remove C=1, and to pass the output of the feedback loop to the bidirectional PC line for comparison.
      PC.set("DE=W;DW=E");
//      cmi.comment("step9: give cell 1 its final configuration");
      cmi.cycle();
      CC.set(false);                         //step10: set cc line low, then use the PC line to drive the feedback loop and to test it.
      PC.set(Pin);                        
      COMPARE.set(Pout);          // compare result to inverted input pattern (held in Pout_1)
      GO.set(true);
//      cmi.comment("step10: pass in the Pin_1, feedback loop test, to cell 3 via cells 1, 4, and 6");
      cmi.cycle();
      GO.set(false);
      //cmi.comment("finished CCTest4");
      } // end CCTest4

/*
*
*              --------------- ------------------
*              |                  |                    |
*  PC ->   |  cell 1        |                    |
*              |                  |                    |
*              --------------- ------------------
*              |                  |                    |
*  CC ->   |  cell 2        |    cell 3        |
*              |                  |                    |
*              --------------- ------------------
*   
*
*
*/ 

 public static void CCTest5(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      // test 1: make sure truth tables load in properly
      cmi.comment("EDGE CASE: beginning CCTest5, to test C-mode for the next-next CC line cell from North side of cell"); 
      w.transfer("CS=1;DS=W;DW=S"); // step 1: redirect stuff down to this cell
      PC.set("DE=N;DN=E;CE=1");  // step 2: configure cell 2 to gain control of cell 3.
      cmi.cycle(); 
      w.transfer("DS=W;DW=S");    // step 3: reconfigure cell 1 to remove C=1
      PC.set(Pin);                            // step 4: perform C-mode test
      cmi.cycle();
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      PC.set(zeros); // step 5: clear out target cell, cell 3,  so that this cell can't affect any other tests down the road
      cmi.cycle();
      w.transfer("CS=1;DS=W;DW=S"); // step 6: regain control of cell 2 so I can zero it out.
      PC.set(zeros);  // step 7: clear out cell 2
      cmi.cycle(); 
      w.transfer(zeros); // step 8: clear out PC cell so that this cell can't affect any other tests down the road
      //cmi.comment("finished CCTest5 and cleanup");
      } // end CCTest1 


public static void CCTest6(cell_matrix_interface cmi, String Pin, String Pout, wire_build w) 
      {
      //test 2: make sure data in and data out lines work properly
      // same cell numbers from picture above CCTest5.
      cmi.comment("EDGE CASE: beginning CCTest6, to test D-mode for the next-next CC line cell from North side of cell"); 
      w.transfer("CS=1;DS=W;DW=S"); // step 1: redirect stuff down to this cell
      PC.set("DE=NE+;DN=E;CE=1");  // step 2: configure cell 2 to gain control of cell 3. Need to feed the TT back into cell 3 to avoid it getting lost.
      cmi.cycle(); 
      w.transfer("DS=W;DW=S");    // step 3: reconfigure cell 1 to remove C=1
      PC.set("DW=W~");                 // step 4: configure cell 3 as an inverter
      cmi.cycle();
      w.transfer("CS=1;DS=W;DW=S"); // step 5: regain control of cell 1 so that I can reconfigure cell 2 to remove the c=1.
      PC.set("DE=N;DN=E");           // step 6: reconfigure cell 2 with no c=1 and no OR loop
      cmi.cycle();
      w.transfer("DS=W;DW=S");   // step 7: reconfigure cell 1 with no c=1
   //   cmi.comment("about to test invertor....");
      PC.set(Pin);                            // step 8: perform D-mode test
      cmi.cycle();
      COMPARE.set(Pout);
      GO.set(true);
      cmi.cycle();
      GO.set(false);
      // CLEANUP
      // cmi.comment("CCTest6: beginning cleanup");
      w.transfer("CS=1;DS=W;DW=S"); // step 9: redirect stuff down to this cell
      PC.set("DE=N;DN=E;CE=1");  // step 10: configure cell 2 to gain control of cell 3.
      cmi.cycle(); 
      w.transfer("DS=W;DW=S");    // step 11: reconfigure cell 1 to remove C=1
      PC.set(zeros);                 // step 12: clear out cell 3
      cmi.cycle();
      w.transfer("CS=1;DS=W;DW=S"); // step 6: regain control of cell 2 so I can zero it out.
      PC.set(zeros);  // step 7: clear out cell 2
      cmi.cycle(); 
      w.transfer(zeros); // step 8: clear out PC cell so that this cell can't affect any other tests down the road
      //cmi.comment("finished CCTest6 and cleanup");
 
      } // end CCTest2 

} // end cellTestF class
