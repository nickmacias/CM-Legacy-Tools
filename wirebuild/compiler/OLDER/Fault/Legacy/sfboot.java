/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class sfboot{
  public static void main(String args[]) {
    int i,c,r,n;
    int delta=4; /* Start BREAK (delta) cells down from upper-left corner */
    cell_matrix_channel pc,cc,pc_cc,brk;

    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("sfboot.seq",msg);

    if (args.length != 1){
      System.out.println("Usage: sfboot filename.bin");
      System.exit(1);
    }

/* Read the binary grid file */
    binary_file_reader bfr=new binary_file_reader(args[0]);

/* Start a wire */
    wire_build w=new wire_build(cmi,"e",delta,0,"BRK,PC,CC");
    brk=w.get_channel("brk"); /* Access wire builder class' break line */
    cmi.push();  /* Save current state */
    w.preclear(true);
    w.extend_break(false);

    cmi.comment("Ready to init");
/* For now, do one init() */
    w.init();
    cmi.comment("Init done");

/* and create the initial break cell */
    w.end_break();
    cmi.comment("Break built");

/* SuperSequence 1-Do All except the leftmost 5 columns */

    for (c=bfr.cols()-1;c>=4;c-=50){ /* Do most columns */
      cmi.pop();cmi.push();
      w.extend();w.turn("ccw");w.extend(delta-1);w.turn("cw");
      w.extend(c-5);w.turn("cw");
      for (r=2;r<bfr.rows()-2;r++){
        w.transfer(bfr.readTT(r,c),"ccw");
        w.extend();
      }
      w.transfer(bfr.readTT(bfr.rows()-1,c),"ccw");
      cmi.comment("Most of column done");

      brk.set(true);cmi.cycle();brk.set(false);cmi.pop();cmi.push();
      cmi.comment("Break executed");

      w.extend();w.turn("ccw");w.extend(delta-1);w.turn("cw");
      w.extend(c-4);w.extend_wire(false);w.extend();
/* Make head cells non-extendible, so cells we're
   building don't interfere with new wire */
      cmi.comment("Ready to transfer last 3 cells");
      w.transfer(bfr.readTT(0,c),"ccw");
      cmi.comment("CCW Done");
      w.transfer(bfr.readTT(2,c),"cw");
      cmi.comment("CW Done");
      w.transfer(bfr.readTT(1,c),"");
      cmi.comment("All done");
      w.extend_wire(true);

      brk.set(true);cmi.cycle();brk.set(false);
    }

/* SuperSequence 2 */

    cmi.comment("Starting super sequence 2");
    for (n=delta;n>=0;n--){
      brk.set(true);cmi.cycle();brk.set(false);
      cmi.pop();cmi.push();
      w.extend(3);
      w.turn("ccw");
/***
 *** If n==0, then don't do any extends. Also, turn("ccw") should be
 *** non-extendible
 ***/
      w.extend(n-1);w.extend_wire(false);w.extend();
      w.transfer(bfr.readTT(delta-n,4),"cw");
      w.transfer(bfr.readTT(delta-n,3),"");
      w.extend_wire(true);
    }

    for (n=delta;n>=1;n--){
      brk.set(true);cmi.cycle();brk.set(false);
      cmi.pop();cmi.push();
      w.extend(1);
      w.turn("ccw");
      w.extend(n-1);w.extend_wire(false);w.extend();
      w.transfer(bfr.readTT(delta-n,0),"ccw");
      w.transfer(bfr.readTT(delta-n,2),"cw");
      w.transfer(bfr.readTT(delta-n,1),"");
      w.extend_wire(true);
    }

    cmi.comment("Finished!");

    cmi.pop();
    cmi.close();
  }
}
