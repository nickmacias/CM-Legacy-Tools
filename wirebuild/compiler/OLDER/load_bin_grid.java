/*
 * Copyright (C) 1992-2002 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

/*
 * load_bin_grid class. Abstract class with static method for reading
 * a binary grid file as written by the Portable Layout Editor (TM).
 */

import java.io.*;
import java.lang.*;
import java.util.*;

abstract class load_bin_grid
{
  static public void load(cell_matrix_sim sim, cell_exchange ce,
                          int[] ind, String filename)
  {
    if (sim.get_dim() == 2){
      load2d(sim,ce,ind,filename);
    } else if (sim.get_dim()==3){
      load3d(sim,ce,ind,filename);
    } else System.out.println("Don't know how to read "+sim.get_dim()+" dimensional files");
  }

  static public void load2d(cell_matrix_sim sim, cell_exchange ce,
                          int[] ind, String filename)
  {
    int row,col,startrow,startcol,rows,cols;
    int i,j,count,temp[]=new int[32],desclen,freq;
    
    boolean V2;

    startrow=ind[0];startcol=ind[1];

    try{
      FileReader fr=new FileReader(filename);
      String buffer;

/* Read comment and check version */
      buffer=readString(fr,80);
      System.out.println(buffer);
      V2=(-1!=buffer.toString().indexOf("Java Loader V2.0"));

/* Read dimensions of grid */
      rows=readInt(fr);cols=readInt(fr);
      System.out.println("Grid is " + rows + "x" + cols);

/* Read cell defs for each cell */
      freq=((rows*cols)/72)+1; if (freq<1) freq=1; /* For progress display*/
      count=0;
      for (row=startrow;row<startrow+rows;row++){
        for(col=startcol;col<startcol+cols;col++){
          if ((++count)%freq == 0) System.out.print(".");
          readByte(fr); /* Ignore inputs */
          readByte(fr); /* Ignore outputs */
          for (i=0;i<16;i++){
            temp[i]=readByte(fr);
          }
          desclen=readInt(fr); /* # of chars in cell desription */
          if (desclen > 0){
            buffer=readString(fr,desclen);
          }

          ce.c_in=ce.d_in=ce.c_out=ce.d_out=0;
/* Now load the TT into the cell_exchange structure */
          for (i=0;i<16;i++){
            ce.tt[i]=(temp[i]>>4)&15; /* C cols are first */
            ce.tt[i+16]=temp[i]&15; /* D cols are last */
          }
/* Convert if V2 */
          if (V2){ /* Need to translate V2 TT to V3 TT */
            for (i=0;i<16;i++){
/* Old input order is SNEW; New order is WSEN */
/* Therefore, map row number [r3r2r1r0]->r0r3r1r2 */
              j=((i&1)<<3) | ((i&8)>>1) | (i&2) | ((i&4)>>2);

/* Old output order was NSWE; New order is WSEN */
/* Therefore, map row value c3c2c1c0->c1c2c0c3 */
              temp[j]=((ce.tt[i]&2)<<2) | (ce.tt[i]&4) |
                       ((ce.tt[i]&1)<<1) | ((ce.tt[i]&8)>>3);
              temp[j+16]=((ce.tt[i+16]&2)<<2) | (ce.tt[i+16]&4) |
                       ((ce.tt[i+16]&1)<<1) | ((ce.tt[i+16]&8)>>3);
            }
            for (i=0;i<32;i++){
              ce.tt[i]=temp[i];
            }
          } /* End of V2 bit twiddling */
          sim.user_write_cell(row,col,ce);
          sim.process_queue();
        }
      } /* All cells have been read */
      System.out.println(" ");
      fr.close();
    } catch (Exception e){
      System.out.println("Error opening " + filename + ":" + e);
    }
  }

/* 3D binary grid loader */
/* Load into a single Z layer */
  static public void load3d(cell_matrix_sim sim, cell_exchange ce,
                          int[] ind, String filename)
  {
    int row,col,startrow,startcol,rows,cols,layer;
    int i,j,count,temp[]=new int[128],desclen,freq;
    
    boolean V2;

    startrow=ind[0];startcol=ind[1];layer=ind[2];

    try{
      FileReader fr=new FileReader(filename);
      String buffer;

/* Read comment and check version */
      buffer=readString(fr,80);
      System.out.println(buffer);
      V2=(-1!=buffer.toString().indexOf("Java Loader V2.0"));

/* Read dimensions of grid */
      rows=readInt(fr);cols=readInt(fr);
      System.out.println("Grid is " + rows + "x" + cols);

/* Read cell defs for each cell */
      freq=((rows*cols)/72)+1; if (freq<1) freq=1; /* For progress display*/
      count=0;
      for (row=startrow;row<startrow+rows;row++){
        for(col=startcol;col<startcol+cols;col++){
          if ((++count)%freq == 0) System.out.print(".");
          readByte(fr); /* Ignore inputs */
          readByte(fr); /* Ignore outputs */
          for (i=0;i<128;i++){
            temp[i]=readByte(fr);
          }
          desclen=readInt(fr); /* # of chars in cell desription */
          if (desclen > 0){
            buffer=readString(fr,desclen);
          }

          ce.c_in=ce.d_in=ce.c_out=ce.d_out=0;
/* Now load the TT into the cell_exchange structure */
          for (i=0;i<128;i++){
            ce.tt[i]=temp[i];
          }

          sim.user_write_cell(row,col,layer,ce);
          sim.process_queue();
        }
      } /* All cells have been read */
      System.out.println(" ");
      fr.close();
    } catch (Exception e){
      System.out.println("Error opening " + filename + ":" + e);
    }
  }





  static int readByte(FileReader fr)
  {
    char c[]=new char[2];

    try{
      fr.read(c);
    } catch (Exception e){
      System.out.println("Read error: " + e);
    }
    return( ((((int)c[0])&15)<<4) | (((int)c[1])&15) );
  }

  static int readInt(FileReader fr)
  {
    int lsb,msb;
    lsb=255&readByte(fr);
    msb=255&readByte(fr);
    return((msb<<8) | lsb);
  }

  static String readString(FileReader fr,int len)
  {
    StringBuffer s=new StringBuffer("");
    int i;

    for (i=0;i<len;i++){
      s.append((char)readByte(fr));
    }
    return(s.toString());
  }
}
