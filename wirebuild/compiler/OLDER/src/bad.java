/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class bad{
  public static void main(String args[]) {
    cell_matrix_channel pc,cc,pc_cc,brk;

    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("bad.seq",msg);
    cell_matrix_channel d=cmi.assign_channel(0,0,"DW");
    cell_matrix_channel c=cmi.assign_channel(0,0,"CW");

    c.set(true);d.set("de=w;ce=1");cmi.cycle();
    c.set(false);d.set("DW=W");cmi.cycle();
    c.set(true);d.set("de=e!");cmi.cycle();
    c.set(false);cmi.cycle();

    cmi.close();
  }
}
