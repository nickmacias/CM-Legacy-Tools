/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

import java.util.*;

/*
 * ttcompile is an abstract class with a single public method compile()
 * Returns Hex string of TT, or "" on error
 */

abstract class ttcompile{
  public static String compile(String src, int version)
  {
    String tmp_desc=""; /* Stores description string */
    String cd,dir; /* C/D and N/S/W/E */
    int i,ind;
    int program[]; /* Ordered same as characters in cellTT */
    int result[]; /* Result of tabulate() call */
    int cnum;
    String cellTT; /* Result */

    program=new int[32];
    result=new int[16];

/* Initialize outputs */
    for (i=0;i<32;i++) program[i]=0;

/* Tokenize the src input and parse each line */
    StringTokenizer st=new StringTokenizer(src,",;:/\\");
    while (st.hasMoreElements()) {
      String line=st.nextToken();
      line=line.toUpperCase();
      String newline="";
      for (i=0;i<line.length();i++) {
        if (!Character.isWhitespace(line.charAt(i)))
          newline=newline+line.substring(i,i+1);
      } /* line is conditioned now */

      if (newline.length() < 4) return(""); /* Short line! */
      if (!newline.substring(2,3).equals("=")) return("");
      cd=newline.substring(0,1);
      dir=newline.substring(1,2);
      newline=newline.substring(3);

/* find index of column */
      if (version==2){
        if (dir.equals("E")) ind=0; else if (dir.equals("W")) ind=1;
        else if (dir.equals("S")) ind=2; else if (dir.equals("N")) ind=3;
        else return("");
        if (cd.equals("C")) ind+=4; else if (!cd.equals("D")) return("");
      } else {
        if (dir.equals("N")) ind=0; else if (dir.equals("E")) ind=1;
        else if (dir.equals("S")) ind=2; else if (dir.equals("W")) ind=3;
        else return("");
        if (cd.equals("C")) ind+=4; else if (!cd.equals("D")) return("");
      }


/* get TT column */
      if (0==tabulate(newline,version,result)) return(""); /* Compile error */

/* store this row in program[] */
      for (i=0;i<16;i++) {
        if (version==2){
          /* Character is at position i*2, +1 if ind >= 4 */
          /* Bit position is 1<<(ind%4) */
          /* Set the bit if result[i]=1 */
          if (result[i]==1) {
            cnum=i*2;if (ind >= 4) ++cnum; /* Character number */
            program[cnum]|=(1<<(ind%4));
          } /* end of SET BIT code */
        } else {
          if (result[i]==1) {
            cnum=i;if (ind < 4) cnum=cnum+16; /* Character number */
            program[cnum]|=(1<<(ind%4));
          }
        }
      } /* End of for loop */
    } /* End of WHILE loop */

/* Save program[] to cellTT */
    cellTT="";
    for (i=0;i<32;i++){
      cellTT=cellTT+Integer.toHexString(program[31-i]);
    }
    return(cellTT);
  } /* End of compile() */

  static int val[]; /* Stack */
  static int sp; /* Stack pointer */
  static boolean fail; /* Set if we fail somewhere */

/* tabulate() returns an array of 16 TT values */
  static int tabulate(String str, int version, int[] result)
  {

/* ljkd's TABULATE.C                                                     */
/* Copyright (C) 1992-1993 Lisa J. K. Durbeck                            */
/*     -  REVERSE POLISH BITWISE CALCULATOR FOR CELL MATRIX TRUTH TABLES */
/* Polish calculator from K&R modified to do bitwise operations          */
/* Reverse Notation required: write DN := ~E N|S  as DN := E! NS|        */
/*     ( or (E!)(NS|) ). Any spaces are ignored.                         */
/* INPUT: takes the symbolic RHS of the statement above -----^           */
/* as subprocedure, takes this RHS as a one-line argument "input"        */
/* OUTPUT: the boolean value of the RHS for all possible values of E, N, &S */
/*         in the form of an array called result.                        */
/* Program can be used interactively. To see partial results, put parens */
/* around subexpressions.                                                */
/* Modified by NJM on Oct 11, 2000 for Java                              */

    int type, i, j;
    val=new int[100];
    fail=false; /* OK for now */

    for (i = 0; i < 16; ++i) {  
      sp=0; /* Empty stack */
      for(j=0;j<str.length();j++){ /* Character by character */
        switch(str.charAt(j)){
            case 'E':if (version==2){
                       if ((i&2) == 0) push(0); else push(1);break;
                     } else {
                       if ((i&2) == 0) push(0); else push(1);break;
                     }
            case 'W':if (version==2){
                       if ((i&1) == 0) push(0); else push(1);break;
                     } else {
                       if ((i&8) == 0) push(0); else push(1);break;
                     }
            case 'S':if (version==2){
                       if ((i&8) == 0) push(0); else push(1);break;
                     } else {
                       if ((i&4) == 0) push(0); else push(1);break;
                     }
            case 'N':if (version==2){
                       if ((i&4) == 0) push(0); else push(1);break;
                      } else {
                       if ((i&1) == 0) push(0); else push(1);break;
                      }
          case '&':push(pop() & pop());break;

          case '(':break;
          case ')':break;

          case '+':
          case '|':push(pop() | pop());break;

          case '!':
          case '~':push(1-pop());break;

          case'^':push(pop() ^ pop());break;

          case '0':push(0);break;
          case '1':push(1);break;

          default:return(0);
        }
      } /* Entire line parsed */
      result[i]=(int) (pop()); /* Final expression value */
      if (sp != 0) return(0); /* bad expression */
    } /* End of all 16 rows */
    if (fail) return(0); /* Failure */
    return(1); /* Success! */
  } /* End of tabulate() */

  static void push(int i)
  {
    if (sp > 99) fail=true;
    if (fail) return;
    val[sp++]=i;
  }

  static int pop()
  {
    if (sp == 0) fail=true;
    if (fail) return(0);
    return(val[--sp]);
  }


} /* End of class compile */
