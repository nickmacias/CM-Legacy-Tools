/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class sfboot_noini{
  public static void main(String args[]) {
    int i,c,r,n;
    int delta=18; /* Start BREAK (delta) cells down from upper-left corner */
    cell_matrix_channel pc,cc,pc_cc,brk;

    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("sfbootni.seq",msg);

    if (args.length != 1){
      System.out.println("Usage: sfboot filename.bin");
      System.exit(1);
    }

/* Read the binary grid file */
    binary_file_reader bfr=new binary_file_reader(args[0]);

/* Start a wire */
    wire_build w=new wire_build(cmi,"e",delta,0,"BRK,PC,CC");
    brk=w.get_channel("brk"); /* Access wire builder class' break line */

//    w.turn("cw"); // BUILD TO THE SOUTH!

    cmi.push();  /* Save current state */
    w.preclear(true);
    w.extend_break(false);

// *** NO INIT!
/* For now, do one init() */
//    w.init();
// ***

/* and create the initial break cell */
    w.end_break();

    cmi.comment("Starting supersequence 1");
/* SuperSequence 1-Do All except the leftmost 5 columns */

    for (c=bfr.cols()-1;c>4;c--){ /* Do most columns */
      cmi.pop();cmi.push();
      w.extend();w.turn("ccw");w.extend(delta-1);w.turn("cw");
      w.extend(c-5);w.turn("cw");
      for (r=2;r<=bfr.rows()-2;r++){
// Check for optional preclear here...
        w.cond_preclear(bfr.readTT(r+1,c),"ccw");
        w.transfer(bfr.readTT(r,c),"ccw");
        w.extend();
      }
      w.transfer(bfr.readTT(bfr.rows()-1,c),"ccw");

      brk.set(true);cmi.cycle();brk.set(false);cmi.pop();cmi.push();

      w.extend();w.turn("ccw");w.extend(delta-1);w.turn("cw");
      w.extend(c-4);w.extend_wire(false);w.extend();
/* Make head cells non-extendible, so cells we're
   building don't interfere with new wire */
      w.transfer(bfr.readTT(1,c),"cw");
      w.transfer(bfr.readTT(0,c),"");
      w.extend_wire(true);

      brk.set(true);cmi.cycle();brk.set(false);
    }

/* SuperSequence 2 */

    cmi.comment("Starting supersequence 2");
    for (n=delta;n>=0;n--){
      brk.set(true);cmi.cycle();brk.set(false);
      cmi.pop();cmi.push();
      w.extend(3);
      if (n==0) w.extend_wire(false); // corner will be end of wire
      w.turn("ccw");
      w.extend(n-1);
      if (n > 0){w.extend_wire(false);w.extend();}
      w.transfer(bfr.readTT(delta-n,4),"cw");
      w.transfer(bfr.readTT(delta-n,3),"");
      w.extend_wire(true);
    }

    for (n=delta;n>=1;n--){
      brk.set(true);cmi.cycle();brk.set(false);
      cmi.pop();cmi.push();
      w.extend(1);
      w.turn("ccw");
      w.extend(n-1);w.extend_wire(false);w.extend();
// This works fine if n=1, which is smallest n value
      w.transfer(bfr.readTT(delta-n,0),"ccw");
      w.transfer(bfr.readTT(delta-n,2),"cw");
      w.transfer(bfr.readTT(delta-n,1),"");
      w.extend_wire(true);
    }

// Supersequence 3
    cmi.comment("Beginning supersequence 3");

    for (r=bfr.rows()-1;r>=delta+3;r--){
      w.extend_wire(true);
      brk.set(true);cmi.cycle();brk.set(false);cmi.pop();cmi.push();
      w.extend(2);
      if (r==delta+3) w.extend_wire(false); // CW turn is end of wire
      w.turn("cw");
      w.extend((r-(delta+3))-1);w.extend_wire(false);
      if (r > delta+3) w.extend();
      w.transfer(bfr.readTT(r,4),"ccw");
      w.transfer(bfr.readTT(r,2),"cw");
      w.transfer(bfr.readTT(r,3),"");
    }

    w.extend_wire(true);
    brk.set(true);cmi.cycle();brk.set(false);cmi.pop();cmi.push();
    w.extend(3);w.extend_wire(false);w.extend();
    w.transfer(bfr.readTT(delta+2,4),"cw");
    w.transfer(bfr.readTT(delta+1,4),"");

    w.extend_wire(true);
    brk.set(true);cmi.cycle();brk.set(false);cmi.pop();cmi.push();
    w.extend(2);w.extend_wire(false);w.extend();
    w.transfer(bfr.readTT(delta+2,3),"cw");
    w.transfer(bfr.readTT(delta+1,3),"");

// Supersequence 4
    cmi.comment("Beginning supersequence 4");

    for (r=bfr.rows()-1;r>=delta+3;r--){
      w.extend_wire(true);
      brk.set(true);cmi.cycle();brk.set(false);cmi.pop();cmi.push();
      if (r==delta+3) w.extend_wire(false); // CW turn is end of wire
      w.turn("cw");
      w.extend((r-(delta+3))-1);w.extend_wire(false);
      if (r > delta+3) w.extend();
      w.transfer(bfr.readTT(r,0),"cw");
      w.transfer(bfr.readTT(r,1),"");
    }

cmi.comment("Last 3 seqs");

    w.extend_wire(true);
    brk.set(true);cmi.cycle();brk.set(false);cmi.pop();cmi.push();
    w.extend();
    w.transfer("","ccw");
    w.extend_wire(false);w.extend();
    w.transfer(bfr.readTT(delta,2),"ccw");
    w.transfer(bfr.readTT(delta+2,2),"cw");
    w.transfer(bfr.readTT(delta+1,2),"");

    w.extend_wire(true);
    brk.set(true);cmi.cycle();brk.set(false);cmi.pop();cmi.push();
    w.extend_wire(false);w.extend();
    w.transfer(bfr.readTT(delta,1),"ccw");
    w.transfer(bfr.readTT(delta+2,1),"cw");
    w.transfer(bfr.readTT(delta+1,1),"");

    brk.set(true);cmi.cycle();brk.set(false);cmi.pop();cmi.push();
    w.extend_wire(false);
    w.transfer(bfr.readTT(delta,0),"ccw");
    w.transfer(bfr.readTT(delta+2,0),"cw");
    w.transfer(bfr.readTT(delta+1,0),"");

    cmi.comment("Finished!");

    cmi.pop();
    cmi.close();
  }
}
