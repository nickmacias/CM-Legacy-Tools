/*
 * Sample main program for Cell Matrix Simulator API
 *
 * Shows how to compile Boolean expressions, set inputs, examine
 * outputs, etc.
 *
 */

#include <stdio.h>
#include "engine.h"

/* These are nominal values for a 50x50 Cell Matrix */
#define HEIGHT 50
#define WIDTH 50
#define QSIZE 80000
#define C_ARR_SIZE 80000

main()
{
  char in,out,program[16];
  int clock=0; /* Initial clock state */
  
/* Initialize the Cell Matrix */
  if (init_grid(HEIGHT,WIDTH,QSIZE,C_ARR_SIZE)) printf("Grid init failed!\n");

/*
 * Note that we're ignoring return values, since we're confident this will
 * all work correctly
 */

/* Load a circuit which inverts its input */
  tt_compile("DE=W;DW=E",program); /* Bi-directional wire */
  write_cell(0,0,program);        /* Load into upper left corner */
  tt_compile("DW=W~",program);   /* Cell which inverts its Western input */
                                /* and returns it to its Western output */
  write_cell(0,1,program);     /* Load into Row 0, Column 1 */

/* It's a good idea to process the queue after writing cells */
  process_queue();

/* Now examine the input/output response of cell [0,0] */
  read_cell(0,0,&in,&out,program);
  printf("Initial outputs=%02x.",out);
/* Let's print out just the value on the DW output, to show the use of masks */
  printf(" [0,0]DWout=%d\n",(out&DOW)?1:0);

  user_set_input(0,0,DIW,1); /* Set input to 1 */
  process_queue();
  read_cell(0,0,&in,&out,program);
  printf("When DWin=1, outputs=%02x. [0,0]DWout=%d\n",out,(out&DOW)?1:0);

  user_set_input(0,0,DIW,0); /* Set input to 1 */
  process_queue();
  read_cell(0,0,&in,&out,program);
  printf("When DWin=0, outputs=%02x. [0,0]DWout=%d\n",out,(out&DOW)?1:0);
}
