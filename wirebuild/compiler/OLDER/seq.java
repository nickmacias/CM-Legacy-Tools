int seq_file(fname)
char *fname;
{
  FILE *fp;
  int num_channels,rows[32],cols[32],masks[32],i,j,val;
  int cycle_number;
  int ind,mask; /* For picking pieces out of tts */
  char tts[32][128]; /* Stores each TT to output */
  char buffer[1024],side[8],c;

  fp=fopen(fname,"r");
  if (fp <= (FILE *) NULL){
    fprintf(stderr,"Can not open %s\n",fname);
    return(1);
  }

  cycle_number=0;

/* Input the header */
  fgets(buffer,1024,fp); /* header */
  buffer[16]='\0';
  if (0 != strcmp(buffer,"\"#SEQUENCE FILE\"")){
    fprintf(stderr,"%s does not appear to be a valid sequence file\n",fname);
    fclose(fp);
    return(1);
  }

  fgets(buffer,1024,fp); /* legal info */
  fgets(buffer,1024,fp); /* misc flags */
  fgets(buffer,1024,fp);sscanf(buffer,"\"%d\"",&num_channels);
  printf("File contains %d channel%s\n",num_channels,(num_channels==1)?"":"s");

/* Read each channel's assignment */
  for (i=0;i<num_channels;i++){
    fgets(buffer,1024,fp);
    sscanf(buffer,"\"%d,%d,%s\"",&rows[i],&cols[i],side);
    side[2]='\0';
/* Convert side to mask */
    if (0==strcmp(side,"CE")) masks[i]=CIE;
    if (0==strcmp(side,"CN")) masks[i]=CIN;
    if (0==strcmp(side,"CS")) masks[i]=0xff& ((unsigned int) CIS);
    if (0==strcmp(side,"CW")) masks[i]=CIW;
    if (0==strcmp(side,"DE")) masks[i]=DIE;
    if (0==strcmp(side,"DN")) masks[i]=DIN;
    if (0==strcmp(side,"DS")) masks[i]=DIS;
    if (0==strcmp(side,"DW")) masks[i]=DIW;
  } /* All channels assigned now */

/* Now we execute the sequence */
  while (1==seq_gets(tts[0],fp)){ /* seq_gets skips comments */
    for (i=1;i<num_channels;i++){
      seq_gets(tts[i],fp);
    }

    mask=1;
/* Now step the clock through these cycles */
    for (i=0;i<128;i++){ /* Clock tick */
      ind=1+(i>>2); /* Index into tts string */
      mask=mask>>1;
      if (mask==0) mask=8; /* mask to pick off bit from nibble */
      for (j=0;j<num_channels;j++){
/* Find the value to set for channel j, step i */
        c=tts[j][ind]; /* This is the nibble */
        val=c-'0';if (val > 9) val=10+c-'A'; /* Convert to integer */
        val=(val&mask)?1:0;
        user_set_input(rows[j],cols[j],masks[j],val);
      } /* End of all channels */
      if (0!=process_queue()) return(1);
      do_tick(1);if (0 != process_queue()) return(1);
      do_tick(0);if (0 != process_queue()) return(1);
    } /* End of all ticks */
    if ((++cycle_number)%100 == 0){
      printf("\rCycle %d",cycle_number);fflush(stdout);
    }
/***
    if (cycle_number%5000 == 0){
      sprintf(buffer,"Save_%d.sta",cycle_number);
      raw_write(0,0,200,200,buffer);
    }
***/
  } /* End of file */

  printf("\nSequence file finished\n");
  fclose(fp);
  return(0);
}

int seq_gets(buffer,fp)
char *buffer;
FILE *fp;
{
  if (NULL == fgets(buffer,1024,fp)) return(0); /* EOF */

  while (buffer[1] == '#'){
/* Echo the comment */
/***
    buffer[-1+strlen(buffer)]='\0';
    printf("%s\r",buffer);
***/
    if (NULL == fgets(buffer,1024,fp)) return(0); /* EOF */
  }

  return(1); /* Normal read */
}
