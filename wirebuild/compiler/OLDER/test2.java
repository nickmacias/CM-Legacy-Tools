class test2{
  public static void main(String args[]) {
    cell_matrix_interface cmi=new cell_matrix_interface("test2.seq","Test file");

/* Start a wire */
    wire_build w=new wire_build(cmi,"s",0,1,"CC,PC");
    w.preclear(true);

    w.init();
    w.extend(10);
    w.turn("CCW");
    w.extend(10);
    cmi.close();
  }
}
