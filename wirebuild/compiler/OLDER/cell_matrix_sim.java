/*
 * Copyright (C) 1992-2002 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

import java.io.*;
import java.lang.*;
import java.util.*;

class cell_matrix_sim{
/*
 * Entire Cell Matrix structure is stored as a single long array
 * Each cell is stored as follows (all entries are 32-bit ints):

	Size	Description
	1	C-latch: Stored at latest uptick
	1	C Inputs
	1	D Inputs
	1	C Outputs
	1	D Outputs
	1	c_arr_ind. Shows where this cell is stored in c_arr[]
	1	chg_arr_ind. Loc of cellbase in chg_arr[]
	1	Misc. Flags (fail, watch, etc.)
	n	Indicies of neighbors
	n	Bitmasks for neighbors I/O
	2*(2^n)	Truth Table
		2^n rows of C outputs
		followed by 2^n rows of D outputs
 */

/* Main Cell Matrix structure */
  int cm[];

/* C-Mode Queue */
  int c_arr[]; /* Array of cells in C-mode */
  int c_arr_num; /* Number of valid entries in c_arr */

/* Change array */
  int chg_arr[]; /* Array of cells which have changed since last unload */
  int chg_arr_num; /* # of valid entries in chg_arr */

/* Store an array of cell indicies for fast lookup */
  int cell_index_list[][];

/*
 * Event Queue
 *
 * Event queue entries appear as follows:
 * ACTION: Marker|Clock1|Clock0|Set1|Set0
 * CELLBASE (if not a CLOCK event)
 * D Mask - Shows input bit to set/clear
 * C Mask - Shows input bit to set/clear
 *
 */
  int q_tail=0; /* next location to write */
  int q_head=0; /* next element to read */
  int queue[];  /* Actual queue */
  int event_limit=100000; /* Default limit */
  int QSIZE=100000; /* Make this variable in instantiation */
/***  int C_ARR_SIZE=10000; ***/ /* Same here */

/* Useful variables */
  int num_sides;	/* # of neighbors each cell has */
  int cellsize;		/* # of words (ints) in cm[] for each cell */
  int dims[]=new int[32];		/* Size of array, each dimension (or 0) */
  int dim; /* # of non-zero dimensions */
  int cmsize; /* # of cells in matrix */
  int ttsize; /* # of rows in truth table */

/* These are basically constants for indexing inside a cell in cm[] array */
  int c_latch,c_in,d_in,c_out,d_out,c_arr_ind,chg_arr_ind,misc_flags,
      nbr_start,nbr_mask_start,
      tt_c_start,tt_d_start;

/*
 * Constructors. General format is cell_matrix_sim(n,d0,d1,d2,...)
 * where n is the number of sides each cell has, and
 * d0, d1, d2, ... are the dimensions of the matrix
 */
  public cell_matrix_sim(int n,int a,int b) /* 2D */
  {
    dims[0]=a;dims[1]=b;dims[2]=0;
    dim=2;
    num_sides=n;
    common_init();
  }

  public cell_matrix_sim(int n,int a,int b, int c) /* 3D */
  {
    dims[0]=a;dims[1]=b;dims[2]=c;dims[3]=0;
    dim=3;
    num_sides=n;
    common_init();
  }

/* common_init performs all initializations, including topo/dim-specific ones
 * based on dim and num_sides */
  void common_init()
  {
    /* Set up common vars */
    ttsize=1<<num_sides; /* # of rows in truth table */

    c_latch=0; /* Offset from start of cll inside cm[] */
    c_in=1;d_in=2;c_out=3;d_out=4;
    c_arr_ind=5;chg_arr_ind=6;
    misc_flags=7;nbr_start=8;
    nbr_mask_start=nbr_start+num_sides;
    tt_c_start=nbr_mask_start+num_sides;
    tt_d_start=tt_c_start+ttsize;
    cellsize=tt_d_start+ttsize;

/* Topology and Dimensionality-specific initializations here */
    if ((num_sides==4) && (dim==2)){
      init_2d4();
    }

    if ((num_sides==6) && (dim==3)){
      init_3d6();
    }

    /* Add other initialization routines here */


    /* Setup c_arr  and chg_arr */
    c_arr=new int[cmsize];
    chg_arr=new int[cmsize];

    queue=new int[QSIZE];

    reinit(); /* Common re-initialization routine-can be called at runtime too */
  }

public void reinit() /* Clear out the internal structures */
{
  int i,j,base;

  base=1; /* Base of first cell */
  for (i=0;i<cmsize;i++){ /* For each cell inside cm[]... */
    cm[base+c_latch]=cm[base+c_in]=cm[base+d_in]=
    cm[base+c_out]=cm[base+d_out]=cm[base+misc_flags]=0;
    cm[base+c_arr_ind]=(-1); /* Shows cell is not in c_arr[] */
    cm[base+chg_arr_ind]=(-1); /* Shows cell is not in chg_arr[] */
    for (j=0;j<ttsize;j++){
      cm[base+tt_c_start+j]=cm[base+tt_d_start+j]=0; /* Clear TT bits */
    }
    base+=cellsize;
  }
  c_arr_num=0; /* No c-mode cells */
  chg_arr_num=0; /* No changes waiting to be unloaded */
  q_tail=q_head=0; /* Empty event queue */
  return;
}

/* Initialization routines. Build cm[] structure */
  void init_2d4()
  {
    int base,i;
    int ind[]=new int[2];

    cmsize=dims[0]*dims[1];
    cm=new int[1+(cmsize*cellsize)]; /* Allocate space */
                                     /* CM starts at [1], so add one to size */
    cell_index_list=new int[cmsize][dim];
/* Scan through all cells */
    for (ind[0]=0;ind[0]<dims[0];ind[0]++){
      for (ind[1]=0;ind[1]<dims[1];ind[1]++){ /* ind[] is cell to init */
        base=compute_index(ind); /* Starting location of this cell */
        for (i=0;i<dim;i++){
          cell_index_list[(base-1)/cellsize][i]=ind[i];
        }
        /* Set up adjacency graph */
        if (ind[0]!=0){
          --ind[0];cm[base+nbr_start+0]=compute_index(ind);++ind[0];
        } else {cm[base+nbr_start+0]=0;} /* No neighbor */
        if (ind[1]!=dims[1]-1){
          ++ind[1];cm[base+nbr_start+1]=compute_index(ind);--ind[1];
        } else {cm[base+nbr_start+1]=0;} /* No neighbor */
        if (ind[0]!=dims[0]-1){
          ++ind[0];cm[base+nbr_start+2]=compute_index(ind);--ind[0];
        } else {cm[base+nbr_start+2]=0;} /* No neighbor */
        if (ind[1]!=0){
          --ind[1];cm[base+nbr_start+3]=compute_index(ind);++ind[1];
        } else {cm[base+nbr_start+3]=0;} /* No neighbor */

        /* Set up adjacency bitmasks */
        cm[base+nbr_mask_start+0]=4; /* Mask for Nout=Sin */
        cm[base+nbr_mask_start+1]=8;
        cm[base+nbr_mask_start+2]=1;
        cm[base+nbr_mask_start+3]=2;
      } /* End of row*/
    } /* End of col */
  } /* End of init_2d4() */

/* 3D initialization routine */
  void init_3d6()
  {
    int base,i;
    int ind[]=new int[3];

    cmsize=dims[0]*dims[1]*dims[2];
    cm=new int[1+(cmsize*cellsize)]; /* Allocate space */
                                     /* CM starts at [1], so add one to size */
    cell_index_list=new int[cmsize][dim];
/* Scan through all cells */
    for (ind[0]=0;ind[0]<dims[0];ind[0]++){ /* 2D Row */
      for (ind[1]=0;ind[1]<dims[1];ind[1]++){ /* 2D Col */
        for (ind[2]=0;ind[2]<dims[2];ind[2]++){ /* 3D Layer */
          base=compute_index(ind); /* Starting location of this cell */
          for (i=0;i<dim;i++){
            cell_index_list[(base-1)/cellsize][i]=ind[i];
          }
          /* Set up adjacency graph */
          if (ind[0]!=0){
            --ind[0];cm[base+nbr_start+0]=compute_index(ind);++ind[0];
          } else {cm[base+nbr_start+0]=0;} /* No neighbor */
          if (ind[1]!=dims[1]-1){
            ++ind[1];cm[base+nbr_start+1]=compute_index(ind);--ind[1];
          } else {cm[base+nbr_start+1]=0;} /* No neighbor */
          if (ind[0]!=dims[0]-1){
            ++ind[0];cm[base+nbr_start+2]=compute_index(ind);--ind[0];
          } else {cm[base+nbr_start+2]=0;} /* No neighbor */
          if (ind[1]!=0){
            --ind[1];cm[base+nbr_start+3]=compute_index(ind);++ind[1];
          } else {cm[base+nbr_start+3]=0;} /* No neighbor */
          if (ind[2]!=dims[2]-1){
            ++ind[2];cm[base+nbr_start+4]=compute_index(ind);--ind[2];
          } else {cm[base+nbr_start+4]=0;}
          if (ind[2]!=0){
            --ind[2];cm[base+nbr_start+5]=compute_index(ind);++ind[2];
          } else {cm[base+nbr_start+5]=0;}
  
          /* Set up adjacency bitmasks */
          cm[base+nbr_mask_start+0]=4; /* Mask for Nout=Sin */
          cm[base+nbr_mask_start+1]=8;
          cm[base+nbr_mask_start+2]=1;
          cm[base+nbr_mask_start+3]=2;
          cm[base+nbr_mask_start+4]=32;
          cm[base+nbr_mask_start+5]=16;
        } /* ENd of layer */
      } /* End of row*/
    } /* End of col */
  } /* End of init_3d6() */


/* Compute an index from an incoming array */
/* This is mainly used during the one-time initialization of cm[] */
  int compute_index(int ind[])
  {
    int index,i;

/* Bounds check */
    for (i=0;i<dim;i++){
      if ((ind[i] >= dims[i]) || (ind[i] < 0)){
        System.out.println("Indicies out of bounds\n");
        return(0);
      }
    }

    i=0;
    index=0; /* but location 0 (cm[0]) is reserved... */
    while (dims[i] != 0){
      index+=ind[i];
      if (dims[++i] != 0) index*=dims[i];
    }
    return((cellsize*index)+1);
  }

/* Misc. public routines */
  public int get_dim()
  {
    return dim;
  }

  public int[] get_dims()
  {
    int ret[]=new int[32],i;
    for (i=0;i<dim;i++) ret[i]=dims[i];
    return(ret);
  }

/*** QUEUE CONSTANTS AND MASKS ***/
/* Define constants for pulling off bits from queue entry */
  final static int SET0=1;
  final static int SET1=2;
  final static int CLOCK0=4;
  final static int CLOCK1=8;
  final static int MARKER=16;

/* Time stamping data */
  int marker=0; /* Toggle at end of all parallel events to show a time tick */
  int timestamp=0; /* # of time units */

/*
 * All these routines will use a raw index into cm[]
 * special user_... versions will accept coordinate arrays[] and convert
 * them to a raw index
 */

/*** QUEUE ROUTINES ***/
  void set_flags(int ind,int mode)
  {
    cm[ind+misc_flags]=mode;
  }                    

  int q_size()   /* Return # of things in queue */
  {
    int d;
    d=q_tail-q_head;
    if (d<0) d=d+QSIZE;
    return(d);
  }

/* peek routines for examining queue without changing pointers */

  int h,zero_queue=0; /* h is a running, temporary queue head pointer */

  void peek_set(int s)
  {
    zero_queue=s; /* 1-->zeroing enabled, 0-->zeroing disabled */
  }

  void init_peek_queue()
  {
    h=q_head;
  }

  queue_entry peek_queue_return=new queue_entry(); /* For returning values */

  queue_entry peek_queue()
  {
    if (h==q_tail){ /* Queue is empty */
      peek_queue_return.empty=true;
      return(peek_queue_return);
    }
    peek_queue_return.empty=false;

    peek_queue_return.value=queue[h++]; /* Next element to return */
    if (h==QSIZE) h=0;  /* Rollover */
    return(peek_queue_return);
  }

  queue_entry remqueue_return=new queue_entry();

  queue_entry remqueue() /* remove head of queue, store in argument */
  {
    if (remqueue_return.empty=(q_head == q_tail)){
      return(remqueue_return); /* failure */
    }
    remqueue_return.value=queue[q_head++];
    if (q_head == QSIZE) q_head=0;
    return(remqueue_return);
  }

  int insqueue(int x) /* add x to tail of queue */
  {
    queue[q_tail++]=x;
    if (q_tail == QSIZE) q_tail=0;
    if (q_tail != q_head) return(0);
    System.out.println("Queue overflow on event insertion!");
    return(1); /* overflow */
  }

  public int user_write_cell(int ind[], cell_exchange newcell)
  {
    return(write_cell(compute_index(ind),newcell));
  }

  public int user_write_cell(int x, int y, cell_exchange newcell)
  {
    int ind[]=new int[2];
    ind[0]=x;ind[1]=y;
    return(user_write_cell(ind,newcell));
  }

  public int user_write_cell(int x, int y, int z, cell_exchange newcell)
  {
    int ind[]=new int[3];
    ind[0]=x;ind[1]=y;ind[2]=z;
    return(user_write_cell(ind,newcell));
  }

  int write_cell(int cellbase,cell_exchange newcell) /* Don't modify inputs */
  {
    int i,ttbase;
    if (cellbase==0) return(1);
    ttbase=cellbase+tt_c_start;

    for (i=0;i<ttsize*2;i++){
      cm[ttbase++]=newcell.tt[i];
    }

/* Now touch an input to cause re-computation */
/* Pick off LSB from D INPUT word and set it to its current value */
    if (0!=set_input(cellbase,0,1,cm[cellbase+d_in]&1)){
      return(1); /* error in set_input probably means event queue is full */
    }
    return(0);
  }


/* Modify inputs and outputs */
/* WARNING! It's up to the caller to ensure that the resulting matrix
  will be consistent! */

/***
  int write_cell_all(int cellbase,cell_exchange newcell)
  {
    int i,ttbase;
    ttbase=cellbase+tt_c_start;

    cm[ttbase+c_in]=newcell.c_in;
    cm[ttbase+d_in]=newcell.d_in;
    cm[ttbase+c_out]=newcell.c_out;
    cm[ttbase+d_out]=newcell.d_out;


    for (i=0;i<ttsize*2;i++){
      cm[ttbase++]=newcell.tt[i];
    }

    return(0);
  }
 ***/

  int init_cmode(int cellbase)   /* Initialize c-mode array structures */
/* This is necessary eg. after re-loading a saved matrix from disk */
  {
    if (0!=cm[cellbase+c_in]){ /* Cell is in c-mode */
/***      if (c_arr_num == C_ARR_SIZE) return(1); ***/ /* Too many c-mode cells! */
      c_arr[c_arr_num]=cellbase;
      cm[cellbase+c_arr_ind]=c_arr_num++; /* Store c_arr[] index inside cell's structure */
    }
    return(0);
  }

  public cell_exchange user_read_cell(int ind[])
  {
    return(read_cell(compute_index(ind)));
  }

  public cell_exchange user_read_cell(int x, int y)
  {
    int ind[]=new int[2];
    ind[0]=x;ind[1]=y;
    return(user_read_cell(ind));
  }

  public cell_exchange user_read_cell(int x, int y,int z)
  {
    int ind[]=new int[3];
    ind[0]=x;ind[1]=y;ind[2]=z;
    return(user_read_cell(ind));
  }

  cell_exchange read_cell(int cellbase)
  {
    int i;
    cell_exchange r=new cell_exchange(dim,num_sides); /* Return value */
    if (cellbase==0){r.error=true;return(r);}
    r.c_in=cm[cellbase+c_in];
    r.d_in=cm[cellbase+d_in];
    r.c_out=cm[cellbase+c_out];
    r.d_out=cm[cellbase+d_out];
    cellbase+=tt_c_start;
    r.tt=new int[2*ttsize];
    for (i=0;i<2*ttsize;i++){
      r.tt[i]=cm[cellbase++];
    }
    return(r);
  }

  public int tick()
  {
    int status;
    status=do_tick(1);if (status != 0) return(status);
    return(do_tick(0));
  }

  int do_tick(int level) /* TICK clock HI or LO */
  {
    if (0!=insqueue((((0!=level)?CLOCK1:CLOCK0)|marker))) return(1); /* Q full */
    return(0);
  }

  public int user_set_input(int ind[], char type, int side, int value)
  {
    int cmask,dmask,cellbase;

    cellbase=compute_index(ind);

    if (type=='C'){cmask=1<<side;dmask=0;}
    else if (type=='D'){cmask=0;dmask=1<<side;}
    else {
      System.out.println("Third argument to user_set_input should be C or D");
      return(1);
    }

/* Make sure we're changing an unconnected input */
    if (cm[cellbase+nbr_start+side] != 0){
      System.out.println("Only edge inputs may be changed");
      return(2);
    }

/* Otherwise, call the set_input routine */
    return(set_input(compute_index(ind),cmask,dmask,value));
  }

  public int user_set_input(int x, int y, char type, int side, int value)
  {
    int ind[]=new int[2];
    ind[0]=x;ind[1]=y;
    return(user_set_input(ind,type,side,value));
  }

  public int user_set_input(int x, int y, int z, char type, int side, int value)
  {
    int ind[]=new int[3];
    ind[0]=x;ind[1]=y;ind[2]=z;
    return(user_set_input(ind,type,side,value));
  }

  int set_input(int cellbase,int cmask, int dmask, int value)
  {
    if (cellbase==0) return(0); /* Not a cell! (edge case) */
    record_change(cellbase); /* Note that an input has changed */
    if (0!=(cm[cellbase+misc_flags] & 1)) return(0); /* Ignore input changes */

/* See if input_mask/row/col is already in queue. If so, zero it out */
    check_zero(cellbase,cmask,dmask);
    if (0!=(insqueue(((0!=value)?SET1:SET0)|marker))) return(1); /* Q full */
    if (0!=insqueue(cellbase)) return(1); /* Q full */
    if (0!=insqueue(cmask)) return(1); /* Q full */
    if (0!=insqueue(dmask)) return(1); /* Q full */
    return(0);
  }

/*
 * check_zero(cell, cmask, dmask)
 * Pre-scan the queue, and see if there's already a change stored for
 * this cell's specified input. If there is, just zero that command out.
 */
  void check_zero(int cellbase, int c_mask,int d_mask)
  {
    int save_h;
    queue_entry qe,cbase,cmask,dmask;

    if (zero_queue==0) return; /* This feature is disabled */

    init_peek_queue(); /* Move temp pointer to start of queue */
    save_h=h; /* This is where current command is stored in queue */

    qe=peek_queue(); /* Initial peek */
    while (!qe.empty){
      if ((0!=(qe.value&CLOCK1)) || (0!=(qe.value&CLOCK0))){ /* SET command */
        cbase=peek_queue();
        cmask=peek_queue();
        dmask=peek_queue();

        if ((cbase.value == cellbase) &&
            (cmask.value==c_mask) && (dmask.value==d_mask)){ /* match! */
          queue[save_h]=queue[save_h]&MARKER; /* Zero out the command */
                                              /* BUT KEEP MARKER! */
        }
      }
      save_h=h; /* Next command's loc */
      qe=peek_queue(); /* Look at next command */
    } /* End of WHILE */
  }


/*** QUEUE PROCESSOR ***/

  int get_timestamp()
  {
    return(timestamp);
  }

  void reset_timestamp()
  {
    timestamp=0;
  }

  public int process_queue()
  {
    int status,count;

    count=0;
    while ((status=process_queue_head())==0){
      if (++count > event_limit){
        System.out.println("Event limit (" + event_limit + ") has been reached.");
        System.out.println("Flushing queue.");
        q_head=q_tail;
        return(-1);
      }
    }
    return(status);
  }

  public int process_queue_head() /* look at top of queue, execute command */
  {
    int cmd,in,cellbase,cmask,dmask,diff;
    int old_cin, old_din, old_cout, old_dout;
    int ptr,i;
    int topbase, newind,cout,dout,cout2,dout2;
    queue_entry qe,qe2;
                 
    qe=remqueue(); 
    if (qe.empty) return(99); /* queue empty */
    cmd=qe.value; /* command word */
    /* See if marker (part of command word) is different from current marker */
    if ((cmd&MARKER) == marker){
      ++timestamp; /* New time unit */
      marker=MARKER-marker; /* Toggle marker */
    }

    if ((0!=(cmd & SET0)) || (0!=(cmd & SET1))){ /* SET INPUT command */
      qe=remqueue();if (qe.empty) return(-1);cellbase=qe.value;
      qe=remqueue();if (qe.empty) return(-1);cmask=qe.value;
      qe=remqueue();if (qe.empty) return(-1);dmask=qe.value;

/* Save current IN values */
      old_cin=cm[cellbase+c_in];
      old_din=cm[cellbase+d_in];

/* set the ->in field of the cell */
      if (cmask != 0){ /* Adjust a C input */
        if (0!=(cmd&SET1)){cm[cellbase+c_in]|=cmask;}
        else {cm[cellbase+c_in]&=(~cmask);}
      } else if (dmask != 0){ /* Adjust a D input */
        if (0!=(cmd&SET1)){cm[cellbase+d_in]|=dmask;}
        else {cm[cellbase+d_in]&=(~dmask);}
      }

                           /*** C-MODE PROCESSING ***/                         
/*
 * If cell is in C-mode, and a change has occured in the inputs (which is
 * why we're here in the first place), only a few things can be happening...
 *  (1) A DATA input has changed, which really doesn't matter to us, or
 *  (2) A CONTROL input has changed. This means we must adjust the C and D
 *      outputs for C-mode operation...COUT(i)=0, and
 *      DOUT(i)=TT[127] if CIN(I)=1, else DOUT(i) is unchanged.  
 ***
 *** TWEEK ALERT *** Change so DOUT(i)=0 if CIN(i)=0  9/15/95 njm
 ***
 ***
 *** TWEEK ALERT #2 ***Entering C-mode, drop Dout before Cout
 *** Since dropping Cout may return neighbor to D-mode, set
 *** D outputs properly first (bad Dout while Cout=1 is harmless,
 *** since Dout is ignored until clock tick during C-mode op).1/11/98 njm
 ***
 */
      if (0!=isin_cmode(cellbase)){ /* drive C-outputs low */   
/* First, see if we're just going into c-mode now. If so, remove latched data */
        if (old_cin == 0){ /* Just entering c-mode now */
          cm[cellbase+c_latch]=2;  /* We'll latch next time clock goes HI */
          
/***          if (c_arr_num == C_ARR_SIZE) return(1); ***/ /* Too many c-mode cells! */
          c_arr[c_arr_num]=cellbase; /* store in c-mode array */
          cm[cellbase+c_arr_ind]=c_arr_num++; /* remember where we stored it */
  	} /* End of initial c-mode entry processing */

        old_cout=cm[cellbase+c_out];
        old_dout=cm[cellbase+d_out]; /* Current outputs before this change */
        
/* Next, clear DOut on each side where CIn=0 */
        ptr=1; /* Sliding bitmask */
        for (i=0;i<num_sides;i++){
          if (((cm[cellbase+c_in]&ptr) == 0) && (0!=(old_dout&ptr))){ /* Clear Dout */
            cm[cellbase+d_out]&=(~ptr);
            /* and set neighbor's D input accordingly */
            if (0!=set_input(cm[cellbase+nbr_start+i],0,cm[cellbase+nbr_mask_start+i],0)) return(1);
          }
          ptr<<=1; /* Next bit */
        }

/* Now we can drive Cout low */
        if (0!=old_cout){ /* Some C-outputs are HI right now */
          ptr=1;
          for (i=0;i<num_sides;i++){
            if (0!=(old_cout & ptr)){
              if (0!=set_input(cm[cellbase+nbr_start+i],cm[cellbase+nbr_mask_start+i],0,0)) return(1);
            }
            ptr<<=1;
          }
          cm[cellbase+c_out]=0; /* clear C outputs */
        }

/* Next we set DOUT (on any side with CIN=1) to the highest TT bit */
        if (0!=set_dout_to_tt127(cellbase)) return(1); /* that was easy */
        record_change(cellbase);
        return(0);
      } /* End of C-mode processing */

                             
                             /*** D-MODE PROCESSING ***/
                             
/* See if we were in c-mode a tick ago */
      if (0!=old_cin){ /* Yup...need to remove it from c_arr stack */
/* Shuffle last entry in array to take this one's place */
        --c_arr_num; /* Bump down stack size */
        topbase=c_arr[c_arr_num]; /* Current top-of-stack member */
        newind=cm[cellbase+c_arr_ind]; /* new loc for TOS member */
        c_arr[newind]=topbase;	/* Shuffle TOS into middle ofstack */
        cm[topbase+c_arr_ind]=newind; /* and point to in from grid */
        cm[cellbase+c_arr_ind]=(-1); /* and remove old one's pointer */
      }
      
/* now re-compute the outputs for that cell */
      cout2=cm[cellbase+c_out];
      dout2=cm[cellbase+d_out]; /* Old C and D outputs */

      cout=cm[cellbase+tt_c_start+cm[cellbase+d_in]];
      dout=cm[cellbase+tt_d_start+cm[cellbase+d_in]]; /* New C and D outputs */

      cm[cellbase+c_out]=cout;
      cm[cellbase+d_out]=dout; /* Save new outputs inside cell's structure */

      if ((cout==cout2) && (dout==dout2)) return(0); /* no output changes */

/* We'll xor the outputs to determine the difference, then shift that difference */
/* and check the MSB. When the difference mask is all 0, we know we're done with it */

      int out_mask;

      out_mask=1; /* Picks off output bits from c/dout */
      diff=cout ^ cout2; /* diff is a bitmask of changes to C inputs */
      for (i=0;(i<num_sides) && (diff != 0);i++){ /* Run until no diffs */
        if (0!=(diff & 1)){
          if (0!=set_input(cm[cellbase+nbr_start+i],cm[cellbase+nbr_mask_start+i],0,(cout&out_mask)))
             return(1);
        } /* End of If Bit Different */
        diff>>>=1; /* Zero-extend right shift */
        out_mask<<=1; /* Shift over to next output bit */
      } /* End of all bits, or no more 0 bits */

      out_mask=1;
      diff=dout ^ dout2; /* diff is a bitmask of changes to D inputs */
      for (i=0;(i<num_sides) && (diff != 0);i++){ /* Run until no diffs */
        if (0!=(diff & 1)){
          if (0!=set_input(cm[cellbase+nbr_start+i],0,cm[cellbase+nbr_mask_start+i],(dout&out_mask)))
             return(1);
        } /* End of If Bit Different */
        diff>>>=1;
        out_mask<<=1; /* Shift over to next output bit */
      } /* End of all bits, or no more 0 bits */

      return(0); /* End of SET queue event processing */
    } else if (0!=(cmd & CLOCK1)){ /* HI clock tick */
                              /* Set output on C=1 sides to prog:127 */
       if (0!=do_tickhi()) return(1);
       return(0);
    } else if (0!=(cmd & CLOCK0)){ /* LO clock tick */
                              /* shift program in C-mode cells */
       if (0!=do_ticklo()) return(1);
       return(0);
    } else { /* Should be a zeroed out command. Flush its args */
      qe=remqueue(); /* cellbase */
      qe=remqueue(); /* cmask */
      qe=remqueue(); /* dmask */
    }
    return(0);
  }


/*** C-MODE ROUTINES ***/

  int isin_cmode(int cellbase) /* is cell in c-mode? */
  {
    return((0!=cm[cellbase+c_in])?1:0);
  }

  int do_tickhi() /* set outputs on c-mode cells */
/* Sweep through the c_arr to pull out indicies for cells in c-mode */
  {
    int i,cellbase;

    for (i=0;i<c_arr_num;i++){
      cellbase=c_arr[i];
/* Store a one if any D input=1 where a C input=1 */
      cm[cellbase+c_latch]=(0!=(cm[cellbase+c_in] & cm[cellbase+d_in]))?1:0;
    }
    return(0);
  }

  int do_ticklo() /* process TICK low across grid */
  {
    int i,cellbase;
    
    for (i=0;i<c_arr_num;i++){
      cellbase=c_arr[i];
      if (cm[cellbase+c_latch] != 2){ /* Cell was in c-mode on last uptick */
        shift(cm[cellbase+c_latch],cellbase); /* Shift in saved bit */
        if (0!=set_dout_to_tt127(cellbase)) return(1); /* set DOUT(s) from new TT */
      }
    }

    return(0);
  }

/*
 * The following routine sets each DOUT where CIN=1. It records the
 * change, and sets neighbor's inputs as appropriate
 */
  int set_dout_to_tt127(int cellbase)
  {
    int bit,dout,dout2,diff,i;

/* Determine the MSB of the truth table */
    bit=(0!=(cm[cellbase+tt_d_start+(ttsize-1)] & (1<<num_sides-1)))?1:0;

/* and send this to each D output where C out=1 */
    dout2=cm[cellbase+d_out]; /* Currend D outputs */
    dout=cm[cellbase+d_out]=(0!=bit)?cm[cellbase+c_in]:0;

/* We need to set neighboring cell's inputs if they've changed */
    diff=dout ^ dout2;
    for (i=0;(i<num_sides) && (diff != 0);i++){
      if (0!=(diff & 1)){
        if (0!=set_input(cm[cellbase+nbr_start+i],0,cm[cellbase+nbr_mask_start+i],bit))
          return(1);
      } /* End of If Bit Different */
      diff>>>=1;
    }
    return(0);
  }

  void shift(int bit, int cellbase) /* Shift in bit to LSB of truth table */
/* Later, let's make this responsive to a shift/noshift flag */
  {
    int temp,base,newbit,i;
    int mask1,mask0;

    record_change(cellbase);
    mask1=1<<num_sides; /* AND with shifted truthtable row to keep MSB+1 */
    mask0=~mask1; /* AND with shifted TT row to drop MSB+1 */
    base=cellbase+tt_c_start; /* Current location in truth table */
    newbit=bit; /* Value to shift into LSB */

    for (i=0;i<2*ttsize;i++){
      temp=(cm[base]<<1) | newbit; /* Shift the row and set LSB */
      newbit=(0!=(temp & mask1))?1:0; /* Save bit which shifts out of MSB */
      cm[base++]=temp & mask0; /* Store shifted row, and bump TT row # (base) */
    }
  }

/* Record a change to a cell */
  void record_change(int cellbase)
  {
    if (cm[cellbase+chg_arr_ind] != -1) return; /* Already on change list */
    cm[cellbase+chg_arr_ind]=chg_arr_num; /* else record */
    chg_arr[chg_arr_num++]=cellbase; /* and add to list */
    return;
  }

/* return indicies of cell which has changed */
  int[] unload_changes()
  {
    int cellbase,i;
    int ret_ind[]=new int[dim];

    if (chg_arr_num==0){ /* No more changes! */
      ret_ind[0]=(-1);
      return(ret_ind);
    }
/* Unload top change */
    cellbase=chg_arr[--chg_arr_num];
    cm[cellbase+chg_arr_ind]=(-1); /* Mark cell unchanged */
/* Store indicies in return array */
    cellbase=(cellbase-1)/cellsize; /* Raw cell number */
    for (i=0;i<dim;i++){
      ret_ind[i]=cell_index_list[cellbase][i];
    }
    return(ret_ind);
  }
}

/* Class for reading queue entries */
class queue_entry{
  boolean empty; /* Queue is empty */
  int value; /* Actual value stored in queue */
}
