/*
 * wire-building class. Defines objects and methods for
 * cell matrix wire building
 *
 * Constructer:
 *  wire_build w=wire_build(cell_matrix_interface cmi,
 *                          String dir, int row, int col,
 *                          String config);
 *    where
 *         dir is N, S, W or E, the primary direction of the wire
 *         row and col locate the cell furthest in the CCW direction from dir
 *         config is "PC,CC" or "CC,PC" and is ordered starting with [row,col]
 *         config can also be "BRK,PC,CC" or "CC,PC,BRK" i.e., BRK must
 *         be adjacent to PC for now.
 *
 * Methods for w are as follows:
 *  w.init(); Hardwires the initial head cells
 *  w.init("CW","CCW"); Build initial cells in corner config
 *  w.init("X",TT,"CW/CCW/"); Build initial transfer cell and do transfer
 *  w.extend(); Extends wire in current direction
 *  w.extend(int n); Extends wire "n" steps
 *  w.extend_break(boolean flag); To include break line in extend() calls
 *  w.extend_wire(boolean flag); Should extend() be extendible?
 *  w.end_break();  Add the action end to the break line
 *  w.turn("CW" or "CCW"); Turns the wire accordingly
 *  w.preclear(boolean flag); Pre-clear on extend and turn
 *  w.transfer(String tt, String dir); Transfers TT to head of wire
 *      dir is "CW" or "CCW" or "" (@head)
 *  w.transfer(String tt);  Same as (tt,"")
 *  w.cond_preclear(String TT,"CW" or "CCW" or "");
 *    Check the truth table TT to see if any C outputs are ever asserted.
 *    If so, then preclear the cell one-spot ahead and in the indicated
 *    direction. This allows live cells to be guarded by an
 *    active guard cell which re-circulates the lvie cell's TT, but
 *    requires the live cell to be initially cleared
 *  w.rotate_transfer(boolean flag);
 *  cell_matrix_channel chan=w.get_channel("PC,CC,BRK");
 *      returns the channel allocated by the wire_build() class
 *
 */
