/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class extend{
  public static void main(String args[]){
    run("e",0,false,false);
    run("s",1,false,false);
    run("w",2,false,false);
    run("n",3,false,false);
    run("eflag",0,false,true);
    run("sflag",1,false,true);
    run("wflag",2,false,true);
    run("nflag",3,false,true);
    run("ep",0,true,false);
    run("sp",1,true,false);
    run("wp",2,true,false);
    run("np",3,true,false);
  }

  static void run(String fname,int rotate,boolean preclear,boolean flag)
  {
    cell_matrix_interface cmi;  // Main interface object
    cell_matrix_channel cc,pc;
    int i;

    fname="B//"+fname;

    cmi=new cell_matrix_interface(fname,"Wire extension");
    for (i=0;i<rotate;i++) cmi.rotate("cw");
    cc=cmi.assign_channel(144,0,"DW");
    pc=cmi.assign_channel(145,0,"DW");

    cc.set(true);
    if (preclear){
      pc.set("DN=W;CN=1;CE=1"); // preclear the next PC cell
    } else {
      pc.set("DN=W;CN=1");
    }
    cmi.cycle();

    cc.set(false);
    pc.set("DW=S;DS=WE~&;DE=WE&");
    cmi.cycle();  /* New CC head cell */

    cc.set(true);
    if (flag){
      pc.set("DW=E;DE=W;DN=1;CE=N;DS=1");  // DS=1 is the "in-use" hook
    } else {
      pc.set("DW=E;DE=W;DN=1;CE=N");
    }
    cmi.cycle();  /* New PC head cell */

    cc.set(false);pc.set(false);cmi.cycle();

    cmi.close();
  }
}
