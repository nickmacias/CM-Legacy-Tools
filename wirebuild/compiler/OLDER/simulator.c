#include <stdio.h>
#include "engine.h"
#include "eng_def.h"
#include <string.h>
#include <fcntl.h>
#ifdef PC 
#include <io.h>
#endif

#define HEIGHT 2000
#define WIDTH 2000
#define QSIZE 8000000
#define C_ARR_SIZE 8000000

int load(int,int,char *);
int *tabulate(char *);
int comp(int,int);
int rread(int,char *,int); 
int wwrite(int,char *,int);
int iread();
int init_cmode(int,int);
void iwrite(int,int);
int dump(int,int);
int smalldump(int,int);
int set(int,int,char *,int);
int picdump(int,int,int,int);
int user_set_input(int,int,int,int);
int init_grid(int,int,int,int);
int read_cell(int,int,char *,char *,char[16]);
int do_tick(int);
int unload_chg(int *,int *, int *,int *);
int write_cell(int,int,char[16]);
int write_cell_all(int,int,char[16],char,char);
int raw_write(int,int,int,int,char *);
int bin_read(int,int,char *);
int raw_read(int,int,char *);
int process_queue_head();
int process_queue(); 
char *bconv(char *);

FILE *fpstack[10];
int fpsp=0;

void fppush(FILE *fp)
{
  if (fpsp>9){
    fprintf(stderr,"Too many nested @ files\n");
    fpsp=0;
    return;
  }
  fpstack[fpsp++]=fp;
}

FILE *fppop()
{
  if (fpsp==0){
    fprintf(stderr,"Stack underflow\n");
    return(stdin);
  }
  return(fpstack[--fpsp]);
}

/* Stuff for command file execution */
FILE *fpin;
int doingfp=0;
int isr=1,isc=1; /* Size of sueprcell */
int ir=0,ic=0;  /* Supercell number for relative addressing */
int ibr=0,ibc=0; /* Base for supercell indexing */

int watch_flag=0;	/* Set when we're watching an output */
int watch_row,watch_col;	/* [r,c] of watched cell */
int watch_mask;	/* eg. COE */
int watch_val;	/* 0 or 1:current value */
int loopout=1; /* 1 means loop detect, 0 means don't */
int zero_queue=0; /* 1 means enable, 0 means disable */
/* When enabled, events added to the queue will
   zero out similar pending events */
char watch_name[8]; /* Name of watched side */

main(argc,argv)
int argc;
char **argv;
{
  char buffer[1024],direc[5],fname[1024];
  char in,out,program[16],*stat;
  int i,j,k,l,val,a1,a2,a3,a4,left,leftsize;         
  int p1,p2,p3,p4,dopicdump=0;
  int clock; 
  
  clock=0; /* initial clock state */
  if (init_grid(HEIGHT,WIDTH,QSIZE,C_ARR_SIZE)) printf("Grid init failed!\n");
  printf("\n%d > ",get_timestamp());
  fpin=stdin;
  if (argc == 2){
    sprintf(buffer,"@%s\n",argv[1]);
    stat=(char *)1; /* Anything not NULL */
  } else stat=fgets(buffer,1024,fpin);

  while (NULL != stat){
    buffer[-1+strlen(buffer)]='\0';
    if (fpin != stdin) printf("%s",buffer);
    switch (buffer[0]){
    case ';':break;
    case '#':break;
    case 'Q':return(0);
    case 'A':
             if (1 != sscanf(buffer,"A %s",fname)){fname[0]='\0';}
             show_all(fname);
             break;
    case 'i':
      if (2==sscanf(buffer,"ib %d %d",&i,&j)){
        printf("Supercell base set to [%d,%d]\n",i,j);
        ibr=i;ibc=j;
        break;
      }
      if (2==sscanf(buffer,"is %d %d",&i,&j)){
        printf("Supercell size set to %d row(s), %d col(s)\n",i,j);
        isr=i;isc=j;
        break;
      }
      if (2==sscanf(buffer,"i %d %d",&i,&j)){
        if ((isc==1) || (isr==1))
          printf("WARNING: Index size has not been set!\n");
        printf("Relative index set to supercell[%d,%d]\n",i,j);
        ir=i;ic=j;
        break;
      }
      break;
    case 'K':zero_queue=1-zero_queue;
      peek_set(zero_queue);
      printf("Queue zeroing is %s.\n",(zero_queue)?"enabled":"disabled");
      break;
    case 'L':loopout=1-loopout;
      printf("Feedback loops %s be detected\n",(loopout)?"will":"will not");
      break;
    case 'I':
      if (init_grid(HEIGHT,WIDTH,QSIZE,C_ARR_SIZE)) printf("Grid init failed!\n");
      break;
    case 'S':   /* Sequence file */
             if (1==sscanf(buffer,"S %s",fname)){
                   if (seq_file(fname)) fprintf(stderr,"Failed\n");
             }
             else printf("?\n");
             break;
    case '!':system(&buffer[1]);
             break;
    case '@':fppush(fpin);
             fpin=fopen(&buffer[1],"r");
             if (fpin <= (FILE *)NULL){
               fprintf(stderr,"ERROR: Can not open %s\n",&buffer[1]);
               fpin=fppop();
             }
             break;
    case 'c':if (2==sscanf(buffer,"c %d %d",&i,&j)){
        fixup(&i,&j);
        if (comp(i,j)) fprintf(stderr,"Failed\n");
      }
      else printf("?\n");
      break;
    case 'H':if (2==sscanf(buffer,"H %d %d",&i,&j)){
        fixup(&i,&j);
        if (HEXdump(i,j)) fprintf(stderr,"Failed\n");
      }
      else printf("?\n");
      break;
    case 'h':if (2==sscanf(buffer,"h %d %d",&i,&j)){
        fixup(&i,&j);
        if (hexdump(i,j)) fprintf(stderr,"Failed\n");
      }
      else printf("?\n");
      break;
    case 'd':if (2==sscanf(buffer,"d %d %d",&i,&j)){
        fixup(&i,&j);
        if (dump(i,j)) fprintf(stderr,"Failed\n");
      }
      else printf("?\n");
      break;
    case 's':if (4==sscanf(buffer,"s %d %d %s %d",&i,&j,direc,&val)){
        fixup(&i,&j);
        if (set(i,j,direc,val)) fprintf(stderr,"Failed\n");
      }
      else printf("?\n");
      break;
    case 'o':if (2==sscanf(buffer,"o %d %d",&i,&j)){
        fixup(&i,&j);
        if (smalldump(i,j)) fprintf(stderr,"Failed\n");
      }
      else printf("?\n");
      break;
    case 'b':if (2==sscanf(buffer,"b %d %d",&i,&j)){
        fixup(&i,&j);
        if (read_cell(i,j,&in,&out,program)) fprintf(stderr,"Failed\n");
        bdump(i,j,program);
      }
      else printf("?\n");
      break;
    case 'r':if (1==sscanf(buffer,"r %d",&j)){
        left=0;leftsize=j/80;
        for (i=0;i<j;i++){
         for (k=0;k<128;k++){
          if (do_tick(1))
            fprintf(stderr,"TICK ERR\n");
          if (process_queue())
           fprintf(stderr,"FAILURE IN PROCESS_QUEUE()\n");
          if (do_tick(0))
            fprintf(stderr,"TICK ERR\n");
          if (process_queue())
           fprintf(stderr,"FAILURE IN PROCESS_QUEUE()\n");

          if ((watch_flag==1) || ((watch_flag==2) && (k==127))){
            read_cell(watch_row,watch_col,&in,&out,program);
            l=(out&watch_mask)?1:0;
            if (l != watch_val){ /* changed! */
              watch_val=l;
              printf("Watched output [%d,%d]%s has changed to %d\n",watch_row,watch_col,watch_name,l);
              if (k != 127){
                printf("WARNING: Stopping mid-cycle (cycle # %d)\n",k);
              }
              watch_val=l;
              goto OUT;
            }
           }

         }

         if (++left >= leftsize){
           left=0;printf(".");fflush(stdout);
         }
        }
      } else printf("?\n");
OUT:
      break;
    case 'p':if (4==sscanf(buffer,"p %d %d %d %d",&a1,&a2,&a3,&a4)){
               fixup(&a1,&a2);fixup(&a3,&a4);
               if (picdump(a1,a2,a3,a4))
                 fprintf(stderr,"Failed\n");
             }
             break;
 
    case 'P':if (4==sscanf(buffer,"P %d %d %d %d",&p1,&p2,&p3,&p4)){
               dopicdump=1;
             } else dopicdump=0;
             break;
    case 'l':if (3==sscanf(buffer,"l %d %d %s",
                &a1,&a2,fname)){
               fixup(&a1,&a2);
                   if (bin_read(a1,a2,fname)) fprintf(stderr,"Failed\n");
             }
             else printf("?\n");
             break;
    case '>':if (5==sscanf(buffer,"> %d %d %d %d %s",
                &a1,&a2,&a3,&a4,fname)){
                fixup(&a1,&a2);
                   if (raw_write(a1,a2,a3,a4,fname)) fprintf(stderr,"Failed\n");
             }
             else printf("?\n");
             break;
    case '<':if (3==sscanf(buffer,"< %d %d %s",
                &a1,&a2,fname)){
                fixup(&a1,&a2);
                   if (raw_read(a1,a2,fname)) fprintf(stderr,"Failed\n");
             }
             else printf("?\n");
             break;
    case 'w':
    case 'W':
          watch_flag=(buffer[0]=='w')?1:2;
          buffer[0]='w';
          if (3==sscanf(buffer,"w %d %d %s",&i,&j,direc)){
            fixup(&i,&j);
            watch_row=i;watch_col=j;
            strcpy(watch_name,direc); /* Save for later output */
            watch_mask=(-1);
            if (0 == strcmp(direc,"ce")) watch_mask=COE;
            if (0 == strcmp(direc,"cn")) watch_mask=CON;
            if (0 == strcmp(direc,"cs")) watch_mask=0xff&((unsigned int)COS);
            if (0 == strcmp(direc,"cw")) watch_mask=COW;
            if (0 == strcmp(direc,"de")) watch_mask=DOE;
            if (0 == strcmp(direc,"dn")) watch_mask=DON;
            if (0 == strcmp(direc,"ds")) watch_mask=DOS;
            if (0 == strcmp(direc,"dw")) watch_mask=DOW;
            read_cell(i,j,&in,&out,program);
            watch_val=(out&watch_mask)?1:0;
            if (watch_mask == (-1)){
              watch_flag=0;
              printf("Cancelling watch\n");
            }
          } else {
            watch_flag=0;
            printf("Cancelling watch\n");
          }
          break;

    case 'v': /* Do vector I/O */
          j=0;
          for (i=2;i<strlen(buffer);i++){
            if (buffer[i]=='0') k=0;
            else if (buffer[i]=='1') k=1;
            else {
              fprintf(stderr,"Usage: v xxxxxxx  where each x is 1 or 0\n");
              break;
            }
            set(270*(j++)+73,0,"dw",k);
            process_queue();
          }

/* Now display output vector */
          printf ("<");
          for (j=73;j<HEIGHT;j+=270){
            read_cell(j,0,&in,&out,program);
            if (out&DOW) printf("1"); else printf("0");
          }
          printf ("%d >\n",get_timestamp());
          break;
    case 't':
          if (1==sscanf(buffer,"t %d",&a1)){a1=a1*2;} else {a1=1;}
          for (a2=0;a2<a1;a2++){
            clock=1-clock;printf("Clock=%d\n",clock);
            if (do_tick(clock))
              fprintf(stderr,"TICK ERR\n");
            if (process_queue())
              fprintf(stderr,"FAILURE IN PROCESS_QUEUE()\n");;
          }
        break;
    case '\n':
    case '\0':
              break;
    default:
        printf("? l [r] [c] fname=load\n  d [r] [c]=dump TT\n  b [r] [c]=Bool dump\n");
	printf("  h [r] [c] =Hex dump of cell at (r,c)\n");
	printf("  i [r][c] =Set relative index to supercell [r,c]\n");
        printf("  is [r][c] =Set supercell size to r and c\n");
	printf("  H [r] [c] =Custom (int) dump of cell at (r,c)\n");
	printf("  t [n] =single clock transition\n");
	printf("  I =Re-init grid\n");
	printf("  c [r] [c] =compile cell at (r,c)\n");
        printf("  s [r] [c] [c|d][n|s|w|e] [0|1]=set input\n  o [r] [c]=output single cell\n");
        printf("  r [n]=run n*128 ticks\n  p [r1] [c1] [r2] [c2]=picdump\n");
        printf("  P [r1] [c1] [r2] [c2]=Persistent picdump\n");
        printf("  A [fname] Post-build dump of all supercells [to a file]\n");
        printf("  > [r] [c] [h] [w] fname=store binary grid state\n");
        printf("  < [r] [c] fname=retrieve binary grid state\n");
        printf("  S sequence-filename=Execute sequence file\n");
        printf("  w [r] [c] [c|d][n|s|w|e]=watch output\n");
        printf("  W=watch only after full 128-tick cycle\n");
        printf("  v=Set a vector of supercell inputs, flush queue and display output vector\n");
        printf("  Q=Exit immediately\n");
        printf("  @filename  executes commands from filename\n");
        printf("  !command executes command on the OS\n\n");
    }
    if (process_queue()) fprintf(stderr,"ERROR: Failed while flushing queue\n");
    if (dopicdump){
      printf("\n");
      a1=p1;a2=p2;a3=p3;a4=p4;
      fixup(&a1,&a2);fixup(&a3,&a4);
      picdump(a1,a2,a3,a4);
    }
    
    printf("\n%d >",get_timestamp());for (i=0;i<fpsp;i++) printf(">");printf(" ");
    if ((ic!=0)||(ir!=0)|(ibr != 0)|(ibc!=0))
      printf("[%d,%d]+[%d,%d] ",ibr,ibc,ir,ic);
    stat=fgets(buffer,1024,fpin); /* Next line */
    while ((stat==NULL) && (fpsp > 0)){
      if (stat==NULL){ /* End of current file */
        if (fpsp>0){
          fclose(fpin);
          printf("<<<EOF>>>\n");
          fpin=fppop(); /* Previous fpin */
          stat=fgets(buffer,1024,fpin); /* Next line */
        } /* else stack is empty-allow normal exit */
      } /* else process next line normally */
    } /* Ready to return to top of main loop */
  }
  printf("^D\n");
  return(0);
}

fixup(r,c)
int *r,*c;
{
  (*r)+=isr*ir+ibr;
  (*c)+=isc*ic+ibc;
}

int hexdump(i,j)  /* output hex version of program */
int i,j;
{
  char program[16],in,out;
  if ((i < 0) || (i >= HEIGHT) || (j < 0) || (j >= WIDTH)){
    printf("Bad indices\n");
    return(1);
  }
  if (read_cell(i,j,&in,&out,program)) return(1);
  printf("\n[%d,%d]=0x",i,j);
  for (i=15;i>=0;i--){
    printf("%01x",0x0f&(program[i]>>4));
    printf("%01x",0x0f&program[i]);
    if (0==i%2) printf(" ");
  }
  printf("\n");
  return(0);
}

int HEXCONV(i,j)  /* Convert 8-bit serial ID to return value...-1 if invalid */
int i,j;
{
  char program[16],in,out;
  int sum,shift;

  if ((i < 0) || (i >= HEIGHT) || (j < 0) || (j >= WIDTH)){
    printf("Bad indices\n");
    return(1);
  }
  if (read_cell(i,j,&in,&out,program)) return(1);
  if (((program[15]>>6) & 3) != 3){
    return(-1); /* Invalid */
  }

  sum=0; /* This is the final output value */
  shift=0; /* Amount to shift bit by */
  for (i=13;i<16;i++){
    sum+=(((program[i]&3)?1:0) << shift);sum<<=1;
    sum+=((((program[i]>>2)&3)?1:0) << shift);sum<<=1;
    sum+=((((program[i]>>4)&3)?1:0) << shift);sum<<=1;
    if (i != 15) {sum+=((((program[i]>>6)&3)?1:0) << shift);sum<<=1;}
  }
  sum>>=1;
  return(sum);
}

int HEXdump(i,j)  /* Special output code */
int i,j;
{
  char program[16],in,out;
  if ((i < 0) || (i >= HEIGHT) || (j < 0) || (j >= WIDTH)){
    printf("Bad indices\n");
    return(1);
  }
  if (read_cell(i,j,&in,&out,program)) return(1);
  printf("\n[%d,%d]=",i,j);
  if (((program[15]>>6) & 3) != 3){
    printf("[INVALID]\n");
    return(0);
  }

  for (i=0;i<16;i++){
    printf("%c",(program[i]&3)?'1':'0');
    printf("%c",((program[i]>>2)&3)?'1':'0');
    printf("%c",((program[i]>>4)&3)?'1':'0');
    if (i != 15){
      printf("%c",((program[i]>>6)&3)?'1':'0');
    }
  }
  printf("\n");
  return(0);
}

int dump(i,j)  /* output program and data */
int i,j;
{
  char program[16],in,out;
  if ((i < 0) || (i >= HEIGHT) || (j < 0) || (j >= WIDTH)){
    printf("Bad indices\n");
    return(1);
  }
  if (read_cell(i,j,&in,&out,program)) return(1);
  printf("\nTRUTH TABLE [%d,%d]\n",i,j);
  printf("SNEW    CN CS CW CE DN DS DW DE\n");
  for (i=0;i<16;i++){
    printf("%d%d%d%d     %d  %d  %d  %d  %d  %d  %d  %d\n",
      (i>>3)&1,(i>>2)&1,(i>>1)&1,(i>>0)&1,
      program[i]&0x80?1:0,
      program[i]&0x40?1:0,
      program[i]&0x20?1:0,
      program[i]&0x10?1:0,
      program[i]&0x08?1:0,
      program[i]&0x04?1:0,
      program[i]&0x02?1:0,
      program[i]&0x01?1:0);
  }
  return(0);
}

/* Supercell structure */
struct sc_struct {
  char path[1024];
  int did;
  int valid;
  char f[3][1024];
  char out_a_n,out_a_s,out_a_w,out_a_e;
  char out_b_n,out_b_s,out_b_w,out_b_e;
  char out_c_n,out_c_s,out_c_w,out_c_e;
};

struct sc_struct sc[25][25];

/* show_all() checks path-building results and dumps in plain text */
/* check() checks to see if a cell is non-empty */
/* check2() checks the PC cell to see if the INUSE flag is asserted */

show_all(fname)
char *fname;
{
  int sc_row,sc_col,channel,i,j,k,sum,r,c;
  char str[16],in,out,program[16];
  char channels[]="ABC";
  char sides[]="ENWS";
  char running_buffer[1024]; /* Build path string here */
  FILE *fp;

  for (sc_row=0;sc_row<WIDTH/270-1;sc_row++){
    for (sc_col=0;sc_col<HEIGHT/270-1;sc_col++){

      sc[sc_row][sc_col].out_a_n=sc[sc_row][sc_col].out_a_s=sc[sc_row][sc_col].out_a_w=sc[sc_row][sc_col].out_a_e='\0';
      sc[sc_row][sc_col].out_b_n=sc[sc_row][sc_col].out_b_s=sc[sc_row][sc_col].out_b_w=sc[sc_row][sc_col].out_b_e='\0';
      sc[sc_row][sc_col].out_c_n=sc[sc_row][sc_col].out_c_s=sc[sc_row][sc_col].out_c_w=sc[sc_row][sc_col].out_c_e='\0';

/* Check truth table for [53,105]-[53,107]. If non-empty, decode and store */
/* functional block location */
/*
      r=ibr+isr*sc_row+53;
      c=ibc+isc*sc_col+105;
*/
      r=270*sc_row+53;
      c=270*sc_col+105;
      for (j=0;j<3;j++){
        read_cell(r,c+j,&in,&out,program);
        strcpy(sc[sc_row][sc_col].f[j],bconv(program));
      }

/* Compute DID */
/*
      r=ibr+isr*sc_row+20;
      c=ibc+isc*sc_col+62;
*/
      r=270*sc_row+20;
      c=270*sc_col+62; /* DID location */
      sc[sc_row][sc_col].did=HEXCONV(r,c);

/* Now extract any paths built through this supercell's steering block */
      strcpy(running_buffer,"");

      /* Check for DST wirings */
/* Base for Bank 1 wiring */
/*
      r=ibr+isr*sc_row+63;
      c=ibc+isc*sc_col+114;
*/
      r=270*sc_row+63;
      c=270*sc_col+114; /* Base for Bank 1 wiring */
      for (j=0;j<2;j++){ /* 2 passes */
        sum=0;
        for (i=0;i<5;i++){
          read_cell(r++,c,&in,&out,program);
          sum+=(((out&DOE)?1:0)<<i); /* = (MSB) S1 S0 C1 C0 inuse (LSB) */
        }
        if (sum&1){
          sprintf(str,"%c:Fin(%d)<-%c\n",channels[((sum>>1)&3)-1],j,sides[(sum>>3)&3]);
          strcat(running_buffer,str);
        }
        /* r=ibr+isr*sc_row+55; */ /* Second bank */
        r=270*sc_row+55; /* Second bank */
      }

/*** SET THE APROPRIATE VARIABLES THROUGHOUT THIS ***/

      /* Channel A, Side N */
      if (check(sc_row,sc_col,106,140)){
        if (check2(sc_row,sc_col,169,106,DOW)){
          strcat(running_buffer,"A:N<-W;");
          sc[sc_row][sc_col].out_a_n='w';
        }
        else if (check2(sc_row,sc_col,203,169,DOS)){
          strcat(running_buffer,"A:N<-S;");
          sc[sc_row][sc_col].out_a_n='s';
        }
        else if (check2(sc_row,sc_col,140,203,DOE)){
          strcat(running_buffer,"A:N<-E;");
          sc[sc_row][sc_col].out_a_n='e';
        }
        else if (check(sc_row,sc_col,106,140)){
          strcat(running_buffer,"A:N<-Fout;");
          sc[sc_row][sc_col].out_a_n='.';
        }
      }

      /* Channel B, Side N */
      if (check(sc_row,sc_col,110,146)){
        if (check2(sc_row,sc_col,163,110,DOW)){
          strcat(running_buffer,"B:N<-W;");
          sc[sc_row][sc_col].out_b_n='w';
        }
        else if (check2(sc_row,sc_col,199,163,DOS)){
          strcat(running_buffer,"B:N<-S;");
          sc[sc_row][sc_col].out_b_n='s';
        }
        else if (check2(sc_row,sc_col,146,199,DOE)){
          strcat(running_buffer,"B:N<-E;");
          sc[sc_row][sc_col].out_b_n='e';
        }
        else if (check(sc_row,sc_col,110,146)){
          strcat(running_buffer,"B:N<-Fout;");
          sc[sc_row][sc_col].out_b_n='.';
        }
      }

      /* Channel C, Side N */
      if (check(sc_row,sc_col,114,152)){
        if (check2(sc_row,sc_col,157,114,DOW)){
          strcat(running_buffer,"C:N<-W;");
          sc[sc_row][sc_col].out_c_n='w';
        }
        else if (check2(sc_row,sc_col,195,157,DOS)){
          strcat(running_buffer,"C:N<-S;");
          sc[sc_row][sc_col].out_c_n='s';
        }
        else if (check2(sc_row,sc_col,152,195,DOE)){
          strcat(running_buffer,"C:N<-E;");
          sc[sc_row][sc_col].out_c_n='e';
        }
        else if (check(sc_row,sc_col,114,152)){
          strcat(running_buffer,"C:N<-Fout;");
          sc[sc_row][sc_col].out_c_n='.';
        }
      }

      /* Channel A, Side S */
      if (check(sc_row,sc_col,179,169)){
        if (check2(sc_row,sc_col,140,179,DOE)){
          strcat(running_buffer,"A:S<-E;");
          sc[sc_row][sc_col].out_a_s='e';
        }
        else if (check2(sc_row,sc_col,130,140,DON)){
          strcat(running_buffer,"A:S<-N;");
          sc[sc_row][sc_col].out_a_s='n';
        }
        else if (check2(sc_row,sc_col,169,130,DOW)){
          strcat(running_buffer,"A:S<-W;");
          sc[sc_row][sc_col].out_a_s='w';
        }
        else if (check(sc_row,sc_col,179,169)){
          strcat(running_buffer,"A:S<-Fout;");
          sc[sc_row][sc_col].out_a_s='.';
        }
      }

      /* Channel B, Side S */
      if (check(sc_row,sc_col,177,163)){
        if (check2(sc_row,sc_col,146,177,DOE)){
          strcat(running_buffer,"B:S<-E;");
          sc[sc_row][sc_col].out_b_s='e';
        }
        else if (check2(sc_row,sc_col,132,146,DON)){
          strcat(running_buffer,"B:S<-N;");
          sc[sc_row][sc_col].out_b_s='n';
        }
        else if (check2(sc_row,sc_col,163,132,DOW)){
          strcat(running_buffer,"B:S<-W;");
          sc[sc_row][sc_col].out_b_s='w';
        }
        else if (check(sc_row,sc_col,177,163)){
          strcat(running_buffer,"B:S<-Fout;");
          sc[sc_row][sc_col].out_b_s='.';
        }
      }

      /* Channel C, Side S */
      if (check(sc_row,sc_col,175,157)){
        if (check2(sc_row,sc_col,152,175,DOE)){
          strcat(running_buffer,"C:S<-E;");
          sc[sc_row][sc_col].out_c_s='e';
        }
        else if (check2(sc_row,sc_col,134,152,DON)){
          strcat(running_buffer,"C:S<-N;");
          sc[sc_row][sc_col].out_c_s='n';
        }
        else if (check2(sc_row,sc_col,157,134,DOW)){
          strcat(running_buffer,"C:S<-W;");
          sc[sc_row][sc_col].out_c_s='w';
        }
        else if (check(sc_row,sc_col,175,157)){
          strcat(running_buffer,"C:S<-Fout;");
          sc[sc_row][sc_col].out_c_s='.';
        }
      }

      /* Channel A, Side W */
      if (check(sc_row,sc_col,169,118)){
        if (check2(sc_row,sc_col,191,169,DOS)){
          strcat(running_buffer,"A:W<-S;");
          sc[sc_row][sc_col].out_a_w='s';
        }
        else if (check2(sc_row,sc_col,140,191,DOE)){
          strcat(running_buffer,"A:W<-E;");
          sc[sc_row][sc_col].out_a_w='e';
        }
        else if (check2(sc_row,sc_col,118,140,DON)){
          strcat(running_buffer,"A:W<-N;");
          sc[sc_row][sc_col].out_a_w='n';
        }
        else if (check(sc_row,sc_col,169,118)){
          strcat(running_buffer,"A:W<-Fout;");
          sc[sc_row][sc_col].out_a_w='.';
        }
      }

      /* Channel B, Side W */
      if (check(sc_row,sc_col,163,122)){
        if (check2(sc_row,sc_col,187,163,DOS)){
          strcat(running_buffer,"B:W<-S;");
          sc[sc_row][sc_col].out_b_w='s';
        }
        else if (check2(sc_row,sc_col,146,187,DOE)){
          strcat(running_buffer,"B:W<-E;");
          sc[sc_row][sc_col].out_b_w='e';
        }
        else if (check2(sc_row,sc_col,122,146,DON)){
          strcat(running_buffer,"B:W<-N;");
          sc[sc_row][sc_col].out_b_w='n';
        }
        else if (check(sc_row,sc_col,163,122)){
          strcat(running_buffer,"B:W<-Fout;");
          sc[sc_row][sc_col].out_b_w='.';
        }
      }

      /* Channel C, Side W */
      if (check(sc_row,sc_col,157,126)){
        if (check2(sc_row,sc_col,183,157,DOS)){
          strcat(running_buffer,"C:W<-S;");
          sc[sc_row][sc_col].out_c_w='s';
        }
        else if (check2(sc_row,sc_col,152,183,DOE)){
          strcat(running_buffer,"C:W<-E;");
          sc[sc_row][sc_col].out_c_w='e';
        }
        else if (check2(sc_row,sc_col,126,152,DON)){
          strcat(running_buffer,"C:W<-N;");
          sc[sc_row][sc_col].out_c_w='n';
        }
        else if (check(sc_row,sc_col,157,126)){
          strcat(running_buffer,"C:W<-Fout;");
          sc[sc_row][sc_col].out_c_w='.';
        }
      }

      /* Channel A, Side E */
      if (check(sc_row,sc_col,140,215)){
        if (check2(sc_row,sc_col, 94,140,DON)){
          strcat(running_buffer,"A:E<-N;");
          sc[sc_row][sc_col].out_a_e='n';
        }
        else if (check2(sc_row,sc_col,169, 94,DOW)){
          strcat(running_buffer,"A:E<-W;");
          sc[sc_row][sc_col].out_a_e='w';
        }
        else if (check2(sc_row,sc_col,215,169,DOS)){
          strcat(running_buffer,"A:E<-S;");
          sc[sc_row][sc_col].out_a_e='s';
        }
        else if (check(sc_row,sc_col,140,215)){
          strcat(running_buffer,"A:E<-Fout;");
          sc[sc_row][sc_col].out_a_e='.';
        }
      }

      /* Channel B, Side E */
      if (check(sc_row,sc_col,146,211)){
        if (check2(sc_row,sc_col, 98,146,DON)){
          strcat(running_buffer,"B:E<-N;");
          sc[sc_row][sc_col].out_b_e='n';
        }
        else if (check2(sc_row,sc_col,163, 98,DOW)){
          strcat(running_buffer,"B:E<-W;");
          sc[sc_row][sc_col].out_b_e='w';
        }
        else if (check2(sc_row,sc_col,211,163,DOS)){
          strcat(running_buffer,"B:E<-S;");
          sc[sc_row][sc_col].out_b_e='s';
        }
        else if (check(sc_row,sc_col,146,211)){
          strcat(running_buffer,"B:E<-Fout;");
          sc[sc_row][sc_col].out_b_e='.';
        }
      }

      /* Channel C, Side E */
      if (check(sc_row,sc_col,152,207)){
        if (check2(sc_row,sc_col,102,152,DON)){
          strcat(running_buffer,"C:E<-N;");
          sc[sc_row][sc_col].out_c_e='n';
        }
        else if (check2(sc_row,sc_col,157,102,DOW)){
          strcat(running_buffer,"C:E<-W;");
          sc[sc_row][sc_col].out_c_e='w';
        }
        else if (check2(sc_row,sc_col,207,157,DOS)){
          strcat(running_buffer,"C:E<-S;");
          sc[sc_row][sc_col].out_c_e='s';
        }
        else if (check(sc_row,sc_col,152,207)){
          strcat(running_buffer,"C:E<-Fout;");
          sc[sc_row][sc_col].out_c_e='.';
        }
      }

      strcpy(sc[sc_row][sc_col].path,running_buffer);
      sc[sc_row][sc_col].valid=1;
    }
  }

/*** If outputting to file, use easier-to-parse format ***/
  if (0 != strlen(fname)){
    fp=fopen(fname,"w");
    fprintf(fp,"%d\n%d\n",WIDTH/270-1,HEIGHT/270-1);
  }


/* Now dump the structure */
  for (sc_row=0;sc_row<WIDTH/270-1;sc_row++){
    for (sc_col=0;sc_col<HEIGHT/270-1;sc_col++){
      if (0 == strlen(fname)){ /* Output to the screen */
        if (sc[sc_row][sc_col].did > 0){
          printf("[%d,%d]:DID=%d\n",sc_row,sc_col,
                                    sc[sc_row][sc_col].did);
        } /* end of DID valid */
        if (strlen(sc[sc_row][sc_col].path) > 0){
          printf("[%d,%d]:%s\n",sc_row,sc_col,
                                sc[sc_row][sc_col].path);
        } /* End of path not blank */
  
  /* Dump the functional spec */
        if (strlen(sc[sc_row][sc_col].f[0])+
            strlen(sc[sc_row][sc_col].f[1])+
            strlen(sc[sc_row][sc_col].f[2]) > 0){
          printf("f(0):%s\n",sc[sc_row][sc_col].f[0]);
          printf("f(1):%s\n",sc[sc_row][sc_col].f[1]);
          printf("f(2):%s\n",sc[sc_row][sc_col].f[2]);
        } /* End of function not blank */
      } /* End of screen output */ else {
        fprintf(fp,"%d\n%d\n%d\n%d\n%s\n%s\n%s\n",
          sc_row,sc_col,sc[sc_row][sc_col].did,sc[sc_row][sc_col].valid,
          sc[sc_row][sc_col].f[0],
          sc[sc_row][sc_col].f[1],
          sc[sc_row][sc_col].f[2]);

        fprintf(fp,"%c\n",sc[sc_row][sc_col].out_a_n); fprintf(fp,"%c\n",sc[sc_row][sc_col].out_a_s);
        fprintf(fp,"%c\n",sc[sc_row][sc_col].out_a_w); fprintf(fp,"%c\n",sc[sc_row][sc_col].out_a_e);
        fprintf(fp,"%c\n",sc[sc_row][sc_col].out_b_n); fprintf(fp,"%c\n",sc[sc_row][sc_col].out_b_s);
        fprintf(fp,"%c\n",sc[sc_row][sc_col].out_b_w); fprintf(fp,"%c\n",sc[sc_row][sc_col].out_b_e);
        fprintf(fp,"%c\n",sc[sc_row][sc_col].out_c_n); fprintf(fp,"%c\n",sc[sc_row][sc_col].out_c_s);
        fprintf(fp,"%c\n",sc[sc_row][sc_col].out_c_w); fprintf(fp,"%c\n",sc[sc_row][sc_col].out_c_e);
      } /* End of file output */
    } /* End of columns */
  } /* End of rows */
  if (0 != strlen(fname)) fclose(fp);
} /* End of show_all() */

/* check() checks to see if a cell is empty */
check(scr,scc,r,c)
int scr,scc,r,c;
{
  char in,out,program[16];
  int i;

/*
  r=ibr+isr*scr+r;
  c=ibc+isc*scc+c;
*/
  r=270*scr+r;
  c=270*scc+c;
  read_cell(r,c,&in,&out,program);
  for (i=0;i<16;i++){
    if (program[i]!='\0') return(1); /* Program is not empty */
  }
  return(0); /* Program is empty */
}

check2(scr,scc,r,c,side)
int scr,scc,r,c,side;
{
  char in,out,program[16];
  int i;

/* Adjust [r,c] for PC vs. CC */

  switch (side){
    case(DON):--r;break;
    case(DOS):++r;break;
    case(DOW):--c;break;
    case(DOE):++c;break;
  }

/*
  r=ibr+isr*scr+r;
  c=ibc+isc*scc+c;
*/
  r=270*scr+r;
  c=270*scc+c;
  read_cell(r,c,&in,&out,program);
  if (out & side) return(1);
  return(0);
}

smalldump(i,j)
int i,j;
{
  char program[16],in,out;

  if ((i < 0) || (i >= HEIGHT) || (j < 0) || (j >= WIDTH)){
    printf("Bad indices\n");
    return(0);
  }

  if (read_cell(i,j,&in,&out,program)) return(1);
  
  printf("\n OUTPUT[%d,%d] \n",i,j);
  printf("   %d %d\n",(out&DON)?1:0,(out&CON)?1:0);
  printf(" +-D-C-+\n");
  printf("%dD     D%d\n",(out&DOW)?1:0,(out&DOE)?1:0);
  printf("%dC     C%d\n",(out&COW)?1:0,(out&COE)?1:0);
  printf(" +-D-C-+\n");
  printf("   %d %d\n",(out&DOS)?1:0,(out&COS)?1:0);
  return(0);
}

int set(i,j,node,val)
int i,j,val;
char *node;
{
  char mask;
  if ((i < 0) || (i >= HEIGHT) || (j < 0) || (j >= WIDTH)){
    printf("Bad indices\n");
    return(1);
  }
  if (0 == strcmp(node,"ce")) mask=CIE;
  if (0 == strcmp(node,"cn")) mask=CIN;
  if (0 == strcmp(node,"cs")) mask=0xff&((unsigned int)CIS);
  if (0 == strcmp(node,"cw")) mask=CIW;
  if (0 == strcmp(node,"de")) mask=DIE;
  if (0 == strcmp(node,"dn")) mask=DIN;
  if (0 == strcmp(node,"ds")) mask=DIS;
  if (0 == strcmp(node,"dw")) mask=DIW;
  if (user_set_input(i,j,mask,val)){
     fprintf(stderr,"FAILURE IN USER_SET_INPUT()\n");
     return(1);
  }
  if (process_queue()){
     fprintf(stderr,"FAILURE IN PROCESS_QUEUE()\n");
     return(1);
  }
  return(0);
}

int picdump(p1,p2,p3,p4)  /* dump from [p1][p2] to [p3][p4] */
int p1,p2,p3,p4;
{
  int i,j;
  char in,out,program[16];

  if ((p1 < 0) || (p3 < 0) || (p1 >= HEIGHT) || (p3 >= HEIGHT)
   || (p2 < 0) || (p4 < 0) || (p2 >= WIDTH ) || (p4 >= WIDTH)){
    printf("Bad indices\n");
    return(1);
  }


  printf("OUTPUT grid, from [%d,%d] to [%d,%d]\n",p1,p2,p3,p4);
  for (j=p2;j<=p4;j++) printf("  D C   ");printf("\n");
  for (i=p1;i<=p3;i++){
      for (j=p2;j<=p4;j++) printf("-------+");printf("\n");
      for (j=p2;j<=p4;j++){
        if (read_cell(i,j,&in,&out,program)) return(1);
        printf("  %d %d  |",out&DON?1:0,out&CON?1:0);
      }
      printf("\n");
      for (j=p2;j<=p4;j++){
        if (read_cell(i,j,&in,&out,program)) return(1);
        printf("%d     %d|",out&DOW?1:0,out&DOE?1:0);
      }
      printf(" D\n");
      for (j=p2;j<=p4;j++){
        if (read_cell(i,j,&in,&out,program)) return(1);
        printf("%d     %d|",out&COW?1:0,out&COE?1:0);
      }
      printf(" C\n");
      for (j=p2;j<=p4;j++){
        if (read_cell(i,j,&in,&out,program)) return(1);
        printf("  %d %d  |",out&DOS?1:0,out&COS?1:0);
      }
      printf("\n");
  }
  return(0);
}

int bin_read(a1,a2,fname)    /* Read in output of LOADER for example */
int a1,a2;
char *fname;
{
  int i,j,ifp,a3,a4,desc_len,left,leftsize;
  char in,out,program[16],buffer[80],desc[1024];

  ifp=open(fname,O_RDONLY);
  if (ifp < 0){
    fprintf(stderr,"ERROR: ()Can not open %s\n",fname);
    return(1);
  }

  rread(ifp,buffer,80); /* read comment */ 
  buffer[79]='\0';
  
  a3=iread(ifp);
  a4=iread(ifp); /* read dims */

/*** KLUDGE !!! ***/

/***
a3=400;a4=240;
***/


 printf("Grid is %dx%d\n%s\n",a3,a4,buffer);
 left=0;leftsize=a3/80;
 for (i=a1;i<a1+a3;i++){
    if (++left >= leftsize){left=0;printf(".");fflush(stdout);}
    for(j=a2;j<a2+a4;j++){
      rread(ifp,&in,1);
      rread(ifp,&out,1);
      rread(ifp,program,16); 
      desc_len=iread(ifp); /* read # bytes in desc */
      if (desc_len > 1023) desc_len=1023;
      if (desc_len > 0) rread(ifp,desc,desc_len); /* and read description */
      else strcpy(desc,"");
      desc[1023]='\0';                 
      if (write_cell(i,j,program)) return(1); /* No in/out recording! */
      if (process_queue()) return(1);
    }
  }
  close(ifp);
  return(0);
}

int raw_read(a1,a2,fname)    /* read previously-saved grid state */
int a1,a2;
char *fname;
{
  int i,j,ifp,a3,a4,left,leftsize;
  char in,out,program[16],buffer[80],desc_len[2];

  ifp=open(fname,O_RDONLY);
  if (ifp < 0){
    fprintf(stderr,"ERROR: ()Can not open %s\n",fname);
    return(1);
  }

  printf("Make sure you only use this command on a fresh grid!\n");


  rread(ifp,buffer,80); /* read comment */ 
  buffer[79]='\0';
  if (0 != strcmp(buffer,"BINARY GRID<<<!>>>")){
    fprintf(stderr,"ERROR: %s is not a binary grid file\n",fname);
/*    return(1); */
  }
  
  a3=iread(ifp);
  a4=iread(ifp); /* read dims */

 printf("Grid is %dx%d\n",a3,a4);
 left=0;leftsize=a3/80;
 for (i=a1;i<a1+a3;i++){
    if (++left >= leftsize){left=0;printf(".");fflush(stdout);}
    for(j=a2;j<a2+a4;j++){
      rread(ifp,&in,1);
      rread(ifp,&out,1);
      rread(ifp,program,16); 
      rread(ifp,desc_len,2);
      if (write_cell_all(i,j,program,in,out)) return(1);
      if (0 != init_cmode(i,j)){
        fprintf(stderr,"ERROR: Too many c-mode cells?\n");
        return(1);
      }
    }
  }
  close(ifp);
  return(0);
}

int raw_write(a1,a2,a3,a4,fname)    /* save grid state */
int a1,a2,a3,a4;
char *fname;
{
  int i,j,ifp,left,leftsize;
  char in,out,program[16],buffer[80],zero[2];

  ifp=open(fname,O_WRONLY|O_CREAT|O_TRUNC,0755);
  if (ifp < 0){
    fprintf(stderr,"ERROR: ()Can not open %s\n",fname);
    return(1);
  }

  printf("Make sure you only do this when CLOCK=LOW and the event queue is empty!\n");

  strcpy(buffer,"BINARY GRID<<<!>>>");
  wwrite(ifp,buffer,80); /* read comment */ 
  
  iwrite(ifp,a3);
  iwrite(ifp,a4);

  zero[0]=zero[1]='\0';

  printf("Saving %dx%d grid state\n",a3,a4);
  left=0;leftsize=a3/80;
  for (i=a1;i<a1+a3;i++){
    if (++left >= leftsize){left=0;printf(".");fflush(stdout);}
    for(j=a2;j<a2+a4;j++){
      if (read_cell(i,j,&in,&out,program)) return(1);
      wwrite(ifp,&in,1);
      wwrite(ifp,&out,1);
      wwrite(ifp,program,16); 
      wwrite(ifp,zero,2);  /* Description length */
    }
   }
   close(ifp);
   return(0);
}

int process_queue() /* Go through entire event queue */
{
  int status;
  int unstable;
  int r,c;
  unstable=0;
  while (0 == (status=process_queue_head())){
   if (loopout){
    if (++unstable > 5000000){
      fprintf(stderr,"Unstable grid?\n");
      last_queue(&r,&c);
      printf("Unstable grid: Last event at [%d,%d]\n",r,c);
      return(1);
    }
   }
  }
  if (status==99) return(0); /* queue empty...normal completion */
  fprintf(stderr,"ERROR: Process queue returned %d\n",status);
  return(status); /* else abnormal failure */
}

int comp(r,c)
int r,c;
{
/* Compile a TT here */

  char buffer[1024],program[16],temp[1024];
  int i,*array,ind,j;

  for (i=0;i<16;i++) program[i]='\0';
  buffer[0]='\0'; /* start w/empty string */
  
  printf("Enter logical expressions, end with single \".\" in column 1:\n");
  while (0 != strcmp(buffer,".")){
    printf("C> ");
    if (NULL != fgets(buffer,1024,fpin)){
      buffer[-1+strlen(buffer)]='\0'; /* no NL */
      printf("%s\n",buffer);
    } else {
      printf("Premature EOF\n");
    }
    
    if (0==strcmp(buffer,".")) continue;

    for (i=0;i<strlen(buffer);i++) buffer[i]=toupper(buffer[i]);

/* Check for .HEX command */
    strcpy(temp,buffer);temp[4]='\0';
    if (0==strcmp(temp,".HEX")){
/* Remove spaces and copy to temp[] */
      j=0;strcpy(temp,"");
      for (i=4;i<strlen(buffer);i++)
        if (buffer[i] != ' ') temp[j++]=buffer[i];
      temp[j]='\0';
/*
 * Want to change ordering so .HEX abcd means bits shifted out are abcd
 * Set cellTT[i]=temp[30-i] (i even)
 * cellTT[i]=temp[32-i] (i odd)
 */

      for (i=0;i<16;i++){
        j=temp[31-2*i]-'0'; /* Low nibble */
        if (j >9) j=j+'0'+10-'A';
        program[i]=j;
        j=temp[30-2*i]-'0'; /* high nibble */
        if (j >9) j=j+'0'+10-'A';
        program[i]|=(j<<4);
      }
    } else {

      switch (buffer[0]){
        case 'D':ind=0;break;
        case 'C':ind=4;break;
        default:ind=(-100);break;
      }
  
      switch (buffer[1]){
        case 'E':ind+=0;break;
        case 'W':ind+=1;break;
        case 'S':ind+=2;break;
        case 'N':ind+=3;break;
        default:ind=(-100);break;
      }
  
      if (buffer[2]!='='){
        ind=(-100);
      }
  
      if (ind < 0){
        printf("? Syntax error on LHS\n");
        continue;
      }
  
/* Here, we know correct index...compile the TT column */

      array=tabulate(&buffer[3]);
      if (array==0){
        printf("? Syntax error on RHS\n");
        continue;
      }

/* Now load it in */
      for (i=0;i<16;i++){
        program[i]&=(0xff^(1<<ind)); /* Clear indicated bit */
        if (array[i]==1) program[i]|=(1<<ind); /* and set if bit should=1 */
      }
    }
  }
  printf("\n");
  if (write_cell(r,c,program)) return(1); /* No in/out recording! */
  return(0);
}

int seq_file(fname)
char *fname;
{
  FILE *fp;
  int num_channels,rows[32],cols[32],masks[32],i,j,val;
  int cycle_number;
  int ind,mask; /* For picking pieces out of tts */
  char tts[32][128]; /* Stores each TT to output */
  char buffer[1024],side[8],c;

  fp=fopen(fname,"r");
  if (fp <= (FILE *) NULL){
    fprintf(stderr,"Can not open %s\n",fname);
    return(1);
  }

  cycle_number=0;

/* Input the header */
  fgets(buffer,1024,fp); /* header */
  buffer[16]='\0';
  if (0 != strcmp(buffer,"\"#SEQUENCE FILE\"")){
    fprintf(stderr,"%s does not appear to be a valid sequence file\n",fname);
    fclose(fp);
    return(1);
  }

  fgets(buffer,1024,fp); /* legal info */
  fgets(buffer,1024,fp); /* misc flags */
  fgets(buffer,1024,fp);sscanf(buffer,"\"%d\"",&num_channels);
  printf("File contains %d channel%s\n",num_channels,(num_channels==1)?"":"s");

/* Read each channel's assignment */
  for (i=0;i<num_channels;i++){
    fgets(buffer,1024,fp);
    sscanf(buffer,"\"%d,%d,%s\"",&rows[i],&cols[i],side);
    side[2]='\0';
/* Convert side to mask */
    if (0==strcmp(side,"CE")) masks[i]=CIE;
    if (0==strcmp(side,"CN")) masks[i]=CIN;
    if (0==strcmp(side,"CS")) masks[i]=0xff& ((unsigned int) CIS);
    if (0==strcmp(side,"CW")) masks[i]=CIW;
    if (0==strcmp(side,"DE")) masks[i]=DIE;
    if (0==strcmp(side,"DN")) masks[i]=DIN;
    if (0==strcmp(side,"DS")) masks[i]=DIS;
    if (0==strcmp(side,"DW")) masks[i]=DIW;
  } /* All channels assigned now */

/* Now we execute the sequence */
  while (1==seq_gets(tts[0],fp)){ /* seq_gets skips comments */
    for (i=1;i<num_channels;i++){
      seq_gets(tts[i],fp);
    }

    mask=1;
/* Now step the clock through these cycles */
    for (i=0;i<128;i++){ /* Clock tick */
      ind=1+(i>>2); /* Index into tts string */
      mask=mask>>1;
      if (mask==0) mask=8; /* mask to pick off bit from nibble */
      for (j=0;j<num_channels;j++){
/* Find the value to set for channel j, step i */
        c=tts[j][ind]; /* This is the nibble */
        val=c-'0';if (val > 9) val=10+c-'A'; /* Convert to integer */
        val=(val&mask)?1:0;
        user_set_input(rows[j],cols[j],masks[j],val);
      } /* End of all channels */
      if (0!=process_queue()) return(1);
      do_tick(1);if (0 != process_queue()) return(1);
      do_tick(0);if (0 != process_queue()) return(1);
    } /* End of all ticks */
    if ((++cycle_number)%100 == 0){
      printf("\rCycle %d",cycle_number);fflush(stdout);
    }
/***
    if (cycle_number%5000 == 0){
      sprintf(buffer,"Save_%d.sta",cycle_number);
      raw_write(0,0,200,200,buffer);
    }
***/
  } /* End of file */

  printf("\nSequence file finished\n");
  fclose(fp);
  return(0);
}

int seq_gets(buffer,fp)
char *buffer;
FILE *fp;
{
  if (NULL == fgets(buffer,1024,fp)) return(0); /* EOF */

  while (buffer[1] == '#'){
/* Echo the comment */
/***
    buffer[-1+strlen(buffer)]='\0';
    printf("%s\r",buffer);
***/
    if (NULL == fgets(buffer,1024,fp)) return(0); /* EOF */
  }

  return(1); /* Normal read */
}
