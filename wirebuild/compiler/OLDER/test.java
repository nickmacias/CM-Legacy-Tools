class test{
  public static void main(String args[]) {
    int i;
    cell_matrix_channel pc,cc,pc_cc,cc_cc;

    cell_matrix_interface cmi=new cell_matrix_interface("test.seq","Test");

    pc=cmi.assign_channel(0,0,"DW");
    pc_cc=cmi.assign_channel(0,0,"CW"); /* For programming initial PC cell */
    cc=cmi.assign_channel(1,0,"DW");
    cc_cc=cmi.assign_channel(1,0,"CW");

/* Initialize head cells */
    pc_cc.set(true);
    pc.set("CE=1;"); // pre-clear
    cc_cc.set(true);
    cc.set("DW=N;DN=WE~&;DE=WE&");
    cmi.cycle();
cmi.comment("GO");

    cc_cc.set(false);
    pc_cc.set(false);
    cmi.cycle();

    pc_cc.set(true);
    pc.set("DW=E;DE=W;DS=1;CE=S");
    cmi.cycle();

/* We're done directly controlling head cells */
    pc_cc.set(false);

/* Call build east method */
    for (i=0;i<5;i++)
      build_east(cmi,pc,cc);

/* Done! */
    cmi.close();
  }

  static void build_east(cell_matrix_interface cmi,
                 cell_matrix_channel pc,
                 cell_matrix_channel cc){
    cc.set(true);
    pc.set("DS=W;CS=1;CE=1"); /* Includes pre-clear */
    cmi.cycle();

    cc.set(false);
    pc.set("DW=N;DN=WE~&;DE=WE&"); // New CC cell
    cmi.cycle();

    cc.set(true);
    pc.set("DW=E;DE=W;DS=1;CE=S"); // New PC cell
    cmi.cycle();

    cc.set(false);pc.set(false);cmi.cycle();
  }

}
