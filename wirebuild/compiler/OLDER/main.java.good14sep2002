/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;

/* Drawing class */
class window_class extends Frame
{
  static int fillsize=30; /* Size of squares+separation */
  static int fillsep=4; /* Space between squares */
  static int display_mode=1;
  static int xoffset=10;
  static int yoffset=35;
  static int DIM; /* # of dimensions in matrix */
  static int DIMS[]; /* Array of dimensions */

/* 3D display parameters */
  static int xmin,xmax,ymin,ymax; /* For 2D display */
  static int xv,yv,zv; /* Center of 3D viewport */

  cell_matrix_sim sim;
  cell_exchange ce;
  Color empty_color,c_color,d0_color,d1_color;
  int size_x,size_y;
  Graphics saveg=null;

  window_class(cell_matrix_sim s, cell_exchange c)
  {
/* Grab info from simulator/etc. class */
    sim=s;
    ce=c;
    DIM=sim.get_dim();
    DIMS=sim.get_dims();

/* Color setups */
    empty_color=new Color(0,0,0);
    c_color=new Color(255,0,0);
    d0_color=new Color(0,128,0);
    d1_color=new Color(0,255,0);
    setBackground(empty_color);

/* Set size of window */
    size_y=400;
    size_x=400;
    setSize(size_x,size_y);

/* This should be more sophisticated for 3D display */

    if (DIM== 3){ /* 3D matrix */
      graph3d.setwin(0,size_x,0,size_y);
      graph3d.setup(0,fillsize*(1+DIMS[1]),
                    0,fillsize*(1+DIMS[2]),
                    0,fillsize*(1+DIMS[0]),
            100*fillsize*DIMS[1],120*fillsize*DIMS[2],200*fillsize*DIMS[0]);
    }

/* Event handlers */
    addMouseListener(new MyMouseAdapter());
    addMouseMotionListener(new MyMouseMotionAdapter());
    addComponentListener(new MyComponentAdapter());
    addKeyListener(new MyKeyAdapter());
    addWindowListener(new MyWindowAdapter());
  }

  class MyWindowAdapter extends WindowAdapter
  {
    public void windowClosing(WindowEvent we)
    {
      System.exit(0);
    }
  }

  class MyKeyAdapter extends KeyAdapter
  {
    public void keyPressed(KeyEvent e)
    {
      switch (e.getKeyCode()){
        case KeyEvent.VK_LEFT:
             graph3d.rotate(.025,0);graph3d.adjust();unload_all();break;
        case KeyEvent.VK_RIGHT:
             graph3d.rotate(-.025,0);graph3d.adjust();unload_all();break;
        case KeyEvent.VK_UP:
             graph3d.rotate(0,-.025);graph3d.adjust();unload_all();break;
        case KeyEvent.VK_DOWN:
             graph3d.rotate(0,.025);graph3d.adjust();unload_all();break;
      }
    }
  }

  class MyComponentAdapter extends ComponentAdapter
  {
    public void componentResized(ComponentEvent e)
    {
      unload_all();
    }
  }

  boolean mousedown=false;

  class MyMouseAdapter extends MouseAdapter
  {
    public void mousePressed(MouseEvent me)
    {
      mousedown=true;
    }
  }

  class MyMouseMotionAdapter extends MouseMotionAdapter
  {

    public void mouseMoved(MouseEvent me)
    {
      int x,y;
/***
      x=(me.getX()-xoffset)/fillsize;
      y=(me.getY()-yoffset)/fillsize;
      System.out.println("["+y+","+x+"]");
***/
    }

    int ind[]=new int[2];
    public void mouseDragged(MouseEvent me)
    {
      int i;

      if (DIM==2){
        ind[1]=(me.getX()-xoffset)/fillsize;
        ind[0]=(me.getY()-yoffset)/fillsize;
        ce=sim.user_read_cell(ind);
        if (ce.error){System.out.println("Failed");return;}
        String t[]=bdump.decomp(ce);
        System.out.println("----------------\n");
        System.out.println("["+ind[0]+","+ind[1]+"]");
        for (i=0;i<8;i++){
          if (t[i].length()>0) System.out.println(t[i]);
        }
        System.out.println("----------------\n");
      } /* End of 2D dump */
    }
  }

/* Graphics display routines */
/***
  public void update(Graphics g)
  {
    paint(g);
  }
***/

  int ind[]=new int[32];
  public void unload_changes()
  {
    if (saveg==null) saveg=getGraphics();

/* Unload any pending changes */
    ind=sim.unload_changes();
    while (ind[0] != -1){
      display_cell(saveg,ind);
      ind=sim.unload_changes();
    }
    
  }

  public void unload_all()
  {
    Dimension d=getSize();
    graph3d.setwin(0,d.width,0,d.height);

    if (saveg==null) saveg=getGraphics();
//    paint(saveg);
    repaint();
  }

  public void paint(Graphics g)
  {
    int ind[]=new int[3];

    if (DIM==2){
      for (ind[0]=0;ind[0]<DIMS[0];ind[0]++){ // Row
        for (ind[1]=0;ind[1]<DIMS[1];ind[1]++){ // Column
          display_cell(g,ind);
        }
      }
    } else if (DIM==3){
      graph3d.box(g);
      for (ind[0]=0;ind[0]<DIMS[0];ind[0]++){ // Row
        for (ind[1]=0;ind[1]<DIMS[1];ind[1]++){ // Column
          for (ind[2]=0;ind[2]<DIMS[2];ind[2]++){ // Layer
            display_cell(g,ind);
          }
        }
      }
    }
  }

  void display_cell(Graphics g, int[] ind)
  {
    if (DIM==2){
      display_cell_2d(g,ind);
      return;
    }

    if (DIM==3){
      display_cell_3d(g,ind);
      return;
    }
  }

  void display_cell_2d(Graphics g, int[] ind)
  {
    int i,xloc,yloc;
    int x1,x2,y1,y2;
    boolean empty;

    x1=xoffset+ind[1]*fillsize;
    y1=yoffset+ind[0]*fillsize;
    x2=x1+fillsize-fillsep;
    y2=y1+fillsize-fillsep;

    ce=sim.user_read_cell(ind);

/* Is cell's TT all 0s? */
    empty=true;
    for (i=0;i<2*ce.get_ttsize();i++){
      if (ce.tt[i]!=0){
        empty=false;
        break;
      }
    }

    if (display_mode==1){ /* Solid rectangles */
/* Is cell in C-mode? */
      if (ce.c_in != 0){
        g.setColor(c_color);
      } else {
/* Cell is in D-mode. Is TT empty? */
        if (empty){
          g.setColor(empty_color);
        } else if ((ce.c_out == 0) && (ce.d_out==0)){ /* All outupts=0 */
          g.setColor(d0_color);
        } else {
          g.setColor(d1_color);
        }
      }
      g.fillRect(x1,y1,fillsize,fillsize);
    } /* End of single-color filled rectangle code */

    if (display_mode==2){/* Unfilled rectangle: mark each side appropriately */
      if (empty){
        g.setColor(empty_color);
        g.fillRect(x1,y1,1+x2-x1,1+y2-y1);
        return;
      }
/* Now draw outline of square */
      our_setcolor(empty,g,ce.c_out&1,ce.d_out&1); g.drawLine(x1,y1,x2,y1);
      our_setcolor(empty,g,ce.c_out&2,ce.d_out&2); g.drawLine(x2,y2,x2,y1);
      our_setcolor(empty,g,ce.c_out&4,ce.d_out&4); g.drawLine(x1,y2,x2,y2);
      our_setcolor(empty,g,ce.c_out&8,ce.d_out&8); g.drawLine(x1,y2,x1,y1);
    }

    if (display_mode==3){/* Use edge thickness */
      if (empty){
        g.setColor(empty_color);
        g.fillRect(x1,y1,1+x2-x1,1+y2-y1);
        return;
      }
/* Now draw outline of square */
      if ((ce.c_out&1)!=0) g.setColor(Color.red); else g.setColor(Color.green);
      g.drawLine(x1,y1,x2,y1);if ((ce.d_out&1)==0) g.setColor(Color.black);
      g.drawLine(x1+1,y1+1,x2-1,y1+1);

      if ((ce.c_out&2)!=0) g.setColor(Color.red); else g.setColor(Color.green);
      g.drawLine(x2,y1,x2,y2);if ((ce.d_out&2)==0) g.setColor(Color.black);
      g.drawLine(x2-1,y1+1,x2-1,y2-1);

      if ((ce.c_out&4)!=0) g.setColor(Color.red); else g.setColor(Color.green);
      g.drawLine(x1,y2,x2,y2);if ((ce.d_out&4)==0) g.setColor(Color.black);
      g.drawLine(x1+1,y2-1,x2-1,y2-1);

      if ((ce.c_out&8)!=0) g.setColor(Color.red); else g.setColor(Color.green);
      g.drawLine(x1,y1,x1,y2);if ((ce.d_out&8)==0) g.setColor(Color.black);
      g.drawLine(x1+1,y1+1,x1+1,y2-1);

    }
  }

  void display_cell_3d(Graphics g, int[] ind)
  {
    int i,xloc,yloc;
    int x1,x2,y1,y2,z1,z2;
    boolean empty;

    x1=ind[1]*fillsize;
/* y should be inverted */
    y1=(DIMS[0]-ind[0])*fillsize;
    y1=(DIMS[2]-ind[2])*fillsize;
    y1=(ind[2])*fillsize;
    z1=ind[0]*fillsize;
    x2=x1+fillsize-fillsep;
    y2=y1+fillsize-fillsep;
    z2=z1+fillsize-fillsep;

    ce=sim.user_read_cell(ind);

/* Is cell's TT all 0s? */
    empty=true;
    for (i=0;i<2*ce.get_ttsize();i++){
      if (ce.tt[i]!=0){
        empty=false;
        break;
      }
    }

if (empty) return; /* For now, only draw non-empty cells */

    if (true){/* Only case for now */
      if (empty){
        g.setColor(empty_color);
        g.fillRect(x1,y1,1+x2-x1,1+y2-y1);
        return;
      }
/* Now draw outline of cube */
      our_setcolor(empty,g,ce.c_out&2,ce.d_out&2); // East
      rect3d(g,1,0,0,x2,y2,z1,x2,y1,z1,x2,y1,z2,x2,y2,z2);

      our_setcolor(empty,g,ce.c_out&8,ce.d_out&8); // West
      rect3d(g,-1,0,0,x1,y2,z1,x1,y1,z1,x1,y1,z2,x1,y2,z2);

      our_setcolor(empty,g,ce.c_out&16,ce.d_out&16); // Top
      rect3d(g,0,1,0,x1,y2,z1,x2,y2,z1,x2,y2,z2,x1,y2,z2);

      our_setcolor(empty,g,ce.c_out&4,ce.d_out&4); // South
      rect3d(g,0,0,1,x1,y2,z2,x2,y2,z2,x2,y1,z2,x1,y1,z2);

      our_setcolor(empty,g,ce.c_out&1,ce.d_out&1); // North
      rect3d(g,0,0,-1,x1,y2,z1,x2,y2,z1,x2,y1,z1,x1,y1,z1);

      our_setcolor(empty,g,ce.c_out&32,ce.d_out&32); // Bottom
      rect3d(g,0,-1,0,x1,y1,z1,x2,y1,z1,x2,y1,z2,x1,y1,z2);

      return;
    } /* End of only case */

  }

/* Draw rectangle in 3space between the four points */
/* *off is an offset for each coord */
  void rect3d(Graphics g,int xoff, int yoff, int zoff,
                         int x1, int y1, int z1,
                         int x2, int y2, int z2,
                         int x3, int y3, int z3,
                         int x4, int y4, int z4)
  {
    x1+=xoff;y1+=yoff;z1+=zoff;
    x2+=xoff;y2+=yoff;z2+=zoff;
    x3+=xoff;y3+=yoff;z3+=zoff;
    x4+=xoff;y4+=yoff;z4+=zoff;

    graph3d.plot(g,x1,y1,z1,x2,y2,z2);
    graph3d.plot(g,x2,y2,z2,x3,y3,z3);
    graph3d.plot(g,x3,y3,z3,x4,y4,z4);
    graph3d.plot(g,x4,y4,z4,x1,y1,z1);
  }

  void line3d(Graphics g, int x1, int y1, int z1, int x2, int y2, int z2)
  {
    int X1,Y1,X2,Y2; /* 2D Coords */
    int scale=1;

    X1=xv+(xv-x1)/(yv-y1);X1*=scale;
    X2=xv+(xv-x2)/(yv-y2);X2*=scale;
    Y1=zv+(zv-z1)/(yv-y1);Y1*=scale;
    Y2=zv+(zv-z2)/(yv-y2);Y2*=scale;

    g.drawLine(X1,Y1,X2,Y2);
  }

  Color bright_red=new Color(255,0,0);
  Color dark_red=new Color(128,0,0);
  Color bright_green=new Color(0,255,0);
  Color dark_green=new Color(255,255,255);

  void our_setcolor(boolean empty,Graphics g,int c, int d)
  {
    if (empty){
      g.setColor(empty_color);return;
    }

    if (c!=0) c=1;
    if (d!=0) d=1;
    switch (2*c+d){
      case 0: g.setColor(dark_green);return;
      case 1: g.setColor(bright_green);return;
      case 2: g.setColor(dark_red);return;
      case 3: g.setColor(bright_red);return;
    }
  }

  void set_display_mode(int dmode, int fsize, int fsep)
  {
    display_mode=dmode;
    if (fsize != -1) fillsize=fsize;
    if (fsep != -1) fillsep=fsep;
    saveg.setColor(Color.black);
    saveg.fillRect(0,0,getSize().width,getSize().height);
    unload_all();
  }
}

public class main{
  static int NUM_SIDES;
  static int DIM;
  static int DIMS[]=new int[32];
  static int update_rate=1; /* Unload every n ticks/cycles */
                       /* < 1 means no update until finished */

  public static void main(String args[]){
    window_class mw; /* Main drawing window */
    int i,ind[]=new int[16],s;
    StringBuffer tt[],labels;
    String buffer;
    int status, freq, value, j;
    String command;
    StringBuffer text[];
    cell_matrix_sim sim; /* Main simulation engine */

    parse_buffer pb;
    msgwin msg=new msgwin();

/* Setup dimensionality/etc. */
    DIM=args.length;
    if (DIM > 0){
      for (i=0;i<DIM;i++) DIMS[i]=Integer.parseInt(args[i]);
    } else {
      DIM=2;DIMS[0]=50;DIMS[1]=50; /* Arbitrary default */
    }

    if (DIM==2){
      NUM_SIDES=4;
      sim=new cell_matrix_sim(NUM_SIDES,DIMS[0],DIMS[1]);
    } else if (DIM==3){
      NUM_SIDES=6;
      sim=new cell_matrix_sim(NUM_SIDES,DIMS[0],DIMS[1],DIMS[2]);
    } else {
      System.out.println("Only 2D/4 Sided and 3D/6 Sided topologies currently supported");
      System.exit(1);
      return;
    }

    cell_exchange ce=new cell_exchange(DIM,NUM_SIDES);

/* Set up graphics */
    mw=new window_class(sim,ce);
    mw.show();
    mw.repaint();

    try{
      InputStreamReader isr=new InputStreamReader(System.in);
      BufferedReader br=new BufferedReader(isr);
      System.out.print("> ");
      while ((buffer=br.readLine()) != null){ /* Parse and process input */
        pb=parse(buffer);
        command=pb.words[0];
/* Shift arguments, so we can feed coords to simulator methods */
        for (i=0;i<14;i++){
          pb.nums[i]=pb.nums[i+1];
          pb.words[i]=pb.words[i+1];
        }

        if (pb.number==0) command="#";
        switch (command.charAt(0)){
          case ';':break;
          case '#':break;
          case 'Q':mw.dispose();System.exit(0);

          case 'c': /* Compile equations into a cell */
            if (check(pb,1)) break;
            System.out.println("Enter equations, end with \".\":");
            buffer=br.readLine();
            StringBuffer full=new StringBuffer("");
            while (!buffer.equals(".")){
              full.append(buffer+";");
              buffer=br.readLine();
            }
            ce.tt_compile(full.toString());
            if (0 != sim.user_write_cell(pb.nums,ce)){
              System.out.println("Failed");
            }
            break;

          case 'M': /* Set display mode */
            mw.set_display_mode(pb.nums[0],pb.nums[1],pb.nums[2]);
            break;

          case 'd': /* Dump TT from a cell */
            if (check(pb,1)) break;
            ce=sim.user_read_cell(pb.nums);
            if (ce.error){System.out.println("Failed");break;}
            text=ce.tt_dump();
            for (i=0;i<=ce.ttsize;i++){
              System.out.println(text[i]);
              msg.addtext(text[i].toString());
            }
            break;

          case 'b': /* Boolean equation dump */
            if (check(pb,1)) break;
            ce=sim.user_read_cell(pb.nums);
            if (ce.error){System.out.println("Failed");break;}
            String t[]=bdump.decomp(ce);
            for (i=0;i<8;i++){
              if (t[i].length()>0) System.out.println(t[i]);
            }
            break;

          case 'l': /* Load a binary grid file */
            if (check(pb,2)) break;
            load_bin_grid.load(sim,ce,pb.nums,pb.words[DIM]);
            break;

          case 'S': /* Sequence file */
            seq_load(mw,sim,pb.words[0]);
            break;

          case 's': /* Set an input to a cell */
            if (check(pb,3)) break;
            char type=pb.words[DIM].toUpperCase().charAt(0); /* D or C */
            char side=pb.words[DIM].toUpperCase().charAt(1); /* eg. N, S, W or E */
            s=ce.get_labels().indexOf(side);
            if (s == -1){
              System.out.println("Side should be one of " + ce.get_labels());
              break;
            }
            if ((pb.nums[DIM+1]!=0) && (pb.nums[DIM+1]!=1)){
              System.out.println("Value must be 1 or 0");
              break;
            }
            status=sim.user_set_input(pb.nums,type,s,pb.nums[DIM+1]);
            break;

          case 'o': /* Show outputs from a cell */
            if (check(pb,1)) break;
            ce=sim.user_read_cell(pb.nums);
            System.out.println("C:"+ce.get_labels());
            System.out.print("  ");
            for (i=0;i<NUM_SIDES;i++){
              System.out.print((0!=(ce.c_out&(1<<i)))?"1":"0");
            }
            System.out.println(" ");

            System.out.println("D:"+ce.get_labels());
            System.out.print("  ");
            for (i=0;i<NUM_SIDES;i++){
              System.out.print((0!=(ce.d_out&(1<<i)))?"1":"0");
            }
            System.out.println(" ");

            break;

          case 'u': /* Specify update rate */
            if (pb.number==1){
              System.out.println("Current update rate=" + update_rate);
            } else {
              update_rate=pb.nums[0];
            }
            if (update_rate < 1){
              update_rate=0;
              System.out.println("No updates until t/r command is finished");
            }
            break;

          case 'r': /* Run (full cycles) */
            if (pb.number==1) pb.nums[0]=1; /* Default is one cycle */
            freq=(pb.nums[0]/72)+1;
            for (j=0;j<pb.nums[0];j++){
              for (i=0;i<2*NUM_SIDES*(1<<NUM_SIDES);i++){
                sim.tick();
                sim.process_queue();
              }
              if (j%freq == 0) System.out.print(".");
              if (update_rate > 0){if (j%update_rate == 0) mw.unload_changes();}
            }
            System.out.println("");
            break;

          case 'I':
            sim.reinit();
            mw.unload_all();
            break;

          case 't': /* Tick */
            if (pb.number==1) pb.nums[0]=1; /* Default is one tick */
            freq=(pb.nums[0]/72)+1;
            for (j=0;j<pb.nums[0];j++){
              sim.tick();
              sim.process_queue();
              if (j%freq == 0) System.out.print(".");
              if (update_rate > 0){if (j%update_rate == 0) mw.unload_changes();}
            }
            System.out.println("");
            break;

          case '\n':
          case '\0':
            break;
          default:
            System.out.println("[ind] is a list of indicies");
            System.out.println("c [ind]            Define cell[ind]'s TT");
            System.out.println("d [ind]            Show cell[ind]'s TT");
            System.out.println("b [ind]            Show eqns for cell[ind]");
            System.out.println("s [ind] side val   Set cell[ind].side=val");
            System.out.println("o [ind]            Show outputs of cell[ind]");
            System.out.println("t [num]            Tick clock num times (default=1");
            System.out.println("r [num]            Run num full programming cycles (default=1)");
            System.out.println("u [num]            Show or set t/r update rate (0 for no updates)");
            System.out.println("l [ind] filename   Load binary grid file, UL at cell[ind]");
            System.out.println("S filename         Process sequence file");
            System.out.println("M mode size sep    Set display mode");
            System.out.println("I                  Reinitialize entire matrix");
            System.out.println("# or ;             Comment");
            System.out.println("Q                  Exit immediately");
        }
        sim.process_queue();
        mw.unload_changes();
        System.out.print("> ");
      } /* End of input */
      System.out.println("^D");
      isr.close();
    } catch (Exception e){
      System.out.println("Input exception:" + e);
    }
  }

/* Legality checker */
  static boolean check(parse_buffer pb,int i)
  {
    if (i+DIM != pb.number){
      System.out.println("Command requires " + (i+DIM) + " arguments");
      return(true);
    }
    return(false);
  }


/* Break a string into pieces */
/* Skip first word! */

  static parse_buffer parse(String buffer)
  {
    parse_buffer pb=new parse_buffer();
    int i,num,nums[]=new int[16];
    String words[]=new String[16];
    StringTokenizer st=new StringTokenizer(buffer," ");
    pb.number=st.countTokens(); /* # of items parsed */
    for (i=0;i<16;i++) pb.nums[i]=-1;
    i=0;

    while (st.hasMoreTokens()){
      pb.words[i]=st.nextToken();
      try{pb.nums[i]=Integer.parseInt(pb.words[i]);}
      catch(Exception e){pb.nums[i]=(-1);}
      ++i;
    }
    return(pb);
  }

  static void seq_load(window_class mw, cell_matrix_sim sim, String filename)
  {
    String buffer;
    int num_channels;
    int inds[][]=new int[32][4]; /* 32 channels, 4 dims */
    char type[]=new char[32]; /* C or D */
    int side[]=new int[32]; /* Side mask (from labels) */
    String tts[]=new String[32]; /* Holds truth tables for each channel */
    int sides,dims;
    String labels;
    int i,j,mask,counter;

    try {
      FileReader fr=new FileReader(filename);
      BufferedReader br=new BufferedReader(fr);
      buffer=br.readLine(); /* Header */
      if (!buffer.equals("\"#SEQUENCE FILE\"")){
        System.out.println(filename + " does not appear to be a valid sequence file.");
        fr.close();
        return;
      }

      buffer=br.readLine(); /* User comment */

      buffer=br.readLine(); /* Flags */
      if (!buffer.equals("\"V3TT\"")){
        System.out.println(filename + " is not a Version 3 sequence file. Please recompile " + filename);
        fr.close();
        return;
      }
      dims=Integer.parseInt(seq_gets(br)); /* # of dims */
      labels=seq_gets(br); /* Ordered labels for sides of cell */
      sides=labels.length(); /* # of sides */

      buffer=seq_gets(br); /* # of channels */
      num_channels=Integer.parseInt(buffer);

/* Load chanel specifications */
      for (i=0;i<num_channels;i++){
        buffer=seq_gets(br); /* ind,ind,...,ind,TYPE+SIDE */
        StringTokenizer st=new StringTokenizer(buffer,",");
        for (j=0;j<dims;j++){
          inds[i][j]=Integer.parseInt(st.nextToken());
        } /* Indicies loaded for this channel */
        String s=st.nextToken(); /* TYPE+SIDE */
        type[i]=s.charAt(0); /* C or D */
        side[i]=labels.indexOf(s.charAt(1)); /* Side */
      }

/* Now execute the sequence */
      counter=0; /* For updating display */
      while ((tts[0]=seq_gets(br)) != null){
        for (i=1;i<num_channels;i++){
          tts[i]=seq_gets(br);
        }
/* tts[] is loaded now. Send the bits to the engine and run the clock */
        mask=1; /* Shows which bit to read from nibble */
        for (i=0;i<tts[0].length();i++){ /* For each nibble */
          for (mask=3;mask>=0;mask--){ /* For each bit inside nibble */
            for (j=0;j<num_channels;j++){ /* For each channel */
              sim.user_set_input(inds[j],type[j],side[j],
  (((Integer.parseInt(tts[j].substring(i,i+1),16) & (1<<mask))==0)?0:1));
            } /* All channels set */
            sim.process_queue();
            sim.tick();sim.process_queue();
            if (++counter >= update_rate){
              counter=0;
              mw.unload_changes();
            }
          } /* End of current nibble */
        } /* End of TT */
      } /* End of file! */
      fr.close();
      return;
    } catch (Exception e){
      System.out.println("Error opening " + filename + ":" + e);
      return;
    }
  }

  static String seq_gets(BufferedReader br)
  {
    String buffer,s;
    try{
      while ((buffer=br.readLine()) != null){
        if (buffer.charAt(1) != '#'){ /* Return it */
          return(buffer.substring(1,buffer.length()-1));
        }
      }
      return(null);
    } catch(Exception e){
      System.out.println("Unexpected file read error: " + e);
      return(null);
    }
  }
}

/* Return object for parse() method */
class parse_buffer{
  String words[]=new String[16]; /* Holds pieces */
  int nums[]=new int[16]; /* Numeric values, -1 if not a number */
  int number; /* # of items parsed */
}
