/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class sf{
  public static void main(String args[]) {

    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("sf.seq",msg);

/* Read the binary grid file */
    binary_file_reader bfr=new binary_file_reader("../loader/sbir/sf.bin");
    binary_file_reader bfr_s=new binary_file_reader("../loader/sbir/sf_s.bin");
    binary_file_reader bfr_w=new binary_file_reader("../loader/sbir/sf_w.bin");
    binary_file_reader bfr_n=new binary_file_reader("../loader/sbir/sf_n.bin");

    space_filler sf=new space_filler(cmi,46);

/* Initialize the first supercell */
    cmi.comment("First (initial) supercell");

    sf.init(bfr);

/* Build a sequrence to the north */
    cmi.comment("Building to north");
    sf.build(bfr_n,3);

// Build a sequence to the East
    cmi.comment("Building in parallel to east");
    sf.build(bfr,0);

    cmi.comment("Should have 4 cells");

// Build a sequence to the south
    cmi.comment("Building in parallel to south");
    sf.build(bfr_s,1);

/* Finish up */
    cmi.comment("Finished");
    cmi.close();
  }
}
