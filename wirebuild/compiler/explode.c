#include <stdio.h>

struct libindexstruct{
  char ttstring[33];
  char libname[128];
  char cellname[128];
} libindex[1024]; /* Allow 1024 unique cells for now */

int libindexsize=0;

main()
{
  /*
   * Read stdin, which should be a binary grid state file.
   * For each cell, try to match to something in the libindex file
   */

int i,j,k;
int h,w,r,c,findstat;
char in,out;
FILE *fp,*outfp;
char buffer[1024];
char tmpttstring[32],tmplibname[128],tmpcellname[128];

  fp=fopen("libindex","r");
  while (NULL != fgets(buffer,1024,fp)){
    for (j=0;j<32;j++)
      tmpttstring[j]=buffer[j];
    tmpttstring[32]='\0';

    tmplibname[0]='"'; /* Start of lib name */
    j=34;i=1;
    while (buffer[j]!='"')
      tmplibname[i++]=buffer[j++];
    tmplibname[i++]='"';
    tmplibname[i]='\0';
  
    tmpcellname[0]='"';
    j+=3;i=1;
    while (buffer[j]!='"')
      tmpcellname[i++]=buffer[j++];
    tmpcellname[i++]='"';
    tmpcellname[i]='\0';
  
    strcpy(libindex[libindexsize].ttstring,tmpttstring);
    strcpy(libindex[libindexsize].libname,tmplibname);
    strcpy(libindex[libindexsize++].cellname,tmpcellname);
  }
  fclose(fp);
  printf("Stored %d cell%s in internal index\n",
    libindexsize,(libindexsize==1)?"":"s");

/*
 * Now we read the input file and try to match each cell
 */
  rread(0,buffer,80); /* Read in description */
  buffer[79]='\0';

  if (0 != strcmp(buffer,"BINARY GRID<<<!>>>")){
    fprintf(stderr,"ERROR: stdin is not a binary grid\n");
    return(1);
  }

  h=iread(0);w=iread(0);
  printf("Grid is %dx%d\n",h,w);

  outfp=fopen("binfile.grd","w");

  fprintf(outfp,"GRID_FILE\r\n%d\r\n%d\r\n",h,w);

  for (r=0;r<h;r++){
    printf("%d\n",h-r);
    for (c=0;c<w;c++){
      rread(0,buffer,1); /* inputs */
      rread(0,buffer,1); /* outputs */
      read(0,buffer,32); /* Read real 32-char TT string */
      buffer[32]='\0';
      if (findstat=find(buffer,tmplibname,tmpcellname))
        fprintf(outfp,"%s\r\n%s\r\n",tmplibname,tmpcellname);
      else fprintf(outfp,"\"\"\r\n\"\"\r\n");
    }
  }
  fclose(outfp);
}

find(in,libout,cellout)
char *in,*libout,*cellout;
{
  int i;

  if (0==strcmp(in,"                                ")) return(0);

  for (i=0;i<libindexsize;i++){
    if (0==strcmp(libindex[i].ttstring,in)){
      strcpy(libout,libindex[i].libname);
      strcpy(cellout,libindex[i].cellname);
      return(1);
    }
  }
  return(0);
}
