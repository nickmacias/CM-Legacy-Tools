class test{
  public static void main(String args[]) {
    int i;
    cell_matrix_channel pc,cc,pc_cc;

    cell_matrix_interface cmi=new cell_matrix_interface("test.seq","Test");

    pc=cmi.assign_channel(0,0,"DW");
    pc_cc=cmi.assign_channel(0,0,"CW"); /* For programming initial PC cell */
    cc=cmi.assign_channel(1,0,"DW");

/* Initialize head cells */
    pc.set("cs=1;ds=w");pc_cc.set(true);cmi.cycle();
    pc.set("dw=n;dn=we~&;de=we&");pc_cc.set(false);cmi.cycle();
    pc.set("dw=e;de=w;ce=s;ds=1");pc_cc.set(true);cmi.cycle();
    pc.set(false);pc_cc.set(false);cmi.cycle();

    for (i=0;i<10;i++){
      pc.set("cs=1;ds=w");cc.set(true);cmi.cycle();
      pc.set("dw=n;dn=we~&;de=we&");cc.set(false);cmi.cycle();
      pc.set("dw=e;de=w;ce=s;ds=1");cc.set(true);cmi.cycle();
      pc.set(false);cc.set(false);cmi.cycle();
    }

    cmi.close();
  }
}
