class test3{
  public static void main(String args[]) {
    cell_matrix_interface cmi=new cell_matrix_interface("test3.seq","Test file");

/* Start a wire */
    wire_build w=new wire_build(cmi,"e",0,0,"PC,CC");
    w.preclear(true);

    w.init();
    w.extend(1);

    for (int i=0;i<3;i++){
      w.extend(10);
      w.turn("CW");
      w.turn("CW");
      w.extend(10);
      w.turn("CCW");
      w.turn("CCW");
    }
    cmi.close();
  }
}
