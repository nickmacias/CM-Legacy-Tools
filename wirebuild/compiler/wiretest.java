/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class wiretest{
  public static void main(String args[]) {
    cell_matrix_channel pc,cc,pc_cc,brk;

    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("wiretest.seq",msg);

/* Start a wire */
    wire_build w=new wire_build(cmi,"e",0,0,"PC,CC");
    w.init();
    w.extend(2);
    w.turn("CW");
    w.extend(2);

    cmi.close();
  }
}
