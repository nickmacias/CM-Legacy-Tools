/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class sftest{
  public static void main(String args[]) {

    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("sfeast.seq",msg);

    if (args.length != 1){
      System.out.println("Usage: sftest filename.bin");
      System.exit(1);
    }

/* Read the binary grid file */
    binary_file_reader bfr=new binary_file_reader(args[0]);
    space_filler sf=new space_filler(cmi,bfr,18);

/* Initialize the first supercell */
    //sf.init();

/* Build a sequrence to the south */
//    sf.rotate(1);
//    sf.build();

// Build a sequence to the East
      sf.rotate(0);
      sf.build();


/* Finish up */
    cmi.close();
  }
}
