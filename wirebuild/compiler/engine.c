#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include "engine.h"          

/*
 * engine.c - package of C routines which implement the main processing
 * engine of the grid.
 *
 */
int set_input(int,int,int,int);
int process_queue_head();
int q_size();   /* Return # of things on queue */
int isin_cmode(int,int);
int do_tickhi();
int do_ticklo();
int shift(char *,char);
int write_cell(int,int,char *); /* don't change inputs! */
int check_rc(int,int); /* make sure r and c are on grid */
int set_dout_to_tt127(int,int);
int reinit(); /* Clear out the grid, empty the queues */ 
void fail(int,int,char); /* Set failure mode on cells */
  
static struct cell*** grid;

int marker=0; /* Toggle at end of all parallel events to show a time tick */
int timestamp=0; /* # of time units */

static int HEIGHT,WIDTH,QSIZE,C_ARR_SIZE; /* Loaded in INIT */

#define MARKER 0x1000

struct rc {
  int row;
  int col;
};        

static struct rc *c_arr;
static int c_arr_num; /* # of c-mode cells in array */ 

/*** QUEUE ROUTINES ***/

static int q_tail=0; /* next location to write */
static int q_head=0; /* next element to read */
static int *queue;   

void fail(int r,int c,char mode)
{
  grid[r][c]->fail=mode;
}                    

int q_size()   /* Return # of things in queue */
{
  int d;
  d=q_tail-q_head;
  if (d<0) d=d+QSIZE;
  return(d);
}

/* peek routines for examining queue without changing pointers */


static int h,zero_queue;

peek_set(s)
int s;
{
  zero_queue=s; /* 1-->zeroing enabled, 0-->zeroing disabled */
}

init_peek_queue()
{
  h=q_head;
}

int peek_queue(x)
int *x;
{
  if (h==q_tail) return(1); /* Queue empty */
  *x=queue[h++]; /* Next element to return */
  if (h==QSIZE) h=0;  /* Rollover */
  return(0);
}

int remqueue(x) /* remove head of queue, store in argument */
int *x;
{
  if (q_head == q_tail) return(1); /* failure */
  *x=queue[q_head++];
  if (q_head == QSIZE) q_head=0;
  return(0);
}

int insqueue(x) /* add x to tail of queue */
int x;
{
  queue[q_tail++]=x;
  if (q_tail == QSIZE) q_tail=0;
  if (q_tail != q_head) return(0);
  fprintf(stderr,"Queue overflow on event insertion!\n");
  return(1); /* overflow */
}

int write_cell(row,col,program) /* don't change inputs! */
int row,col;
char program[16];
{
  int i;

  if (check_rc(row,col)){
    return(1);
  }
  for (i=0;i<16;i++)
    grid[row][col]->program[i]=program[i];

/* Now touch an input to cause re-computation */
  if (set_input(row,col,DIN,(grid[row][col]->in&DIN)?1:0)){
    return(1);                                             
  }
    /* error in set_input probably means event queue is full */
  return(0);
}

int read_cell(row,col,in,out,program)
int row,col;
char *in,*out,program[16];
{
  int i;

  if (check_rc(row,col)){
    return(1);
  }
  *in=grid[row][col]->in;
  *out=grid[row][col]->out;
  for (i=0;i<16;i++)
    program[i]=grid[row][col]->program[i];
  return(0);
}


/*** QUEUE EVENT GENERATOR ROUTINES ***/
#define SET1 0x100     /* Use to indicate event is INPUT=1 */
#define SET0 0x200     /* Use to indicate event is INPUT=0 */
                       /* (OR with the appropriate (CD)I(NSWE) mask) */
#define TICKHI 0x400   /* use for TICK HI event */
#define TICKLO 0x800   /* use for TICK LO event */

int do_tick(level) /* TICK clock HI or LO */
{
  if (insqueue(((level?TICKHI:TICKLO)|marker))) return(1); /* Q full */
  return(0);
}

int set_input(row,col,input_mask,value)
int row,col,input_mask; /* input_mask is (C|D)I(N|S|W|E) */
int value;
{
  if (check_rc(row,col)) return(0); /* Allow attempts outside grid! */
  if (grid[row][col]->fail == 'I') return(0); /* Ignore input changes */

/* See if input_mask/row/col is already in queue. If so, zero it out */
  check_zero(input_mask,row,col);
  if (insqueue((value?SET1:SET0)|input_mask|marker)) return(1); /* Q full */
  if (insqueue(row)) return(1);
  if (insqueue(col)) return(1);
  return(0);
}

/*
 * check_zero(input_mask,row,col)
 * Pre-scan the queue, and see if there's already a change stored for
 * this cell's specified input. If there is, just zero that command out.
 */
check_zero(input_mask,row,col)
{
  int x,r,c,save_h;

  if (zero_queue==0) return;

  init_peek_queue();
  save_h=h; /* This is where current command is stored in queue */

  while (0==peek_queue(&x)){
    if (!((x&TICKHI) || (x&TICKLO))){ /* SET command */
      peek_queue(&r);
      peek_queue(&c);
      if ((r==row) && (c==col) && ((x&0xff) == input_mask)){ /* match! */
        queue[save_h]=queue[save_h]&MARKER; /* Zero out the command */
                                            /* BUT KEEP MARKER! */
      }
    }
    save_h=h; /* Next command's loc */
  }
}


/*** SPECIAL USER-VERSION OF SET_INPUT (only allows border changes) ***/

int user_set_input(row,col,input_mask,value)
int row,col,input_mask,value;
{
  if (((col==0) && (input_mask==CIW || input_mask==DIW)) ||
      ((col==WIDTH-1) && (input_mask==CIE || input_mask==DIE)) ||
      ((row==0) && (input_mask==CIN || input_mask==DIN)) ||
      ((row==HEIGHT-1) && (input_mask==CIS || input_mask==DIS)))
    return(set_input(row,col,input_mask,value));
    
/* Not allowed to set cell input here */      
  return(1);
}

/*** QUEUE PROCESSOR ***/


show_queue()
{
  int cmd,row,col;
  remqueue(&cmd);
  remqueue(&row);
  remqueue(&col);
  fprintf(stderr,"Latest cell=[%d,%d]\n",row,col);
}

int get_timestamp()
{
  return(timestamp);
}

int reset_timestamp()
{
  timestamp=0;
}

static int lastrow,lastcol;

last_queue(r,c)
int *r,*c;
{
  *r=lastrow;*c=lastcol;
}

int process_queue_head() /* look at top of queue, execute command */
{
  int cmd,cmd2,row,col,didChg,in,out,out2;
  int topr,topc,newind; /* For shuffling c-mode structure */
  char oldin; /* Previous input byte */
                
  if (remqueue(&cmd)) return(99); /* queue empty */
  /* See if marker (LSB of CMD) is different from current marker */
  if ((cmd&MARKER) == marker){
    ++timestamp; /* New time unit */
    marker=MARKER-marker; /* Toggle marker */
  }

  if ((cmd & SET0) || (cmd & SET1)){ /* SET INPUT command */
    if (remqueue(&row)) return(-1); /* wierd failure */
    if (remqueue(&col)) return(-1); /* wierd failure */

  lastrow=row;lastcol=col;

/* Better be seeing legal coords! */
   if (check_rc(row,col)) return(-1); /* WIERD! */

/* set the ->in field of the cell */
    cmd2=cmd&0xFF;                     
    oldin=grid[row][col]->in;
    if (cmd & SET1) grid[row][col]->in|=cmd2; /* set proper bit */
    else grid[row][col]->in&=(cmd2^0xFF); /* else clear bit */
    
                         /*** C-MODE PROCESSING ***/                         
/*
 * If cell is in C-mode, and a change has occured in the inputs (which is
 * why we're here in the first place), only a few things can be happening...
 *  (1) A DATA input has changed, which really doesn't matter to us, or
 *  (2) A CONTROL input has changed. This means we must adjust the C and D
 *      outputs for C-mode operation...COUT(i)=0, and
 *      DOUT(i)=TT[127] if CIN(I)=1, else DOUT(i) is unchanged.  
 ***
 *** TWEEK ALERT *** Change so DOUT(i)=0 if CIN(i)=0  9/15/95 njm
 ***
 ***
 *** TWEEK ALERT #2 ***Entering C-mode, drop Dout before Cout
 *** Since dropping Cout may return neighbor to D-mode, set
 *** D outputs properly first (bad Dout while Cout=1 is harmless,
 *** since Dout is ignored until clock tick during C-mode op).1/11/98 njm
 ***
 */
    if (isin_cmode(row,col)){ /* drive C-outputs low */   
/* First, see if we're just going into c-mode now. If so, remove latched data */
      if ((oldin & 0xf0) == 0){ /* Just entering c-mode now */
        grid[row][col]->latch='\2';  /* We'll latch next time clock goes HI */
        
        if (c_arr_num == C_ARR_SIZE) return(1); /* Too many c-mode cells! */
        c_arr[c_arr_num].row=row;
        c_arr[c_arr_num].col=col; /* store in c-mode array */
		grid[row][col]->c_arr_ind=c_arr_num++; /* remember where we stored it */
	  } /* End of initial c-mode entry processing */

      out2=grid[row][col]->out; /* current output */
      
/* Next, clear DOut on each side where CIn=0 */
      in=grid[row][col]->in;

      if (((in&CIN)==0) && (out2&DON)){   /* Clear DOut on this side */
        grid[row][col]->out&=DM^DON;
        if (set_input(row-1,col,DIS,0)) return(1);
      }

      if (((in&CIS)==0) && (out2&DOS)){   /* Clear DOut on this side */
        grid[row][col]->out&=DM^DOS;
        if (set_input(row+1,col,DIN,0)) return(1);
      }
      
      if (((in&CIW)==0) && (out2&DOW)){   /* Clear DOut on this side */
        grid[row][col]->out&=DM^DOW;
        if (set_input(row,col-1,DIE,0)) return(1);
      }
      
      if (((in&CIE)==0) && (out2&DOE)){   /* Clear DOut on this side */
        grid[row][col]->out&=DM^DOE;
        if (set_input(row,col+1,DIW,0)) return(1);
      }
                                                       
/* Now we can drive Cout low */
      if (out2&CM){ /* Some C-outputs are HI right now */
        if (out2&CON) {if (set_input(row-1,col,CIS,0)) return(1);}
        if (out2&COS) {if (set_input(row+1,col,CIN,0)) return(1);}
        if (out2&COW) {if (set_input(row,col-1,CIE,0)) return(1);}
        if (out2&COE) {if (set_input(row,col+1,CIW,0)) return(1);}
        grid[row][col]->out&=DM; /* clear C outputs */
      }                          

/* Next we set DOUT (on any side with CIN=1) to the highest TT bit */
      if (set_dout_to_tt127(row,col)) return(1); /* that was easy */
      return(0);
    } /* End of C-mode processing */
                           
                           
                           /*** D-MODE PROCESSING ***/
                           
/* See if we were in c-mode a tick ago */
    if (oldin&0xf0){ /* Yup...need to remove it from c_arr stack */
/* Shuffle last entry in array to take this one's place */
      --c_arr_num; /* Bump down stack size */
      topr=c_arr[c_arr_num].row;
      topc=c_arr[c_arr_num].col; /* Current top-of-stack member */
      newind=grid[row][col]->c_arr_ind; /* new loc for TOS member */
      c_arr[newind].row=topr;  
      c_arr[newind].col=topc;  /* Shuffle TOS into middle ofstack */
      grid[topr][topc]->c_arr_ind=newind; /* and point to in from grid */
      grid[row][col]->c_arr_ind=(-1); /* and remove old one's pointer */
    }
    
/* now re-compute the outputs for that cell */
    out=grid[row][col]->program[grid[row][col]->in&DM]; /* output values */
    out2=grid[row][col]->out;
    if (out == out2) return(0); /* no output changes */

/* and compare to old values. Where different, set new input */
    if ((out&CON) != (out2&CON))
      {if (set_input(row-1,col,CIS,out&CON)) return(1);}
    if ((out&COS) != (out2&COS))
      {if (set_input(row+1,col,CIN,out&COS)) return(1);}
    if ((out&COW) != (out2&COW))
      {if (set_input(row,col-1,CIE,out&COW)) return(1);}
    if ((out&COE) != (out2&COE))
      {if (set_input(row,col+1,CIW,out&COE)) return(1);}
    if ((out&DON) != (out2&DON))
      {if (set_input(row-1,col,DIS,out&DON)) return(1);}
    if ((out&DOS) != (out2&DOS))
      {if (set_input(row+1,col,DIN,out&DOS)) return(1);}
    if ((out&DOW) != (out2&DOW))
      {if (set_input(row,col-1,DIE,out&DOW)) return(1);}
    if ((out&DOE) != (out2&DOE))
      {if (set_input(row,col+1,DIW,out&DOE)) return(1);}

    grid[row][col]->out=out; /* save for next comparison */
    return(0);
  } else if (cmd & TICKHI){ /* HI clock tick */
                            /* Set output on C=1 sides to prog:127 */
     if (do_tickhi()) return(1);
     return(0);
  } else if (cmd & TICKLO){ /* LO clock tick */
                            /* shift program in C-mode cells */
     if (do_ticklo()) return(1);
     return(0);
  } else { /* Should be a zeroed out command. Flush its args */
    if (remqueue(&row)) return(-1); /* wierd failure */
    if (remqueue(&col)) return(-1); /* wierd failure */
  }
  return(0);
}


/*** C-MODE ROUTINES ***/

int isin_cmode(r,c) /* is cell in c-mode? */
int r,c;
{
  if (check_rc(r,c)) return(0); /* Guess this is cool */
  return((grid[r][c]->in & CM)?1:0);
}

int do_tickhi() /* set outputs on c-mode cells */
/* Sweep through the c_arr to pull out indicies for cells in c-mode */
{
  int r,c,in,iii;
  struct cell* cell; /* points to cell being processed */
       
  for (iii=0;iii<c_arr_num;iii++){
    r=c_arr[iii].row;
    c=c_arr[iii].col;
    cell=grid[r][c];
    in=0xFF & cell->in;
    cell->latch=(0x0f & ((in>>4)&in))?'\1':'\0'; /* =1 if (DIN=1 where any CIN=1) */

  }
  return(0);
}

int do_ticklo() /* process TICK low across grid */
{
  int r,c,iii;
  
  for (iii=0;iii<c_arr_num;iii++){
    r=c_arr[iii].row;
    c=c_arr[iii].col;             
    if (grid[r][c]->latch != '\2'){ /* Cell was in c-mode on last uptick */
      shift(grid[r][c]->program,grid[r][c]->latch); /* Shift in saved bit */
      if (set_dout_to_tt127(r,c)) return(1); /* set DOUT(s) from new TT */
    }
  }

  return(0);
}

/*
 * The following routine sets each DOUT where CIN=1. It records the
 * change, and sets neighbor's inputs as appropriate
 */
int set_dout_to_tt127(int r,int c)
{
  struct cell *cell;
  char out;
  int bit;

  cell=grid[r][c];
  bit=(cell->program[15]&0x80)?1:0; /* value to output */

  if (cell->in & CIN){
    out=cell->out; /* current value */
    cell->out=bit?cell->out|DON:cell->out&(0xFF^DON); /* set properly */
    if (cell->out != out){
      if (set_input(r-1,c,DIS,cell->out&DON)) return(1);
    }
  }
  if (cell->in & CIS){
    out=cell->out;
    cell->out=bit?cell->out|DOS:cell->out&(0xFF^DOS);
    if (cell->out != out){
      if (set_input(r+1,c,DIN,cell->out&DOS)) return(1);
    }
  }
  if (cell->in & CIW){
    out=cell->out;
    cell->out=bit?cell->out|DOW:cell->out&(0xFF^DOW);
    if (cell->out != out){
      if (set_input(r,c-1,DIE,cell->out&DOW)) return(1);
    }
  }
  if (cell->in & CIE){
    out=cell->out;
    cell->out=bit?cell->out|DOE:cell->out&(0xFF^DOE);
    if (cell->out != out){
      if (set_input(r,c+1,DIW,cell->out&DOE)) return(1);
    }
  }
  return(0);
}

int shift(program,bit) /* rotate left 1 bit, shift in bit */
char *program;
char bit;
{
  long *cool;
            
  cool=(long *)program;
  cool[3]<<=1;if (program[11]&0x80) program[12]|=1;
  cool[2]<<=1;if (program[7]&0x80) program[8]|=1;
  cool[1]<<=1;if (program[3]&0x80) program[4]|=1;
  cool[0]<<=1;if (bit) program[0]|=1;  
  return(0);
}

/*** INITIALIZATION CODE ***/
static int idone=0;

int init_grid(rows,cols,q_size,carrsize)
int rows,cols,q_size,carrsize;
{
  int r,c,i;                              
  
  HEIGHT=rows;
  WIDTH=cols;
  QSIZE=q_size;       
  C_ARR_SIZE=carrsize;
                       
  if (idone==0) grid=malloc(rows*(sizeof (struct cell *)));
  for (r=0;r<rows;r++){                   
    if (idone==0) grid[r]=malloc(cols*sizeof(struct cell *));
    if (grid[r]==(struct cell**) NULL) {
         /*fprintf(stderr,"Ran out of memory initializing row %d\n",r);*/
         return(1); /* out of memory */
    }
    for (c=0;c<cols;c++){
      if (idone==0) grid[r][c]=(struct cell*) malloc(sizeof(struct cell));
      if (grid[r][c]==(struct cell*) NULL) {
         /*fprintf(stderr,"Ran out of memory initializing cell [%d,%d]\n",r,c);*/
         return(1); /* out of memory */
      }

      grid[r][c]->in=grid[r][c]->out='\0'; /* clear inputs and otuput */
      for (i=0;i<16;i++) grid[r][c]->program[i]='\0';

      grid[r][c]->fail='0';
/*
 * When a cell enters C-mode, we'll record its coords in the c_arr array,
 * and set the c_arr_ind to point to that entry. When the cell leaves c-mode,
 * we clear this flag (set to -1) and remove it from the c_arr array.
 * When we want to sweep through the c-mode cells, we can just pick them off
 * the c_arr.
 */
      grid[r][c]->c_arr_ind=(-1);
    }
  }                                    

  if (idone==0) queue=malloc(QSIZE * sizeof(int));
  if (queue == (int*) NULL) return(1);    
  
  if (idone==0) c_arr=malloc(C_ARR_SIZE*sizeof(struct rc));
  if (c_arr == (struct rc*) NULL) return(1); 
  c_arr_num=0; /* c-mode stack is empty */ 

  idone=1;

  return(0);
}

int reinit()
{
  int r,c;
  int i;

  for (r=0;r<HEIGHT;r++){
    for (c=0;c<WIDTH;c++){
      grid[r][c]->out=grid[r][c]->in='\0';    
      for (i=0;i<16;i++) grid[r][c]->program[i]='\0';
      grid[r][c]->c_arr_ind=(-1);        /* Loc in chg stack */
      grid[r][c]->fail='0';
    }
  }
  c_arr_num=0;            /* Change stack */
  q_tail=q_head=0;        /* Event queue */
  return(0);
}

int check_rc(r,c)
int r,c;
{
  if (r < 0 || r >= HEIGHT || c < 0 || c >= WIDTH) return(1);
  return(0);
}

int write_cell_all(row,col,program,in,out) /* don't change inputs! */
int row,col;
char program[16],in,out;
{
  int i;

  if (check_rc(row,col)){
    fprintf(stderr,"ERROR: Bad rc ref (%d,%d) in write_cell\n",
       row,col);
    return(1);
  }
  for (i=0;i<16;i++)
    grid[row][col]->program[i]=program[i];
  grid[row][col]->in=in;
  grid[row][col]->out=out;  /* This is dangerous stuff! */

  return(0);
}

init_cmode(r,c)   /* Called to initialize c-mode array structures */
int r,c;
{
  if ((grid[r][c]->in) & 0xf0){  /* Cell is in c-mode */
    if (c_arr_num == C_ARR_SIZE) return(1); /* Too many c-mode cells! */
    c_arr[c_arr_num].row=r;
    c_arr[c_arr_num].col=c;
    grid[r][c]->c_arr_ind=c_arr_num++;
/*printf("%d [%d,%d]\n",c_arr_num,r,c);*/
  }
  return(0);
}
