class test2{
  public static void main(String args[]) {
    cell_matrix_channel pc,cc,pc_cc,brk;

    cell_matrix_interface cmi=new cell_matrix_interface("test2.seq","test2");

/* Start a wire */
    wire_build w=new wire_build(cmi,"e",0,0,"PC,CC");
    w.init();
    w.extend(2);
    w.turn("CW");
    w.extend(2);

    cmi.close();
  }
}
