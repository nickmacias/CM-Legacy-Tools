#include <stdio.h>
#ifdef PC
#include <io.h>
#endif

/*
 * (*&$*(#& microsoft totally screws up binary IO...it writes CR in front of any
 * LF, and if it reads Ascii 26, it fails on future read()s
 */

rread(fp,buffer,len)
int fp,len;
char *buffer;
{
 int i;
 char in[2];

 for (i=0;i<len;i++){
   if (2 != read(fp,in,2)) fprintf(stderr,"ERROR: Failed on read\n");
   buffer[i]=((in[0]<<4)&0xf0) | (in[1]&0xf);
 }
 return(len);
}
  
wwrite(fp,buffer,len)
int fp,len;
char *buffer;
{
   int i;
   char out[2];
   
   for (i=0;i<len;i++){
     out[0]=((buffer[i]>>4)&0xf) | 0x20;  
     out[1]=((buffer[i])&0xf) | 0x20; 
     if (2 != write(fp,out,2)) fprintf(stderr,"ERROR: Failed on write\n");
   }
   return(len);
}

iread(fp)   /* Read a 16-bit integer */
int fp;
{
  char c1,c2;
  unsigned int i,j;
  
  rread(fp,&c1,1);i=0xff&c1;
  rread(fp,&c2,1);j=0xff&c2;
  return(i+256*j);
}                 

void iwrite(fp,i)
int fp,i;
{
  char c;
  c=0xff&i;
  wwrite(fp,&c,1);
  c=(i>>8)&0xff;
  wwrite(fp,&c,1);
}
