/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class multi_sf{
  public static void main(String args[]) {

    cell_matrix_interface cmi;
    binary_file_reader bfr;
    space_filler sf;

    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cmi=new cell_matrix_interface("sf_w.seq",msg);
    bfr=new binary_file_reader("../loader/sbir/sf_w.bin");
    sf=new space_filler(cmi,46);
    sf.build(bfr,2);
    cmi.close();

    cmi=new cell_matrix_interface("sf_n.seq",msg);
    bfr=new binary_file_reader("../loader/sbir/sf_n.bin");
    sf=new space_filler(cmi,46);
    sf.build(bfr,3);
    cmi.close();

    cmi=new cell_matrix_interface("sf_s.seq",msg);
    bfr=new binary_file_reader("../loader/sbir/sf_s.bin");
    sf=new space_filler(cmi,46);
    sf.build(bfr,1);
    cmi.close();

    cmi=new cell_matrix_interface("sf_e.seq",msg);
    bfr=new binary_file_reader("../loader/sbir/sf.bin");
    sf=new space_filler(cmi,46);
    sf.build(bfr,0);
    cmi.close();

    cmi=new cell_matrix_interface("sf_init.seq",msg);
    bfr=new binary_file_reader("../loader/sbir/sf.bin");
    sf=new space_filler(cmi,46);
    sf.init(bfr);
    cmi.close();

  }
}
