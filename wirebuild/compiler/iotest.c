#include <stdio.h>
#include <string.h>
#include <fcntl.h>
 
#include <io.h>


void sendbyte(int);
int getbyte();

/* Stuff for command file execution */

main()
{
  char buffer[120],*stat;
  int i,j,k;         
 
  printf("\n> ");
  stat=gets(buffer);

  while (NULL != stat){
    j=0;k=1;
    for (i=strlen(buffer)-1;i>=0;i--){
      if (buffer[i]=='1') j+=k;k<<=1;
    }                                
    sendbyte(j);
    j=getbyte();
    printf("Return=%02x\n",j);
    printf("\n> ");
    stat=gets(buffer); 
  }
  printf("^D\n");
  return(0);
}


void sendbyte(int b)
{
  int i;
  float j;
  b^=3;	/* Toggle lowest 2 bits */
  outp(0x378,b);	/* Send it out! */
  j=0.;
  for (i=0;i<1000;i++){
    j=j+(float)i;
  }
  if (j == 0.){
    printf("Huh?\n");
  }
}

int getbyte()
{
  int i;
  i=inp(0x379);
  return(i);
}
