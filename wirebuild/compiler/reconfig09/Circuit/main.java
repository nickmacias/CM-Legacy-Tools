/*
 * Copyright (C) 2009 Nicholas J. Macias and
 * Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class main{
  public static void main(String args[]) {
    int i,c,r;
    cell_matrix_channel pc,cc,mbrk,bpc,bcc,bbrk;
    int stage,num_stages=3; // # of Medusa stages (across)

// these are for saving rotate/flip states
    cmstate rstate,wstate,bstate,mstate; // reset, main wire, break wire, Medusa wire

// Define circuits...
// ckt1 is a 4x3 1-D parallel repliction circuit
    int ckt1_h=4; // Height of initial circuit (Step 1)
    String ckt1[][]={
        {"dn=s", "dn=s;cn=e;de=1", "dw=n~s&;dn=ns&;ds=w"}, // head cells:brk pc cc
        {"dn=w,de=w", "de=w,dn=s", "de=w,dn=s"}, // brk channel
        {"de=w", "dn=w,de=w", "dn=s,de=w"}, // PC channel
        {"de=w", "de=w", "dn=w,de=w"} // CC channel
    };

// ckt2 is the 6x3 Medusa circuit
    int ckt2_h=6; // Height of Medusa circuit (Step 2)
    String ckt2[][]={
        {"dn=s", "dn=s;cn=e;de=1", "dw=n~s&;dn=ns&;ds=w"}, // head cells:brk pc cc
        {"dn=s;de=w", "dn=w;ds=w;de=w", "dn=s;de=w"}, // PC channel
        {"dn=s;de=w", "ds=n;de=w", "dn=w;de=w"}, // CCN channel
        {"dn=w;de=w;ds=w", "ds=n;de=w", "de=w"}, // BRK channel
        {"ds=n;de=w", "ds=n;de=w", "ds=w;de=w"}, // CCS channel
        {"ds=n", "ds=n;cs=e;de=1", "dn=w;dw=s~n&;ds=ns&"} // Head cells to S
    };

    String msg="Copyright (C) 2009 Nicholas J. Macias/Cell Matrix Corp. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("S",msg);

// Useful truth tables
    String cfg_west="dw=s;cw=1;";
    String cfg_east="de=s;ce=1;";

    rstate=cmi.state(); // This is the default ("reset") state of flip/rot

// Start a wire
    wire_build w=new wire_build(cmi,"s",0,5,"BRK,PC,CC");
// This is a bit odd...the BRK line is actually MBRK (the brk line for the
// Medusa wire). The main (side) wire's BRK line is to the Left of CC, and
// will be built as a separate wire ("bw")
//
// (so bw+w form the sidewire...)

// Instantiate the separate wire ("bw") just for the sidewire's BRK line
    wire_build bw=new wire_build(cmi,"s",0,2,"BRK,PC,CC");

// Access channels within the wires
    cc=w.get_channel("cc");
    pc=w.get_channel("pc");
    mbrk=w.get_channel("brk"); // Medusa's break line

    bcc=bw.get_channel("cc");
    bpc=bw.get_channel("pc");
    bbrk=bw.get_channel("brk");

// Init and build the break piece of the sidewire
    bw.preclear(true);
    bw.extend_wire(true);
    bw.extend_break(true);
    bw.init();
    bstate=cmi.state(); // Save this

// and the main sidewire...
    w.extend_break(true); // bring BRK line along
    w.preclear(true); // clear pathway in front before building
    w.extend_wire(true); // and make an extendible wire, so we can extend it :P
    w.init();
    wstate=cmi.state();


// For building the initial circuit, we need to move far South, turn East, and
// move across building ckt1 stages to the North
    w.extend(10);

// turn East, and begin building the Medusa wire across the matrix
// We'll have to route the BRK line around the corner ourselves...
    cmi.state(rstate);
    pc.set("de=n;ce=1");cc.set(true);cmi.cycle();
    pc.set("de=n");cc.set(false);cmi.cycle(); // cfg corner BRK cell
    pc.set(false);cc.set(true);cmi.cycle();
    pc.set(false);cc.set(false);cmi.cycle();

    w.extend_break(false); // Skip the break extension right b4/after corner
    cmi.state(wstate);
    w.extend();

    w.turn("CCW");
    mstate=cmi.state(); // Medusa wire's state

    w.extend();
    w.extend_break(true);

// and extend the Medusa wire East a few steps...
    w.extend(3);

// Initial Medusa loop: repeat this block once for each stage to be built...
    for (stage=0;stage<num_stages;stage++){

// Extend three steps, then turn North and build initial circuits
      w.extend_break(true); // bring BRK line along
      w.preclear(true); // clear pathway in front before building
      w.extend_wire(true); // and make an extendible wire, so we can extend it :P
      w.turn("ccw");

      cmi.push();  // Save current state
      cmi.rotate("RESET");

// Make active break wire head
      pc.set(cfg_west);cc.set(true);cmi.cycle();
      pc.set("CE=w");cc.set(false);cmi.cycle(); // Build bent break line
      w.extend_break(false);

// Since we're only building a circuit 4 cells high, we'll just
// extend/build/brk and not worry about Pascal (4+3+2+1)

      for (i=ckt1_h;i>0;i--){
        cmi.pop();
        w.extend(i);
        cmi.push();cmi.rotate("RESET");

// now build tgt cells
        pc.set(cfg_west);cc.set(true);cmi.cycle();
        pc.set(ckt1[ckt1_h-(i)][0]);cc.set(false);cmi.cycle(); // cfg cell to left
        pc.set(cfg_east);cc.set(true);cmi.cycle();
        pc.set(ckt1[ckt1_h-(i)][2]);cc.set(false);cmi.cycle(); // cfg cell to right
        pc.set(ckt1[ckt1_h-(i)][1]);cc.set(true);cmi.cycle(); // cfg tgt cell
        cc.set(false);mbrk.set(true);cmi.cycle(); // and break wire
        mbrk.set(false);
      }

// Now we've built a stage of ckt1. Break the W->E wire and repeat
// the whole cycle
      pc.set(cfg_west);cc.set(true);cmi.cycle();
      pc.set("cs=w");cc.set(false);cmi.cycle(); // this makes a brk for W->E
      pc.set(false);cc.set(true);cmi.cycle(); // Clear pc tgt cell
      cc.set(false);mbrk.set(true);cmi.cycle(); // break the W->E wire
      mbrk.set(false);cmi.cycle(); // cycle is optional???
      cmi.pop();cmi.rotate("RESET"); // Return to W->E orientation

      w.extend_break(true);
      w.extend(4); // Move to next loc
    }

// Now break the main sidwewire, and re-build it to connect to the Medusa wire
// We'll need the sidewire's breakwire also (to drive CCs)
    cmi.state(wstate);
    w.init();
    w.extend_break(true);
    w.preclear(true);
    w.extend(7);
// MANUALLY EXTEND BREAK!
    w.turn("ccw");
    w.extend(2); // Connect to top 3 control lines of Medusa wire
    w.preclear(false);
// DON'T EXTEND...set PC and CC to driver D outputs
    w.extend();

/***
// run the break wire South a bunch of steps
    cmi.state(bstate);
    bw.extend(5);
// build a break cell to disrupt both pieces of the sidewire
    cmi.state(rstate);
    bpc.set("de=n;ce=1");bcc.set(true);cmi.cycle();
    bpc.set("ce=n");bcc.set(false);cmi.cycle();
    bpc.set(false);bcc.set(true);cmi.cycle();
    bpc.set(false);bcc.set(false);cmi.cycle();

    cmi.state(bstate);
    bw.extend_break(false);

    bw.extend(1);
    bw.turn("ccw");
    bw.extend(7);

***/


    cmi.cycle();
    cmi.close();
  }

}
