/*
 * Copyright (C) 2009 Nicholas J. Macias and
 * Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class main2{
  public static void main(String args[]) {
    int i,c,r;
    cell_matrix_channel pc,cc,pc_cc,brk;

// Define circuits...

    int stage,num_stages=3; // # of medussa stages (across)

// ckt2 is the 6x3 Medusa circuit
    String ckt2[][]={
        {"dn=s", "dn=s;cn=e;de=1", "dw=n~s&;dn=ns&;ds=w"}, // head cells:brk pc cc
        {"dn=s;de=w", "dn=w;ds=w;de=w", "dn=s;de=w"}, // PC channel
        {"dn=s;de=w", "ds=n;de=w", "dn=w;de=w"}, // CCN channel
        {"dn=w;de=w;ds=w", "ds=n;de=w", "de=w"}, // BRK channel
        {"ds=n;de=w", "ds=n;de=w", "ds=w;de=w"}, // CCS channel
        {"ds=n", "ds=n;cs=e;de=1", "dn=w;dw=s~n&;ds=ns&"} // Head cells to S
    };
    int ckt2_h=6; // Height of Medusa circuit (Step 2)

    String msg="Copyright (C) 2009 Nicholas J. Macias/Cell Matrix Corp. All Rights Reserved";

    cell_matrix_interface cmi=new cell_matrix_interface("S2",msg);

// Useful cells
    String cfg_west="dw=s;cw=1;";
    String cfg_east="de=s;ce=1;";

// Start a wire
    wire_build w=new wire_build(cmi,"e",7,0,"BRK,PC,CC");
    pc=w.get_channel("pc");
    cc=w.get_channel("cc");
    brk=w.get_channel("brk"); // Access wire builder class' wire channels

    w.extend_break(false); // bring BRK line along
    w.preclear(true); // clear pathway in front before building
    w.extend_wire(true); // and make an extendible wire, so we can extend it :P

// Initial main loop: repeat this block once for each stage to be built...

    for (i=ckt2_h;i>0;i--){
      cmi.rotate("RESET");
// Build pull-back break cell
      pc.set(cfg_west);cc.set(true);cmi.cycle();
      pc.set("ce=s");cc.set(false);cmi.cycle(); // configure brk cell
      pc.set(false);cc.set(true);cmi.cycle(); // clear tgt cell
      cc.set(false);

      cmi.rotate("ccw"); // since we're moving North
      w.extend(i-1);
      cmi.rotate("RESET");

// now build tgt cells
      pc.set(cfg_west);cc.set(true);cmi.cycle();
      pc.set(ckt2[ckt2_h-(i)][0]);cc.set(false);cmi.cycle(); // cfg cell to left
      pc.set(cfg_east);cc.set(true);cmi.cycle();
      pc.set(ckt2[ckt2_h-(i)][2]);cc.set(false);cmi.cycle(); // cfg cell to right
      pc.set(ckt2[ckt2_h-(i)][1]);cc.set(true);cmi.cycle(); // cfg tgt cell
      cc.set(false);brk.set(true);cmi.cycle(); // and break wire
      brk.set(false);
    }

// Now we've built multiple copies of ckt2, *in parallel* :)

    cmi.cycle();
    cmi.close();
  }
}
