import java.util.*;
import java.awt.*;
import java.awt.event.*;

class msgwin extends Frame
implements ActionListener /* for CLEAR button */
{
  TextArea ta;

/*** CONSTRUCTOR ***/
  msgwin()
  {
    super("");

/* Now we add the elements to the cell-selector window */
    setLayout(new BorderLayout());

    Panel p1=new Panel();
/* Add buttons */
    Button b1=new Button("Clear");
    b1.addActionListener(this);
    p1.add(b1);

    Panel p3=new Panel();
    p3.setLayout(new BorderLayout());
    ta=new TextArea();
    p3.add(ta,"Center");

/* Now place these in window */
    add(p3,"Center");
    add(p1,"South");

    this.setSize(200,150);

    repaint();
    show();

    addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent we){
        dispose();
      }
    });
  } /* End of constructor */

  public void actionPerformed(ActionEvent ae)
  {
    if (ae.getActionCommand().equals("Clear")){
      ta.setText("");
    } else dispose();
  }

  public void addtext(String t)
  {
    ta.setText(ta.getText() + t + "\n");
    ta.select((ta.getText()).length()-1,ta.getText().length()-1);
  }

} /* End of msgwin */
