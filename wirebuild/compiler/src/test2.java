/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class test2{
  public static void main(String args[]){

  String msg="Copyright(C) 2001 Cell Matrix Corporation. All Rights Reserved";

  cell_matrix_interface cmi;
  wire_build w;
  cell_matrix_channel brk,pc,pc_c,cc;

// Create an interface
  cmi=new cell_matrix_interface("test.seq",msg);  // Main interface object
  pc=cmi.assign_channel(0,0,"dw");
  pc_c=cmi.assign_channel(0,0,"cw");
  pc_c.set(true);
  cmi.rotate("cw");
  pc.set("DE=W");
  cmi.cycle();
  pc_c.set(false);cmi.cycle();
  cmi.close();
  }
}
