/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class ctest2{
  public static void main(String args[]) {
    int i;
    cell_matrix_channel pc,cc,pc_cc,cc_cc;

    String msg="Copyright (C) 2001 Cell Matrix Corporation. All Rights Reserved";
    cell_matrix_interface cmi=new cell_matrix_interface("test.seq",msg);

    pc=cmi.assign_channel(0,0,"DW");
    pc_cc=cmi.assign_channel(0,0,"CW"); /* For programming initial PC cell */
    cc=cmi.assign_channel(1,0,"DW");
    cc_cc=cmi.assign_channel(1,0,"CW");

/* Initialize head cells */
    pc_cc.set(true);
    pc.set("CE=1;");
    cc_cc.set(true);
    cc.set("DW=N;DN=WE~&;DE=WE&");
    cmi.cycle();

    cc_cc.set(false);
    pc_cc.set(false);
    cmi.cycle();

    pc_cc.set(true);
    pc.set("DW=E;DE=W;DS=1;CE=S");
    cmi.cycle();

/* We're done directly controlling head cells */
    pc_cc.set(false);

/* Call build east method */
    for (i=0;i<5;i++)
      build_east(cmi,pc,cc);

/* Done! */
    cmi.close();
  }

  static void build_east(cell_matrix_interface cmi,
                 cell_matrix_channel pc,
                 cell_matrix_channel cc){
    cc.set(true);
    pc.set("DS=W;CS=1;CE=1"); /* Includes pre-clear */
    cmi.cycle();

    cc.set(false);
    pc.set("DW=N;DN=WE~&;DE=WE&");
    cmi.cycle();

    cc.set(true);
    pc.set("DW=E;DE=W;DS=1;CE=S");
    cmi.cycle();

    cc.set(false);pc.set(false);cmi.cycle();
  }

}
