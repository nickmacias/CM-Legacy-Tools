import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;

/*
 * constcomp is the main class
 * compiles constants into .GRD files using math/poly/1 or 0
 */

public class constcomp extends Frame 
{
  public static void main(String args[])
  {
    if (args.length != 1){
      System.out.println("Usage: constcomp inputfilename");
      return;
    }
    constcomp c=new constcomp(args[0]);
/* pause here */
    System.out.print("Hit enter to finish: ");
    BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    try{br.readLine();}catch(Exception e){};
  }

/* A few global variables */

/* Constructor for this class */
  constcomp(String name)
  {
    String buffer,cbuf;
    int v;

    if (!myopen(name)) return;
    start_grid_file();

    while (more()){
      buffer=next();
      System.out.println("> " + buffer);
      v=breakout(buffer);
      dump(v);
    }
    close_grid_file();
    myclose();
  }

/*
 * My file input routines
 */
BufferedReader br;
  boolean myopen(String fname) /* 0=success */
  {
    try {
      FileReader fr=new FileReader(fname);
      br=new BufferedReader(fr);
      return(true);
    } catch (Exception e){
      System.out.println("File open error:" + e);
      return(false);
    }
  }

  String nextline;

  boolean more() /* true if more input available */
  {
    int i;

    try {
      nextline=br.readLine();
      return(nextline != null);
    } catch (Exception e){
      System.out.println("File read error:" + e);
      return(false);
    }
  }

  String next() /* Return next string (up to \n) */
  {
    return(nextline);
  }

  void myclose() /* Close the input stream */
  {
    try {
      br.close();
    } catch (Exception e){
    }
  }

  FileWriter fw;
  BufferedWriter out;
  void start_grid_file()
  {
/* Start the GRID file */
    try
    {
      fw=new FileWriter("CONST.GRD");
      out=new BufferedWriter(fw);
      out.write("GRID_FILE\n32\n64\n");
    } catch (Exception e){
      System.out.println("Failure while creating grid file: "+e);
    }
  }

  void write_grid_file(int i)
  {
    try
    {
      out.write("MATH\\POLY.LIB" + "\002" + "CELL_2_" +
                ((i==1)?"1":"2") +
                "\002" + "0\n"); 
    } catch (Exception e){
      System.out.println("Failure while adding to  grid file: "+e);
    }
  }

  void close_grid_file()
  {
    try
    {
      out.close();	/* Done with file */
    } catch (Exception e){
      System.out.println("Failure while closing grid file: "+e);
    }
  }

  int breakout(String buffer)
  {
    int sign,mask,iexp,val,bitnum;
    double exp,work;

    double fval=Double.valueOf(buffer).doubleValue();

/* Is fval=0? Too bad! We'll deal with that later... */

/* Store sign, and make fval positive */
    sign=0;
    if (fval < 0){
      sign=1;
      fval=(-fval);
    }

/* Compute exponent */
    exp=Math.floor(Math.log(fval)/Math.log(2.));  /* cool... */

/* Now break out the mantissa's bits,and store them in result */
    work=fval/Math.pow(2.,exp); /* Normalized mantissa */
    work=work-1; /* Toss the 1 left of the BP */
    mask=1<<22; /* OR this if the bit is set */
    val=0;
    for (bitnum=22; bitnum>=0; bitnum--){ /* really just a counter */
      work=work*2.;
      if (work >= 1.){
        val|=mask;
        work=work-1.;
      }
      mask>>=1;
    }

    exp=exp+127.; /* Final exponent adjustment */
System.out.println("Converting " + fval + " to exp=" + exp + " and " + val);
    iexp=(int)exp;
    iexp&=0xff;
    val|=(iexp<<23);

/* and set the sign bit */
    if (sign == 1) val|=(1<<31);

    return(val);
  }

static int firstdump=1;

  void dump(int v)
  {
    int i;

try{
// Do we need inter-row spacing?
    if (firstdump==1){firstdump=0;} else {
      for (i=0;i<32;i++){
        out.write("WIRE.LIB\002West -> East\0023\n\n");
      }
    }

    for (i=0;i<32;i++){
      if ((v&0x80000000)==0)
        {write_grid_file(0);
      } else {
        write_grid_file(1);
      }
      if (i != 31){
        out.write("WIRE.LIB\002West -> East\0020\n");
      } else {out.write("\n");}
      v=v<<1;
    }
  }
catch(Exception e){};
}
}
