/*
 * Copyright (C) 1992-2002 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

/*
 * cell_exchange class. Basic class used to exchange cell information with
 * the cell_matrix_sim class. Includes a truth table compiler
 * for loading the tt[] array via equations
 */

import java.io.*;
import java.lang.*;
import java.util.*;

class cell_exchange{
  public int c_in;
  public int c_out;
  public int d_in;
  public int d_out;
  public int tt[];
  public String desc;
  boolean error;
  int num_dim, num_sides; /* Save these at instantiation */
  int ttsize; /* # of rows */
  String labels;

  BufferedWriter bw=null;
/* Constructor requires two arguments: dimensionality and # of sides */
  public cell_exchange(int dim, int sides)
  {
    num_dim=dim;
    num_sides=sides;
    ttsize=1<<num_sides; /* so 4-sided cll has 16 rows */
    tt=new int[2*ttsize]; /* Make room for truth table */

/* Add label/etc. setups here */

    if ((num_sides==4) && (num_dim==2)){ /* Standard setup */
      labels=new String("NESW");
    }

    if ((num_sides==6) && (num_dim==3)){ /* Standard 3D setup */
      labels=new String("NESWTB");
    }

    desc="";

  }

/* Compile a hex string */
  public int hex_comp(String val)
  {
    int ind,i;

    val=val.toLowerCase();
    if (ttsize==16){ // four-sided
      for (i=0;i<2*ttsize;i++){
        if (i < val.length()){
          ind="0123456789abcdef".indexOf(val.charAt(i));
          if (ind < 0) return(1); /* Error */
        } else {ind=0;}
        tt[(2*ttsize-1)-i]=ind;
      }
    } else { // assume 6-sided!
// In this case, use 2 hex chars for each 6-bit half-row
// Assume 00-3f
      for (i=0;i<4*ttsize;i++){ // 2 hex chars per half-row
        if (i < val.length()){
          ind="0123456789abcdef".indexOf(val.charAt(i));
          if (ind < 0) return(1); /* Error */
        } else {ind=0;}
        if ((i&1)==1){ // LSBs
          ind &= 3; // only keep 2 LSBs of hex value
          tt[(2*ttsize-1)-(i>>1)] |= (ind<<4);
        } else {
          tt[(2*ttsize-1)-(i>>1)]=ind;
        }
      }
    }

    return(0);
  }

/* Allow caller to see interesting vars */
  public String get_labels()
  {
    return(labels);
  }

  public int get_dim()
  {
    return(num_dim);
  }

  public int get_num_sides()
  {
    return(num_sides);
  }

  public int get_ttsize(){return(ttsize);}

  public StringBuffer[] tt_dump()
  {
    int i,j,row;
    StringBuffer r[]=new StringBuffer[1+2*ttsize];
/* Make header line first */
    r[0]=new StringBuffer("");
    for (i=num_sides-1;i>=0;i--){
      r[0].append(labels.charAt(i) + " ");
    }
    r[0].append("|");
    for (i=num_sides-1;i>=0;i--){
      r[0].append("C"+labels.charAt(i)+" ");
    }
    for (i=num_sides-1;i>=0;i--){
      r[0].append("D"+labels.charAt(i)+" ");
    }

/* Now make each row */
    for (row=0;row<ttsize;row++){
      r[row+1]=new StringBuffer("");
      for (i=num_sides-1;i>=0;i--){ /* Store input pattern */
        r[row+1].append(((0!=(row&(1<<i)))?"1":"0")+" ");
      }
      r[row+1].append("|");
      for (j=0;j<2;j++){ /* C and D */
        for (i=num_sides-1;i>=0;i--){ /* Each output column */
          r[row+1].append(" "+((0!=(tt[row+j*ttsize]&(1<<i)))?"1":"0")+" ");
        }
      }
    }
    return(r);
  }

/*
 * Truth table compiler: reads a string of equations (separated by ";")
 * and compiles them into celltio.tt[]
 * Also sets cell_exchange.error and returns that as function's return
 */

  public int tt_compile(String in,BufferedWriter bwin)
  {
    int i,ind,j,offset,column,array[];
    char c;
    String buffer,temp;
    StringBuffer program=new StringBuffer(16);

    bw=bwin;
    stack_init(); /* Clean up stack */
    error=true; /* Default state */
  
    array=new int[ttsize]; /* Stores bits in output column */

/* Clear the cell's truth table */
    for (i=0;i<ttsize*2;i++) tt[i]=0;
    buffer=in.toUpperCase(); /* Turn argument into SB */

/* Break into pieces separated by semicolons */
    StringTokenizer st=new StringTokenizer(buffer,";");

    while (st.hasMoreTokens()){
      temp=st.nextToken();
/* remove spaces, and skip if empty ***/
      StringBuffer tempsb=new StringBuffer("");
      for (i=0;i<temp.length();i++){
        if (temp.charAt(i) != ' '){
          tempsb.append(temp.charAt(i));
        }
      }
      temp=tempsb.toString();

      if (temp.length()==0) return(0);
/* Process temp */

      switch (temp.charAt(0)){
        case 'C':offset=0;break;
        case 'D':offset=ttsize;break;
        default:
                 my_outln(bw,"Expecting C or D in LHS, got " + temp.charAt(0));
                 return(1); /* Error */
      }
    
      c=temp.charAt(1); /* Destination side */
      column=labels.indexOf(c); /* Which column we're setting */
      if (column == -1){
        my_outln(bw,"Expecting one of " + labels + " but saw " + c);
        return(2);
      }

      column=1<<column; /* Turn into a bitmask */

      c=temp.charAt(2); /* "=" */
      if (c != '='){
        my_outln(bw,"Expecting \"=\" but saw " + c);
        return(3);
      }

/* Compile the TT row using the remainder of temp */
/* array[] contains a stream of 1s and 0s. array[0]=-1 on compile error */
      array=tabulate(temp.substring(3));
      if (array[0]==-1) return(4);
  
/* Now load it in */
      
      for (i=0;i<ttsize;i++){
        tt[offset]&=(~column); /* Clear indicated bit */
        if (array[i]==1) tt[offset]|=column; /* and set if bit should=1 */
        ++offset; /* Next row of TT */
      }
    } /* End of current equation */
    error=false; /* Success! */
    return(0);
  } /* End of tt_compile */


/* ljkd's TABULATE.C                                                     */
/*     -  REVERSE POLISH BITWISE CALCULATOR FOR NICK'S TRUTH TABLES      */
/* Polish calculator from K&R modified to do bitwise operations          */
/* Reverse Notation required: write DN := ~E N|S  as DN := E! NS|        */
/*     ( or (E!)(NS|) ). Any spaces are ignored.                         */
/* INPUT: takes the symbolic RHS of the statement above -----^           */
/* as subprocedure, takes this RHS as a one-line argument "input"        */
/* OUTPUT: the boolean value of the RHS for all possible values of E, N, &S */
/*         in the form of an array called result.                        */
/* Program can be used interactively. To see partial results, put parens */
/* around subexpressions.                                                */

  int buf_loc; /* Marches through get_op()'s argument */
  char op; /* latest meaninfgul character from input string */
  int op_value; /* If op is a side, returns value of that input for this
                 * row of the truth table. Also used for constants "1" or "0"
                 */
  int op_type; /* Codes type of latest operator, as follows */
    static final int ERROR=0; /* Unknown character */
    static final int SIDE=1; /* eg. N S, W, E */
    static final int AND=2; /* & */
    static final int OR=3; /* + | */
    static final int XOR=4; /* ^ */
    static final int NOT=5; /* ~ ! */
    static final int NUMBER=6; /* 1 0 */

  int[] tabulate(String buffer)
  {
    int result[]=new int[ttsize]; /* Store results here */
    int row; /* Current row in TT-determines values of sides */

    for (row=0;row<ttsize;row++){ /* Each input combination */
      buf_loc=0; /* Current character to parse */
      while (get_op(buffer,row)){
        switch (op_type){ /* Loaded by get_op() */
          case SIDE:
            push(op_value); /* Store value of that side for this row */
            break;
          case AND: push(pop() & pop());break;
          case OR: push(pop() | pop());break;
          case XOR: push(pop() ^ pop());break;
          case NOT: push(~pop());break;
          case NUMBER: push(op_value);break;
          default:
            my_outln(bw,"Error? op=" + op);
            result[0]=(-1); /* Store error flag */
            return(result);
        }
      } /* Done getting ops-line processed */
      result[row]=pop()&1;/* Store final result in current row */
    } /* result[] fully loaded now */
    return(result);
  }

/*
 * get_op() reads the next character in the input string
 * and does preliminary parsing (type, value, etc.)
 * Also handles spaces
 * Second argument passes current row to get_op so we can
 * determine value of each side.
 */

  boolean get_op(String buffer,int row)
  {
    int i;

    while (buf_loc < buffer.length()){
      op=buffer.charAt(buf_loc++); /* Current character */
      if (op=='0'){op_type=NUMBER;op_value=0;return(true);}
      if (op=='1'){op_type=NUMBER;op_value=1;return(true);}
      if ((op=='+')||(op=='|')){op_type=OR;return(true);}
      if (op=='&'){op_type=AND;return(true);}
      if (op=='^'){op_type=XOR;return(true);}
      if ((op=='~')||(op=='!')){op_type=NOT;return(true);}
      if ((i=labels.indexOf(op)) != -1){ /* Got a label */
        op_type=SIDE;
        op_value=(0!=((1<<i) & row))?1:0; /* Value of side in this row */
        return(true);
      }
/* Haven't matched the character yet. If it's a space, w can skip it. */
/* Otherwise, we have an error */
      if ((op != ' ') && (op != '\t')){
        my_outln(bw,"Unknown character on RHS: " + op);
        op_type=ERROR; /* Flag caller */
        return(true); /* bt return TRUE so caller checks */
      }
    } /* Otherwise, just continue through the string */
/* Hit end of string! */
    return(false);
  }

/* Stack code */
  int[] stack=new int[100];
  int sp;

  void stack_init()
  {
    sp=0;
    return;
  }

  void push(int i)
  {
    stack[sp++]=i;
    if (sp==99){ /* Close to an overflow */
      my_outln(bw,"Stack overflow in parser. Resetting stack.");
      sp=0;
    }
  return;
  }

  int pop()
  {
    return(stack[--sp]);
  }

  void my_outln(BufferedWriter bw,String buf)
  {
    my_out(bw,buf + "\n");
  }

  void my_out(BufferedWriter bw,String buf)
  {
    if (bw==null){
      System.out.print(buf);
      return;
    }

    try{
      bw.write(buf);
      bw.flush();
    } catch (Exception e){
    }
  }
}
