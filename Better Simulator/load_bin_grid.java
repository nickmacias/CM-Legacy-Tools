/*
 * Copyright (C) 1992-2002 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

/*
 * load_bin_grid class. Abstract class with static method for reading
 * a binary grid file as written by the Portable Layout Editor (TM).
 */

/*
 * There's a special hook in here: If a cell's description includes the
 * string <<<PERSERVEORDER>>> then the bit pattern of the TT is left
 * unchanged. The special loader .H3X directive does two things:
 * (1) it writes the user's desired bit patern unchanged; and
 * (2) it appends the above tag to the cell's description, so that the
 *     simulator does not modify the bit pattern.
 * This is useful (critical) for specifying crystals, integers, etc.
 *
 */

import java.io.*;
import java.lang.*;
import java.util.*;

abstract class load_bin_grid
{
  static BufferedWriter bw;

  static public void load(cell_matrix_sim sim, cell_exchange ce,
                          int[] ind, String filename,int rbase,int cbase,
                          boolean threaded,BufferedWriter bwin)
  {
    bw=bwin;
    if (sim.get_dim() == 2){
      load2d(sim,ce,ind,filename,rbase,cbase,threaded);
    } else if (sim.get_dim()==3){
      load3d(sim,ce,ind,filename);
    } else my_outln(bw,"Don't know how to read "+sim.get_dim()+" dimensional files");
  }

  static public void load2d(cell_matrix_sim sim, cell_exchange ce,
                          int[] ind, String filename,int rbase,int cbase,
                          boolean threaded)
  {
    int row,col,startrow,startcol,rows,cols;
    int i,j,count,temp[]=new int[32],desclen,freq,r,c;
    
    boolean V2;

    startrow=ind[0];startcol=ind[1];

    try{
      FileReader fr=new FileReader(filename);
      String buffer,desc;

/* Read comment and check version */
      buffer=readString(fr,80);
      my_outln(bw,buffer);

      if (buffer.toString().indexOf("3-D") >= 0){
        my_outln(bw,"ERROR: Grid is 3-D. Cannot load");
        return;
      }

      V2=(-1!=buffer.toString().indexOf("Java Loader V2.0"));

/* Read dimensions of grid */
      rows=readInt(fr);cols=readInt(fr);
      my_outln(bw,"Grid is " + rows + "x" + cols);

/* Read cell defs for each cell */
      freq=((rows*cols)/72)+1; if (freq<1) freq=1; /* For progress display*/
      count=0;
      for (row=0;row<rows;row++){
        for(col=0;col<cols;col++){
// [row,col] is loc in file
          if ((++count)%freq == 0) my_out(bw,".");
          readByte(fr); /* Ignore inputs */
          readByte(fr); /* Ignore outputs */
          for (i=0;i<16;i++){
            temp[i]=readByte(fr); // TT bits
          }
          desclen=readInt(fr); /* # of chars in cell desription */
          if (desclen > 0){
            desc=readString(fr,desclen);
            ce.desc=desc;
          } else {ce.desc="";}

          ce.c_in=ce.d_in=ce.c_out=ce.d_out=0;

// See if we have the special <<<PRESERVEORDER>>> tag in the descriptor
          if (-1!=ce.desc.indexOf("<<<PRESERVEORDER>>>")){ // No TT rearrange!
            for(i=0;i<16;i++){
              ce.tt[2*i]=(temp[i]>>4)&15;
              ce.tt[2*i+1]=temp[i]&15;
            }
          } else { // No tag, so convert TT from V2 to V3

/* Now load the TT into the cell_exchange structure */
            for (i=0;i<16;i++){
              ce.tt[i]=(temp[i]>>4)&15; /* C cols are first */
              ce.tt[i+16]=temp[i]&15; /* D cols are last */
            }
/* Convert if V2 */
            if (V2){ /* Need to translate V2 TT to V3 TT */
              for (i=0;i<16;i++){
  /* Old input order is SNEW; New order is WSEN */
  /* Therefore, map row number [r3r2r1r0]->r0r3r1r2 */
                j=((i&1)<<3) | ((i&8)>>1) | (i&2) | ((i&4)>>2);
  
  /* Old output order was NSWE; New order is WSEN */
  /* Therefore, map row value c3c2c1c0->c1c2c0c3 */
                temp[j]=((ce.tt[i]&2)<<2) | (ce.tt[i]&4) |
                         ((ce.tt[i]&1)<<1) | ((ce.tt[i]&8)>>3);
                temp[j+16]=((ce.tt[i+16]&2)<<2) | (ce.tt[i+16]&4) |
                         ((ce.tt[i+16]&1)<<1) | ((ce.tt[i+16]&8)>>3);
              }
              for (i=0;i<32;i++){
                ce.tt[i]=temp[i];
              }
            } /* End of V2 bit twiddling */
          } // End of bit twiddle OR leave alone (H3X)
// See if we should load this [row,col] cell
          if ((row >= rbase) && (col >= cbase)){ // we want this cell
            r=row-rbase;c=col-cbase; // load in here in target
            r+=startrow;c+=startcol;
            if ((r<sim.dims[0])&&(c<sim.dims[1])){
              sim.user_write_cell(r,c,ce);
              if (!threaded) sim.process_queue();
            }
          }
        }
      } /* All cells have been read */
      my_outln(bw," ");
      fr.close();
    } catch (Exception e){
      my_outln(bw,"Error opening " + filename + ":" + e);
    }
  }

/* 3D binary grid loader */
/* Load into a single Z layer */
  static public void load3d(cell_matrix_sim sim, cell_exchange ce,
                          int[] ind, String filename)
  {
    int row,col,startrow,startcol,rows,cols,layer;
    int i,j,count,temp[]=new int[128],desclen,freq;
    
    boolean V2;

    startrow=ind[0];startcol=ind[1];layer=ind[2];

    try{
      FileReader fr=new FileReader(filename);
      String buffer;

/* Read comment and check version */
      buffer=readString(fr,80);
      my_outln(bw,buffer);

      if (buffer.toString().indexOf("3-D") == -1){
        my_outln(bw,"ERROR: Grid is 2-D. Cannot load");
        return;
      }
      V2=(-1!=buffer.toString().indexOf("Java Loader V2.0"));

/* Read dimensions of grid */
      rows=readInt(fr);cols=readInt(fr);
      my_outln(bw,"Grid is " + rows + "x" + cols);

/* Read cell defs for each cell */
      freq=((rows*cols)/72)+1; if (freq<1) freq=1; /* For progress display*/
      count=0;
      for (row=startrow;row<startrow+rows;row++){
        for(col=startcol;col<startcol+cols;col++){
          if ((++count)%freq == 0) my_out(bw,".");
          readByte(fr); /* Ignore inputs */
          readByte(fr); /* Ignore outputs */
          for (i=0;i<128;i++){
            temp[i]=readByte(fr);
          }
          desclen=readInt(fr); /* # of chars in cell desription */
          if (desclen > 0){
            buffer=readString(fr,desclen);
          }

          ce.c_in=ce.d_in=ce.c_out=ce.d_out=0;
/* Now load the TT into the cell_exchange structure */
          for (i=0;i<128;i++){
            ce.tt[i]=temp[i];
          }

          sim.user_write_cell(row,col,layer,ce);
          sim.process_queue();
        }
      } /* All cells have been read */
      my_outln(bw," ");
      fr.close();
    } catch (Exception e){
      my_outln(bw,"Error opening " + filename + ":" + e);
    }
  }





  static int readByte(FileReader fr)
  {
    char c[]=new char[2];

    try{
      fr.read(c);
    } catch (Exception e){
      my_outln(bw,"Read error: " + e);
    }
    return( ((((int)c[0])&15)<<4) | (((int)c[1])&15) );
  }

  static int readInt(FileReader fr)
  {
    int lsb,msb;
    lsb=255&readByte(fr);
    msb=255&readByte(fr);
    return((msb<<8) | lsb);
  }

  static String readString(FileReader fr,int len)
  {
    StringBuffer s=new StringBuffer("");
    int i;

    for (i=0;i<len;i++){
      s.append((char)readByte(fr));
    }
    return(s.toString());
  }

  static void my_outln(BufferedWriter bw,String buf)
  {
    my_out(bw,buf + "\n");
  }

  static void my_out(BufferedWriter bw,String buf)
  {
    if (bw==null){
      System.out.print(buf);
      return;
    }

    try{
      bw.write(buf);
      bw.flush();
    } catch (Exception e){
    }
  }

}
