abstract class ByteIO
{
  static byte[] b=new byte[4];

  static byte[] to_byte(int i)
  {
    b[0]=(byte) (i&0xff);
    b[1]=(byte) ((i>>8)&0xff);
    b[2]=(byte) ((i>>16)&0xff);
    b[3]=(byte) ((i>>24)&0xff);
//System.out.println("Changing " + i + " to " + b[0]+" "+b[1]+" "+b[2]+" "+b[3]);
    return(b);
  }

  static int from_byte(byte b[])
  {
    int i;
    i=(0xff&b[0]) | (0xff00&(b[1]<<8)) |
      (0xff0000&(b[2]<<16)) | (0xff000000&(b[3]<<24));
//System.out.println("Changed " + b[0]+" "+b[1]+" "+b[2]+" "+b[3]+ " to " + i);
    return(i);
  }
}
