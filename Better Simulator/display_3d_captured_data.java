import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.text.*;

class display_3d_captured_data
{
  public static void main(String args[])
  {
    BufferedReader infile=null;
    int h=0,w=0,zz=0; // Height, width and Z height
    int i,j,k;
    int mult=10; // Size of each pixelblock
    String buffer,title="";
    boolean more=true;

  boolean setup_3d_done=false;

/* 3D display parameters */
  int xmin,xmax,ymin,ymax; /* For 2D display */
  int xv,yv,zv; /* Center of 3D viewport */

    if (args.length!=2){
      System.out.println("Usage: display_3d_captured_data filename pixelsize");
      System.exit(1);
    }

    try{
      mult=Integer.parseInt(args[1]);
    } catch (Exception e) {
      System.out.println("Unknown pixel size (" + args[1] + ")");
      System.exit(1);
    }
// open infile name
    try{
      FileReader fr=new FileReader(args[0]);
      infile=new BufferedReader(fr);
    } catch (Exception e){
      System.out.println("Error opening " + args[0] + ": " + e);
      System.exit(1);
    }

// Get dims
    try{
      String dims=infile.readLine();

      h=Integer.parseInt(dims.substring(0,dims.indexOf("x")));
      zz=Integer.parseInt(dims.substring(dims.indexOf(":")+1));
      i=dims.indexOf("x")+1;j=dims.indexOf(":");
      w=Integer.parseInt(dims.substring(i,j));

    } catch (Exception e){
      System.out.println("Empty file or bad header? " + e);
      System.exit(1);
    }

// make a display object
    my_display md=new my_display(h,w,zz,mult);

// Main loop: read title, then run-length-encoded display information
    while (more){
      try{
for (i=0;i<51;i++){
        title=infile.readLine();
}
        if (title==null){
          more=false;
        } else {
          buffer=infile.readLine();
          if (buffer==null){
            more=false;
          } else {
            md.refresh(buffer,title);
          }
        }
      } catch(Exception e){
        System.out.println("EOF");
        more=false;
      }
    }
    try{
      infile.close();
    } catch(Exception e){
      System.out.println("Close error: " + e);
    }
  }
}

class my_display extends Frame
{
  String buffer;
  int h,w,zz,mult;
  //Color bgcol=new Color(32,32,32);
  Color bgcol=new Color(0,0,0);
  Color ccol=new Color(255,0,0);
  Color d0col=new Color(0,96,0);
  Color d1col=new Color(0,255,0);
  Color fcol=Color.orange;

  my_display(int w_in,int h_in,int zz_in,int mult_in)
  {
    mult=mult_in;
    h=h_in;
    w=w_in;
    zz=zz_in;

    setSize(20+w*mult,70+h*mult);
    setSize(1000,1000);
    setBackground(new Color(0,0,0));
    setVisible(true); // now we're showing the drawing window

// 3D setup
    graph3d.setwin(0,1000,0,1000);
    graph3d.setup(0,mult*(1+w),
                  0,mult*(1+zz),
                  0,mult*(1+h),
          100*mult*w,120*mult*zz,200*mult*h);
int m=40;
    graph3d.setup(0,m*(1+w),
                  0,m*(1+zz),
                  0,m*(1+h),
          100*m*w,120*m*zz,200*m*h);
 
// event handlers
    addWindowListener(new MyWindowAdapter());
  }

  String global_title="";

// Main display routine - show a single page
  public void refresh(String bufferin,String title)
  {
    global_title=title;

    buffer=bufferin;

    Graphics g=getGraphics();
    update(g);
  }

  public void update(Graphics g) 
  {
    paint(g);
  }


  public void paint(Graphics g)
  {
    int i,r,c,z,run;
    char type;

//graph3d.box(g);
    //g.setColor(Color.green);
    //g.drawString(global_title,50,45);

    r=c=z=0; // *next* loc

    try{
      while (buffer.length()!=0){
// scan the incoming line
        if (buffer.charAt(0)=='<'){ // run
          type=buffer.charAt(1);
          run=Integer.parseInt(buffer.substring(2,buffer.indexOf('>')));
          switch(type){
            case '0': g.setColor(bgcol);break;
            case '1': g.setColor(d0col);break;
            case '2': g.setColor(d1col);break;
            case '3': g.setColor(ccol);break;
            case '4': g.setColor(fcol);break;
            default: System.out.println("Unknown color: " + type);
          }
          for (i=0;i<run;i++){
            //g.fillRect(10+c*mult,60+r*mult,mult,mult);
if (!((type=='0') && (run>500))){
            graph3d.rect(g,c*mult,z*mult,r*mult,
                           (c+1)*mult,z*mult,r*mult,
                           (c+1)*mult,z*mult,(r+1)*mult,
                           c*mult,z*mult,(r+1)*mult);
}

            if (++c >= w){c=0;if (++r >= h){r=0;++z;}}
          }
          buffer=buffer.substring(buffer.indexOf(">")+1);
        } else { // single char
          type=buffer.charAt(0);
          switch(type){
            case '0': g.setColor(bgcol);break;
            case '1': g.setColor(d0col);break;
            case '2': g.setColor(d1col);break;
            case '3': g.setColor(ccol);break;
            case '4': g.setColor(fcol);break;
            default: System.out.println("Unknown color: " + type);
          }
          //g.fillRect(10+c*mult,60+r*mult,mult,mult);
          graph3d.rect(g,c*mult,z*mult,r*mult,
                         (c+1)*mult,z*mult,r*mult,
                         (c+1)*mult,z*mult,(r+1)*mult,
                         c*mult,z*mult,(r+1)*mult);
          if (++c >= w){c=0;if (++r >= h){r=0;++z;}}
          buffer=buffer.substring(1);
        }
      }
    } catch (Exception e){
      System.out.println("Unknown error displaying page: "+ e);
      return;
    }
  }

  class MyMouseAdapter extends MouseAdapter
  {
    public void mouseReleased(MouseEvent me)
    {
    }

    public void mousePressed(MouseEvent me)
    {
    }
  }

  class MyWindowAdapter extends WindowAdapter
  {
    public void windowClosing(WindowEvent we)
    {
      System.out.println("Bye!");
      System.exit(0);
    }
  }
}
