import java.io.*;
import java.util.*;
import java.awt.event.KeyEvent;

public class main3{
  static int NUM_SIDES=6;
  static int DIM=3;
  static int DIMS[]=new int[32];

  public static void main(String args[])
  {
    window_class mw; /* Main drawing window */
    cell_matrix_sim sim; /* Main simulation engine */
    cell_exchange ce;
    user_callback uc; // this is how the window class can pass key events back to us

/* Setup dimensionality/etc. */
    DIMS[0]=8;DIMS[1]=8;DIMS[2]=8; /* Arbitrary default */

    sim=new cell_matrix_sim(NUM_SIDES,DIMS[0],DIMS[1],DIMS[2],false);
    ce=new cell_exchange(DIM,NUM_SIDES);
    uc=new user_callback();

    sim.set_unload_rate(1);

/* Set up graphics */
    mw=new window_class(sim,ce,uc);
    sim.save_window_class(mw);
    mw.show();
    mw.repaint();
    mw.unload_changes();
    mw.init_done();


/* Strt a free-running thread for continuous execution */
    //freerun fr=new freerun(sim);
    //fr.start();

    int coords[]=new int[3],i,key;

    coords[0]=0;coords[1]=0;coords[2]=0;
    ce=sim.user_read_cell(coords);
    ce.tt_compile("CE=W;DE=N");
    sim.user_write_cell(coords,ce);
    sim.user_set_input(coords,'D',ce.get_labels().indexOf('W'),1);
    sim.user_set_input(coords,'D',ce.get_labels().indexOf('N'),1);

    for (i=0;i<4;i++){
      sim.tick();sim.process_queue();mw.unload_changes();
    }
    sim.user_set_input(coords,'D',ce.get_labels().indexOf('W'),0);

    while (true){
      sim.tick();sim.process_queue();mw.unload_changes();
      sim.do_pending();
      if (uc.check()){
        key=uc.get_key();
        switch(key){
          case KeyEvent.VK_Q: mw.dispose();System.exit(0);
          case KeyEvent.VK_A:sim.user_set_input(coords,'D',ce.get_labels().indexOf('W'),0);break;
          case KeyEvent.VK_B:sim.user_set_input(coords,'D',ce.get_labels().indexOf('W'),1);break;
        }
      }
    }
  }
}

class user_callback
{
  int lastkey; // stores last keypress
  boolean newkey;  // set when a key is pressed; clear once we read it

  user_callback()
  {
    newkey=false;
  }

  boolean check()
  {
    return(newkey);
  }

  int get_key()
  {
    newkey=false;
    return(lastkey);
  }

  void set_key(int key)
  {
    lastkey=key;
    newkey=true;
  }
}
