/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;

/*
 * DISPLAY MODES
 *
 * 1 - single-color blocks (selectable size)
 * 2 - block outlines, each side color-coded (selectable size/sep)
 * 3 - like 2, codes sides by thinkness
 * 4 - unused?
 * 5 - Solid blocks, edges highlighted (selectable size/sep)
 * 6  -Colors and arrows (selectable size)
 *      3-D MODES
 * 7 - normal filled blocks
 * 8 - filled blocks with gaps
 * 9 - very flat blocks with gaps
 * 10 - 2-D rects in XZ plane, colored based on ALL outputs
 * 13 - red/blue dual-view for 3-D viewing
 *
 */

/* Drawing class */
class window_class extends Frame
  implements AdjustmentListener
{
  static int fillsize=56; /* 30 Size of squares+separation */
  static int fillsep=4; /* 4 Space between squares */
  static int display_mode=6;
  static int xoffset=10;
  static int yoffset=35;
  static int DIM; /* # of dimensions in matrix */
  static int DIMS[]; /* Array of dimensions */
  static int lastr=(-1);
  static int lastc;
  static boolean is_threaded=false; // is the main loop threaded?
  static boolean THREED=false; // 3-D red/blue display
  static double delta3D=1; // x units from nose to eye :P

// The following are used for recording display actions
  boolean capture=false; // set if we want to record Mode 1 screen output
  BufferedWriter capture_bw;
  int lastrun=0; // saves index of current run
  int runlength=0; // length of current run

  static int lastrow;
  static int lastcol=-1;

  Scrollbar vscroll,hscroll;
  static int row_origin=0;
  static int col_origin=0;

  boolean setup_3d_done=false;

/* 3D display parameters */
  static int xmin,xmax,ymin,ymax; /* For 2D display */
  static int xv,yv,zv; /* Center of 3D viewport */

  cell_matrix_sim sim;
  cell_exchange ce;
  Color empty_color,c_color,d0_color,d1_color,blackcolor;
  Color faultcolor=Color.orange;
  Color fill_color,d_color; /* Mode 6 */
  Color m6_empty_color;
  int size_x,size_y;
  Graphics saveg=null;
  user_callback save_uc;
  window_class savethis;

// left/right red/blue colors
    Color right0=new Color(255,0,0);
    Color right1=new Color(255,0,0);
    Color left0=new Color(64,255,255);
    Color left1=new Color(64,255,255);

  window_class(cell_matrix_sim s, cell_exchange c,user_callback uc)
  {
    savethis=this; // this is probably totally unnecessary...
/* Grab info from simulator/etc. class */
    sim=s;
    ce=c;
    DIM=sim.get_dim();
    DIMS=sim.get_dims();
    save_uc=uc;

/* Color setups */
    //empty_color=new Color(128,128,128);
    empty_color=new Color(64,96,128);
    c_color=new Color(255,0,0);
    d0_color=new Color(0,128,0);
    d1_color=new Color(0,255,0);
    //fill_color=new Color(255,255,0);
    fill_color=new Color(0,128,255);
    m6_empty_color=new Color(160,160,160);
    d_color=new Color(0,0,0);
    blackcolor=new Color(128,128,128);
    setBackground(empty_color);
//    setBackground(new Color(255,255,255));

/* Set size of window */
    size_y=400;
    size_x=400;
    setSize(size_x,size_y);

/* This should be more sophisticated for 3D display */

    if (DIM== 3){ /* 3D matrix */
      graph3d.setwin(0,size_x,0,size_y);
      graph3d.setup(0,fillsize*(1+DIMS[1]),
                    0,fillsize*(1+DIMS[2]),
                    0,fillsize*(1+DIMS[0]),
            100*fillsize*DIMS[1],120*fillsize*DIMS[2],200*fillsize*DIMS[0]);
    }

// Add scrollbars for 2D display
    if (DIM==2){
      setLayout(new BorderLayout());
      vscroll=new Scrollbar(Scrollbar.VERTICAL,0,5,0,0);
      vscroll.addAdjustmentListener(this);
      add(vscroll,"East");
      hscroll=new Scrollbar(Scrollbar.HORIZONTAL,0,5,0,0);
      hscroll.addAdjustmentListener(this);
      add(hscroll,"South");
      vscroll.setBlockIncrement(16);vscroll.setMaximum(DIMS[0]);
      hscroll.setBlockIncrement(16);hscroll.setMaximum(DIMS[1]);
    }

/* Event handlers */
    addMouseListener(new MyMouseAdapter());
    addMouseMotionListener(new MyMouseMotionAdapter());
    addComponentListener(new MyComponentAdapter());
    addKeyListener(new MyKeyAdapter());
    addWindowListener(new MyWindowAdapter());
  }

  void set3d(boolean f)
  {
    THREED=f;
  }

  void threaded(boolean t)
  {
    is_threaded=t;
  }

  void set_size(int x,int y)
  {
    size_x=x;size_y=y;
    setSize(x,y);
    repaint();
  }

  public void adjustmentValueChanged(AdjustmentEvent ae)
  {
    Scrollbar sb=(Scrollbar) ae.getAdjustable();
    row_origin=vscroll.getValue();
    col_origin=hscroll.getValue();
    repaint();
    savethis.requestFocusInWindow();
  }


  class MyWindowAdapter extends WindowAdapter
  {
    public void windowClosing(WindowEvent we)
    {
      System.exit(0);
    }
  }

  class MyKeyAdapter extends KeyAdapter
  {
    public void keyPressed(KeyEvent e)
    {
      boolean hs;

      switch (e.getKeyCode()){
        case KeyEvent.VK_LEFT:
             graph3d.rotate(.025,0);graph3d.adjust();unload_all();break;
        case KeyEvent.VK_RIGHT:
             graph3d.rotate(-.025,0);graph3d.adjust();unload_all();break;
        case KeyEvent.VK_UP:
             graph3d.rotate(0,-.025);graph3d.adjust();unload_all();break;
        case KeyEvent.VK_DOWN:
             graph3d.rotate(0,.025);graph3d.adjust();unload_all();break;
        case 27: /* ESC */
          if (!is_threaded){ // ESC only STOPS clock ('g' starts it)
            sim.halted_state(true);
            System.out.println("Clock is halted.");
          } else {
            hs=sim.halted_state(); // read state
            sim.halted_state(!hs);
            System.out.println("Clock is " + ((!hs)?"halted.":"running."));
          }
          break;
        case KeyEvent.VK_I: // reset viewpoint
          graph3d.view(100*fillsize*DIMS[1],
                       120*fillsize*DIMS[2],
                       200*fillsize*DIMS[0]);
          graph3d.savea1=graph3d.ANG1;
          graph3d.savea2=graph3d.ANG2;
          graph3d.adjust();
          unload_all();
          break;
        case KeyEvent.VK_MINUS: // - key...bring eyes closer together
          delta3D=delta3D-.1*fillsize*DIMS[1];
          //if (delta3D < 0.) delta3D=0.;
          unload_all();
          System.out.println("eye-nose delta: " + delta3D);
          break;
        case KeyEvent.VK_EQUALS: // =/+ key...bring eyes further apart
          delta3D=delta3D+.1*fillsize*DIMS[1];
          unload_all();
          System.out.println("eye_nose delta: " + delta3D);
          break;
        default:
             save_uc.set_key(e.getKeyCode());
      }
    }
  }

  class MyComponentAdapter extends ComponentAdapter
  {
    public void componentResized(ComponentEvent e)
    {
      unload_all();
    }
  }

  boolean mousedown=false;

  class MyMouseAdapter extends MouseAdapter
  {
    public void mouseReleased(MouseEvent me)
    {
      mousedown=false;
    }

    public void mousePressed(MouseEvent me)
    {
      int row,col,x,y, clickx,clicky;
      int d1,d2,d3,d4; // Displacements for arrows, etc
      int snum;
      int val;
      boolean cmode;
      int tol=4; // Click tolerance (closeness to line)

      if (mousedown) return;
      mousedown=true;

// Right click for C-mode change
      cmode=((me.getModifiers() & me.BUTTON3_MASK) == me.BUTTON3_MASK) ? true:false;

      d1=2*fillsize/10;
      d2=4*fillsize/10;
      d3=6*fillsize/10;
      d4=8*fillsize/10;

      clickx=me.getX()-xoffset;
      clicky=me.getY()-yoffset;

// See in which cell we clicked
      row=clicky/fillsize;
      col=clickx/fillsize;

// Find displacement inside cell
      y=clicky-fillsize*row;
      x=clickx-fillsize*col;

      if ((x >= 0) && (x <= d1) &&
          (y >= d2-tol) && (y <= d2+tol)){
        snum=3; // West
      } else if ((x >= d4) && (x <= fillsize) &&
                 (y >= d3-tol) && (y <= d3+tol)){
        snum=1; // East
      } else if ((y >= 0) && (y <= d1) &&
                 (x >= d3-tol) && (x <= d3+tol)){
        snum=0; // North
      } else if ((y >= d3) && (y <= fillsize) &&
                 (x >= d2-tol) && (x <= d2+tol)){
        snum=2; // South
      } else if ((x >= d1) && (x <= d4) &&
                 (y >= d1) && (y <= d4)){
        dump_tt_data(row+row_origin,col+col_origin);
        return;
      } else return;

// Read current cell's inputs
      ce=sim.user_read_cell(row+row_origin,col+col_origin);

// Set the input to its opposite value
      if (!cmode) val=((ce.d_in & (1<<snum)) == 0)?1:0;
      else val=((ce.c_in & (1<<snum)) == 0)?1:0;

/*
 * Since we're inside an event handler, we don't want to 
 * start manipulating the queue here, in case the main program
 * was already midway through maniuplating the queue.
 *
 * First, record the input change as a pending request
 *
 */
      sim.pending_user_set_input(row+row_origin,col+col_origin,(cmode)?'C':'D',snum,val);

// apply this immediately if safe to do so
      if (sim.halted_state()){
        sim.do_pending(); // process GUI input changes
      }
// Request and release a lock. If the release works, then we had the lock,
// and we'll do the sim.do_pending() call
// Otherwise, someone else has the lock, and their release
// will cause sim.do_pending() to be called.

    }

    void dump_tt_data(int row, int col)
    {
      int ind[]={0,0},i;

      if (DIM==2){
        ind[0]=row;
        ind[1]=col;
        ce=sim.user_read_cell(ind);
        if (ce.error){System.out.println("Failed");return;}
        String t[]=bdump.decomp(ce);
        System.out.println("\n");
        System.out.println("["+ind[0]+","+ind[1]+"]");
        for (i=0;i<8;i++){
          if (t[i].length()>0) System.out.println(t[i]);
        }
        System.out.println("----------------\n");
        lastr=ind[0];
        lastc=ind[1];
      } /* End of 2D dump */
    }
  }

  class MyMouseMotionAdapter extends MouseMotionAdapter
  {

    public void mouseMoved(MouseEvent me)
    {
      int clickx,clicky,row,col;
      int ind[]={0,0},i;
      StringBuffer title;
      String ti;

      if (!savethis.hasFocus()) savethis.requestFocusInWindow();

      clickx=me.getX()-xoffset;
      clicky=me.getY()-yoffset;

// See which cell we're pointing to
      row=clicky/fillsize;
      col=clickx/fillsize;
row+=row_origin;
col+=col_origin;

// is this a new cell?
      if ((row==lastrow) && (col==lastcol)) return;

// Find its [r,c] index
      lastrow=row;lastcol=col;
      title=new StringBuffer("["+row + "," + col+"]: ");

// Now grab this cell's truth table

      if (DIM==2){
        ind[0]=row;
        ind[1]=col;
        ce=sim.user_read_cell(ind);
        if (ce.error){/*System.out.println("Failed");*/return;}
// probably just moved outside cell region in a big window

        String t[]=bdump.decomp(ce);

        for (i=0;i<8;i++){
          if (t[i].length()>0) title.append(t[i]+";");
        }

// and append the cell's description if available
        if (ce.desc.length() > 0){
          title.append(" <" + ce.desc + ">");
        }

// Remove newlines
        ti=title.toString();
        while (ti.indexOf("\n") >= 0){
          ti=ti.substring(0,ti.indexOf("\n")) + ". " +
             ti.substring(1+ti.indexOf("\n"));
        }

        setTitle(ti);
      } /* End of 2D dump */
    }


    public void mouseDragged(MouseEvent me)
    {
    }
  }

  public void init_done()
  {
    setup_3d_done=true;
  }

/* Graphics display routines */
/***
  public void update(Graphics g)
  {
    paint(g);
  }
***/

  int ind[]=new int[32];
  public void unload_changes()
  {
    if (!isShowing()) return;

    if (capture) capture_do(DIM); // record everytime we unload

    saveg=getGraphics();

/* Unload any pending changes */
    ind=sim.unload_changes();
    while (ind[0] != -1){
//System.out.println("Unloading [" + ind[0] + "," + ind[1] + "]");
      display_cell(saveg,ind);
//###      draw_wiremesh(saveg,ind);
      ind=sim.unload_changes();
    }
  }

  public void unload_all()
  {
    if (!isShowing()) return;

    Dimension d=getSize();
    graph3d.setwin(0,d.width,0,d.height);

    saveg=getGraphics();
//    paint(saveg);
    repaint();
  }

  public void paint(Graphics g)
  {
    if (!isShowing()) return;

    int ind[]=new int[3];

    if (DIM==2){
      for (ind[0]=0;ind[0]<DIMS[0];ind[0]++){ // Row
        for (ind[1]=0;ind[1]<DIMS[1];ind[1]++){ // Column
          display_cell(g,ind);
        }
      }
    } else if (DIM==3){
      if (display_mode==13){ // red/blue
        setBackground(new Color(255,255,255));
        g.setXORMode(Color.white);
        graph3d.view1(delta3D);
        graph3d.box(g,right1);
        graph3d.view0(delta3D);
        graph3d.box(g,left1);
      } else graph3d.box(g);

      for (ind[0]=0;ind[0]<DIMS[0];ind[0]++){ // Row
        for (ind[1]=0;ind[1]<DIMS[1];ind[1]++){ // Column
          for (ind[2]=0;ind[2]<DIMS[2];ind[2]++){ // Layer
            display_cell(g,ind);
          }
        }
      }
//###     draw_wiremesh_all(g);
    }
  }

  void display_cell(Graphics g, int[] ind)
  {
    if (DIM==2){
      display_cell_2d(g,ind);
      return;
    }

    if (DIM==3){
      display_cell_3d(g,ind);
      return;
    }
  }

  static int hlen=3,wlen=4; // Height and Width of arrowhead

// General arrow drawing routine
  void arrow(Graphics g,int xstart,int ystart,int length,int dx, int dy,boolean head)
  {

    if (head) g.drawLine(xstart,ystart,xstart+(length*dx),ystart+((length-1)*dy));
    else g.drawLine(xstart,ystart,xstart+(length*dx),ystart+((length)*dy));

    if (!head) return;

    if (dy == 0){ // Horizontal arrow
      g.drawLine(xstart+length*dx,ystart,xstart+(length-wlen)*dx,ystart+hlen);
      g.drawLine(xstart+length*dx,ystart,xstart+(length-wlen)*dx,ystart-hlen);
    } else { // Vertical arrow
      g.drawLine(xstart,ystart+(length*dy),xstart+hlen,ystart+((length-wlen)*dy));
      g.drawLine(xstart,ystart+(length*dy),xstart-hlen,ystart+((length-wlen)*dy));
    }
  }

  void capture_begin(String fname)
  {
    if (capture){
      System.out.println("ERROR: Already capturing!");
      return;
    }

    try{
       FileWriter fw=new FileWriter(fname);
       capture_bw=new BufferedWriter(fw);
    } catch (Exception e){
      System.out.println("Open error: " + e);
      capture=false;
      return;
    }
    capture=true; // Capture is on!
    lastrun=runlength=0; // doesn't really matter here...
    try{
      if (DIM==2){capture_bw.write(sim.dims[0]+"x"+sim.dims[1]+"\n");}
      else if (DIM==3){capture_bw.write(sim.dims[0]+"x"+sim.dims[1]+":"+
                                        sim.dims[2]+"\n");}
    } catch (Exception e){
      System.out.println("Write error: " + e);
    }
  }

  void capture_end()
  {
    if (!capture){
      //System.out.println("ERROR: Not currently capturing");
      return;
    }

// dump last run
    if (runlength > 0){
      try{
        capture_bw.write("<" + lastrun + runlength + ">");
      } catch (Exception e){
        System.out.println("Write error: " + e);
      }
      runlength=0;
    }

    try{
      capture_bw.close();
    } catch (Exception e){
      System.out.println("Close error: " + e);
    }
    capture=false;
  }

  void capture_do(int dim) // Do a capture
  {
    int row,col,z,i,ff;
    int ind[]={0,0,0};
    int index=0; // 0:empty; 1:all Dout=0; 2:some Dout=1; 3: C-mode.
// 4=defective cell
// index is used if we're capturing screen activity (mode 1)
    cell_exchange ce;
    boolean empty=true;

    if (!capture) return;

// First, write the window's title to the file
    try{
      capture_bw.write(getTitle()+"\n");
    } catch(Exception e){
      System.out.println("Write error: " + e);
    }

    if (dim==2){
      for (row=0;row<sim.dims[0];row++){
        for (col=0;col<sim.dims[1];col++){
          ce=sim.user_read_cell(row,col);

// Is cell's TT all 0s?
          empty=true;
          for (i=0;i<2*ce.get_ttsize();i++){
            if (ce.tt[i]!=0){
              empty=false;
              break;
            }
          }

// Determine nature of this cell's state

// is cell defective?
          ind[0]=row;ind[1]=col;
          ff=sim.get_fault_flags(ind);
          if ((ff==1) || (ff==3) || (ff==4) || (ff==5)){
            index=4;
          } else if (ce.c_in != 0){
// Is cell in C-mode?
            index=3;
          } else if (empty){
// Cell is in D-mode. Is TT empty?
            index=0;
          } else if ((ce.c_out == 0) && (ce.d_out==0)){ // All outupts=0
            index=1;
          } else {
            index=2;
          }
// Now deal with the index
// See if we're continuing a run
          if (index==lastrun){
            ++runlength; // yup
          } else { // dump last run
            if (runlength < 5){ // not cost-effective
              for (i=0;i<runlength;i++){
                try{
                  capture_bw.write(lastrun + "");
                } catch(Exception e){
                  System.out.println("Write error: " + e);
                }
              }
            } else {
              try{
                capture_bw.write("<" + lastrun + runlength + ">");
              } catch(Exception e){
                System.out.println("Write error: " + e);
              }
            }
            lastrun=index; // this is our new run
            runlength=1; // start counting again
          } // last run is dumped
        } // end of cols
      } // end of rows

// dump last run
      if (runlength > 0){
        try{
          capture_bw.write("<" + lastrun +  runlength + ">");
        } catch (Exception e){
          System.out.println("Write error: " + e);
        }
        runlength=0;
      }
      try{
        capture_bw.write("\n");
      } catch (Exception e){
        System.out.println("Write error: " + e);
      }
    } // end of dim=2

    else if (dim==3){ // fun!
      for (z=0;z<sim.dims[2];z++){
        for (row=0;row<sim.dims[0];row++){
          for (col=0;col<sim.dims[1];col++){
            ce=sim.user_read_cell(row,col,z);

// Is cell's TT all 0s?
            empty=true;
            for (i=0;i<2*ce.get_ttsize();i++){
              if (ce.tt[i]!=0){
                empty=false;
                break;
              }
            }

// Determine nature of this cell's state

// is cell defective?
            ind[0]=row;ind[1]=col;ind[2]=z;
            ff=sim.get_fault_flags(ind);
            if ((ff==1) || (ff==3) || (ff==4) || (ff==5)){
              index=4;
            } else if (ce.c_in != 0){
// Is cell in C-mode?
              index=3;
            } else if (empty){
// Cell is in D-mode. Is TT empty?
              index=0;
            } else if ((ce.c_out == 0) && (ce.d_out==0)){ // All outupts=0
              index=1;
            } else {
              index=2;
            }
// Now deal with the index
// See if we're continuing a run
            if (index==lastrun){
              ++runlength; // yup
            } else { // dump last run
              if (runlength < 5){ // not cost-effective
                for (i=0;i<runlength;i++){
                  try{
                    capture_bw.write(lastrun + "");
                  } catch(Exception e){
                    System.out.println("Write error: " + e);
                  }
                }
              } else {
                try{
                  capture_bw.write("<" + lastrun + runlength + ">");
                } catch(Exception e){
                  System.out.println("Write error: " + e);
                }
              }
              lastrun=index; // this is our new run
              runlength=1; // start counting again
            } // last run is dumped
          } // end of cols
        } // end of rows
      } // end of z

// dump last run
      if (runlength > 0){
        try{
          capture_bw.write("<" + lastrun +  runlength + ">");
        } catch (Exception e){
          System.out.println("Write error: " + e);
        }
        runlength=0;
      }
      try{
        capture_bw.write("\n");
      } catch (Exception e){
        System.out.println("Write error: " + e);
      }
    } // end of dim=3
  }

  void display_cell_2d(Graphics g, int[] ind)
  {
    int i,xloc,yloc;
    int x1,x2,y1,y2;
    int d1,d2,d3,d4;
    boolean h; /* to pass arrowhead command */

    Color savec;
/*
 * d1=edge to start of inner cell (Mode 6)
 * d2=edge to inner arrow
 * d3=edge to outer arrow
 * d4=edge to end of inner cell
 */
    int ff; /* Fault flag */
    boolean empty;

    x1=xoffset+(ind[1]-col_origin)*fillsize;
    y1=yoffset+(ind[0]-row_origin)*fillsize;
    x2=x1+fillsize-fillsep;
    y2=y1+fillsize-fillsep;

    ce=sim.user_read_cell(ind);
    ff=sim.get_fault_flags(ind);

/* Is cell's TT all 0s? */
    empty=true;
    for (i=0;i<2*ce.get_ttsize();i++){
      if (ce.tt[i]!=0){
        empty=false;
        break;
      }
    }

    if (display_mode==6){ /* Show arrows */
// Upper left corner of cell space:
      x1=xoffset+(ind[1]-col_origin)*fillsize;
      y1=yoffset+(ind[0]-row_origin)*fillsize;

      if ((x1 > 2+getWidth()) || (y1 > 2+getHeight())) return;

// Compute displacements
      d1=2*fillsize/10; // Inner box egde
      d2=4*fillsize/10; // Inner arrow
      d3=6*fillsize/10; // Outer arrow
      d4=8*fillsize/10; // Outer box egde

// Fill in the middle
      if (empty){
        g.setColor(m6_empty_color);
      } else {
        g.setColor(fill_color);
      }
      g.fillRect(x1+d1,y1+d1,d4-d1,d4-d1);



// Start with outputs
// Normally, we skip arrowheads on outputs. But display them on edge cells */

// NORTH first
      if (ind[0]==0) h=true; else h=false;
      if (0 != (ce.c_out & 1)){savec=c_color;} else {savec=d_color;}
// RED if C out=1, otherwise BLACK

// Draw darker if D out is active
      if (0 != (ce.d_out & 1)){
        g.setColor(savec);
        arrow(g,x1+d2+1,y1+d1,d1,0,-1,h);
      } else {
        g.setColor(empty_color);
        arrow(g,x1+d2+1,y1+d1,d1,0,-1,h);
        g.setColor(savec);
      }

// Draw this arrow regardless
      arrow(g,x1+d2,y1+d1,d1,0,-1,h);


// SOUTH
      if (ind[0]==DIMS[0]-1) h=true;else h=false;
      if (0 != (ce.c_out & 4)){savec=c_color;} else {savec=d_color;}
// RED if C out=1, otherwise BLACK

// Draw darker if D out is active
      if (0 != (ce.d_out & 4)){
        g.setColor(savec);
        arrow(g,x1+d3+1,y1+d4,d1,0,1,h);
      } else {
        g.setColor(empty_color);
        arrow(g,x1+d3+1,y1+d4,d1,0,1,h);
        g.setColor(savec);
      }

// Draw this arrow regardless
      arrow(g,x1+d3,y1+d4,d1,0,1,h);


// WEST
      if (ind[1]==0) h=true;else h=false;
      if (0 != (ce.c_out & 8)){savec=c_color;} else {savec=d_color;}
// RED if C out=1, otherwise BLACK

// Draw darker if D out is active
      if (0 != (ce.d_out & 8)){
        g.setColor(savec);
        arrow(g,x1+d1,y1+d3+1,d1,-1,0,h);
      } else {
        g.setColor(empty_color);
        arrow(g,x1+d1,y1+d3+1,d1,-1,0,h);
        g.setColor(savec);
      }

// Draw this arrow regardless
      arrow(g,x1+d1,y1+d3,d1,-1,0,h);

// EAST
      if (ind[1]==DIMS[1]-1) h=true;else h=false;
      if (0 != (ce.c_out & 2)){savec=c_color;} else {savec=d_color;}
// RED if C out=1, otherwise BLACK

// Draw darker if D out is active
      if (0 != (ce.d_out & 2)){
        g.setColor(savec);
        arrow(g,x1+d4,y1+d2+1,d1,1,0,h);
      } else {
        g.setColor(empty_color);
        arrow(g,x1+d4,y1+d2+1,d1,1,0,h);
        g.setColor(savec);
      }

// Draw this arrow regardless
      arrow(g,x1+d4,y1+d2,d1,1,0,h);

// Now draw inputs (always do arrowheads)
// NORTH
      if (0 != (ce.c_in & 1)){savec=c_color;} else {savec=d_color;}
// RED if C out=1, otherwise BLACK

// Draw darker if D out is active
      if (0 != (ce.d_in & 1)){
        g.setColor(savec);
        arrow(g,x1+d3+1,y1,d1,0,1,true);
      } else {
        g.setColor(empty_color);
        arrow(g,x1+d3+1,y1,d1,0,1,true);
        g.setColor(savec);
      }

// Draw this arrow regardless
      arrow(g,x1+d3,y1,d1,0,1,true);


// SOUTH
      if (0 != (ce.c_in & 4)){savec=c_color;} else {savec=d_color;}
// RED if C out=1, otherwise BLACK

// Draw darker if D out is active
      if (0 != (ce.d_in & 4)){
        g.setColor(savec);
        arrow(g,x1+d2+1,y1+fillsize,d1,0,-1,true);
      } else {
        g.setColor(empty_color);
        arrow(g,x1+d2+1,y1+fillsize,d1,0,-1,true);
        g.setColor(savec);
      }

// Draw this arrow regardless
      arrow(g,x1+d2,y1+fillsize,d1,0,-1,true);


// WEST
      if (0 != (ce.c_in & 8)){savec=c_color;} else {savec=d_color;}
// RED if C out=1, otherwise BLACK

// Draw darker if D out is active
      if (0 != (ce.d_in & 8)){
        g.setColor(savec);
        arrow(g,x1,y1+d2+1,d1,1,0,true);
      } else {
        g.setColor(empty_color);
        arrow(g,x1,y1+d2+1,d1,1,0,true);
        g.setColor(savec);
      }

// Draw this arrow regardless
      arrow(g,x1,y1+d2,d1,1,0,true);

// EAST
      if (0 != (ce.c_in & 2)){savec=c_color;} else {savec=d_color;}
// RED if C out=1, otherwise BLACK

// Draw darker if D out is active
      if (0 != (ce.d_in & 2)){
        g.setColor(savec);
        arrow(g,x1+fillsize,y1+d3+1,d1,-1,0,true);
      } else {
        g.setColor(empty_color);
        arrow(g,x1+fillsize,y1+d3+1,d1,-1,0,true);
        g.setColor(savec);
      }

// Draw this arrow regardless
      arrow(g,x1+fillsize,y1+d3,d1,-1,0,true);




      if ((ff==1) || (ff==3) || (ff==4) || (ff==5)){
        g.setColor(faultcolor);
        //g.fillRect(x1+d2,y1+d2,d3-d2,d3-d2);
        g.fillRect(x1+d1+2,y1+d1+2,(d4-d1)-4,(d4-d1)-4);
      }
    } /* End of Show Arrows mode */

    if (display_mode==5){ /* Show directions of data flow */
// Adjust x1 and y1 to include separation between cells
      x1=xoffset+(ind[1]-col_origin)*(fillsize+fillsep);
      y1=yoffset+(ind[0]-row_origin)*(fillsize+fillsep);
/* Is cell in C-mode? */
      if (ce.c_in != 0){
        g.setColor(c_color);
        g.fillRect(x1,y1,fillsize,fillsize);
      } else {
/* Cell is in D-mode. Is TT empty? */
        if (empty){
          g.setColor(empty_color);
          g.fillRect(x1,y1,fillsize,fillsize);
        } else {
          g.setColor(d0_color);
          g.fillRect(x1+1*fillsize/10,y1+1*fillsize/10,8*fillsize/10,8*fillsize/10);
// AND ce.d_out with 1->North, 2->East, 4->South and 8->West
          if ((ce.d_out & 4) == 0){
            g.setColor(d0_color);
          } else {
            g.setColor(d1_color);
          }
          g.fillRect(x1,y1+9*fillsize/10,fillsize,1*fillsize/10);

          if ((ce.d_out & 2) == 0){
            g.setColor(d0_color);
          } else {
            g.setColor(d1_color);
          }
          g.fillRect(x1+9*fillsize/10,y1,1*fillsize/10,fillsize);

          if ((ce.d_out & 1) == 0){
            g.setColor(d0_color);
          } else {
            g.setColor(d1_color);
          }
          g.fillRect(x1,y1,fillsize,1*fillsize/10);

          if ((ce.d_out & 8) == 0){
            g.setColor(d0_color);
          } else {
            g.setColor(d1_color);
          }
          g.fillRect(x1,y1,1*fillsize/10,fillsize);

        }
      }

      if ((ff==1) || (ff==3) || (ff==4) || (ff==5)){
        g.setColor(faultcolor);
        g.fillRect(x1+fillsize/4,y1+fillsize/4,fillsize/2,fillsize/2);
      }
    } /* End of Show Direction of Data Flow mode */

    if (display_mode==1){ /* Solid rectangles */
/* Is cell in C-mode? */
      if (ce.c_in != 0){
        g.setColor(c_color);
      } else {
/* Cell is in D-mode. Is TT empty? */
        if (empty){
          g.setColor(empty_color);
        } else if ((ce.c_out == 0) && (ce.d_out==0)){ /* All outupts=0 */
          g.setColor(d0_color);
        } else {
          g.setColor(d1_color);
        }
      }
      g.fillRect(x1,y1,fillsize,fillsize);
      if ((ff==1) || (ff==3) || (ff==4) || (ff==5)){
        g.setColor(faultcolor);
        g.fillRect(x1+fillsize/4,y1+fillsize/4,fillsize/2,fillsize/2);
      }

    } // End of single-color filled rectangle (Mode 1) code

    if (display_mode==2){/* Unfilled rectangle: mark each side appropriately */
      if (empty){
        g.setColor(empty_color);
        g.fillRect(x1,y1,1+x2-x1,1+y2-y1);
        return;
      }
/* Now draw outline of square */
      our_setcolor(empty,g,ce.c_out&1,ce.d_out&1); g.drawLine(x1,y1,x2,y1);
      our_setcolor(empty,g,ce.c_out&2,ce.d_out&2); g.drawLine(x2,y2,x2,y1);
      our_setcolor(empty,g,ce.c_out&4,ce.d_out&4); g.drawLine(x1,y2,x2,y2);
      our_setcolor(empty,g,ce.c_out&8,ce.d_out&8); g.drawLine(x1,y2,x1,y1);
    }

    if (display_mode==3){/* Use edge thickness */
      if (empty){
        g.setColor(empty_color);
        g.fillRect(x1,y1,1+x2-x1,1+y2-y1);
        return;
      }
/* Now draw outline of square */
      if ((ce.c_out&1)!=0) g.setColor(Color.red); else g.setColor(Color.green);
      g.drawLine(x1,y1,x2,y1);if ((ce.d_out&1)==0) g.setColor(blackcolor);
      g.drawLine(x1+1,y1+1,x2-1,y1+1);

      if ((ce.c_out&2)!=0) g.setColor(Color.red); else g.setColor(Color.green);
      g.drawLine(x2,y1,x2,y2);if ((ce.d_out&2)==0) g.setColor(blackcolor);
      g.drawLine(x2-1,y1+1,x2-1,y2-1);

      if ((ce.c_out&4)!=0) g.setColor(Color.red); else g.setColor(Color.green);
      g.drawLine(x1,y2,x2,y2);if ((ce.d_out&4)==0) g.setColor(blackcolor);
      g.drawLine(x1+1,y2-1,x2-1,y2-1);

      if ((ce.c_out&8)!=0) g.setColor(Color.red); else g.setColor(Color.green);
      g.drawLine(x1,y1,x1,y2);if ((ce.d_out&8)==0) g.setColor(blackcolor);
      g.drawLine(x1+1,y1+1,x1+1,y2-1);

    }
  }

  void display_cell_3d(Graphics g, int[] ind)
  {
    int i,xloc,yloc;
    int x1,x2,y1,y2,z1,z2,x2p=0,y2p=0,z2p=0;
    boolean empty=false;

    x1=ind[1]*fillsize;
/* y should be inverted */
    y1=(ind[2])*fillsize;
    z1=ind[0]*fillsize;
    x2=x1+(fillsize)-fillsep;
    y2=y1+(fillsize)-fillsep;
    z2=z1+(fillsize)-fillsep;

    if ((display_mode==8) || (display_mode==13)){
      x2=x1+(fillsize/4)-fillsep;
      y2=y1+(fillsize/4)-fillsep;
      z2=z1+(fillsize/4)-fillsep;

// setup larger cube
      x2p=x1+(fillsize/3)-fillsep;
      y2p=y1+(fillsize/3)-fillsep;
      z2p=z1+(fillsize/3)-fillsep;
    }

    if (display_mode==9){
      x2=x1+(fillsize/2)-fillsep;
      y2=y1+2;
      z2=z1+(fillsize/2)-fillsep;
    }
//###

    ce=sim.user_read_cell(ind);
    int ff=sim.get_fault_flags(ind);

/* Is cell's TT all 0s? */
    empty=true;
    for (i=0;i<2*ce.get_ttsize();i++){
      if (ce.tt[i]!=0){
        empty=false;
        break;
      }
    }

if (empty && (ff==0)) return; // For now, only draw non-empty cells

    if (true){// Only case for now
      if (empty && (ff==0)){
        return;
      }

/* Now draw outline of cube */

      if (display_mode==10){ // Draw one rect in XZ plane
        if (ff != 0) {g.setColor(faultcolor);} else
          our_setcolor(empty,g,ce.c_out,ce.d_out);
// set color based on ALL outputs
        rect3d(g,0,1,0,x1,y1,z1,x2,y1,z1,x2,y1,z2,x1,y1,z2);
        return;
      }

// special case - Red/Blue 3-D view :)
      if (display_mode == 13){ // draw from two POV
        int eye;
        g.setXORMode(Color.white);

        for (eye=0;eye<2;eye++){
          if (eye==0){
            graph3d.view0(delta3D);
            g.setColor(left1);
          } else {
            graph3d.view1(delta3D);
            g.setColor(right1);
          }
 
          if (ce.d_out == 0){ // draw smaller box
            rect3d(g,1,0,0,x2,y2,z1,x2,y1,z1,x2,y1,z2,x2,y2,z2); // East
            rect3d(g,0,0,1,x1,y2,z2,x2,y2,z2,x2,y1,z2,x1,y1,z2); // South
            rect3d(g,0,1,0,x1,y2,z1,x2,y2,z1,x2,y2,z2,x1,y2,z2); // Top
          } else {
            rect3d(g,1,0,0,x2p,y2p,z1,x2p,y1,z1,x2p,y1,z2p,x2p,y2p,z2p);
            rect3d(g,0,0,1,x1,y2p,z2p,x2p,y2p,z2p,x2p,y1,z2p,x1,y1,z2p);
            rect3d(g,0,1,0,x1,y2p,z1,x2p,y2p,z1,x2p,y2p,z2p,x1,y2p,z2p);
          }

        }
        graph3d.view0(delta3D);
        return;
      }

      if (ff != 0) {g.setColor(faultcolor);} else
        our_setcolor(empty,g,ce.c_out&2,ce.d_out&2); // East
      rect3d(g,1,0,0,x2,y2,z1,x2,y1,z1,x2,y1,z2,x2,y2,z2);

      if (ff != 0) {g.setColor(faultcolor);} else
        our_setcolor(empty,g,ce.c_out&4,ce.d_out&4); // South
      rect3d(g,0,0,1,x1,y2,z2,x2,y2,z2,x2,y1,z2,x1,y1,z2);

      if (ff != 0) {g.setColor(faultcolor);} else
        our_setcolor(empty,g,ce.c_out&16,ce.d_out&16); // Top
      rect3d(g,0,1,0,x1,y2,z1,x2,y2,z1,x2,y2,z2,x1,y2,z2);

/***
      our_setcolor(empty,g,ce.c_out&8,ce.d_out&8); // West
      rect3d(g,-1,0,0,x1,y2,z1,x1,y1,z1,x1,y1,z2,x1,y2,z2);

      our_setcolor(empty,g,ce.c_out&1,ce.d_out&1); // North
      rect3d(g,0,0,-1,x1,y2,z1,x2,y2,z1,x2,y1,z1,x1,y1,z1);

      our_setcolor(empty,g,ce.c_out&32,ce.d_out&32); // Bottom
      rect3d(g,0,-1,0,x1,y1,z1,x2,y1,z1,x2,y1,z2,x1,y1,z2);
***/

      return;
    } /* End of only case */

  }

/* Draw rectangle in 3space between the four points */

  void unirect3d(Graphics g,int xoff, int yoff, int zoff,
                         int x1, int y1, int z1,
                         int x2, int y2, int z2,
                         int x3, int y3, int z3,
                         int x4, int y4, int z4, int mask)
  {
    x1+=xoff;y1+=yoff;z1+=zoff;
    x2+=xoff;y2+=yoff;z2+=zoff;
    x3+=xoff;y3+=yoff;z3+=zoff;
    x4+=xoff;y4+=yoff;z4+=zoff;

    if (mask==0){
      graph3d.orect(g,x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4);
    } else {
      graph3d.rect(g,x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4);
    }
  }

/* *off is an offset for each coord */
  void rect3d(Graphics g,int xoff, int yoff, int zoff,
                         int x1, int y1, int z1,
                         int x2, int y2, int z2,
                         int x3, int y3, int z3,
                         int x4, int y4, int z4)
  {
    x1+=xoff;y1+=yoff;z1+=zoff;
    x2+=xoff;y2+=yoff;z2+=zoff;
    x3+=xoff;y3+=yoff;z3+=zoff;
    x4+=xoff;y4+=yoff;z4+=zoff;

    graph3d.rect(g,x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4);
  }

/* Draw outline only of rectangle in 3-space */
  void orect3d(Graphics g,int xoff, int yoff, int zoff,
                         int x1, int y1, int z1,
                         int x2, int y2, int z2,
                         int x3, int y3, int z3,
                         int x4, int y4, int z4)
  {
    x1+=xoff;y1+=yoff;z1+=zoff;
    x2+=xoff;y2+=yoff;z2+=zoff;
    x3+=xoff;y3+=yoff;z3+=zoff;
    x4+=xoff;y4+=yoff;z4+=zoff;

    graph3d.orect(g,x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4);
  }

  void line3d(Graphics g, int x1, int y1, int z1, int x2, int y2, int z2)
  {
    int X1,Y1,X2,Y2; /* 2D Coords */
    int scale=1;

    X1=xv+(xv-x1)/(yv-y1);X1*=scale;
    X2=xv+(xv-x2)/(yv-y2);X2*=scale;
    Y1=zv+(zv-z1)/(yv-y1);Y1*=scale;
    Y2=zv+(zv-z2)/(yv-y2);Y2*=scale;

    g.drawLine(X1,Y1,X2,Y2);
  }

  Color bright_red=new Color(255,0,0);
  Color dark_red=new Color(128,0,0);
  Color bright_green=new Color(0,255,0);
  Color dark_green=new Color(128,128,128);
  Color black=new Color(0,0,0);

  void our_3Dcolor(Graphics g, int c, int d, int eye)
  {
    if ((d==0)&&(eye==0)) g.setColor(left0);
    if ((d==1)&&(eye==0)) g.setColor(left1);
    if ((d==0)&&(eye==1)) g.setColor(right0);
    if ((d==1)&&(eye==1)) g.setColor(right1);
  }

  void our_setcolor(boolean empty,Graphics g,int c, int d)
  {
    if (empty){
      g.setColor(empty_color);return;
    }

    if (c!=0) c=1;
    if (d!=0) d=1;
    switch (2*c+d){
      case 0: g.setColor(dark_green);return;
      case 1: g.setColor(bright_green);return;
      case 2: g.setColor(dark_red);return;
      case 3: g.setColor(bright_red);return;
    }
  }

  public void set_display_mode(int dmode, int fsize, int fsep)
  {
    saveg=getGraphics();
    display_mode=dmode;
    if (fsize != -1) fillsize=fsize;
    if (fsep != -1) fillsep=fsep;
    saveg.setColor(blackcolor);
    saveg.fillRect(0,0,getSize().width,getSize().height);
    unload_all();
  }

/* draw full wiremesh around all non-empty cells */
  void draw_wiremesh_all(Graphics g)
  {
    int ind[]=new int[3];
    for (ind[0]=0;ind[0]<DIMS[0];ind[0]++){
      for (ind[1]=0;ind[1]<DIMS[1];ind[1]++){
        for (ind[2]=0;ind[2]<DIMS[2];ind[2]++){
          draw_wiremesh(g,ind);
        }
      }
    }
  }

/* Draw a wire mesh around cell [i0,i1,i2] if not empty */
  void draw_wiremesh(Graphics g,int ind[])
  {
    int x,y,z,x1,y1,z1,x2,y2,z2;
    int i,xloc,yloc;
    boolean empty;

    if (DIM != 3) return; /* Only for 3D display! */
    if (!setup_3d_done) return;

    x1=ind[1]*fillsize;
/* y should be inverted */
    y1=(DIMS[0]-ind[0])*fillsize;
    y1=(DIMS[2]-ind[2])*fillsize;
    y1=(ind[2])*fillsize;
    z1=ind[0]*fillsize;
    x2=x1+fillsize-fillsep;
    y2=y1+fillsize-fillsep;
    z2=z1+fillsize-fillsep;

    ce=sim.user_read_cell(ind);
/* Is cell's TT all 0s? */
    empty=true;
    for (i=0;i<2*ce.get_ttsize();i++){
      if (ce.tt[i]!=0){
        empty=false;
        break;
      }
    }

    if (empty) return;

/* Now draw outline of cube */
    g.setColor(black);
//###    orect3d(g,1,0,0,x2,y2,z1,x2,y1,z1,x2,y1,z2,x2,y2,z2);
//###    orect3d(g,0,0,1,x1,y2,z2,x2,y2,z2,x2,y1,z2,x1,y1,z2);
//###    orect3d(g,0,1,0,x1,y2,z1,x2,y2,z1,x2,y2,z2,x1,y2,z2);
/***
    orect3d(g,-1,0,0,x1,y2,z1,x1,y1,z1,x1,y1,z2,x1,y2,z2);
    orect3d(g,0,0,-1,x1,y2,z1,x2,y2,z1,x2,y1,z1,x1,y1,z1);
    orect3d(g,0,-1,0,x1,y1,z1,x2,y1,z1,x2,y1,z2,x1,y1,z2);
***/
  }
}
