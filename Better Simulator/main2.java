import java.io.*;
import java.util.*;
import java.awt.event.KeyEvent;

public class main2{
  static int NUM_SIDES=4;
  static int DIM=2;
  static int DIMS[]=new int[32];

  public static void main(String args[])
  {
    window_class mw; 		// Main drawing window
    cell_matrix_sim sim;	// Main simulation engine
    cell_exchange ce;		// Cell Exchange object
				// used for interacting with single cells
    user_callback uc;		// this is how the window class can pass
				// keystroke events back to us

// Setup dimensionality/etc.
    DIMS[0]=50;DIMS[1]=50; // Arbitrary default

    sim=new cell_matrix_sim(NUM_SIDES,DIMS[0],DIMS[1],false);
    ce=new cell_exchange(DIM,NUM_SIDES);
    uc=new user_callback();

// setup some symbols for identifying cell sides
    int N=ce.get_labels().indexOf('N');
    int S=ce.get_labels().indexOf('S');
    int W=ce.get_labels().indexOf('W');
    int E=ce.get_labels().indexOf('E');

/* Set up graphics */
    mw=new window_class(sim,ce,uc);
    sim.save_window_class(mw);
    mw.show();
    mw.repaint();
    mw.unload_changes();
    mw.init_done();

    int coords[]=new int[2],i,key;
    coords[0]=0;coords[1]=0;

    ce.tt_compile("CE=W;DE=N");		// compile a truth table
    sim.user_write_cell(coords,ce);	// write to [0,0]

// now setup some initial inputs
    sim.user_set_input(coords,'D',N,1);

// let's check the cell's outputs now
    ce=sim.user_read_cell(coords);
    System.out.println("Labels:" + ce.get_labels());
    System.out.println("C outputs:" + ce.c_out + "\nD outputs:" + ce.d_out);

// now process the even queue
    sim.process_queue();
    ce=sim.user_read_cell(coords);
    System.out.println("C outputs:" + ce.c_out + "\nD outputs:" + ce.d_out);

// main processing loop...
    while (true){		// could do something more creative here
      sim.tick();		// tick the simulator's clock
      sim.process_queue();	// process *all* pending events
      mw.unload_changes();	// display all changes in the GUI
      sim.do_pending();		// process GUI input events

// pickup any keystrokes
      if (uc.check()){		// there's a keystroke waiting for us
        key=uc.get_key();
        switch(key){
          case KeyEvent.VK_Q: mw.dispose();System.exit(0); // quit
          case KeyEvent.VK_A:sim.user_set_input(coords,'D',W,0);break;
          case KeyEvent.VK_B:sim.user_set_input(coords,'D',W,1);break;
        }
      }
    }
  }
}
