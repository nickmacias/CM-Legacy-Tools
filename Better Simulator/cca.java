import java.io.*;
import java.util.*;
import java.net.*;


public class cca{
  static boolean debug=false;
  static boolean fastcca=false; // true for faster clocking!

  public static void main(String args[])
  {
    Node nodes[]=new Node[2048]; // modest-sized :)
    int maxid; // # of nodes
    int i,side,id,nbr,j,k,l;
    boolean ready;
    byte b[]=new byte[4];

// the flip[] array tells us which sides are connected to which...
    cell_exchange ce=new cell_exchange(2,4);
    int N=ce.get_labels().indexOf('N');
    int S=ce.get_labels().indexOf('S');
    int W=ce.get_labels().indexOf('W');
    int E=ce.get_labels().indexOf('E');
    int flip[]=new int[6];
    flip[N]=S;
    flip[S]=N;
    flip[E]=W;
    flip[W]=E;
    flip[4]=flip[5]=(-1); // will gen an error if we try to use these

    for (i=0;i<2048;i++) nodes[i]=new Node();

// loop and accept connections
    GetCons gc=new GetCons(nodes); // accept connections
    gc.start();

// wait for user input to start clock processing
    System.out.println("To initiate clock processing, hit ENTER after all nodes are inter-connected...");
    try{
      InputStreamReader isr=new InputStreamReader(System.in);
      BufferedReader br=new BufferedReader(isr);
      br.readLine();
// Now we're done accepting new nodes...
// meanwhile, nodes have been sending us their ID/Neighbor ID info...so let's read those now
      maxid=1;
// note that nodes[0] is RESERVED
      while (nodes[maxid].id != 0){ // this node exists 
// read neighbor info
        while (nodes[maxid].in.available() >= 4){ // data available
          nodes[maxid].in.read(b,0,4);
          i=ByteIO.from_byte(b);
          id=i&0x3fff;
          side=(i>>14)&7;
          nbr=(i>>17)&0x3fff;
System.out.println("ID " + id + ":Side=" + side + " nbr=" + nbr);
          nodes[maxid].nbr[side]=nbr; // now we know ID of node's neighbors
          nodes[maxid].active[side]=true; // Remember this neighbor exists!
        } // end of that node's data
        ++maxid;
      } // all nodes processed

// we should be all set now...send GO signal to each node
      b[0]=2;
      for (i=1;i<maxid;i++){
        nodes[i].out.write(b,0,1); // semi-random code
      }
System.out.println("-----------");

/*
 * The "2" to each node means the main clocking loop is beginning.
 * During this loop, each node will process its queue, and, when the queue is empty,
 * will report event counts to us.
 *
 * Meanwhile, we will:
 * Scan each node for input, which will be either:
 *  <num-msb ... num-lsb> 0 <s2 s1 s0> for the # of inputs received from a side; or
 *  <num-msb ... num-lsb> 1 <s2 s1 s0> for the # of outputs sent to a side
 * and finally, the value (-1), indicating completion of the report;
 * compare the # of inputs to each side's corresponding output; and
 * if everything matches, mark the node as READY.
 *
 * Once all nodes are READY, each node is sent a byte "0" (clear counters) followed by
 * a byte "1" (Tick your clocks), and the loop repeats.
 *
 */

      System.out.print("Hit Enter to begin clocking");
      br.readLine();

      while (true){ // MAIN LOOP!
        ready=false;
        for (i=1;i<maxid;i++){
          nodes[i].ready=false;
        } // init those!

        while (!ready){
          for (i=1;i<maxid;i++){
            if (nodes[i].in.available() >= 4){ // read info from node
              nodes[i].in.read(b,0,4);
              l=ByteIO.from_byte(b);
              if (l==(-1)){ // all data received - see if node is ready!
                nodes[i].ready=true; // for now...
                for (j=0;j<6;j++){ // working on Side j
                  if (nodes[i].active[j]){ // check counts
                    id=nodes[i].nbr[j]; // neighbor's ID
                    if (debug)System.out.println("Checking node " +
                       i + ", side " + j + ": num_in=" + nodes[i].num_in[j]+
                       " vs. node " + id + " side " + flip[j] + " num_out=" +
                       nodes[id].num_out[flip[j]]);
                    if (nodes[i].num_in[j] < nodes[id].num_out[flip[j]]){
                      nodes[i].ready=false; // we're not ready - more events coming!
                    }
                    if (debug)System.out.println("and node " + id +
                       ", side " + flip[j] + ": num_in=" +
                       nodes[id].num_in[flip[j]]+ " vs. node " + i +
                       " side " + j + " num_out=" + nodes[i].num_out[j] + "\n");
                    if (nodes[i].num_out[j] > nodes[id].num_in[flip[j]]){
                      nodes[id].ready=false; // neighbor isn't ready-will be getting more events
                    }
                  }
                } // all neighbors processed
                if (debug) System.out.println("Node " + i + " reported all sides: final ready=" + nodes[i].ready);
              } else { // store event info
//                nodes[i].ready=false; // ** Not necessary???
                side=l&7; // side
                j=l>>4; // # of events received or sent
                if ((l&8) == 0){ // input count
                  nodes[i].num_in[side]=j;
                  if (debug) System.out.println("Node " + i +
                     " reporting side " + side + " num IN=" + j);
                } else { // output count
                  nodes[i].num_out[side]=j;
                  if (debug) System.out.println("Node " + i +
                     " reporting side " + side + " num OUT=" + j);
                }
                if (nodes[i].ready){ // this is a weird case...
int k1,k2;
                  if (debug){
                    System.out.println("Odd case at node " + i +
                      " side=" + side + " was already ready!");
                    for (k1=1;k1<maxid;k1++){
                      System.out.println(k1 + " ready=" + nodes[k1].ready);
                      for (k2=0;k2<6;k2++){
                        if (nodes[k1].active[k2]){
                          System.out.println(k1 + ":" + k2 + " in=" + nodes[k1].num_in[k2]);
                          System.out.println(k1 + ":" + k2 + " out=" + nodes[k1].num_out[k2]);
                          System.out.println(k1 + " nbr" + k2 + "=" + nodes[k1].nbr[k2]);
                        }
                      }
                    }
                    System.out.println("Hit Enter to continue...");
                    br.readLine();
                  } // end of debug
                }
                nodes[i].ready=false;
//
// This is necessary, for the following case:
// Node 1->3->5
// Nodes 3 and 5 report READY
// Then node 1 sends an event to node 3...3 processes it,
// sends an event to node 5, and 3's Q is then empty
// 3 then begins to report data to CCA.
// IF 1 reported first, CCA would see an unreported IN to 3,
// and set 3 as NOT READY. But if 3 reports first, the count is OK,
// and 3 remains ready. 1 can then report, be marked ready, and
// 3 remains ready.
//
// IF we flush 3's messages, we would miss the output to 5!
// so we need to process all of 3's I/O counts as usual,
// which means we'll clear 5's READY flag.
//
// marking 3 as NOT READY once it begins reporting ensures
// that we process all of it's I/O counts before exiting the
// while (!ready)... loop
//
              }
            } // end of Info Available
          } // all nodes swept
// are all nodes ready?
// If we count carefully, we can tell by the # that change each pass
// for now, just sweep all flags
          ready=true;
          for (i=1;i<maxid;i++){
            if (!nodes[i].ready) ready=false;
          }
          if (debug && !ready) System.out.println("NOT READY");
        } // repeat till all nodes ready
        if (debug) System.out.println("Ready to tick!\n");

// tick each node
// tell nodes to prepare to tick
        b[0]='0';
// could try multicast on a (unreliable) datagram socket...
        for (i=1;i<maxid;i++){
          nodes[i].out.write(b,0,1); // clear your event counts
        }
// nodes may have more status info queued for us, even though we've detected that all nodes are ready...
if (!fastcca){
        for (i=1;i<maxid;i++){
          l=0;
          while (l != (-2)){
            nodes[i].in.read(b,0,4);
            l=ByteIO.from_byte(b);
if (l!=(-2)) System.out.println("Flushing " + l + " from node " + i);
          }
        }

// safe now to zero-out our event counts
        for (i=1;i<maxid;i++){
          for (side=0;side<6;side++){
            nodes[i].num_in[side]=nodes[i].num_out[side]=0;
          }
        }

// tell nodes to TICK
        b[0]='1';
        for (i=1;i<maxid;i++){
          nodes[i].out.write(b,0,1); // and tick your clock
        }
} // end of !fastcca block
// and mark each node as NOT ready!
        for (i=1;i<maxid;i++){
          nodes[i].ready=false;
        }
        if (debug) System.out.println("Ticked");
// wait here
        //System.out.print("Hit enter> ");
        //br.readLine();

      } // loop here forever!
    } catch (Exception e) {
      System.out.println("Unexpected input error: " + e);
    }
  }
}

class GetCons extends Thread
{
  int nextid=1;
  int port=1203;
  Node nodes[];  

  GetCons(Node[] n)
  {
    nodes=n;
  }

  public void run()
  {
    while (true){
// look for another connection
      try{
        ServerSocket ss=new ServerSocket(port);
        Socket sock=ss.accept();
        System.out.println("CCA received another connection");
        ss.close();

        sock.setTcpNoDelay(true);
        InputStream is=sock.getInputStream();
        OutputStream os=sock.getOutputStream();

        nodes[nextid].id=nextid;
        nodes[nextid].in=is;
        nodes[nextid].out=os;
        nodes[nextid].ready=false;

        os.write(ByteIO.to_byte(nextid++),0,4); // tell node it's ID
      } catch(Exception e){
        System.out.println("CCA Init error:" + e);
      }
    }
//System.out.println("CCA Exiting");
//sock.close();
  } // end of run()
} // end of thread

// class for holding info about each node
class Node{
  int id; // redundant, but may be useful;
  InputStream in; // incoming FROM the node
  OutputStream out; // TO the node
  boolean ready; // set when node's Q is empty and all inbound events have been received
  boolean active[]; // is a neighbor present on that side?
  int nbr[]; // Id of each neighbor
  int num_in[]; // # of events received from each neighbor
  int num_out[]; // # of events sent to each neighbor

  Node()
  {
    int i;

    ready=false;
    active=new boolean[6];
    nbr=new int[6];
    num_in=new int[6];
    num_out=new int[6];
    for (i=0;i<6;i++) active[i]=false;
    id=0; // special hook - indicates this node does not exist!
  }
} // end of Node class
