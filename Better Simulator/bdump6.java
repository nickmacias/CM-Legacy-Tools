/*
 * bdump class is used to convert a 6-var truth table into a set of Boolean
 * equations.
 */

class bdump6
{
  static StringBuffer retbuf; /* Loaded by cover() method */
  static String labels; /* char(0)=bit0=LSB */
  static String buffer; /* This stores final equation */

/* decomp decompiles a cell_exchange structure into a String[] array */
  public static String[] decomp(cell_exchange ce,term oldterms[],
                                term copyterms[])
  {
    String s[]=new String[12];
    String sout[]=new String[12];
    int temp[]=new int[64];
    int numStr; /* # of strings in output */
    int i,j;

    labels=ce.get_labels();
    numStr=0;
    for (i=0;i<12;i++){
      s[i]="";
/* Load up temp[] array */
      for (j=0;j<64;j++){
        temp[j]=((ce.tt[j+((i>5)?64:0)]&(1<<(i%6)))==0)?0:1;
/* That just picks tt[] rows based on i and j and checks bit based on i */
      }
      s[numStr]=((i<6)?"C":"D") + (labels.charAt(i%6)) + "=" + 
                one_decomp(temp,oldterms,copyterms);
      if (s[numStr].length() > 3){
        ++numStr;
      }
    } /* All columns finished */
    if (numStr < 12) s[numStr]="";
    return(s);
  }

/* one_decomp reads an int[] of 1s and 0s and returns RHS */
  public static String one_decomp(int program[],term old[],term copy[])
  {
    int i,j;
    int numold;
    int numcopy;

// populate old[] array;
    numold=0;
    for (i=0;i<64;i++){
      if (program[i]==1){
        populate(old[numold],i);
        old[numold].covered=false;
        ++numold;
      }
    }

// now begin covering terms
    boolean more=true;

    while (more){
      more=false;
      numcopy=0; // copy outputs to here

// compare each old[] term to all other old[] terms, see if you can cover pair
      for (i=0;i<numold-1;i++){
        for (j=i+1;j<numold;j++){
          if (covers(old[i],old[j],copy[numcopy])){
            more=true; // since we reduced things further!
            ++numcopy; // save this term
            old[i].covered=true;
            old[j].covered=true;
          }
        }
      } // done trying to cover

// now sweep old [] and copy any uncovered terms to copy[]
      for (i=0;i<numold;i++){
        if (!old[i].covered) clone(old[i],copy[numcopy++]);
      }

// Now eliminate duplicates in copy[], and copy back to old[]
      numold=0;
      for (i=0;i<numcopy;i++){
        boolean dup=false;
        for (j=0;j<i;j++){ // does [i] match an earlier entry?
          if (terms_equal(copy[i],copy[j])) dup=true;
        }
        if (!dup){
          clone(copy[i],old[numold]);
          old[numold++].covered=false;
        }
      }
// old[] is setup for new sweep
    }

// done reducing
    buffer=""; /* Final output buffer (RHS) */

    for (i=0;i<numold;i++){
// Check case of all don't-cares
      if ((old[i].vars[0]==2)&& (old[i].vars[1]==2)&&
          (old[i].vars[2]==2)&& (old[i].vars[3]==2)&&
          (old[i].vars[4]==2)&& (old[i].vars[5]==2)) return("1");

      if (i > 0) buffer=buffer+"+"; // OR

      for (j=0;j<6;j++){
        if (old[i].vars[j]==0) buffer=buffer+"~"+labels.charAt(j);
        if (old[i].vars[j]==1) buffer=buffer+labels.charAt(j);
      }
    }

    //System.out.println(buffer);
    return(buffer);
  }

  static public void clone(term in,term out)
  {
    int i;
    for (i=0;i<6;i++) out.vars[i]=in.vars[i];
    out.covered=in.covered;
  }

  static public void populate(term in,int num)
  {
    int i;
    for (i=0;i<6;i++){
      in.vars[i]=((0==(num & (1<<i)))?0:1);
    }
    in.covered=false;
  }

// compare two terms, see if they cover: if so, copy to output
  static public boolean covers(term a, term b, term out)
  {
    int i;
    boolean mismatch;

    mismatch=false; // set when we miss one var
    for (i=0;i<6;i++){
      if (a.vars[i]!=b.vars[i]){ // mismatch
        if (mismatch) return(false); // missed two terms
        if ((a.vars[i]==2) || (b.vars[i]==2)) return(false); // missed X
        mismatch=true; // record this one difference
        out.vars[i]=2; // Don't care (X)
      } else { // these terms agree in their i-th bit
        out.vars[i]=a.vars[i];
      }
    } // matched in all but possibly one term
    return(true);
  }

// Do we match another term?
  static public boolean terms_equal(term a,term b)
  {
    int i;
    for (i=0;i<6;i++){
      if (a.vars[i] != b.vars[i]) return(false);
    }
    return(true);
  }
}
