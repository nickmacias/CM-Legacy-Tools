import java.io.*;
import java.net.*;
import java.lang.*;
import java.util.*;
import java.awt.event.*;

/* Class for monitoring output changes */
class watch_class{
  static boolean firstreg=true; /* So we can initialize */

  static int saveind[]=new int[32]; // Temporary for holding indicies
  static int saveside,savevalue;
  static char savetype;
  static char savesidename;
  static boolean changed=false; // set after change
  static cell_exchange ce;

  public watch_class(cell_exchange cein)
  {
    ce=cein;
  }

// old_changed is the older code, where we simply record that a change has
// occurred, and remember the details for later recall
  public static void old_changed(int ind[], char type, int side, int value)
  {
    saveind=ind;savetype=type;saveside=side;savevalue=value;
    savesidename=ce.labels.charAt(saveside);
    changed=true;
  }
  public boolean status(){boolean back=changed;changed=false;return(back);}

// "real" changed() routine - send change to neighbor
  public static void changed(cell_matrix_sim sim,int ind[], char type, int side, int value)
  {
    int i; /* Outgoing message */
    int cnum; // Cell number
    int cnum2; // for 3-D case
// ordering is NS, WE, BT  corresponding to [row, col, level]
    boolean edge;
    char c;
    byte b[]=new byte[4];

//
// See if this is an (active) edge change - and if not, use the OLD code
//
//System.out.println("CHANGED!");

    edge=false;
    c=ce.get_labels().charAt(side);
    if (((c=='N') && (ind[0]==0)) ||
        ((c=='S') && (ind[0]==sim.dims[0]-1)) ||
        ((c=='W') && (ind[1]==0)) ||
        ((c=='E') && (ind[1]==sim.dims[1]-1))) edge=true;
    if (sim.get_dim() == 3){
      if (((c=='B') && (ind[2]==0)) ||
          ((c=='T') && (ind[2]==sim.dims[2]-1))) edge=true;
    }
    if (!edge){ // non-edge change - just record the event for later pickup
      saveind=ind;savetype=type;saveside=side;savevalue=value;
      savesidename=ce.labels.charAt(saveside);
      changed=true;
      return;
    }

    value=(value==0)?0:1; // Normalize
//System.out.println("Prepping to send...");
    if (sim.get_cons().active[side]){ // will only happen after con is active!
//System.out.println("Good!");
      if (sim.get_dim()==2){
// Find cell number
        switch(ce.get_labels().charAt(side)){
          case 'N':
          case 'S':cnum=ind[1];break;
          case 'W':
          case 'E':cnum=ind[0];break;
          default:System.out.println("Unknown side changed:" + side);return;
        }

        i=1 | (value<<1) | ((type=='C')?0:4) | (cnum<<4);
      } else { // 3D
// Find cell numbers
        switch(ce.get_labels().charAt(side)){
          case 'N':
          case 'S':cnum=ind[1];cnum2=ind[2];break;
          case 'W':
          case 'E':cnum=ind[0];cnum2=ind[2];break;
          case 'T':
          case 'B':cnum=ind[0];cnum2=ind[1];break;
          default:System.out.println("Unknown side changed:" + side);return;
        }

        i=1 | (value<<1) | ((type=='C')?0:4) |
              ((cnum&0x3fff)<<4) |
              ((cnum2&0x3fff)<<18);
      }
/*
 * while the sync with the CCA works perfectly up to the next clocktick,
 * the following condition must be considered:
 *  1. all nodes are ready;
 *  2. CCA sends TICK to each node;
 *  3. node 1 processes the tick, and sends an event to node 2;
 *  4. node 2 processes that event, BEFORE it sees the tick from the CCA.
 * In this case, node 2 has now processed events out of order relative to
 * the clock.
 *
 * To prevent this, the CCA could wait for each node to acknowledge the tick,
 * then send a GO signal to each node. But this would require interacting
 * with each node between event-completion and the next tick...
 *
 * Instead, we'll let each node begin to work as soon as it receives the tick.
 * The only danger here is the situation described above, where such a node
 * interacts with a node which has not yet seen the tick. To handle this case,
 * inter-node events will be sent with a clock count. If we receive an event
 * from a neighboring node, and the neighbor's clock count differs from ours,
 * then we know we have an unnoticed clock tick in the pipe. At that point,
 * we simply read and process the clock tick, and then continue normally.
 *
 * We can't get more than 1-cycle behind, so a 1-bit counter will suffice.
 * This is sent in Bit 3 of the inter-node event message.
 *
 */

// Set bit 3 :)
      if (sim.tick1()) i=i|8;
//System.out.println("Writing...");

      try{
        sim.get_cons().out[side].write(ByteIO.to_byte(i),0,4);
        ++(sim.get_cons().num_out[side]);
      } catch (Exception e){
       System.out.println("Watch Output Error:" + e);
       return;
      }
      //System.out.println("Sending edge change: " + cnum + ":" + type +
                         //":" + value);
    }
  }
}
