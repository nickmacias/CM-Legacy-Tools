import java.net.*;
import java.util.*;

public class main{
  static int NUM_SIDES;
  static int DIM;
  static int DIMS[]=new int[32];
  static int update_rate=1; /* Unload every n ticks/cycles */
                       /* < 1 means no update until finished */

  static Hashtable vectorhash; /* Store vector definitions */
  static BufferedWriter bw=null;
  static BufferedReader br;
  static InputStreamReader isr=null;

  public static void main(String args[])
  {
    window_class mw; /* Main drawing window */
    int i,ind[]=new int[16],s;
    int i1,i2;
    StringBuffer tt[],labels;
    String buffer,lastbuffer="";
    //String server,myid;int server_port; // for connection to server
    int status, freq, value, j, count;
    String command;
    StringBuffer text[];
    cell_matrix_sim sim; /* Main simulation engine */
    user_callback uc;		// this is how the window class can pass
				// keystroke events back to us
    //gas_pedal gp;
    Date mydate;
    msgwin msg=null; // for misc display
    char type,side;
    boolean secondtry=false; // set after unstable grid detected
                             // this shows we're r-trying with
                             // queue zeroing enabled
    int saved_zero_queue=0;

    boolean threaded=false; // set once we've started the event-handling thread
    vectorhash=new Hashtable();
    vstruct v_s=new vstruct();
    boolean eventmode=false; // set when we're running event-by-event
    boolean dark=false; // set with -dark to prevent all GUI output
    //edge_monitor ems[]=new edge_monitor[6];

    parse_buffer pb;
    boolean CLIENT=false; // set to TRUE if you only want to drive via socket
    boolean THREED=false; // set to TRUE for red/blue 3-D display

    SetupConThread sct[]=new SetupConThread[6];
    int num_sct=0;

    term oldterms[]=new term[1024];
    term copyterms[]=new term[1024];

    for (i=0;i<1024;i++){
      oldterms[i]=new term();
      copyterms[i]=new term();
    }

    DIM=2;DIMS[0]=50;DIMS[1]=50; /* Arbitrary default */

// parse incoming arguments
    i=0;
    while (i < args.length){
      if (args[i].equals("-help")){ // show help
        my_outln(bw,"Legal switches include:\n -help\n -dark\n -size rxc[xh]\n -client\n @initfile\n");
      }
      if (args[i].equals("-dark")){dark=true;}
      if (args[i].equals("-size")){ // rxc or rxcxh
        ++i;
        i1=args[i].indexOf('x');
        i2=args[i].indexOf('x',i1+1);
        if (i2==-1){ // 2-D
          DIM=2;
          DIMS[0]=Integer.parseInt(args[i].substring(0,i1));
          DIMS[1]=Integer.parseInt(args[i].substring(i1+1));
        } else { // 3-D
          DIM=3;
          DIMS[0]=Integer.parseInt(args[i].substring(0,i1));
          DIMS[1]=Integer.parseInt(args[i].substring(i1+1,i2));
          DIMS[2]=Integer.parseInt(args[i].substring(i2+1));
        }
      }
      if (args[i].charAt(0)=='@'){ // @initfile
        buffer=args[i];
      }
      if (args[i].equals("-client")){ // only talk to us via sockets
        CLIENT=true;
      }
      if (args[i].equals("-3D")){ // red/blue :)
        THREED=true;
      }
      ++i;
    }

// --- end of argument parsing

    if (DIM==2){
      NUM_SIDES=4;
      sim=new cell_matrix_sim(NUM_SIDES,DIMS[0],DIMS[1],false);
    } else if (DIM==3){
      NUM_SIDES=6;
      sim=new cell_matrix_sim(NUM_SIDES,DIMS[0],DIMS[1],DIMS[2],false);
    } else {
      my_outln(bw,"Only 2D/4 Sided and 3D/6 Sided topologies currently supported");
      System.exit(1);
      return;
    }

    if (!dark) msg=new msgwin();

    cell_exchange ce=new cell_exchange(DIM,NUM_SIDES);

// when we compile a TT into memory, we'll save it in cemem...
    cell_exchange cemem=new cell_exchange(DIM,NUM_SIDES);
    uc=new user_callback();
    //gp=new gas_pedal(sim);
    watch_class wc=new watch_class(ce);
    //gp.setVisible(true);

// useful labels for sides
// setup some symbols for identifying cell sides
    int N=ce.get_labels().indexOf('N');
    int S=ce.get_labels().indexOf('S');
    int W=ce.get_labels().indexOf('W');
    int E=ce.get_labels().indexOf('E');

    sim.set_unload_rate(update_rate);
    sim.peek_set(1); // this just seems to work better lol

/* Set up graphics */
    if (!dark){
      mw=new window_class(sim,ce,uc);
      mw.setVisible(true);
      mw.set3d(THREED);
      mw.repaint();
      mw.unload_changes();
      mw.init_done();
    } else mw=null;
    sim.save_window_class(mw);

// Input file stack
    Stack fpstack=new Stack();
    Stack fnamestack=new Stack();

    Socket sock=null;
    ServerSocket ss=null;
    boolean got_error=false;

    String fnprompt=""; // Current filename for prompt

// setup input stream
    try{
      if (!CLIENT){
/*** stdin ***/
        isr=new InputStreamReader(System.in);
        br=new BufferedReader(isr);
      } else {
/*** socket mode ***/
        ss=new ServerSocket(1204);
      }
    } catch (Exception e){
      System.out.println("Low-level error establishing input stream - giving up");
      System.exit(0);
    }
   while (true && (got_error==false)){
    buffer=""; // flag indicating we should read first command from stdin
    if (CLIENT){
      try{
        sock=ss.accept();

        InputStream is=sock.getInputStream();
        isr=new InputStreamReader(is);
        br=new BufferedReader(isr);

        OutputStream os=sock.getOutputStream();
        OutputStreamWriter osw=new OutputStreamWriter(os);
        bw=new BufferedWriter(osw);
        my_outln(bw,"Received a connection...");
      } catch(Exception e){
        System.out.println("Failed to accept connection - giving up");
        System.exit(0);
      }
    }

// enter main loop - go back to top on exception!
    try{
      my_out(bw,sim.get_clockcount()+":" + sim.get_timestamp()+":"+
                       sim.get_eventcount()+"> ");
      if (buffer.length()==0){
        buffer=br.readLine(); // read from stdin unless initialized
      }
      boolean more=(buffer != null);

      boolean repeat=false; // set when <CR> should repeat last non-empty cmd
      while (more){ /* Parse and process input */
        if (fpstack.empty()){ // input is coming from keyboard
          if (repeat && buffer.equals("")){
            buffer=lastbuffer;
            my_outln(bw,buffer);
          } else if (buffer.equals(lastbuffer)) repeat=true;else repeat=false;
          lastbuffer=buffer; // repeat last cmd if user hits Enter
        }
        if (!fpstack.empty()) my_outln(bw,"<"+buffer+">");

        pb=parse(buffer);  /* Break into words and store in array */
        command=pb.words[0];
/* Shift arguments, so we can feed coords to simulator methods */
        for (i=0;i<14;i++){
          pb.nums[i]=pb.nums[i+1];
          pb.words[i]=pb.words[i+1];
          pb.fnums[i]=pb.fnums[i+1];
        }

        if (pb.number==0) command="#";

        switch (command.charAt(0)){

// edge commands
          case 'E': // E side host - establish 2-way communication
            if (command.concat(" ").charAt(1)=='S'){ // connect to server
              if (pb.number==1){
                cca("localhost",sim);
              } else if (pb.number != 2){
                my_outln(bw,"ES CCAhostname");
                break;
              } else {
                cca(pb.words[0],sim); // setup all cca communication
              }
              break;
            } // end of ES command

            if (command.concat(" ").charAt(1)=='W'){// EW - Wait
              my_out(bw,"# of requested connections: " + num_sct);
              boolean sctdone=false;
              while (!sctdone){
                sctdone=true;
                for (i=0;i<num_sct;i++){
                  if (!sct[i].done) sctdone=false;
                }
                if (!sctdone) my_out(bw,".");
                Thread.sleep(1000);
              }
              my_outln(bw,"\nAll connections completed\n");
              break;
            }

// assuming normal E command
            if ((pb.number != 3) && (pb.number != 2)){
              my_outln(bw,"E side [host]");
              break;
            }
            side=pb.words[0].toUpperCase().charAt(0); /* eg. N, S, W or E */
            s=ce.get_labels().indexOf(side);
            if (s<0){
              my_outln(bw,"Expecting a side, but got <"+
                                  pb.words[0]+">");
              break;
            }

            if (pb.number==2) pb.words[1]="localhost";
// we'll start a subprocess to setup the connection, so we don't have to wait
            sct[num_sct]=new SetupConThread(sim,side,s,pb.words[1],bw);
// the cons structure is used to record connection info
            sct[num_sct++].start();
            break;

          case '@': // New input file
            fpstack.push(br); /* Save current buffered reader */
            buffer=buffer.substring(1); // Filename
            try{
              FileReader fire=new FileReader(buffer);
              br=new BufferedReader(fire);
              fnprompt=":" + buffer;
              fnamestack.push(fnprompt);
            } catch (Exception e){
              my_outln(bw,"Cannot open <" + buffer + ">:" + e);
              br=(BufferedReader)fpstack.pop();
            }
            break;
          case ';':break;
          case '#':break;
          case 'Q':if (!dark){
            mw.capture_end();
            mw.dispose();System.exit(0);
            break;
          }

          case 'X': /* Compile a hex string */
            if (check(pb,2)) break;
            if (0 != ce.hex_comp(pb.words[DIM])){
              my_outln(bw,"Error parsing " + pb.words[DIM] + " as hex");
              break;
            }
            if (0 != sim.user_write_cell(pb.nums,ce)){
              my_outln(bw,"Failed");
            }
            break;

          case 'C': // capture. Filename to start, no filename to end
            if (pb.number==1){ // close
              mw.capture_end();
            } else if (pb.number==2){
              mw.capture_begin(pb.words[0]);
            } else {
              System.out.println("Usage: C fname (start capture) or C (end)");
            }
            break;

          case 'c': /* Compile equations into a cell */
            if (pb.number==1){
              System.out.println("Compiling into Memory (m)");
            } else if (check(pb,1)) break;
            my_outln(bw,"Enter equations, end with \".\":");
            buffer=br.readLine();
            StringBuffer full=new StringBuffer("");
            while (!buffer.equals(".")){
              full.append(buffer+";");
              buffer=br.readLine();
            }
            if (pb.number==1){
              cemem.tt_compile(full.toString(),bw);
              break;
            }
            ce.tt_compile(full.toString(),bw);
            if (0 != sim.user_write_cell(pb.nums,ce)){
              my_outln(bw,"Failed");
            }
            break;

          case 'M': /* Set display mode */
            if (!dark) mw.set_display_mode(pb.nums[0],pb.nums[1],pb.nums[2]);
            break;

          case 'W': // Set window size
            mw.set_size(pb.nums[1],pb.nums[0]); // rows, cols
            break;

          case 'd': /* Dump TT from a cell */
            if (pb.number==1){ // dump from memory
              for (i=0;i<2*ce.ttsize;i++) ce.tt[i]=cemem.tt[i];
            } else {
              if (check(pb,1)) break;
              ce=sim.user_read_cell(pb.nums);
            }
            if (ce.error){my_outln(bw,"Failed");break;}
            text=ce.tt_dump();
            for (i=0;i<=ce.get_ttsize();i++){
              my_outln(bw,text[i].toString());
              if (!dark) msg.addtext(text[i].toString());
              //msg.addtext(" " + ce.tt[i] + " : " + ce.tt[i+ce.get_ttsize()]);
            }
            break;

          case 'b': /* Boolean equation dump */
            if (pb.number==1){ // dump from memory
              for (i=0;i<2*ce.ttsize;i++) ce.tt[i]=cemem.tt[i];
            } else {
              if (check(pb,1)) break;
              ce=sim.user_read_cell(pb.nums);
            }
            if (ce.error){my_outln(bw,"Failed");break;}
            if (DIM==2){
              String t[]=bdump.decomp(ce);
              for (i=0;i<8;i++){
                if (t[i].length()>0) my_outln(bw,t[i]);
              }
            } else if (DIM==3){
              String t[]=bdump6.decomp(ce,oldterms,copyterms);
              for (i=0;i<12;i++){
                if (t[i].length()>0) my_outln(bw,t[i]);
              }
            } else System.out.println("What dimensionality are you working in anyway?");
            break;

          case 'l': /* Load a binary grid file */
            if (pb.number==6){ // extended load command
              load_bin_grid.load(sim,ce,pb.nums,pb.words[DIM],
                                 pb.nums[3],pb.nums[4],threaded,bw);
              break;
            }
            if (check(pb,2)) break;
            load_bin_grid.load(sim,ce,pb.nums,pb.words[DIM],0,0,threaded,bw);
            break;

          case 'S': /* Sequence file */
            seq_load_class.seq_load(mw,sim,pb.words[0],update_rate);
            break;
 
          case 'g': /* GO with freerun */
            if (threaded){
              my_outln(bw,"'g' and 'T' are incompatible\nUse 'g' in non-threaded mode, to freerun from a single thread.\nUse 'T' to start a separate freeruning thread.\n'T' is also appropriate for multi-node simulations.");
              sim.halted_state(!sim.halted_state());
              mydate=new Date();
              my_outln(bw,mydate.toString() + " " +
                   (sim.halted_state()?"Halting":"Starting") +
                   "system clock.");
              break;
            }
            mydate=new Date();
            my_outln(bw,mydate.toString());
            eventmode=false;
            sim.halted_state(false); // doesn't actually run anything
            secondtry=false;
            count=0; // # of events since Q was last empty
gloop:
            while (!sim.halted_state()){
              s=sim.process_queue_head();
              if (wc.status()){ // watched-event triggered
                eventmode=true;
                report_change(wc);
                sim.halted_state(true); // freeze here
                if (secondtry){
                  sim.zero_queue=saved_zero_queue;
                  secondtry=false;
                }
                break gloop;
              }
              if (s < 0){  // Q overflow maybe?
                my_outln(bw,"process_queue_head returned " + s);
                if (!dark) mw.unload_changes();
                sim.halted_state(true);
                if (secondtry){
                  sim.zero_queue=saved_zero_queue;
                  secondtry=false;
                }
                break gloop;
              }
              if (s==99){ // queue empty
                count=0; // reset
                if (secondtry){
                  my_outln(bw,"Zero-checking cleared the problem! Returning to previous look-ahead state.");
                  sim.zero_queue=saved_zero_queue;
                  secondtry=false;
                }
                sim.tick(); // this also toggles the sim's bin clock counter
              }
// queue overflow?
              if (++count > sim.event_limit){
                if (secondtry){ // no hope
                  my_outln(bw,"Zero-checking did not solve the problem. Dumping queue to .QDump and halting");
                  sim.display_queue(".QDump");
                  sim.zero_queue=saved_zero_queue;
                  sim.halted_state(true);
                  eventmode=true;
                  break gloop;
                } else { // try again with queue zeroing enabled
                  my_outln(bw,count + "events w/out emptying queue. Possible unstable grid?");
                  my_outln(bw,"Enabling queue look-ahead zeroing and re-trying...");
                  saved_zero_queue=sim.zero_queue; // need to restore later
                  count=0;
                  sim.zero_queue=1;
                  secondtry=true;
                }
              }
              if (sim.input_pending()){
                sim.do_pending_inonly(); // process GUI input changes
                if (!dark) mw.unload_changes();
              }
            }
            break;

/* VECTOR commands! */

          case 'V': /* V (show) - compile cells to set vector */
            if ((command.length() == 1) && (pb.number == 3)){ /* V x 12 */
              v_set(sim,ce,pb.words[0],1,pb.nums[1],(float)pb.fnums[1]);
              break;
            }
            my_outln(bw,"Usage: V name value");
            break;

          case 'v': /* v (show), vs (set) or dv (define) */
            if ((command.length() == 1) && (pb.number == 2)){ /* v x */
              v_s=v_get(sim,ce,pb.words[0]); // v structure
              if (v_s.type==v_s.FP_TYPE){
                my_out(bw,pb.words[0] + " = " + v_s.fp);
              } else if (v_s.type == v_s.INT_TYPE){
                my_out(bw,pb.words[0] + " = " + v_s.i);
// Negative?
                if (v_s.i != v_s.int2){
                  my_out(bw," (" + v_s.int2 + ")");
                }
              } else {
                my_outln(bw,"Can't decode " + pb.words[0]);
              }
              my_outln(bw,"");
              break;
            }

            if ((command.length() == 1) && (pb.number == 3)){ /* v x 12 */
              v_set(sim,ce,pb.words[0],0,pb.nums[1],(float)pb.fnums[1]);
              break;
            }

            if ((command.length() == 2) && (pb.number == 7)){ /* vd x r c dw -1 8 */
              v_def(sim,ce,pb.words[0], // sim, ce + NAME
                    pb.nums[1],pb.nums[2], // row col
                    pb.words[3], // side
                    pb.nums[4], // increment
                    pb.nums[5],pb.words[5]); // length, as numnber and as string
              break;
            }

// 3-D???
            if ((command.length() == 2) && (pb.number == 9)){ /* vd x r c h side inc1 inc2 8 */
              v_def(sim,ce,pb.words[0], // sim, ce + NAME
                    pb.nums[1],pb.nums[2],pb.nums[3], // row col height
                    pb.words[4], // side
                    pb.nums[5],pb.nums[6], // inc1, inc2
                    pb.nums[7],pb.words[7]); // length, as numnber and as string
              break;
            }

            my_outln(bw,"???");
            break;

          case 'q': // display queue's contents
            if (pb.number == 1) sim.display_queue(".QDump");
            else if (pb.number == 2) sim.display_queue(pb.words[0]);
            else my_outln(bw,"Expecting \"q\" or \"q filename\"");
            break;
           
          case 'w': // set/clear a watch point
            if (check(pb,3)) break;
            type=pb.words[DIM].toUpperCase().charAt(0); /* D or C */
            side=pb.words[DIM].toUpperCase().charAt(1); /* eg. N, S, W or E */
            s=ce.get_labels().indexOf(side);
            if (s == -1){
              my_outln(bw,"Side should be one of " + ce.get_labels());
              break;
            }
            if ((pb.nums[DIM+1]!=0) && (pb.nums[DIM+1]!=1)){
              my_outln(bw,"Value must be 1 or 0");
              break;
            }
            sim.set_watch(pb.nums,type,s,pb.nums[DIM+1]);
            break;

          case 'F': // flush queue
            sim.flush_queue();
            my_outln(bw,"Event queue has been cleared");
            break;

          case 'f': // set a fault
            if (check(pb,1)) break; // want "f row col"
            sim.set_fault_flags(pb.nums,1);
            break;

          case 's': /* Set an input to a cell */
            if (check(pb,3)) break;
            type=pb.words[DIM].toUpperCase().charAt(0); /* D or C */
            side=pb.words[DIM].toUpperCase().charAt(1); /* eg. N, S, W or E */
            s=ce.get_labels().indexOf(side);
            if (s == -1){
              my_outln(bw,"Side should be one of " + ce.get_labels());
              break;
            }
            if (pb.words[DIM+1].toUpperCase().charAt(0)=='M'){
// tick clock 128 times, setting input from memory TT before each tick

              for (i=2*cemem.ttsize-1;i>=0;i--){
                for (j=0;j<cemem.num_sides;j++){
                  if (!eventmode){ // DON'T process in event mode...
                    status=sim.user_set_input(pb.nums,type,s,
                      ((cemem.tt[i] & (1<<((cemem.num_sides-1)-j)))==0)?0:1);
                    if (our_pq(sim,wc)){ // triggered a watched event
                      if (!eventmode) my_outln(bw,"Entering event mode");
                      eventmode=true;
                    }
                    if (!eventmode) sim.tick();
                    if (our_pq(sim,wc)){ // triggered a watched event
                      if (!eventmode) my_outln(bw,"Entering event mode");
                      eventmode=true;
                    }
                  }
                  if (!dark) mw.unload_changes();
                }
              } // end of all 128 ticks
            } else { // normal SET command
              if ((pb.nums[DIM+1]!=0) && (pb.nums[DIM+1]!=1)){
                my_outln(bw,"Value must be 1 or 0 or \"m\" (memory)");
                break;
              }
              status=sim.user_set_input(pb.nums,type,s,pb.nums[DIM+1]);
            }
            break;

          case 'o': /* Show outputs from a cell */
            if (check(pb,1)) break;
            ce=sim.user_read_cell(pb.nums);
            my_outln(bw,"C:"+ce.get_labels());
            my_out(bw,"  ");
            for (i=0;i<NUM_SIDES;i++){
              my_out(bw,(0!=(ce.c_out&(1<<i)))?"1":"0");
            }
            my_outln(bw," ");

            my_outln(bw,"D:"+ce.get_labels());
            my_out(bw,"  ");
            for (i=0;i<NUM_SIDES;i++){
              my_out(bw,(0!=(ce.d_out&(1<<i)))?"1":"0");
            }
            my_outln(bw," ");

            break;

          case 'u': /* Specify update rate */
            if (pb.number==1){
              my_outln(bw,"Current update rate=" + update_rate);
            } else {
              update_rate=pb.nums[0];
              sim.set_unload_rate(update_rate);
            }
            if (update_rate < 1){
              update_rate=0;
              my_outln(bw,"No updates until t/r command is finished");
            }
            break;

          case 'e':/* single event */
            if (pb.number==1) pb.nums[0]=1; /* Default is one event */
            if (pb.nums[0]==0){ // normal mode
              my_outln(bw,"Resuming normal runmode");
              eventmode=false;
            } else {
              if (!eventmode) my_outln(bw,"Entering single-event runmode. No events occur except after 'e' command");
              eventmode=true;
            }
            for (j=0;j<pb.nums[0];j++){
              if (99==sim.process_queue_head()){
                my_outln(bw,"Event queue is empty");
                j=pb.nums[0];
              }
              if (!dark) mw.unload_changes();
              if (wc.status()){
                report_change(wc);
                j=pb.nums[0];
              }
            }
            if (!dark) mw.unload_changes();
            my_outln(bw,"");
            break;
            

          case 'r': /* Run (full cycles) */
            if (pb.number==1) pb.nums[0]=1; /* Default is one cycle */
            freq=(pb.nums[0]/72)+1;
            for (j=0;j<pb.nums[0];j++){
              for (i=0;i<2*NUM_SIDES*(1<<NUM_SIDES);i++){
                sim.tick();
                if (our_pq(sim,wc)){ // watch point hit!
                  i=99999999;j=pb.nums[0];
                  if (!eventmode) my_outln(bw,"Entering event mode");
                  eventmode=true;
                }
              }
              if (j%freq == 0) my_out(bw,".");
              if (update_rate > 0){if (j%update_rate == 0) if (!dark) mw.unload_changes();}
            }
            my_outln(bw,"");
            break;

          case 'I':
            sim.reinit();
            if (!dark) mw.unload_all();
            break;

          case 'K': /* Toggle queue lookahead and zeroing */
            sim.peek_set(pb.nums[0]);
            my_outln(bw,"Queue lookahead and zeroing is " +
             ((pb.nums[0]==0)?"disabled.":"enabled."));
            break;

          case 'T': // start *the* thread
            mydate=new Date();
            my_outln(bw,mydate.toString());
            if (threaded){
              my_outln(bw,"Thread is already running");
              break;
            }
            threaded=true;
            Thread t_t=new the_thread(sim,mw,ce,wc,dark,pb.nums[0],bw);
            t_t.start();
            break;

          case 't': /* Tick */
            if (pb.number==1) pb.nums[0]=1; /* Default is one tick */
            freq=(pb.nums[0]/72)+1;
            if (pb.nums[0]==0){
              sim.tick();
            }
            for (j=0;j<pb.nums[0];j++){ // will be skipped for "t 0"
              sim.tick();
              if (!eventmode){
                if (our_pq(sim,wc)){
                  j=pb.nums[0]; // exit loop on watchpt hit
                  if (!eventmode) my_outln(bw,"Entering event mode");
                  eventmode=true;
                }
              }
              if (j%freq == 0) my_out(bw,".");
              if (update_rate > 0){if (j%update_rate == 0) if (!dark) mw.unload_changes();}
            }
            my_outln(bw,"");
            break;

          case '\n':
          case '\0':
            break;
          default:
            my_outln(bw,"[ind] is a list of indicies");
            my_outln(bw,"c [ind]            Define cell[ind]'s TT");
            my_outln(bw,"X [ind] hex        Fill cll [ind]'s TT with hex pattern");
            my_outln(bw,"d [ind]            Show cell[ind]'s TT");
            my_outln(bw,"b [ind]            Show eqns for cell[ind]");
            my_outln(bw,"s [ind] side val   Set cell[ind].side=val");
            my_outln(bw,"T [msec]           Enter threaded mode (wait msec between steps)");
            my_outln(bw,"vd name r c side inc len\n");
            my_outln(bw,"   creates a vector called \"name\" starting ay [r,c], on the given side\n");
            my_outln(bw,"   inc is the change in r or c frmo bit to bit, and len is the number of bits\n");
            my_outln(bw,"   If len is \"32F\" then the number is IEEE-754 32-bit floating point\n");
            my_outln(bw,"v name value       Set the vector name to the given value\n");
            my_outln(bw,"V name value       Force name to the given value by compiling nearby cells\n");
            my_outln(bw,"v name             Show the value of the vector \"name\"\n");
            my_outln(bw,"o [ind]            Show outputs of cell[ind]");
            my_outln(bw,"t [num]            Tick clock num times (default=1");
            my_outln(bw,"t0 means tick but don't process any events yet");
            my_outln(bw,"w [ind] side val   set/clear a watch point");
            my_outln(bw,"e                  process single event");
            my_outln(bw,"e0 means resume end-of-loop Q processing");
            my_outln(bw,"E side nodename    Connect to another node");
            my_outln(bw,"ES server          Connect to Central Clock Arbitrator server");
            my_outln(bw,"K [1 or 0]         Enable/Disable queue lookahead and zeroing");
            my_outln(bw,"r [num]            Run num full programming cycles (default=1)");
            my_outln(bw,"u [num]            Show or set t/r update rate (0 for no updates)");
            my_outln(bw,"l [ind] filename [r c]  Load binary grid file, UL at cell[ind]");
            my_outln(bw,"S filename         Process sequence file");
            my_outln(bw,"M mode size sep    Set display mode");
            my_outln(bw,"g                  Toggle free-run mode");
            my_outln(bw,"@filename          Execute commands from filename");
            my_outln(bw,"I                  Reinitialize entire matrix");
            my_outln(bw,"F                  Flush queue, discarding all pending events");
            my_outln(bw,"f row col          set a fault at cell [row,col]");
            my_outln(bw,"C [fname]          Start or end Mode 1 capture");
            my_outln(bw,"D fname n bin      Run tests on fname. 0<=n<=3 (test type). bin is input grid");
            my_outln(bw,"W rows cols        Set window size");
            my_outln(bw,"# or ;             Comment");
            my_outln(bw,"Q                  Exit immediately");
        }
        //sim.do_pending(); // Pending mouse-based requests
        if ((!threaded)&&(!eventmode)){
          if (our_pq(sim,wc)){
            if (!eventmode) my_outln(bw,"Entering event mode");
            eventmode=true;
          }
        }
        if (!dark) mw.unload_changes();
        //if (!dark && (DIM==3)) mw.unload_all(); // slow but looks better
//mw.repaint();

        my_out(bw,sim.get_clockcount()+":"+sim.get_timestamp() + 
                         ":"+sim.get_eventcount() + fnprompt + "> ");
        buffer=br.readLine();

        if (buffer==null){ // End of file
          my_outln(bw,"^D");
          if (fpstack.empty()){
            more=false;
          } else {
br.close(); /*** DO WE WANT THIS ??? ### ***/
            br=(BufferedReader)fpstack.pop();
            if (!fnamestack.empty()){
              fnamestack.pop(); // Remove current name
              if (!fnamestack.empty()){
                fnprompt=(String)fnamestack.peek();
              } else {
                fnprompt="";
              }
            }
            buffer=";------------";
          }
        }
      } /* End of input */
      my_outln(bw,"^D");
      isr.close();
    } catch (Exception e){
      System.out.println("Error: " + e);
      got_error=true;
    }
   }
   System.exit(0);
  }

// process queue till empry, return WATCH_HIT
  static boolean our_pq(cell_matrix_sim sim,watch_class wc)
  {
    int status,count;

    count=0;
    if (wc.status()){ // hit a watch point
      report_change(wc);
    }
    while ((status=sim.process_queue_head())==0){
      if (++count > sim.event_limit){
        my_outln(bw,"Event limit (" + sim.event_limit + ") has been reached.");
        return(true); // hehehe everything stops now :)
      }
      if (wc.status()){ // hit a watch point
        report_change(wc);
        return(true);
      }
    }
    return(false);
  }

  static void report_change(watch_class wc)
  {
    Date d=new Date();
    my_outln(bw,d.toString() + " WATCH POINT EVENT: ["+
                       wc.saveind[0]+","+wc.saveind[1]+
                       "]D"+wc.savesidename+".out");
  }


/* Legality checker */
  static boolean check(parse_buffer pb,int i)
  {
    if (i+DIM != pb.number){
      my_outln(bw,"Command requires " + (i+DIM) + " arguments");
      return(true);
    }
    return(false);
  }


/* Break a string into pieces */
/* Skip first word! */

  static parse_buffer parse(String buffer)
  {
    parse_buffer pb=new parse_buffer();
    int i,num,nums[]=new int[16];
    String words[]=new String[16];
    double fnums[]=new double[16];
    StringTokenizer st=new StringTokenizer(buffer," ");
    pb.number=st.countTokens(); /* # of items parsed */
    for (i=0;i<16;i++) pb.nums[i]=-1;
    i=0;

    while (st.hasMoreTokens()){
      pb.words[i]=st.nextToken();
      pb.nums[i]=(-1);pb.fnums[i]=0.; // Error values
      try{pb.nums[i]=Integer.parseInt(pb.words[i]);}
        catch(Exception e){}
      ++i;
    }
    num=i; // Number of words parsed

    for (i=0;i<num;i++){
      try{pb.fnums[i]=Double.valueOf(pb.words[i]).doubleValue();}
        catch(Exception e){}
    }
    return(pb);
  }

  static int coord[]={-1,0};

/*
 * VECTOR command processors
 */

  static void v_def(cell_matrix_sim sim,cell_exchange ce, String name, int row, int col,
                    String sidestr, int inc, int len, String lens)
  {
    char type;
    int side;

    if (sidestr.length() != 2){
      my_outln(bw,"Side should be 2 chars (e.g., DW, CS, etc.)\n");
      return;
    }

/* Change the side string into a type and sidenum */
    type=sidestr.toUpperCase().charAt(0);
    side=ce.get_labels().indexOf(sidestr.toUpperCase().charAt(1));
    if (side == -1){
      my_outln(bw,"Side should be [D or C] followed by one of: " + ce.get_labels());
      return;
    }

    vector_class vc=new vector_class(row,col,0,type,side,
                                     sidestr.toUpperCase().charAt(1),
                                     inc,0,len,lens,2);
/* Insert into hash! */
    try{
      vectorhash.put(name,vc);
    } catch (Exception e) {my_outln(bw,"Bad error: hash failue");}
  }

// 3-D vector definition
// inc1, inc2 work as follows:
// Side    inc1    inc2
//  N       c       h
//  S       c       h
//  W       r       h
//  E       r       h
//  T       r       c
//  B       r       c

  static void v_def(cell_matrix_sim sim,cell_exchange ce, String name,
                    int row, int col, int height,
                    String sidestr,
                    int inc1,int inc2,
                    int len, String lens)
  {
    char type;
    int side;

    if (sidestr.length() != 2){
      my_outln(bw,"Side should be 2 chars (e.g., DW, CS, etc.)\n");
      return;
    }

/* Change the side string into a type and sidenum */
    type=sidestr.toUpperCase().charAt(0);
    side=ce.get_labels().indexOf(sidestr.toUpperCase().charAt(1));
    if (side == -1){
      my_outln(bw,"Side should be [D or C] followed by one of: " + ce.get_labels());
      return;
    }

    vector_class vc=new vector_class(row,col,height,type,side,
                                     sidestr.toUpperCase().charAt(1),
                                     inc1,inc2,len,lens,3);
/* Insert into hash! */
    try{
      vectorhash.put(name,vc);
    } catch (Exception e) {my_outln(bw,"Bad error: hash failue");}
  }


// Method to set a vector's value
// If comp=0, just set inputs (edge assumed)
// If comp=1, compile cells to set inputs (assumed internal)

  static void v_set(cell_matrix_sim sim,cell_exchange ce, String name, int comp,int val, float fval)
  {
    int row,col,height,rowinc,colinc,heightinc,bitnum,result,out,bit;
    int newr,newc,newh;

    if (!vectorhash.containsKey(name)){
      my_outln(bw,"Vector " + name  + " has not been defined.");
      return;
    }

    vector_class vc=(vector_class) vectorhash.get(name);

/* Check fp flag, and if set, convert from floating point */

    if (vc.fp){
/* copy fval's bits into val */
      int sign,mask,iexp;
      double exp,work;

/* Is fval=0? Too bad! We'll deal with that later... */

/* Store sign, and make fval positive */
      sign=0;
      if (fval < 0){
        sign=1;
        fval=(-fval);
      }

/* Compute exponent */
      exp=Math.floor(Math.log(fval)/Math.log(2.));  /* cool... */

/* Now break out the mantissa's bits,and store them in result */
      work=fval/Math.pow(2.,exp); /* Normalized mantissa */
      work=work-1; /* Toss the 1 left of the BP */
      mask=1<<22; /* OR this if the bit is set */
      val=0;
      for (bitnum=22; bitnum>=0; bitnum--){ /* really just a counter */
        work=work*2.;
        if (work >= 1.){
          val|=mask;
          work=work-1.;
        }
        mask>>=1;
      }

      exp=exp+127.; /* Final exponent adjustment */
      iexp=(int)exp;
      iexp&=0xff;
      val|=(iexp<<23);

/* and set the sign bit */
      if (sign == 1) val|=(1<<31);
    }

    row=vc.row;col=vc.col; height=vc.height;

/* Set up r/c/h increments */
    if ((vc.sidestr == 'N') || (vc.sidestr == 'S')){
      rowinc=0;colinc=vc.inc1;heightinc=vc.inc2;
    } else if ((vc.sidestr == 'E') || (vc.sidestr == 'W')){
      rowinc=vc.inc1;colinc=0;heightinc=vc.inc2;
    } else if ((vc.sidestr == 'T') || (vc.sidestr == 'B')){
      rowinc=vc.inc1;colinc=vc.inc2;heightinc=0;
    } else {
      my_outln(bw,"Vector " + name + " references an unknown side: "
                          + vc.sidestr);
      return;
    }

/* Now break down the vector and set inputs */
    for (bitnum=0;bitnum < vc.len;bitnum++){
      bit=(0==(val&(1<<bitnum)))?0:1;

      if (comp==0){ // Just set the input normally
        switch (vc.dims){
          case 2:sim.user_set_input(row,col,vc.type,vc.side,bit);break;
          case 3:sim.user_set_input(row,col,height,vc.type,vc.side,bit);break;
          default:System.out.println("??? Unknown dims: " + vc.dims);
        }
      } else { // compile a neighboring cell to set the input
        StringBuffer sb=new StringBuffer("D");
        newr=row;newc=col;newh=height;

        switch(vc.sidestr){
          case 'N':sb.append("S=");--newr;break;
          case 'S':sb.append("N=");++newr;break;
          case 'W':sb.append("E=");--newc;break;
          case 'E':sb.append("W=");++newc;break;
          case 'B':sb.append("T=");--newh;break;
          case 'T':sb.append("B=");++newh;break;
          default:break;
        }
        sb.append(" "+bit);

        ce.tt_compile(sb.toString(),bw);
        if (vc.dims==2){
          if (0 != sim.user_write_cell(newr,newc,ce)){
            my_outln(bw,"Can't compile cell in ["+row+","+col+"]");
          }
        } else if (vc.dims==3){
          if (0 != sim.user_write_cell(newr,newc,newh,ce)){
            my_outln(bw,"Can't compile cell in ["+row+","+col+","+height+"]");
          }
        } else {
          System.out.println("Unknown #dims: " + vc.dims);
        }
      }
      row+=rowinc;col+=colinc;height=height+heightinc; /* ready for next read */
    }
  }

  static vstruct v_s;

// twoc=1 means do 2's complement; else just a positive integer
  static vstruct v_get(cell_matrix_sim sim,cell_exchange ce, String name)
  {
    int row,col,height,rowinc,colinc,heightinc,bitnum,result,out,mask;

    v_s=new vstruct();

    if (!vectorhash.containsKey(name)){
      my_outln(bw,"Vector " + name  + " has not been defined.");
      v_s.type=v_s.ERROR;
      return(v_s);
    }

    vector_class vc=(vector_class) vectorhash.get(name);

    row=vc.row;col=vc.col;height=vc.height;
    mask=1<<vc.side; /* For picking appropriate bit from output byte */

/* Set up r/c/h increments */
    if ((vc.sidestr == 'N') || (vc.sidestr == 'S')){
      rowinc=0;colinc=vc.inc1;heightinc=vc.inc2;
    } else if ((vc.sidestr == 'E') || (vc.sidestr == 'W')){
      rowinc=vc.inc1;colinc=0;heightinc=vc.inc2;
    } else if ((vc.sidestr == 'T') || (vc.sidestr == 'B')){
      rowinc=vc.inc1;colinc=vc.inc2;heightinc=0;
    } else {
      my_outln(bw,"Vector " + name + " references an unknown side: "
                          + vc.sidestr);
      v_s.type=v_s.ERROR;
      return(v_s);
    }

/* Now build the vector */
    result=0;out=0; /* Just to make compiler happy */
    for (bitnum=0;bitnum < vc.len;bitnum++){
      if (vc.dims==2) ce=sim.user_read_cell(row,col); /* Read the output byte */
      else if (vc.dims==3) ce=sim.user_read_cell(row,col,height);
      else System.out.println("ERROR: Unknown dims: " + vc.dims);

      if (vc.type=='D') {out=ce.d_out;} else {out=ce.c_out;} /* choose C or D */
      if (0 != (out & mask)) result|=(1<<bitnum); /* Set bit! */

      row+=rowinc;col+=colinc;height+=heightinc; /* ready for next read */
    }

    if (!vc.fp){
      v_s.type=v_s.INT_TYPE; // Int
      v_s.i=result;
      if (0 != (out & mask)){
        result=result-(1<<vc.len);
      }
      v_s.int2=result; // 2's comp result
      return(v_s);
    }

/* We want to interpret result as IEEE-754 */
    double exp,expm, mant,mantm, fresult;

/* Compute EXP */
    exp=0.;expm=1;
    for (bitnum=23;bitnum<=30;bitnum++){
      if (0 != (result & (1<<bitnum))) exp=exp+expm;
      expm=expm*2.;
    }
    exp=exp-127.;

/* Compute Mantissa */
    mant=1; /* implicit one left of BP */
    mantm=.5;
    for (bitnum=22;bitnum>=0;bitnum--){
      if (0 != (result & (1<<bitnum))) mant=mant+mantm;
      mantm=mantm/2.;
    }

    fresult=mant*Math.pow(2.,exp);
    if (0 != (result & (1<<31))) fresult=(-fresult);

    v_s.type=v_s.FP_TYPE; // FP
    v_s.fp=(float)fresult;
    return(v_s);
  }

/*
 * CCA connection.
 * We need to connect to a Central Clock Arbitrator (CCA)
 * which will tell all nodes when it is safe to tick their system clocks
 *
 * To initiate connection, this node must:
 *	connect to CCA;
 *	receive unique ID from CCA;
 *	sends own ID to each neighbor;
 *	waits to receive each neighbor's ID;
 *	report all neighbor IDs to CCA; and
 *	wait for INIT_DONE signal from CCA
 *
 */

  static void cca(String hostname,cell_matrix_sim sim){
    int port=1203;
    int i,j,k,l;
    Cons cons;
    byte b[]=new byte[4];

    cons=sim.get_cons(); // main cons structure

// connect to CCA server
    try{
      InetAddress ias[]=InetAddress.getAllByName(hostname);
      my_outln(bw,hostname + " addr=" + ias[0].getHostAddress());
      Socket sock=new Socket(ias[0],port);
      sock.setTcpNoDelay(true);
// create a pair of streams
      OutputStream os=sock.getOutputStream();
      InputStream is=sock.getInputStream();
// SAVE these in the cons structure
      cons.ccain=is;
      cons.ccaout=os;
// Read our ID
      is.read(b,0,4);
      cons.myid=ByteIO.from_byte(b);
// send to each neighbor
      for (i=0;i<6;i++){
        if (cons.active[i]){
          cons.out[i].write(ByteIO.to_byte(cons.myid),0,4);
        }
      }
// read ID of each neighbor
      for (i=0;i<6;i++){
        if (cons.active[i]){
          cons.in[i].read(b,0,4);
          cons.id[i]=ByteIO.from_byte(b);
        }
      }
// send ID info to CCA
// <i13 i12 i11 ... i1 i0> <s2 s1 s0> <m13 m12 m11 ... m1 m0>
// where i is the ID of the neighbor on side S, and m is myid
      for (i=0;i<6;i++){
        l=(cons.myid & 0x3fff) |
          ((i&7) << 14) |
          ((cons.id[i]&0x3fff)<<17);
        if (cons.active[i]) os.write(ByteIO.to_byte(l),0,4);
        if (cons.active[i]) my_outln(bw,"Sent " + l);
      }
// Tell CCA we've sent all nbr info
      ////dos.writeInt(-1);
// and now wait for final sync ('2') from CCA
      cons.ccain.read(b,0,1);
      i=(int) (b[0]);
    } catch (Exception e){
      my_outln(bw,"CCA Connect error:" + e);
      return;
    }
  }

  static void my_outln(BufferedWriter bw,String buf)
  {
    my_out(bw,buf + "\n");
  }

  static void my_out(BufferedWriter bw,String buf)
  {
    if (bw==null){
      System.out.print(buf);
      return;
    }

    try{
      bw.write(buf);
      bw.flush();
    } catch (Exception e){
    }
  }
}

/* Return object for parse() method */
class parse_buffer{
  String words[]=new String[16]; /* Holds pieces */
  int nums[]=new int[16]; /* Numeric values, -1 if not a number */
  double fnums[]=new double[16];
  int number; /* # of items parsed */
}


/* Vector class */
// Make space for 3-D vector elements, but keep height at 0 if 2D
class vector_class{
  int row,col,height;
  char type; /* D or C */
  int side; /* index in ce.getLabels() */
  char sidestr; /* N, S, W or E */
  int inc1,inc2; /* cell step size */
  int len; /* # bits */
  boolean fp; /* Floating point vector? */
  int dims; // 2 or 3!

  vector_class(int r, int c, int h,
               char t, int s, char sstr, int i1, int i2, int l, String ls,
               int d)
  {
    row=r;col=c;height=h;type=t;side=s;sidestr=sstr;inc1=i1;inc2=i2;dims=d;
    if (ls.equals("32F")){ /* Fp */
      len=32;fp=true;
    } else {
      len=l;fp=false;
    }
  }
}

// vector return class
class vstruct{
  static int INT_TYPE=1;
  static int FP_TYPE=2;
  static int ERROR=3; // values for vstruct.type

  int type=0;
  int i=0; // integer value
  int int2=0;  // 2's complement integer value
  float fp=(float)0.;  // IEEE-754 Floating Point (single precision) value

  vstruct()
  {
    type=ERROR;
  }
}

// free-running class
class the_thread extends Thread{
  cell_matrix_sim sim;
  window_class mw;
  cell_exchange ce;
  watch_class wc;
  boolean dark;
  int delay;
  BufferedWriter bw;

  the_thread(cell_matrix_sim s,window_class m_w,cell_exchange c,
             watch_class w2,boolean d,
             int del,BufferedWriter bwin)
  {
    sim=s; // save ref to simulator engine
    mw=m_w;
    ce=c;
    wc=w2;
    dark=d;
    delay=(del>0)?del:0;
    bw=bwin;
  }

  public void run()
  {
    boolean eventmode=false,secondtry=false;
    int count,s,saved_zero_queue=0,i;
    boolean queue_empty;
    byte b[]=new byte[4];

    if (mw != null)
      mw.threaded(true); // tell window we're running in threaded mode
    my_outln(bw,"Starting system clock...");

    try{
      eventmode=false;
      sim.halted_state(false); // was already set by 'T' command :)
      secondtry=false;
      count=0; // # of events since Q was last empty
      queue_empty=false; // if true, we've sent one QMT msg to the CCA

      while (true){
// see if there are pending input changes from other nodes
        while (multinode_process(sim,mw)){ // process one event per call
// returns FALSE if nothing processed
/***
my_outln(bw,"multinode_process...#in=" + sim.get_cons().num_in[0] + " " + sim.get_cons().num_in[1] + " " + sim.get_cons().num_in[2]+ " " + sim.get_cons().num_in[3]);
my_outln(bw,"multinode_process...#out=" + sim.get_cons().num_out[0] + " " + sim.get_cons().num_out[1] + " " + sim.get_cons().num_out[2]+ " " + sim.get_cons().num_out[3]);
***/
          pause();
          queue_empty=false;
        }
        pause();

// see if a watch has triggered
        if (wc.status()){ // watched-event triggered
          report_change(wc);
          sim.halted_state(true);
          if (!dark) mw.unload_changes();
          eventmode=true;
        }

        s=sim.process_queue_head();
        if (s < 0){  // Q overflow maybe?
          my_outln(bw,"process_queue_head returned " + s);
          my_outln(bw,"Halting the system clock for now");
          if (!dark) mw.unload_changes();
          sim.halted_state(true);
          queue_empty=false;
          if (secondtry){
            sim.zero_queue=saved_zero_queue;
            secondtry=false;
          }
          continue;
        }
        if (s==99){ // queue empty
/***
if (!queue_empty){
my_outln(bw,"Queue empty...#in=" + sim.get_cons().num_in[0] + " " + sim.get_cons().num_in[1] + " " + sim.get_cons().num_in[2]+ " " + sim.get_cons().num_in[3]);
my_outln(bw,"Queue empty...#out=" + sim.get_cons().num_out[0] + " " + sim.get_cons().num_out[1] + " " + sim.get_cons().num_out[2]+ " " + sim.get_cons().num_out[3]);
}
***/
          count=0; // reset
          if (secondtry){
            my_outln(bw,"Zero-checking cleared the problem! Returning to previous look-ahead state.");
            sim.zero_queue=saved_zero_queue;
            secondtry=false;
          }
          if (!sim.halted_state()){ // we're (maybe) ready to tick the clock...
            if (!queue_empty){ // this is first time queue has emptied...
              queue_empty=true; // so flag that fact
              //my_outln(bw,sim.get_cons().myid + "'s queue is empty...");
// send counts to CCA
              for (i=0;i<6;i++){
                if (sim.get_cons().active[i]){ // send event counts
                  sim.get_cons().ccaout.write(ByteIO.to_byte(
                     i | 0 |
                     ((sim.get_cons().num_in[i])<<4)),0,4);
                  sim.get_cons().ccaout.write(ByteIO.to_byte(
                     i | 8 | // 8 flags this as # events sent
                     ((sim.get_cons().num_out[i])<<4)),0,4);
                }
              } // all sides sent to CCA
              sim.get_cons().ccaout.write(ByteIO.to_byte(-1),0,4); // signal that we've sent all events
// we only want to signal once - then keep checking for events, or 0/1 from CCA
            } // end of if (!queue_empty)
            if (sim.get_cons().ccain.available() > 0){ // msg coming in
              sim.get_cons().ccain.read(b,0,1); // '0'
              if (b[0]!='0') my_outln(bw,"Expecting CCA msg '0' but got " + b[0]);
//////clear event counts
            for (i=0;i<6;i++) sim.get_cons().num_in[i]=sim.get_cons().num_out[i]=0;
// notify CCA that we're done sending node info and are ready to tick
              if (!sim.fastcca) sim.get_cons().ccaout.write(ByteIO.to_byte(-2),0,4);
//and wait for "tick" signal
              if (!sim.fastcca) sim.get_cons().ccain.read(b,0,1); // '1'
              if (b[0]!='1') my_outln(bw,"Expecting CCA msg '1' but got " + b[0]);
              sim.tick(); // and tick sim's binary clock counter
              queue_empty=false; // since there's a TICK event in it now!
            }
          }
          //mw.unload_changes();
        } else queue_empty=false; // end of status=99 (Q empty)
// queue overflow?
        if (++count > sim.event_limit){
          queue_empty=false;
          if (secondtry){ // no hope
            my_outln(bw,"Zero-checking did not solve the problem. Dumping queue to .QDump and halting system clock");
            sim.display_queue(".QDump");
            sim.zero_queue=saved_zero_queue;
            sim.halted_state(true);
            if (!dark) mw.unload_changes();
            eventmode=true;
            continue;
          } else { // try again with queue zeroing enabled
            my_outln(bw,count + "events w/out emptying queue. Possible unstable grid?");
            my_outln(bw,"Enabling queue look-ahead zeroing and re-trying...");
            saved_zero_queue=sim.zero_queue; // need to restore later
            count=0;
            sim.zero_queue=1;
            secondtry=true;
            if (!dark) mw.unload_changes();
          }
        }
// process pending GUI changes
        if (sim.input_pending()){
          queue_empty=false;
          sim.do_pending();
          if (!dark) mw.unload_changes();
        }
      }

    } catch (Exception e){
      my_outln(bw,"Thread exception: " + e);
    }
  }

  void pause()
  {
    try{
      if (delay > 0) sleep(delay);
    } catch (Exception e){}
  }

// check active input ports for incoming changes
  boolean multinode_process(cell_matrix_sim sim,window_class mw)
  {
    int side,i;
    boolean did_something;
    byte b[]=new byte[4];

    did_something=false;
    for (side=0;side<6;side++){
      if (sim.get_cons().active[side]){ // connection is there - any input?
//my_outln(bw,"Active on " + side);
        try{
          if (sim.get_cons().in[side].available() >= 4){
//my_outln(bw,"Got input!");
            ++(sim.get_cons().num_in[side]);
            did_something=true; // :P
            sim.get_cons().in[side].read(b,0,4);
            i=ByteIO.from_byte(b);
// Parse the integer command
// 2D:
//   Bit 0=1
//   Bit 1=Value
//   Bit 2=0:C,1:D
//   Bit 3 1-bit clock count: toggles after each clock tick
//   Bit 4-31=Cell number<<4
//
// 3D:
//   Bit 0=1
//   Bit 1=Value
//   Bit 2=0:C,1:D
//   Bit 3 1-bit clock count: toggles after each clock tick
//   Bit 4-17=Cell number1<<4 e.g. ROW
//   Bit 18-31=Cell number2<<18 e.g. COL

// See if our node's clock is in sync with this neighbor's
            if ( (((i&8)==0) && sim.tick1()) ||
                 (((i&8)==8) && !sim.tick1())){ // out of sync
my_outln(bw,"We're out of sync...reading CCA tick...");
my_outln(bw,"clock=" + sim.tick1());
              sim.get_cons().ccain.read(b,0,1); // read TICK signal
my_outln(bw,"and ticking clock...");
              sim.tick(); // tick the clock
my_outln(bw,"clock=" + sim.tick1());
my_outln(bw,"done!\n");
// we'll be inserting an event below, so queue_empty will go non-empty then
            }

// continue processing neighbor's event
            if ((i&1) != 1){my_outln(bw,"Unknown edge command:" + i);}
            int value=(i>>1)&1;
            char type=(((i>>2)&1)==1)?'D':'C';
            int num1=0,num2=0;
            int r=0,c=0,l=0;
            char cside=ce.get_labels().toLowerCase().charAt(side);

            if (sim.get_dim()==2){ // 2D
// Determine [r,c] to change
              num1=i>>4;
//my_outln(bw,"Got input: side=" + cside + " value=" + value + " type=" + type + " num1=" + num1);
              switch(cside){
                case 'n':r=0;c=num1;break;
                case 's':r=sim.dims[0]-1;c=num1;break;
                case 'w':r=num1;c=0;break;
                case 'e':r=num1;c=sim.dims[1]-1;break;
                default:r=(-1);c=(-1);break;
              }

// Now we can record the input change
              sim.user_set_input(r,c,type,side,value);
            } else { // 3D case
// Determine [r,c,l] to change
              num1=(i>>4) & 0x3fff;;
              num2=(i>>18) & 0x3fff;
//my_outln(bw,"Got input: side=" + cside + " value=" + value + " type=" + type + " num1=" + num1 + " num2=" + num2);
              switch(cside){
                case 'n':r=0;c=num1;l=num2;;break;
                case 's':r=sim.dims[0]-1;c=num1;l=num2;break;
                case 'w':r=num1;c=0;l=num2;break;
                case 'e':r=num1;c=sim.dims[1]-1;l=num2;break;
                case 'b':r=num1;c=num2;l=0;break;
                case 't':r=num1;c=num2;l=sim.dims[2]-1;break;
                default:r=(-1);c=(-1);break;
              }

// Now we can record the input change
              sim.user_set_input(r,c,l,type,side,value);
            }
            sim.process_queue_head();
            if (sim.get_window_class() != null)
              sim.get_window_class().unload_changes();///
          } // else no input available
        } catch(Exception e){
          my_outln(bw,"readint error:" + e);
        } // Connection probably closed
      } // end of SIDE ACTIVE
    } // end of all sides
    return(did_something);
  } // end of multinode_process()

  void report_change(watch_class wc)
  {
    Date d=new Date();
    my_outln(bw,d.toString() + " WATCH POINT EVENT: ["+
                       wc.saveind[0]+","+wc.saveind[1]+
                       "]D"+wc.savesidename+".out");
  }


  void my_outln(BufferedWriter bw,String buf)
  {
    my_out(bw,buf + "\n");
  }

  void my_out(BufferedWriter bw,String buf)
  {
    if (bw==null){
      System.out.print(buf);
      return;
    }

    try{
      bw.write(buf);
      bw.flush();
    } catch (Exception e){
    }
  }
}
