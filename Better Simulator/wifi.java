import java.net.*;
import java.util.*;

public class wifi{

  public static void main(String args[])
  {
    Socket sock=null;
    ServerSocket ss=null;
    InputStream is;
    InputStreamReader isr;
    BufferedReader br;
    OutputStream os;
    OutputStreamWriter osw;
    boolean got_error=false;

// setup input stream
    try{
      ss=new ServerSocket(1204);
      sock=ss.accept();

      is=sock.getInputStream();
      isr=new InputStreamReader(is);
      br=new BufferedReader(isr);

      os=sock.getOutputStream();
      osw=new OutputStreamWriter(os);
      bw=new BufferedWriter(osw);
      bw.write("Hello");
      bw.flush();
    } catch (Exception e){
      System.out.println("Low-level error establishing input stream - giving up");
      System.exit(0);
    }
  }
}
