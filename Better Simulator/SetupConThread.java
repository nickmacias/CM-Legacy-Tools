import java.io.*;
import java.net.*;
import java.lang.*;
import java.util.*;
import java.awt.event.*;

class Cons{
  boolean active[];        // is this side active?
  InputStream in[];        // for receiving data
  OutputStream out[];      // for sending data
  int num_in[],num_out[];  // # of events in/out
  int id[];                // ID # of each neighbor
  int myid;                // My ID # (assigned by CCA)
  InputStream ccain;       // FROM cca
  OutputStream ccaout;     // TO cca

  Cons()
  {
    int i;
    active=new boolean[6];
    in=new InputStream[6];
    out=new OutputStream[6];
    num_in=new int[6]; // from neighbor
    num_out=new int[6];
    id=new int[6];
    myid=0;

    for (i=0;i<6;i++){
      active[i]=false;
    }
  }
}

class SetupConThread extends Thread{
  cell_matrix_sim sim;
  char cside; // N, S, W, E
  int side; // actually, an index into ce.get_labels()
  String host;
  static int ind[]=new int[32];
  boolean done=false;
  BufferedWriter bw;

  SetupConThread(cell_matrix_sim s,char cs,int si,String hos,
                 BufferedWriter bwin)
  {
// just save info for subsequent start() call
    sim=s;
    cside=cs;
    side=si;
    host=hos;
    bw=bwin;

// Setup watch points here, as opposed to in the run() method
// otherwise, we might have two threads calling sim.set_watch()
// at the same time, which can cause major problems

    if (sim.get_dim() == 2){
// turn on WATCH points on the indicated side
      switch(cside){
        case 'N':ind[0]=0;
                 for (ind[1]=0;ind[1]<sim.dims[1];ind[1]++){
                   sim.set_watch(ind,'D',side,1);
                   sim.set_watch(ind,'C',side,1);
                 }
                 break;
        case 'S':ind[0]=sim.dims[0]-1;
                 for (ind[1]=0;ind[1]<sim.dims[1];ind[1]++){
                   sim.set_watch(ind,'D',side,1);
                   sim.set_watch(ind,'C',side,1);
                 }
                 break;
        case 'W':ind[1]=0;
                 for (ind[0]=0;ind[0]<sim.dims[0];ind[0]++){
                   sim.set_watch(ind,'D',side,1);
                   sim.set_watch(ind,'C',side,1);
                 }
                 break;
        case 'E':ind[1]=sim.dims[1]-1;
                 for (ind[0]=0;ind[0]<sim.dims[0];ind[0]++){
                   sim.set_watch(ind,'D',side,1);
                   sim.set_watch(ind,'C',side,1);
                 }
                 break;
        default:my_outln(bw,"Unknown side: " + cside);
      }
    } else { // 3D
// turn on WATCH points on the indicated face
      switch(cside){
        case 'N':ind[0]=0;
                 for (ind[1]=0;ind[1]<sim.dims[1];ind[1]++){
                   for (ind[2]=0;ind[2]<sim.dims[2];ind[2]++){
                     sim.set_watch(ind,'D',side,1);
                     sim.set_watch(ind,'C',side,1);
                   }
                 }
                 break;
        case 'S':ind[0]=sim.dims[0]-1;
                 for (ind[1]=0;ind[1]<sim.dims[1];ind[1]++){
                   for (ind[2]=0;ind[2]<sim.dims[2];ind[2]++){
                     sim.set_watch(ind,'D',side,1);
                     sim.set_watch(ind,'C',side,1);
                   }
                 }
                 break;
        case 'W':ind[1]=0;
                 for (ind[0]=0;ind[0]<sim.dims[0];ind[0]++){
                   for (ind[2]=0;ind[2]<sim.dims[2];ind[2]++){
//System.out.println("Setting watch for [" + ind[0] + "," + ind[1] + "," + ind[2] + "]");
                     sim.set_watch(ind,'D',side,1);
                     sim.set_watch(ind,'C',side,1);
                   }
                 }
                 break;
        case 'E':ind[1]=sim.dims[1]-1;
                 for (ind[0]=0;ind[0]<sim.dims[0];ind[0]++){
                   for (ind[2]=0;ind[2]<sim.dims[2];ind[2]++){
//System.out.println("Setting watch for [" + ind[0] + "," + ind[1] + "," + ind[2] + "]");
                     sim.set_watch(ind,'D',side,1);
                     sim.set_watch(ind,'C',side,1);
                   }
                 }
                 break;
        case 'T':ind[2]=sim.dims[2]-1;
                 for (ind[0]=0;ind[0]<sim.dims[0];ind[0]++){
                   for (ind[1]=0;ind[1]<sim.dims[1];ind[1]++){
                     sim.set_watch(ind,'D',side,1);
                     sim.set_watch(ind,'C',side,1);
                   }
                 }
                 break;
        case 'B':ind[2]=0;
                 for (ind[0]=0;ind[0]<sim.dims[0];ind[0]++){
                   for (ind[1]=0;ind[1]<sim.dims[1];ind[1]++){
                     sim.set_watch(ind,'D',side,1);
                     sim.set_watch(ind,'C',side,1);
                   }
                 }
                 break;
        default:my_outln(bw,"Unknown side: " + cside);
      }
    }
  }

// go ahead and make the connection
  public void run()
  {
// use a sub-thread to connect, and monitor from here
// this allows us to abort connection attempts gracefully
// and also lets us wrap up the connection process from a single point

    if ((cside=='W')||(cside=='N')||(cside=='B')){ // the 0 indexes
      ServerThread st=new ServerThread();st.start();
      while (st.isAlive()); // wait
      done=true;
    }
    if ((cside=='E')||(cside=='S')||(cside=='T')){ // the dim-1 indexes
      ClientThread ct=new ClientThread();ct.start();
      while (ct.isAlive()); // wait
      done=true;
    }

// this is not totally-random: most-likely, we'll be controlling this
// cluster from behind a firewall, which is easier to connect to/from as a
// client vs. a server. Since we'll most-likely want control from the
// upper-left, we'll be connecting our E or, perhaps, S edge.

// thread has died - we'll assume this means the connection is completed,
// though we could check this to make sure...

// now that we've got a connection, let's arm it...
    sim.get_cons().active[side]=true;

// now, changes on our edge will generate watch-events, which we'll use
// to send change info to a neighbor.
// We'll also check our input port periodically, for incoming edge events
// :)
  }

  class ClientThread extends Thread
  {
    public void run()
    {
// Connect to a server
// find the port number
      int port;
      switch (cside){
        case 'E':port=1201;break;
        case 'S':port=1202;break;
        case 'T':port=1205;break;
        default: my_outln(bw,"Expecting side to be E, S or T, not " +
                                     cside);
        return;
      }

      boolean done=false;
      while (!done){
        try{
          InetAddress ias[]=InetAddress.getAllByName(host);
          my_outln(bw,"addr=" + ias[0].getHostAddress());
          Socket sock=new Socket(ias[0],port);
          sim.get_cons().in[side]=sock.getInputStream();
          sim.get_cons().out[side]=sock.getOutputStream();
          my_outln(bw,"Connected to server!");
          done=true;
        } catch (Exception e){my_outln(bw,"Client error:" + e);}
          if (!done){
            try{sleep(5000);}catch(Exception e){};
          }
      } // connected!
    }
  } // end of client thread

// Server is for outgoing events
  class ServerThread extends Thread
  {
    public void run()
    {
      int port;
      switch (cside){
        case 'W':port=1201;break;
        case 'N':port=1202;break;
        case 'B':port=1205;break;
        default: my_outln(bw,"Expecting W, N or B, but got " + cside);
        return;
      }

      try{
        ServerSocket ss=new ServerSocket(port);
//my_outln(bw,"Server waiting for connection on port " + port);
        Socket sock=ss.accept();
        ss.close();
        sim.get_cons().in[side]=sock.getInputStream();
        sim.get_cons().out[side]=sock.getOutputStream();
my_outln(bw,"Server has received a connection from a client!");
      } catch (Exception e){my_outln(bw,"Server Err:" + e);}
    }
  }

  void my_outln(BufferedWriter bw,String buf)
  {
    my_out(bw,buf + "\n");
  }

  void my_out(BufferedWriter bw,String buf)
  {
    if (bw==null){
      System.out.print(buf);
      return;
    }

    try{
      bw.write(buf);
      bw.flush();
    } catch (Exception e){
    }
  }
}
