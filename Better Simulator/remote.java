import java.io.*;
import java.util.*;
import java.net.*;


public class remote{

  public static void main(String args[])
  {
    int port=1204;
    int i;
    String hostname="localhost";
    String buffer;
    String cmd[]=new String[10];
    int num_cmd;
    boolean send; // set if cmd goes to a client
    int client=0; // default client
    BufferedWriter to_sock[]=new BufferedWriter[2048];
    InputStream from_sock[]=new InputStream[2048];
    BufferedReader from_kb=null;
    Stack fpstack=new Stack(); // input pointer stack

// setup KB input
    try{
      InputStreamReader isr2=new InputStreamReader(System.in);
      from_kb=new BufferedReader(isr2);
    } catch (Exception e){
      System.out.println("Failed to open KB: " + e + ": giving up");
      System.exit(0);
    }

    help();
    while(true){
// Read commands from KB and execute
      try{
        buffer=from_kb.readLine(); // may be file
      } catch(Exception e) { // EOF
        buffer=null; // EOF flag
      }
      if (buffer==null){ // which will also happen on EOF from @file
                         // but won't throw exception
        if (fpstack.empty()){
          System.out.println("EOF on read from keyboard - giving up");
          System.exit(0);
        }
// pop stack
        from_kb=(BufferedReader)(fpstack.pop());
        buffer="; EOF";
      }
      try{
        if (!(buffer+" ").substring(0,1).equals(":")){ // bare cmd
          send=true; // just send buffer
        } else { // parse : command
          send=false; // If true, send to current client
          StringTokenizer st=new StringTokenizer(buffer," ",false);
          num_cmd=0;

          while ((num_cmd<10) && (st.hasMoreTokens())){
            cmd[num_cmd++]=st.nextToken();
          } // ignore everything beyond 10 tokens!

// crude parsing...
// first, see if this has the form :### (more stuff)
          if ((cmd[0].length() > 1) && (num_cmd > 1)){
            try{
              client=Integer.parseInt(cmd[0].substring(1));
              System.out.println("CURRENT CLIENT=" + client);
              buffer=cmd[1];
              send=true;
              for (i=2;i<num_cmd;i++){
                buffer=buffer+" "+cmd[i];
              }
            } catch(Exception e){} // wasn't a number
// or is just a bare command...
          }

          if (!send){ // has the form :...
            if (num_cmd==1){ // :Q or :### or :@filename
              if (cmd[0].equals(":Q")){
                System.exit(0);
              } else if (cmd[0].length() > 2){
                if ((cmd[0].substring(0,2)).equals(":@")){
                  buffer=cmd[0].substring(2); // filename
                  fpstack.push(from_kb); /* Save current buffered reader */
                  try{
                    FileReader fire=new FileReader(buffer);
                    from_kb=new BufferedReader(fire);
                  } catch (Exception e){
                    System.out.println("Cannot open <" + buffer + ">:" + e);
                    from_kb=(BufferedReader)fpstack.pop();
                  }
                } // end of :@
              } else { // end of length>2
// assume :###
                client=Integer.parseInt(cmd[0].substring(1));
                System.out.println("CURRENT CLIENT=" + client);
              }
            } else if (num_cmd==2){ // :x ###
              if (cmd[0].equals(":x")){ // close a conn
                client=Integer.parseInt(cmd[1]);
                System.out.println("CURRENT CLIENT=" + client);
                if (from_sock[client] != null){
                  from_sock[client].close();
                  from_sock[client]=null;
                  to_sock[client].close();
                  to_sock[client]=null;
                } else {
                  System.out.println("*** NO CONNECTION TO CLIENT " + client);
                  help();
                }
              }
            } else if (num_cmd==3){
              if (cmd[0].equals(":c")){ // :c ### hostname
                client=Integer.parseInt(cmd[1]);
                System.out.println("CURRENT CLIENT=" + client);
                hostname=cmd[2];
                InetAddress ias[]=InetAddress.getAllByName(hostname);
                System.out.println(hostname + " addr=" + ias[0].getHostAddress());
                Socket sock=new Socket(ias[0],port);
// create a pair of streams
                OutputStreamWriter osw1=new OutputStreamWriter(sock.getOutputStream());
                to_sock[client]=new BufferedWriter(osw1);
                from_sock[client]=sock.getInputStream();

// start thread to listen to socket
                listener l=new listener(from_sock[client],client);
                l.start();
              } // end of :c ### hostname
            } // end of 3-part :command
          } // end of !send
        } // end of all parsing
  
        if (send){
          if (to_sock[client]==null){
            System.out.println("*** NO CONNECTION TO CLIENT " + client);
            help();
          } else {
System.out.print("[" + client + "] ");
            to_sock[client].write(buffer,0,buffer.length());
            to_sock[client].newLine();to_sock[client].flush();
          }
        }
     } catch (Exception e) {
       help();
     }
   } // loop forever
 }

  static void help()
  {
    System.out.println("Legal commands are:");
    System.out.println(":c ### hostname - connect to a client");
    System.out.println(":x ###          - close connection");
    System.out.println(":### [cmd]      - set default client [and send cmd]");
    System.out.println(":Q              - exit this program");
    System.out.println("cmd             - send cmd to default client");
    System.out.println("");
  }
}

// listener for a single connection
class listener extends Thread
{
  InputStream from_sock;
  int client;

  listener(InputStream is,int c)
  {
    from_sock=is;
    client=c; // My id #
  }

  public void run()
  {
    byte b[]=new byte[1];
    boolean show_id=true;

    try{
      while (-1 != from_sock.read(b,0,1)){
        if (show_id){System.out.print("["+client+"] ");}
        show_id=(b[0]=='\n');
        System.out.print(new String(b));
      }
    } catch (Exception e) {
      System.out.println("Exception " + e);
      return;
    }
    System.out.println("\nSocket has closed");
    return;
  }
}
