import java.io.*;
import java.net.*;
import java.lang.*;
import java.util.*;
import java.awt.event.*;

/* Class for monitoring output changes */
class watch_class{
  static boolean firstreg=true; /* So we can initialize */
  static DataOutputStream ports[]=new DataOutputStream[32]; // Hold sockets

  static int saveind[]=new int[32]; // Temporary for holding indicies
  static int saveside,savevalue;
  static char savetype;
  static char savesidename;

  static boolean changed=false; // set after change
  static Socket sock[]=new Socket[32];
  static OutputStream os[]=new OutputStream[32];

  static cell_exchange ce;

  public watch_class(cell_exchange cein)
  {
    ce=cein;
  }

  public static void changed(int ind[], char type, int side, int value)
  {
    saveind=ind;savetype=type;saveside=side;savevalue=value;
    savesidename=ce.labels.charAt(saveside);
    changed=true;
  }
  public boolean status(){boolean back=changed;changed=false;return(back);}
}
