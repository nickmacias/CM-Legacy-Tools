import java.io.*;
import java.util.*;

abstract class seq_load_class
{
  public static void seq_load(window_class mw, cell_matrix_sim sim,
                              String filename,int update_rate)
  {
    String buffer;
    int num_channels;
    int inds[][]=new int[32][4]; /* 32 channels, 4 dims */
    char type[]=new char[32]; /* C or D */
    int side[]=new int[32]; /* Side mask (from labels) */
    String tts[]=new String[32]; /* Holds truth tables for each channel */
    int sides,dims;
    String labels;
    int i,j,mask,counter;

    try {
      FileReader fr=new FileReader(filename);
      BufferedReader br=new BufferedReader(fr);
      buffer=br.readLine(); /* Header */
      if (!buffer.equals("\"#SEQUENCE FILE\"")){
        System.out.println(filename + " does not appear to be a valid sequence file.");
        fr.close();
        return;
      }

      buffer=br.readLine(); /* User comment */

      buffer=br.readLine(); /* Flags */
      if (!buffer.equals("\"V3TT\"")){
        System.out.println(filename + " is not a Version 3 sequence file. Please recompile " + filename);
        fr.close();
        return;
      }
      dims=Integer.parseInt(seq_gets(br)); /* # of dims */
      labels=seq_gets(br); /* Ordered labels for sides of cell */
      sides=labels.length(); /* # of sides */

      buffer=seq_gets(br); /* # of channels */
      num_channels=Integer.parseInt(buffer);

/* Load channel specifications */
      for (i=0;i<num_channels;i++){
        buffer=seq_gets(br); /* ind,ind,...,ind,TYPE+SIDE */
        StringTokenizer st=new StringTokenizer(buffer,",");
        for (j=0;j<dims;j++){
          inds[i][j]=Integer.parseInt(st.nextToken());
        } /* Indicies loaded for this channel */
        String s=st.nextToken(); /* TYPE+SIDE */
        type[i]=s.charAt(0); /* C or D */
        side[i]=labels.indexOf(s.charAt(1)); /* Side */
      }

/* Now execute the sequence */
      counter=0; /* For updating display */
      while ((tts[0]=seq_gets(br)) != null){
        for (i=1;i<num_channels;i++){
          tts[i]=seq_gets(br);
        }
/* tts[] is loaded now. Send the bits to the engine and run the clock */
        mask=1; /* Shows which bit to read from nibble */
        for (i=0;i<tts[0].length();i++){ /* For each nibble */
          for (mask=3;mask>=0;mask--){ /* For each bit inside nibble */
            for (j=0;j<num_channels;j++){ /* For each channel */
              sim.user_set_input(inds[j],type[j],side[j],
  (((Integer.parseInt(tts[j].substring(i,i+1),16) & (1<<mask))==0)?0:1));

            } /* All channels set */
            sim.process_queue();
            sim.tick();sim.process_queue();
            if (++counter >= update_rate){
              counter=0;
              mw.unload_changes();
            }
          } /* End of current nibble */
        } /* End of TT */
      } /* End of file! */
      fr.close();
      return;
    } catch (Exception e){
      System.out.println("Error opening " + filename + ":" + e);
      return;
    }
  }

  static String seq_gets(BufferedReader br)
  {
    String buffer,s;
    try{
      while ((buffer=br.readLine()) != null){
        if (buffer.charAt(1) != '#'){ /* Return it */
          return(buffer.substring(1,buffer.length()-1));
        }
      }
      return(null);
    } catch(Exception e){
      System.out.println("Unexpected file read error: " + e);
      return(null);
    }
  }
}
