class user_callback
{
  int lastkey; // stores last keypress
  boolean newkey;  // set when a key is pressed; clear once we read it

  user_callback()
  {
    newkey=false;
  }

  boolean check()
  {
    return(newkey);
  }

  int get_key()
  {
    newkey=false;
    return(lastkey);
  }

  void set_key(int key)
  {
    lastkey=key;
    newkey=true;
  }
}
