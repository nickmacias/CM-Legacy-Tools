/*
 * Copyright (C) 1992-2007 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;

// gas pedal, for controling single-event execution/update
class gas_pedal extends Frame
  implements AdjustmentListener
{
  Scrollbar vscroll,hscroll;
  int speed=0; // 1000=max
  boolean running=false;

  cell_matrix_sim sim;
  Graphics saveg=null;

  gas_pedal(cell_matrix_sim s)
  {
/* Grab info from simulator/etc. class */
    sim=s;

    setSize(20,100);

    setLayout(new BorderLayout());
    vscroll=new Scrollbar(Scrollbar.VERTICAL,0,5,0,0);
    vscroll.addAdjustmentListener(this);
    add(vscroll,"East");
    vscroll.setBlockIncrement(16);
    vscroll.setMaximum(1000); // inter-event delay

/* Event handlers */
    addMouseListener(new MyMouseAdapter());
    //addMouseMotionListener(new MyMouseMotionAdapter());
    //addComponentListener(new MyComponentAdapter());
    addWindowListener(new MyWindowAdapter());
  }

  public void adjustmentValueChanged(AdjustmentEvent ae)
  {
    Scrollbar sb=(Scrollbar) ae.getAdjustable();
    speed=vscroll.getValue();
//    repaint();
  }


  class MyWindowAdapter extends WindowAdapter
  {
    public void windowClosing(WindowEvent we)
    {
      //System.exit(0);
    }
  }

  boolean mousedown=false;

  class MyMouseAdapter extends MouseAdapter
  {
    public void mouseReleased(MouseEvent me)
    {
      mousedown=false;
    }

    public void mousePressed(MouseEvent me)
    {
      if (mousedown) return;
      mousedown=true;

      running=!running;
    }

  }

}
