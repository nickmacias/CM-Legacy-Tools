import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;
import Acme.JPM.Encoders.*;


/* Create a frame class for main window */
class main_window extends Frame
  implements ActionListener, ItemListener,
             AdjustmentListener {

  boolean DOS=false;
  Color BG=Color.gray;
  Color GRID=Color.yellow;
  Color SEL=Color.blue;

  int limitsize=50;
  boolean limit=false; /* Limit grid size to 50x50 */
  boolean doing_select=false;
  boolean grid=true;
  boolean newzoom=false;   // Set when we've just changed zoom factor
                           // paint() will catch this and erase before redraw

  int ISIZE=32;

  String rots="0123"; /* For converting string to int */

  int XSTART=10,YSTART=80; /* UL corner of drawing region */
// %%% NO GRID=0,GRID=1
  int SEP=1; /* Separation between cells, =2*SEP+1 */
  int ROWS,COLS; /* Size of grid currently displayed in this window */
  int NEWR=8,NEWC=8; /* # of rows and cols in NEW grid */
  griddataclass grid_data[][];
  griddataclass undo[][][]; /* 5 levels of undo */
  int r1,c1,r2,c2; /* Current select region;< 0 if not valid */
  int ULrow,ULcol,LRrow,LRcol; /* What's displayed */
  Scrollbar vscroll,hscroll;
  String last_cell_lib=""; /* Remember last library select */
  String thisgridname=""; /* Name of grid being edited */
  String basetitle=""; /* Add [r,c] to this */
  String last_binary_filename="";
  String last_image_filename="*.gif";
  String last_grid_filename="";
  String last_saveas_filename="";
  String last_open_filename="";

  loader parent; /* This will hold the parent object */

/* class constructor */
  main_window(loader par, String input_name)
  {
/* Normal constructor */
    setFont(new Font("Serif",Font.PLAIN,14));
    setBackground(Color.gray);
    setForeground(Color.black);

    if (!grid)SEP=0;
    r1=c1=r2=c2=-1; /* No region selected */
    ULrow=0;ULcol=0;
    COLS=0;ROWS=0;
    parent=par;	/* Remember parent object */

    parent.register(this);
    setLayout(new BorderLayout());
    basetitle="Cell Matrix Layout Editor";
    setTitle(basetitle);
    add_scrollbars();
    add_menus(); /* Do all the menubar stuff here */
    add_mouse_handler();

/* Extra steps for loading grid */
    readgrid(this,input_name);
    hscroll.setValue(0);vscroll.setValue(0);ULrow=ULcol=0;r1=c1=r2=c2=(-1);
    hscroll.setBackground(Color.blue);
    vscroll.setBackground(Color.blue);
//    setBackground(Color.red);
    thisgridname=input_name;
    basetitle=tail(input_name);
    setTitle(basetitle);
    last_grid_filename=input_name;
    last_saveas_filename=input_name;
    last_open_filename=input_name;
    repaint();
  }

  main_window(loader par)
  {
    setFont(new Font("Serif",Font.PLAIN,14));
    setBackground(Color.gray);
    setForeground(Color.black);

    if (!grid)SEP=0;
    r1=c1=r2=c2=(-1); /* No region selected yet */
    ULrow=0;ULcol=0;
    COLS=0;ROWS=0;
    parent=par;	/* Remember parent object */

    parent.register(this);
    setLayout(new BorderLayout());
    basetitle="Cell Matrix Layout Editor";
    setTitle(basetitle);
    add_scrollbars();
    add_menus(); /* Do all the menubar stuff here */
    add_mouse_handler();
  } /* That's the constructor */

  main_window(loader par,int R,int C) /* Constructor for NEW in new window */
  {
    setFont(new Font("Serif",Font.PLAIN,14));
    setBackground(Color.gray);
    setForeground(Color.black);

    if (!grid)SEP=0;
    r1=c1=r2=c2=(-1); /* No region selected yet */
    ULrow=0;ULcol=0;
    if (limit){if (R>=limitsize) R=limitsize;if (C>=limitsize) C=limitsize;}
    NEWR=R;NEWC=C; /* Remember these */
    COLS=C;ROWS=R;
    parent=par;	/* Remember parent object */

    parent.register(this);
    setLayout(new BorderLayout());
    basetitle="Cell Matrix Layout Editor";
    setTitle(basetitle);
    add_scrollbars();
    add_menus(); /* Do all the menubar stuff here */
    add_mouse_handler();

    newgrid(this);
    hscroll.setValue(0);vscroll.setValue(0);ULrow=ULcol=0;r1=c1=r2=c2=(-1);
    hscroll.setBackground(Color.blue);
    vscroll.setBackground(Color.blue);
    repaint();
  } /* That's the constructor */


/* Constructor for an exploded composite cell */
  main_window(loader par,String libraries, String cells)
  {
    int r,c;

    setFont(new Font("Serif",Font.PLAIN,14));
    setBackground(Color.gray);
    setForeground(Color.black);

    if (!grid)SEP=0;
    r1=c1=r2=c2=(-1); /* No region selected yet */
    ULrow=0;ULcol=0;
    NEWR=3;NEWC=4; /* Remember these */
    COLS=3;ROWS=4;
    parent=par;	/* Remember parent object */

    parent.register(this);
    setLayout(new BorderLayout());
    basetitle="Composite Cell Contents";
    setTitle(basetitle);
    add_scrollbars();
    add_menus(); /* Do all the menubar stuff here */
    add_mouse_handler();

    newgrid(this);

/* Explode the composite cell */
    Vector v=get_cell_vector(libraries,cells);

/* Now populate the grid data */
    r=c=0;
    Enumeration e=v.elements();
    while (e.hasMoreElements()){
      singlecellclass singlecell=(singlecellclass) e.nextElement();
      grid_data[r][c]=new griddataclass();
      grid_data[r][c].library=singlecell.library;
      grid_data[r][c].cell=singlecell.cell+"\002"+singlecell.rot;
      if (++c >= 4){c=0;if (++r>= 3) r=0;}
    }

    hscroll.setValue(0);vscroll.setValue(0);ULrow=ULcol=0;r1=c1=r2=c2=(-1);
    hscroll.setBackground(Color.blue);
    vscroll.setBackground(Color.blue);
    repaint();

  } /* That's the constructor */

/* Override update() method to avoid clearing background */
  public void update(Graphics g)
  {
    paint(g);
  }

/* Add scrollbars to the main window */
  void add_scrollbars()
  {
    vscroll=new Scrollbar(Scrollbar.VERTICAL,0,5,0,0);
    vscroll.addAdjustmentListener(this);
    add(vscroll,"East");

    hscroll=new Scrollbar(Scrollbar.HORIZONTAL,0,5,0,0);
    hscroll.addAdjustmentListener(this);
    add(hscroll,"South");
  }

  public void adjustmentValueChanged(AdjustmentEvent ae)
  {
    Scrollbar sb=(Scrollbar) ae.getAdjustable();
    do_scroll();
  }
  void do_scroll()
  {
    ULrow=vscroll.getValue();
    ULcol=hscroll.getValue();
    repaint();
    requestFocus();  // So keystrokes still go to the main window
  }

  void add_mouse_handler()
  {
// add keyboard listener
    addKeyListener(new MyKeyAdapter());

    addMouseListener(new mymouseadapter(this)); /* pass loader obj */
    addMouseMotionListener(new mymousemotionadapter(this)); /* pass loader obj */
  }

  class MyKeyAdapter extends KeyAdapter
  {
    public void keyPressed(KeyEvent e)
    {
      switch(e.getKeyCode()){
        case KeyEvent.VK_LEFT:hscroll.setValue(hscroll.getValue()-1);do_scroll();break;
        case KeyEvent.VK_RIGHT:hscroll.setValue(hscroll.getValue()+1);do_scroll();break;
        case KeyEvent.VK_UP:vscroll.setValue(vscroll.getValue()-1);do_scroll();break;
        case KeyEvent.VK_DOWN:vscroll.setValue(vscroll.getValue()+1);do_scroll();break;
      }
    }
  }

/* Call start_select when you get mouse-down on a grid */
  void start_select(Graphics g,int r,int c)
  {
    draw_select(g,BG,2); /* Erase current rectangle if drawn */
    draw_select(g,GRID,1); /* redraw grid over current area */
    r1=r;c1=c;r2=r;c2=c;
    draw_select(g,SEL,2); /* Draw new rectangle */
    doing_select=true;
  }

/* Call end_select on mouse-up or mouse-drag */
  void end_select(Graphics g,int r,int c)
  {
    if (!doing_select) return;
    if ((r==r2) && (c==c2)) return; /* No change */
    draw_select(g,BG,2); /* else erase current rectangle */
    draw_select(g,GRID,1);
    r2=r;c2=c; /* set new region */

    draw_select(g,SEL,2); /* and draw new rectangle */
    return;
  }

  public void paint(Graphics g)
  {
    Rectangle R=getBounds();

// Set scrolling params, so resizing window recomputes these
    hscroll.setBlockIncrement(((R.width-XSTART-20)/(ISIZE+1+2*SEP)-1)/2);
    vscroll.setBlockIncrement(((R.height-YSTART-20)/(ISIZE+1+2*SEP)-1)/2);
    if (newzoom){ // Need to erase window first
      Color savebg=g.getColor();
      g.setColor(BG);
      g.fillRect(0,0,R.width,R.height);
      g.setColor(savebg);
      newzoom=false;
    }

    setBackground(BG);

/* Recompute LR row and col */
/* Check the bounds of the window */

    LRrow=ULrow+(R.height-YSTART-20)/(ISIZE+1+2*SEP)-1;
    LRcol=ULcol+(R.width-XSTART-20)/(ISIZE+1+2*SEP)-1;

    if (LRrow >= ROWS){LRrow=ROWS-1;}
    if (LRcol >= COLS){LRcol=COLS-1;}
  
    drawemptygrid(g);
    drawgrid(g);
    draw_select(g,SEL,2); /* Show current selection */
  }

/* drawemptygrid draws the blank grid with a dark grey color */
  void drawemptygrid(Graphics g)
  {
    int R1,C1,R2,C2;
    R1=r1;R2=r2;C1=c1;C2=c2; /* Save these */
    r1=ULrow;c1=ULcol;r2=LRrow;c2=LRcol;
    draw_select(g,BG,2); /* Draw this grid */
    draw_select(g,GRID,1);
    r1=R1;r2=R2;c1=C1;c2=C2;
  }

/* draw_select draws a rectangle around the currently selected region */
  void draw_select(Graphics g,Color color,int width)
  {
    int R1,R2,C1,C2,r,c;

    if (r1 < 0) return; /* No region drawn */
    g.setColor(color);

    C1=c1;C2=c2;R1=r1;R2=r2;
    if (r1 > r2){R1=r2;R2=r1;}
    if (c1 > c2){C1=c2;C2=c1;} /* Order these */

/* Now adjust R/C 1/2 according to UL/LR vars */
    if (C1 < ULcol) C1=ULcol;
    if (C2 > LRcol) C2=LRcol;
    if (R1 < ULrow) R1=ULrow;
    if (R2 > LRrow) R2=LRrow;

/* Now draw grid to show selected region */
    for (c=C1;c<=C2+1;c++){
      g.fillRect((c-ULcol)*(ISIZE+1+2*SEP)+XSTART-width,
                 (R1-ULrow)*(ISIZE+1+2*SEP)+YSTART-width,
                 width,
                 (1+R2-R1)*(ISIZE+1+2*SEP));
    }

    for (r=R1;r<=R2+1;r++){
      g.fillRect((C1-ULcol)*(ISIZE+1+2*SEP)+XSTART-width,
                 (r-ULrow)*(ISIZE+1+2*SEP)+YSTART-width,
                 (1+C2-C1)*(ISIZE+1+2*SEP)+2,
                 width);
    }
    return;
  }

/* Inner class for mouse adapter definition */
  class mymouseadapter extends MouseAdapter
  {
    main_window parent; /* Remembers parent (main_window) object */

    public mymouseadapter(main_window par) /* constructor gets main_window obj */
    {
      parent=par;
    }

    public void mouseReleased(MouseEvent me)
    {
      doing_select=false;
    }

    public void mousePressed(MouseEvent me)
    {
      int r,c; /* Will hold row/col of current click */

      c=ULcol+(me.getX()-XSTART)/(ISIZE+1+2*SEP);
      r=ULrow+(me.getY()-YSTART)/(ISIZE+1+2*SEP);
      if (r >= ROWS) r=ROWS-1;
      if (c >= COLS) c=COLS-1;

      parent.start_select(parent.getGraphics(),r,c);
      parent.end_select(parent.getGraphics(),r,c);
    }
  }

  class mymousemotionadapter extends MouseMotionAdapter
  {
    main_window parent; /* Remembers parent (main_window) object */
    int pastedge=0;  // Set when we're past the edge

    public void mouseMoved(MouseEvent me)
    {
      int r,c; /* Will hold row/col of current click */
      c=ULcol+(me.getX()-XSTART)/(ISIZE+1+2*SEP);
      r=ULrow+(me.getY()-YSTART)/(ISIZE+1+2*SEP);
      setTitle(basetitle+":["+r+","+c+"]");
    }

    public mymousemotionadapter(main_window par) /* constructor gets main_window obj */
    {
      parent=par;
    }

    public void mouseDragged(MouseEvent me)
    {
      int r,c; /* Will hold row/col of current click */
      int i;
      int pe;  // Temp flag
      c=ULcol+(me.getX()-XSTART)/(ISIZE+1+2*SEP);
      r=ULrow+(me.getY()-YSTART)/(ISIZE+1+2*SEP);

/* Handle movements past edge of window */

      pe=0;
      if (c > LRcol){
       pe=1;
       for(i=0;i<pastedge;i++){
        if ((LRcol < COLS-1) && (LRcol > 0)){
          ++LRcol;++ULcol;hscroll.setValue(ULcol);
        }
        c=LRcol;
       }
       repaint();
      }

      if (me.getX() < XSTART){
       pe=1;
       for(i=0;i<pastedge;i++){
        if (ULcol > 0){
          --ULcol;--LRcol;hscroll.setValue(ULcol);
        }
        c=ULcol;
       }
       repaint();
      }

      if (r > LRrow){
       pe=1;
       for(i=0;i<pastedge;i++){
        if ((LRrow < ROWS-1) && (LRrow > 0)){
          ++LRrow;++ULrow;vscroll.setValue(ULrow);
        }
        r=LRrow;
       }
       repaint();
      }

      if (me.getY() < YSTART){
       pe=1;
       for(i=0;i<pastedge;i++){
        if (ULrow > 0){
          --ULrow;--LRrow;vscroll.setValue(ULrow);
        }
        r=ULrow;
       }
       repaint();
      }

     if (pe==1) pastedge+=2; // Increase the scrolling speed
     if (pastedge > 31) pastedge=31;
     if (pe==0) pastedge-=2;
     if (pastedge < 1) pastedge=1;

     setTitle(basetitle+":["+r+","+c+"]");
/* Now update selection grid based on mouse position */

      parent.end_select(parent.getGraphics(),r,c);
    }
  }

/* Menu bar code */
  void add_menus()
  {
    addWindowListener(new window_event_handler(this));

    MenuBar mb=new MenuBar();
    setMenuBar(mb);
    mb.setFont(new Font("Serif",Font.PLAIN,14));

    Menu mfile=new Menu("File");
    mb.add(mfile);	/* Add menu to menubar */

    MenuItem main_new=new MenuItem("New Grid",new MenuShortcut(KeyEvent.VK_N));
    main_new.addActionListener(this);
    mfile.add(main_new);

    MenuItem main_open=new MenuItem("Open Grid",new MenuShortcut(KeyEvent.VK_O));
    main_open.addActionListener(this);
    mfile.add(main_open);

    MenuItem main_save=new MenuItem("Save Grid",new MenuShortcut(KeyEvent.VK_S));
    main_save.addActionListener(this);
    mfile.add(main_save);

    MenuItem main_saveas=new MenuItem("Save Grid As");
    main_saveas.addActionListener(this);
    mfile.add(main_saveas);

    MenuItem main_hcopy=new MenuItem("Hardcopy Grid");
    main_hcopy.addActionListener(this);
    mfile.add(main_hcopy);

    MenuItem main_print=new MenuItem("Print Current Window");
    main_print.addActionListener(this);
    mfile.add(main_print);

    MenuItem main_image=new MenuItem("Image to File in Pieces");
    main_image.addActionListener(this);
    mfile.add(main_image);

    MenuItem main_image_NG=new MenuItem("Toggle Grid");
    main_image_NG.addActionListener(this);
    mfile.add(main_image_NG);

    MenuItem main_bin=new MenuItem("Write Binary Grid",new MenuShortcut(KeyEvent.VK_W));
    main_bin.addActionListener(this);
    mfile.add(main_bin);

    MenuItem main_zin=new MenuItem("Zoom In");
    main_zin.addActionListener(this);
    mfile.add(main_zin);

    MenuItem main_zout=new MenuItem("Zoom Out");
    main_zout.addActionListener(this);
    mfile.add(main_zout);

    MenuItem main_redraw=new MenuItem("Redraw",new MenuShortcut(KeyEvent.VK_L));
    main_redraw.addActionListener(this);
    mfile.add(main_redraw);

    MenuItem main_cl=new MenuItem("Close");
    main_cl.addActionListener(this);
    mfile.add(main_cl);

    MenuItem main_close=new MenuItem("Exit");
    main_close.addActionListener(this);
    mfile.add(main_close);

    Menu medit=new Menu("Edit");
    mb.add(medit);	/* Add menu to menubar */

    MenuItem edit_copy=new MenuItem("Copy",new MenuShortcut(KeyEvent.VK_C));
    edit_copy.addActionListener(this);
    medit.add(edit_copy);

    MenuItem sel_all=new MenuItem("Select All",new MenuShortcut(KeyEvent.VK_A));
    sel_all.addActionListener(this);
    medit.add(sel_all);

    MenuItem edit_cut=new MenuItem("Cut",new MenuShortcut(KeyEvent.VK_X));
    edit_cut.addActionListener(this);
    medit.add(edit_cut);

    MenuItem edit_paste=new MenuItem("Paste",new MenuShortcut(KeyEvent.VK_V));
    edit_paste.addActionListener(this);
    medit.add(edit_paste);

    MenuItem edit_orpaste=new MenuItem("Or-Paste",new MenuShortcut(KeyEvent.VK_Z));
    edit_orpaste.addActionListener(this);
    medit.add(edit_orpaste);

    MenuItem rot_paste=new MenuItem("Rotate",new MenuShortcut(KeyEvent.VK_R));
    rot_paste.addActionListener(this);
    medit.add(rot_paste);

    MenuItem rrot_paste=new MenuItem("Region Rotate",new MenuShortcut(KeyEvent.VK_G));
    rrot_paste.addActionListener(this);
    medit.add(rrot_paste);

    MenuItem explode=new MenuItem("Explode Composite Cell",new MenuShortcut(KeyEvent.VK_P));
    explode.addActionListener(this);
    medit.add(explode);

    MenuItem edit_undo=new MenuItem("Undo",new MenuShortcut(KeyEvent.VK_U));
    edit_undo.addActionListener(this);
    medit.add(edit_undo);

    MenuItem flush_cache=new MenuItem("Flush Cache",new MenuShortcut(KeyEvent.VK_F));
    flush_cache.addActionListener(this);
    medit.add(flush_cache);

    Menu mcell=new Menu("Cell");
    mb.add(mcell);

    MenuItem cell_edit=new MenuItem("Edit",new MenuShortcut(KeyEvent.VK_E));
    cell_edit.addActionListener(this);
    mcell.add(cell_edit);

    MenuItem cell_load=new MenuItem("Load",new MenuShortcut(KeyEvent.VK_K));
    cell_load.addActionListener(this);
    mcell.add(cell_load);
  }

/* Menu action handlers */
  public void actionPerformed(ActionEvent ae){
    String input_name,l,c;

    //parent.msg("Command="+ae.getActionCommand());

    if (ae.getActionCommand() == "Exit") {dispose();System.exit(0);}
    if (ae.getActionCommand() == "Close") {
      thisgridname="";
      parent.deregister(this);
      --parent.numWindows;
      setVisible(false);
      if (0==parent.numWindows) System.exit(0);
    }

    if (ae.getActionCommand() == "New Grid"){ /* See if this grid is in use */
      if (COLS == 0){ /* Our grid is empty, so open in this window */
        newgrid(this);
        hscroll.setValue(0);vscroll.setValue(0);ULrow=ULcol=0;r1=c1=r2=c2=(-1);
        hscroll.setBackground(Color.blue);
        vscroll.setBackground(Color.blue);
        repaint();
      } else { /* Open a new main_window */
/* See how big a grid to open */
        gridsizeclass gs=new gridsizeclass(this,NEWR,NEWC);
        main_window mw=new main_window(parent,gs.getNEWR(),gs.getNEWC()); /* Alternate constructor */
/* Pass my parent to constructor, so we both share same parent */
        mw.setSize(400,400);
        mw.setLocation(getLocation().x+100,getLocation().y+100);
        mw.show();
        ++parent.numWindows;
      }
    }


    if (ae.getActionCommand() == "Open Grid"){
      get_name_class get_name = new get_name_class(last_open_filename); /* Do it! */
      input_name=get_name.get_name();
      if (input_name.equals("")) return;
      last_open_filename=input_name;
      if (COLS == 0){ /* Our grid is empty, so open in this window */
        readgrid(this,input_name);
        thisgridname=input_name;
        basetitle=tail(input_name);
        setTitle(basetitle);
        hscroll.setValue(0);vscroll.setValue(0);ULrow=ULcol=0;r1=c1=r2=c2=(-1);
        hscroll.setBackground(Color.blue);
        vscroll.setBackground(Color.blue);
        repaint();
      } else { /* Open a new main_window */
        main_window mw=new main_window(parent,input_name); /* Alternate constructor */
/* Pass my parent to constructor, so we both share same parent */
        mw.setSize(400,400);
        mw.setLocation(getLocation().x+100,getLocation().y+100);
        mw.show();
        ++parent.numWindows;
      }
    }

    if (ae.getActionCommand() == "Old Print"){
      Color oldbg=BG;BG=Color.white;
      Properties prefs=new Properties(); /* User's printer preferences */
      Toolkit tk=getToolkit(); /* Get toolkit object */
      PrintJob pj=tk.getPrintJob(this,"test",prefs); /* Open a print job */
      Graphics g=pj.getGraphics();

/* Recompute LR row and col */
      LRrow=ULrow+(getSize().height-YSTART-20)/(ISIZE+1+2*SEP)-1;
      LRcol=ULcol+(getSize().width-XSTART-20)/(ISIZE+1+2*SEP)-1;

      if (LRrow >= ROWS){LRrow=ROWS-1;}
      if (LRcol >= COLS){LRcol=COLS-1;}

// ULrow=0;ULcol=0;LRrow=ROWS-1;LRcol=COLS-1;
      drawemptygrid(g);
      drawgrid(g);
      g.dispose(); /* Send page to printer */
      pj.end();   /* End of print job */
      BG=oldbg;
    }

    if (ae.getActionCommand() == "Toggle Grid"){
      grid=!grid;
      SEP=(grid)?1:0;
      parent.msg("Grid is "+((grid)?"on":"off"));
      if (grid){GRID=Color.gray;} else {GRID=Color.black;}
      repaint();
    }

    if (ae.getActionCommand() == "Image to File in Pieces"){
      int i3; // For output file index
      int XCHUNK=40;
      int YCHUNK=52;
// Get output file name
      get_write_name_class get_write_name=new get_write_name_class(last_image_filename);
      String fname=get_write_name.get_name();
      if (fname.equals("")) return;
      last_image_filename=fname;
/* Make an output stream */
      //Image i=createImage(getSize().width,getSize().height);
      Image i=createImage(XCHUNK*(ISIZE+1+2*SEP)+XSTART,YCHUNK*(ISIZE+1+2*SEP)+YSTART);
// We want to image ALL rows
      int saveULrow,saveULcol,saveLRrow,saveLRcol;
      saveULrow=ULrow;saveULcol=ULcol;saveLRrow=LRrow;saveLRcol=LRcol;

      Color bg;
      bg=BG;
      BG=Color.white;
      i3=100;

// Traverse the entire grid in XCHUNK x YCHUNK pieces
      for (ULrow=0;ULrow<ROWS-1;ULrow+=YCHUNK){
        LRrow=ULrow+(YCHUNK-1);if (LRrow > ROWS-1) LRrow=ROWS-1;
        for (ULcol=0;ULcol<COLS-1;ULcol+=XCHUNK){
          LRcol=ULcol+(XCHUNK-1);if (LRcol > COLS-1) LRcol=COLS-1;
          Graphics memg=i.getGraphics();
          Color savec=memg.getColor();
          memg.setColor(Color.white);
          memg.fillRect(0,0,XSTART+XCHUNK*(ISIZE+1+2*SEP),YSTART+YCHUNK*(ISIZE+1+2*SEP));
          memg.setColor(savec);
//%%% NO GRID
          if (grid) drawemptygrid(memg);
          drawgrid(memg);
          try{
            FileOutputStream fos=new FileOutputStream(fname+i3+".gif");
            parent.msg("Writing " + fname + i3 + ".gif...\n");
            System.out.print("Writing " + fname + i3 + ".gif...");
            ++i3;
            GifEncoder ge=new GifEncoder(i,(OutputStream) fos);
            ge.encode();
            fos.close();
            System.out.println("Done");
            parent.msg("Done\n");
          } catch (IOException e){System.out.println("File error: " + e);};
        } // End of COLS
      } // End of ROWS
      ULrow=saveULrow;ULcol=saveULcol;LRrow=saveLRrow;LRcol=saveLRcol;
      BG=bg;
    }

    if (ae.getActionCommand() == "Print Current Window"){ // Image the current window
// Get output file name
      get_write_name_class get_write_name=new get_write_name_class(last_image_filename);
      String fname=get_write_name.get_name();
      if (fname.equals("")) return;
      last_image_filename=fname;
/* Make an output stream */
      Image i=createImage(getSize().width,getSize().height);
// We want to image ALL rows
      int saveULrow,saveULcol,saveLRrow,saveLRcol;
      saveULrow=ULrow;saveULcol=ULcol;saveLRrow=LRrow;saveLRcol=LRcol;

      Color bg;
      bg=BG;
      BG=Color.white;

/* Recompute LR row and col */
      LRrow=ULrow+(getSize().height-YSTART-20)/(ISIZE+1+2*SEP)-1;
      LRcol=ULcol+(getSize().width-XSTART-20)/(ISIZE+1+2*SEP)-1;

      if (LRrow >= ROWS){LRrow=ROWS-1;}
      if (LRcol >= COLS){LRcol=COLS-1;}

      Graphics memg=i.getGraphics();
      Color savec=memg.getColor();
      memg.setColor(Color.white);
      memg.fillRect(0,0,getSize().width,getSize().height);
      memg.setColor(savec);
//%%% NO GRID
      if (grid) drawemptygrid(memg);
      drawgrid(memg);
      try{
        FileOutputStream fos=new FileOutputStream(fname);
        parent.msg("Writing " + fname +"...\n");
        GifEncoder ge=new GifEncoder(i,(OutputStream) fos);
        ge.encode();
        fos.close();
        parent.msg("Done\n");
      } catch (IOException e){System.out.println("File error: " + e);};
      ULrow=saveULrow;ULcol=saveULcol;LRrow=saveLRrow;LRcol=saveLRcol;
      BG=bg;
    }

    if (ae.getActionCommand() == "Hardcopy Grid"){
/* Write c:\temp\gridimg.xfr and store iconbytes[] array */
      try{
        int r,C,ii,jj;

// Store each row of the image into the "out" array
// Then traverse this row and output it to the PPM

        int out[][];out=new int[1024][1024];

        FileWriter ibyte=new FileWriter("c:\\temp\\gridimg.ppm");
        BufferedWriter bb=new BufferedWriter(ibyte);
        bb.write("P3\n" + COLS*32 + " " + ROWS*32 + "\n1\n");

        for (r=0;r<ROWS;r++){
          for (C=0;C<COLS;C++){
/* Get the icon for this cell */

            int i,j,maxind,realcell;
            String iconbytes,key;
            int[] iarray,oarray,color_counts;
            color_counts=new int[10]; /* Record how many of each color we have in icon */
            iarray=new int[1024]; /* Where we load and rotate each cell */
            oarray=new int[1024]; /* Final (ORd) image to display */
            for (i=0;i<1024;i++) oarray[i]=0xffffffff;
            realcell=0; /* Set if we have actual icon data */
    
           if (grid_data[r][C].library.length() > 0){ /* Not blank cell spec */
            Vector cell_vector=get_cell_vector(grid_data[r][C].library,grid_data[r][C].cell);
    
            for (i=0;i<1024;i++) oarray[i]=0xffffffff;
            Enumeration e=cell_vector.elements();
            while (e.hasMoreElements()){
              singlecellclass singlecell=(singlecellclass) e.nextElement();
              key=singlecell.library+"\002"+singlecell.cell; /* standard hash key format */
              if (!parent.cellHash.containsKey(key)){
                if (!key.equals("\002")) parent.msg("Cell <" + singlecell.cell + ">, Library <" + singlecell.library+"> not found in hash.");
    /*** DO DUMMY ICON ***/
              } else {
                realcell=1; /* Shows we're doing a real cell */
                celldataclass celldata=(celldataclass)parent.cellHash.get(key);
                iconbytes=celldata.iconbytes;
    
    /* If there's multiple icons involved, make them monochrome */
    /* Do this my finding the iconbytes value of the background */
                if (cell_vector.size() > 1){
                  for (i=0;i<10;i++) color_counts[i]=0;
                  for(i=0;i<1024;i++) ++color_counts[((int)iconbytes.charAt(i))-'0'];
                  maxind=0;
                  for (i=1;i<10;i++) if (color_counts[i] > color_counts[maxind]) maxind=i;
                } else maxind=(-1); /* Never occurs in iconbytes array */
         maxind=(-1); /* Disable color cleansing */
    
                for(i=0;i<1024;i++){
                  j=((int) iconbytes.charAt(i))-'0';
                  if (maxind > 0){ /* Adjust the icon here */
                    if (j == maxind) j=7; else j=0;
                  }

                  iarray[i]=0xff000000;
                  if(0!=(j&4)) iarray[i]|=0xff;
                  if(0!=(j&2)) iarray[i]|=0xff00;
                  if(0!=(j&1)) iarray[i]|=0xff0000;
                }
                rotate(iarray,singlecell.rot); /* Rotate the array */
                for(i=0;i<1024;i++) oarray[i]=oarray[i] & iarray[i]; /* OR the icon bytes */
              } /* end of key found in hash */
             } /* End of enumeration */
           } /* end of cell name not blank in grid */

/* Now we save oarray[] in the output file */
           for (i=0;i<1024;i++){
             out[C][i]=oarray[i];
           }
          } /* End of C */

          for (jj=0;jj<32;jj++){  // Row number within each cell
            for (C=0;C<COLS;C++){  // Cell number across row
              for (ii=0;ii<32;ii++){  // Pixel number across each cell
                if (((out[C][32*jj+ii] & 0xff0000) != 0)){bb.write("1 ");}else{bb.write("0 ");}
                if (((out[C][32*jj+ii] & 0xff00) != 0)){bb.write("1 ");}else{bb.write("0 ");}
                if (((out[C][32*jj+ii] & 0xff) != 0)){bb.write("1");}else{bb.write("0");}
                bb.write("\n");
              }
            }
          }
        } /* End of R */
        bb.close(); /* File written */
      } catch (Exception e){
        ((main_window) ae.getSource()).parent.msg("Can't open grid image xfer file:" + e);
      }
    }

    if (ae.getActionCommand() == "Save Grid"){
      if (thisgridname.length() <= 0){ /* Need grid name */
        get_write_name_class get_write_name=new get_write_name_class(last_grid_filename);
        input_name=get_write_name.get_name();
        if (input_name.equals("")) return;
        last_grid_filename=input_name;
      } else input_name=thisgridname;
      writegrid(input_name);
      thisgridname=input_name;
      basetitle=tail(input_name);
      setTitle(basetitle);
    }

    if (ae.getActionCommand() == "Redraw"){
      repaint();
      requestFocus();
    }

    if (ae.getActionCommand() == "Save Grid As"){
      get_write_name_class get_write_name=new get_write_name_class(last_saveas_filename);
      input_name=get_write_name.get_name();
      if (input_name.equals("")) return;
      last_saveas_filename=input_name;
      writegrid(input_name);
      thisgridname=input_name;
      basetitle=tail(input_name);
      setTitle(basetitle);
    }

    if (ae.getActionCommand() == "Write Binary Grid"){
      get_write_name_class get_write_name=new get_write_name_class(last_binary_filename);
      input_name=get_write_name.get_name();
      if (input_name.equals("")) return;
      last_binary_filename=input_name; /* Save for next time */
      writebinarygrid(input_name);
    }

    if (ae.getActionCommand() == "Zoom Out"){
      ISIZE=ISIZE/2;
      if (ISIZE < 4) {ISIZE=4;}
      newzoom=true;
      repaint();
    }

    if (ae.getActionCommand() == "Zoom In"){
      ISIZE=ISIZE+ISIZE;
      if (ISIZE > 32) {ISIZE=32;}
      newzoom=true;
      repaint();
    }

    if (ae.getActionCommand() == "Explode Composite Cell"){
      if ((r1 >= 0) && (c1 >= 0) && (r1==r2) && (c1==c2)){
        if (grid_data[r1][c1].library.indexOf("\002") <= 0){
/* Cell is not composite */
          parent.msg("Not a composite cell");
        } else {
        main_window mw=new main_window(parent,grid_data[r1][c1].library,grid_data[r1][c1].cell); /* Alternate constructor */
/* Pass my parent to constructor, so we both share same parent */
          mw.setSize(200,200);
          mw.setLocation(getLocation().x+100,getLocation().y+100);
          mw.show();
          ++parent.numWindows;
        }
      }
    }

    if (ae.getActionCommand() == "Edit"){
      if ((r1 >= 0) && (c1 >= 0) && (r1==r2) && (c1==c2))
        do_cell_edit(r1,c1);
    }

    if (ae.getActionCommand() == "Load"){
      if ((r1 >= 0) && (c1 >= 0) && (r1==r2) && (c1==c2)){
        cellselectclass csc=new cellselectclass(parent,this,parent.libroot);
        if ((csc.getlib().length() > 0) && (csc.getcell().length() > 0)){
          save_undo();
          do_cell_load(r1,c1,csc.getlib(),csc.getcell());
        }
      }
    }

    if (ae.getActionCommand() == "Select All"){
      r1=0;c1=0;r2=ROWS-1;c2=COLS-1;
      repaint();
    }

    if (ae.getActionCommand() == "Copy"){
      do_cell_copy(r1,c1,r2,c2);
    }

    if (ae.getActionCommand() == "Cut"){
      save_undo();
      do_cell_copy(r1,c1,r2,c2);
      do_cell_clear(r1,c1,r2,c2);      
    }

    if (ae.getActionCommand() == "Paste"){
      save_undo();
      do_cell_paste(0,r1,c1,r2,c2);
    }

    if (ae.getActionCommand() == "Or-Paste"){
      save_undo();
      do_cell_paste(1,r1,c1,r2,c2);
    }

    if (ae.getActionCommand() == "Undo"){
      do_undo();
    }

    if (ae.getActionCommand() == "Flush Cache"){
      do_flush_cache();
    }

    if (ae.getActionCommand() == "Rotate"){
      save_undo();
      do_rotate(r1,c1,r2,c2); /* Rotate cells in the selected region */
      repaint(); /* and re-display */
    }

    if (ae.getActionCommand() == "Region Rotate"){
      save_undo();
      do_region_rotate(r1,c1,r2,c2); /* Rotate cells in the selected region */
      repaint(); /* and re-display */
    }
  }

  void do_cell_load(int r,int c,String lib,String cell)
  {
    grid_data[r][c].library=lib;
    grid_data[r][c].cell=cell+"\002"+"0"; /* Set the rotation */
    repaint();
  }

  void do_cell_copy(int R1,int C1,int R2,int C2)
  {
    int r,c,r1,r2,c1,c2;
    griddataclass gd;

    if ((R1 < 0) || (R2 < 0) || (C1 < 0) || (C2 < 0)) return;

    r1=R1;r2=R2;c1=C1;c2=C2;
    if (r1 > r2){r1=R2;r2=R1;}
    if (c1 > c2){c1=C2;c2=C1;}

    parent.clipboard_r=1+r2-r1;
    parent.clipboard_c=1+c2-c1;  /* Remember dims of clipboard */
    parent.clipboard=new griddataclass[parent.clipboard_r][parent.clipboard_c];

    for (r=r1;r<=r2;r++){
      for (c=c1;c<=c2;c++){
        gd=new griddataclass();
        gd.cell=grid_data[r][c].cell;
        gd.library=grid_data[r][c].library;
        parent.clipboard[r-r1][c-c1]=gd;
      }
    }
  }

/* Paste the clipboard into the currently selected region */
/* If sel=single cell, paste whole clipboard */
/* Else just recirculate */

/* if or=0, do a replacement paste; else do an OR on the icon and truth table */

  void do_cell_paste(int or,int R1,int C1,int R2,int C2)
  {
    int r,c,rr,cc,r1,r2,c1,c2;
    String cell,library;

    if (parent.clipboard_r <= 0) return; /* No clipboard */
/*** Later, gray out the PASTE option if no clipboard */

    r1=R1;r2=R2;c1=C1;c2=C2;
    if (r1 > r2){r1=R2;r2=R1;}
    if (c1 > c2){c1=C2;c2=C1;}

    if ((r1==r2) && (c1==c2)){ /* Single cell-expand */
      r2=r1+parent.clipboard_r-1;
      c2=c1+parent.clipboard_c-1; /* Set new region */
      if (r2 >= ROWS) r2=ROWS-1;
      if (c2 >= COLS) c2=COLS-1;
    }

    rr=0; /* Row in clipboard */
    for (r=r1;r<=r2;r++){
      cc=0; /* Column in clipboard */
      for (c=c1;c<=c2;c++){
        library=grid_data[r][c].library;
        cell=grid_data[r][c].cell;
        grid_data[r][c]=new griddataclass();

        if (or==0){ /* pure replacement */
          grid_data[r][c].library=parent.clipboard[rr][cc].library;
          grid_data[r][c].cell=parent.clipboard[rr][cc].cell;
        } else { /* do an OR */
          if ((library.length() > 0) && (parent.clipboard[rr][cc].library.length() > 0))
             grid_data[r][c].library=library + "\002";
             else grid_data[r][c].library=library;
          grid_data[r][c].library=grid_data[r][c].library + 
                                  parent.clipboard[rr][cc].library;

          if ((library.length() > 0) && (parent.clipboard[rr][cc].library.length() > 0))
             grid_data[r][c].cell=cell+"\002";
             else grid_data[r][c].cell=cell;
          grid_data[r][c].cell=grid_data[r][c].cell + parent.clipboard[rr][cc].cell;
        }
        if (++cc >= parent.clipboard_c) cc=0;
      }
      if (++rr >= parent.clipboard_r) rr=0;
    }
    repaint();
  }

  void do_cell_clear(int R1,int C1,int R2,int C2)
  {
    int r,c,r1,r2,c1,c2;

    if ((R1 < 0) || (R2 < 0) || (C1 < 0) || (C2 < 0)) return;

    r1=R1;r2=R2;c1=C1;c2=C2;
    if (r1 > r2){r1=R2;r2=R1;}
    if (c1 > c2){c1=C2;c2=C1;}


    for (r=r1;r<=r2;r++){
      for (c=c1;c<=c2;c++){
        grid_data[r][c].cell="";
        grid_data[r][c].library="";
      }
    }
    repaint();
  }

  public Vector get_cell_vector(String library,String cell)
  {
    Vector cellvector;
    singlecellclass singlecell;
    int i;

    cellvector=new Vector(1,1); /* Only increase one member at a time */

/* Scan library and cell, pick off pieces, and form vector */
    library=library+"\002";cell=cell+"\002"; /* Add delimiters */
    while ((i=library.indexOf("\002")) > 0){ /* More in library */
      singlecell=new singlecellclass();
      singlecell.library=library.substring(0,i); /* Store library piece */
      library=library.substring(i+1); /* Adjust library string */

      i=cell.indexOf("\002"); /* break between cell and rot */
      singlecell.cell=cell.substring(0,i);
      singlecell.rot=rots.indexOf(cell.substring(i+1,i+2)); /* singlecell loaded now */
      cellvector.addElement(singlecell); /* Add to vector */
      cell=cell.substring(i+3); /* Adjust cell string */
    } /* Continue scanning for more elements */

    return cellvector;
  }

  void do_region_rotate(int R1,int C1,int R2,int C2)
  {
    int r,c,rr,cc;
    String cell,library;

    if ((R1 < 0) || (R2 < 0) || (C1 < 0) || (C2 < 0)) return;

    do_rotate(R1,C1,R2,C2); /* Rotate each cell */

/* Now copy region to clipboard */
    do_cell_copy(R1,C1,R2,C2);
    do_cell_clear(R1,C1,R2,C2); /* Clear old region */

/* Do special paste-rotated! */

    if (parent.clipboard_r <= 0) return; /* No clipboard */

    r1=R1;r2=R2;c1=C1;c2=C2;
    if (r1 > r2){r1=R2;r2=R1;}
    if (c1 > c2){c1=C2;c2=C1;}

/* Set r2 and c2 for rotated region */
    r2=r1+parent.clipboard_c-1;
    c2=c1+parent.clipboard_r-1; /* Set new region */
    if (r2 >= ROWS) r2=ROWS-1;
    if (c2 >= COLS) c2=COLS-1;

    cc=0; /* Column in clipboard */
    for (r=r1;r<=r2;r++){
      rr=parent.clipboard_r-1; /* Row in clipboard */
      for (c=c1;c<=c2;c++){
        library=grid_data[r][c].library;
        cell=grid_data[r][c].cell;
        grid_data[r][c]=new griddataclass();

        grid_data[r][c].library=parent.clipboard[rr][cc].library;
        grid_data[r][c].cell=parent.clipboard[rr][cc].cell;
        if (--rr < 0) rr=parent.clipboard_r-1;
      }
      if (++cc >= parent.clipboard_c) cc=0;
    }
  }

  void do_rotate(int R1,int C1,int R2,int C2)
  {
    int r,c,i,rot;
    griddataclass gd;
    String cell;

    Vector cell_vector; /* Holds elements of class singlecellclass */
    singlecellclass singlecell; /* Each cell from vector */
    Enumeration e; /* Enumerates the vector of single cells */

    if (r1 < 0) return; /* No region selected */

    r1=R1;r2=R2;c1=C1;c2=C2;
    if (r1 > r2){r1=R2;r2=R1;}
    if (c1 > c2){c1=C2;c2=C1;}

    for (r=r1;r<=r2;r++){
      for (c=c1;c<=c2;c++){ /* Rotate cell in grid_data[r][c] */
        cell_vector=get_cell_vector(grid_data[r][c].library,grid_data[r][c].cell);
        cell="";
        e=cell_vector.elements();
        while (e.hasMoreElements()){ /* For each single cell */
          singlecell=(singlecellclass) e.nextElement();
          singlecell.rot=(++singlecell.rot)%4; /* Rotate this element */
          if (cell.length() > 0) cell=cell+"\002"; /* intra-cell delimiter */
          cell=cell+singlecell.cell+"\002"+singlecell.rot; /* add ^Brot */
        }
        grid_data[r][c].cell=cell;
      }
    }
  }


/*** Cell editor only works for single cell right now ***/

  void do_cell_edit(int r,int c)
  {
    int i;

    celleditorclass ce;

/* See if this is a multi-cell block */
    if (grid_data[r][c].library.indexOf("\002") > 0){
      new msgbox("Can not edit composite cell");
      return;
    }

    if ((grid_data[r][c].library.length() <= 0) || (grid_data[r][c].cell.length() <= 0)){
      ce=new celleditorclass(parent,this); /* Empty one */
    } else {
      ce=new 
celleditorclass(parent,this,grid_data[r][c].library,grid_data[r][c].cell); /* Do the edit */
    }

//    ce.setSize(500,300);
    ce.show();

  }

  public void itemStateChanged(ItemEvent ie){}

  public void drawgrid(Graphics g)
  {
    int x,y,l,i,j,r,c,maxind;
    String rot;
    Image img;

    int[] iarray,oarray,color_counts;;
    int rows,cols; /* What we draw */
    String iconbytes,key;
    Vector cell_vector;
    Enumeration e;
    singlecellclass singlecell;

    color_counts=new int[10]; /* Record how many of each color we have in icon */
    x=XSTART;y=YSTART; /* Coords for UL corner of grid image */
    iarray=new int[1024]; /* Where we load and rotate each cell */
    oarray=new int[1024]; /* Final (ORd) image to display */

      for (r=ULrow;r<=LRrow;r++){
        for(c=ULcol;c<=LRcol;c++){
         if (grid_data[r][c].library.length() > 0){ /* Not blank cell spec */
          cell_vector=get_cell_vector(grid_data[r][c].library,grid_data[r][c].cell);       

          for (i=0;i<1024;i++) oarray[i]=0xffffffff;

          e=cell_vector.elements();
          while (e.hasMoreElements()){
            singlecell=(singlecellclass) e.nextElement();
            key=singlecell.library+"\002"+singlecell.cell; /* standard hash key format */
            if (!parent.cellHash.containsKey(key)){
              if (!key.equals("\002")) parent.msg("Cell <" + singlecell.cell + ">, Library <" + singlecell.library+"> not found in hash.");
  /***            g.setColor(BG);
              g.fillRect(x,y,32,32);***/
            } else {
              celldataclass celldata=(celldataclass)parent.cellHash.get(key);
              iconbytes=celldata.iconbytes;
  
  /* If there's multiple icons involved, make them monochrome */
  /* Do this my finding the iconbytes value of the background */
              if (cell_vector.size() > 1){
                for (i=0;i<10;i++) color_counts[i]=0;
                for(i=0;i<1024;i++) ++color_counts[((int)iconbytes.charAt(i))-'0'];
                maxind=0;
                for (i=1;i<10;i++) if (color_counts[i] > color_counts[maxind]) maxind=i;
              } else maxind=(-1); /* Never occurs in iconbytes array */
  maxind=(-1); /* Disable color clensing */
  
              for(i=0;i<1024;i++){
                j=((int) iconbytes.charAt(i))-'0';
                if (maxind > 0){ /* Adjust the icon here */
                  if (j == maxind) j=7; else j=0;
                }
  
                iarray[i]=0xff000000;
                if(0!=(j&4)) iarray[i]|=0xff;
                if(0!=(j&2)) iarray[i]|=0xff00;
                if(0!=(j&1)) iarray[i]|=0xff0000;
              }
              rotate(iarray,singlecell.rot); /* Rotate the array */
              for(i=0;i<1024;i++) oarray[i]=oarray[i] & iarray[i]; /* OR the icon bytes */
            } /* end of key found in hash */
           } /* End of enumeration */

// Now we need to post-process oarray for zoomed images
           if (ISIZE != 32){
             int fact=32/ISIZE; // Zoom factor
             int rr,cc,ri,ci;
             int old[]=new int[1024];
             for (i=0;i<1024;i++) old[i]=oarray[i];

             for (rr=0;rr<32;rr+=fact){
               for (cc=0;cc<32;cc+=fact){
                 oarray[ISIZE*(rr/fact)+(cc/fact)]=0xffffffff;
                 for (ri=0;ri<fact;ri++){
                   for (ci=0;ci<fact;ci++){
                     oarray[ISIZE*(rr/fact)+(cc/fact)]&=old[32*(rr+ri)+(cc+ci)];
                   }
                 }
               }
             }
           }
           img=createImage(new MemoryImageSource(ISIZE,ISIZE,oarray,0,ISIZE));
           g.drawImage(img,x,y,parent);
  /*** If you pass "this" as observer, you get stuck in paint() loop ***/
  
         } /* end of cell name not blank in grid */ else { /* library="" */
            g.setColor(BG);
            g.fillRect(x,y,ISIZE,ISIZE);
         }
  // %%% NO GRID
         x+=ISIZE+2*SEP;if (grid)++x;
        } /* End of FOR C */
        x=XSTART;
        y+=ISIZE+2*SEP;if (grid)++y;
      } /* End of FOR R */

  /* Now erase area to right and below grid */
  
      Rectangle R=getBounds();
      g.setColor(BG);
      g.fillRect(XSTART+(1+LRcol-ULcol)*(ISIZE+1+2*SEP),0,R.width,R.height);
      g.fillRect(0,YSTART+(1+LRrow-ULrow)*(ISIZE+1+2*SEP),R.width,R.height);
  } /* End of drawgrid() method */

/* Rotate image in ARRAY by (rot) 90 degree CW turns */
  void rotate(int iarray[],int rot)
  {
    int x,y,X,Y,xt,yt,i,oarray[];

    oarray=new int[1024];


    for (x=0;x<32;x++){
      for (y=0;y<32;y++){
        X=x;Y=y;
        for (i=0;i<rot;i++){
          yt=X;xt=31-Y;
          Y=yt;X=xt;
        }
        oarray[X+32*Y]=iarray[x+32*y];
      }
    }

/* Copy output */
    for (i=0;i<=1023;i++) iarray[i]=oarray[i];
  }


  public void newgrid(main_window parent){
    int r,c,i;

    ROWS=NEWR;
    COLS=NEWC;

    vscroll.setMaximum(ROWS-1);
    hscroll.setMaximum(COLS-1);

/* Allocate space for grid data */
    grid_data=new griddataclass[ROWS][COLS];
    undo=new griddataclass[5][ROWS][COLS];

    for (r=0;r<ROWS;r++){
      for (c=0;c<COLS;c++){
        grid_data[r][c]=new griddataclass();
        grid_data[r][c].library="";
        grid_data[r][c].cell="";
        for (i=0;i<5;i++){
          undo[i][r][c]=new griddataclass();
          undo[i][r][c].library="";
          undo[i][r][c].cell="";
       }
      }
    }
  }


  public void readgrid(main_window parent,String input_name){
    int r,c,i;
    int oldrows,oldcols;

    try{
      FileReader fr=new FileReader(input_name);
      BufferedReader in=new BufferedReader(fr);
      String library,cell,temp,nextlib;

      temp=in.readLine();
      if (!temp.equals("GRID_FILE")){
        parent.parent.msg(input_name + " does not appear to be a valid grid file:<" + temp+">");
        in.close();
        return;
      }

      temp=in.readLine();
      ROWS=Integer.parseInt(temp.trim());oldrows=ROWS;
if (limit){if (ROWS>=limitsize)ROWS=limitsize;}
      temp=in.readLine();
      COLS=Integer.parseInt(temp.trim());oldcols=COLS;
if (limit){if (COLS>=limitsize)COLS=limitsize;}

      vscroll.setMaximum(ROWS-1);
      hscroll.setMaximum(COLS-1);

/* Allocate space for grid data */
      grid_data=new griddataclass[ROWS][COLS];
      undo=new griddataclass[5][ROWS][COLS];
/* Allocate space for undo elements too */
      for (r=0;r<ROWS;r++){
       for (c=0;c<COLS;c++){
        for (i=0;i<5;i++){
          undo[i][r][c]=new griddataclass();
        }
       }
      }

      r=c=0;
      while ((temp=in.readLine()) != null){ /* Read data and store in grid_data */
        library="";cell="";
        i=temp.indexOf("\002"); /* break between library and cell */
        if (i > 0){ /* Parse the line */
          StringTokenizer st3=new StringTokenizer(temp,"\002");
          while (st3.hasMoreTokens()){
            if (library.length() > 0){ /* Already have at least one cell */
              library=library+"\002";
              cell=cell+"\002";
            }
            nextlib=st3.nextToken();
/* Load this library into cache */
 
if (DOS){
      while ((i=nextlib.indexOf(File.separatorChar)) >= 0)
        nextlib=nextlib.substring(0,i) + "/" + nextlib.substring(i+1);
} else { // linux
      while ((i=nextlib.indexOf("\\")) >= 0)
        nextlib=nextlib.substring(0,i) + "/" + nextlib.substring(i+1);
}
            if (nextlib.length() > 0)
              loadcelllibrary(nextlib); /* Load into cache */
 
            library=library+nextlib; /* add library */
            cell=cell+st3.nextToken()+"\002"; /* Add cell plus ^B */
            cell=cell+st3.nextToken(); /* Add rotation */
          } /* End of this parse */
        } /* End of this line */
        if ((r < ROWS) && (c < COLS)){
          grid_data[r][c]=new griddataclass();
          grid_data[r][c].library=library;
          grid_data[r][c].cell=cell;
        }
        c=c+1;
        if (c >= oldcols){
          c=0;
          ++r;
        }
     } /* End of all lines */
      in.close();	/* Done with file */
    } catch (Exception e){
      parent.parent.msg("Failure inside readgrid():"+e);
    }
  }

/* Call loadcelllibrary() each time we see a library/cell combo */
/* If library is already loaded, just return */
  public void loadcelllibrary(String library)
  {
    int i;
    String lr;

    try
    {
/* See if library is already loaded */
      if (parent.lib_loaded_hash.containsKey(library.toUpperCase())) return;

/* Library hasn't been loaded into hashes-so load it here */ 

/***
 *** Convert libroot to other format
 ***/
      lr=parent.libroot;

if (DOS){
      while ((i=lr.indexOf(File.separatorChar)) >= 0)
        lr=lr.substring(0,i) + "/" + lr.substring(i+1);
} else { // linux
      while ((i=lr.indexOf("\\")) >= 0)
        lr=lr.substring(0,i) + "/" + lr.substring(i+1);
}

      FileReader fr=new FileReader(lr + library.toUpperCase());
      BufferedReader in=new BufferedReader(fr);
      String line;
      String name,desc,tt,src,iconbytes;

      parent.lib_loaded_hash.put(library,"T"); /* Store library name in hash to show it's loaded */

      while ((name=in.readLine()) != null){
        celldataclass celldata=new celldataclass(); /* Need a new object! */
        celldata.name=name;
        celldata.library=library;
        celldata.desc=in.readLine();
        celldata.cellTT=in.readLine();
        celldata.src=in.readLine();
        celldata.iconbytes=in.readLine();
        String key=library + "\002" + name;
        if (name.length() > 0) {parent.cellHash.put(key,celldata);}
      }
      in.close();	/* Done with file */
    } catch (Exception e){
      parent.msg("Failure inside loadcelllibrary():"+e);
    }
  }

/* writecelllibrary(lib) writes-thru to the .LIB file */
  public void writecelllibrary(String library)
  {
    int i;
    String lr;

    try
    {
/***
 *** Convert libroot to other format
 ***/
      lr=parent.libroot;

if (DOS){
      while ((i=lr.indexOf(File.separatorChar)) >= 0)
        lr=lr.substring(0,i) + "/" + lr.substring(i+1);
} else { // linux
      while ((i=lr.indexOf("\\")) >= 0)
        lr=lr.substring(0,i) + "/" + lr.substring(i+1);
}

      FileWriter fw=new FileWriter(lr + library.toUpperCase());
      BufferedWriter out=new BufferedWriter(fw);
      String line;
      String name,desc,tt,src,iconbytes;

/* Read all entries in hash and look for ones from this library */
      Enumeration e=parent.cellHash.keys();
      while (e.hasMoreElements()){
        Object k=e.nextElement(); /* Next key */
        celldataclass celldata=(celldataclass) (parent.cellHash.get(k));
        if (library.equals(celldata.library)){ /* Write this one */
          out.write(celldata.name);out.newLine();

          desc="";
          StringTokenizer st=new StringTokenizer(celldata.desc,"\r\n");
          while (st.hasMoreTokens()){
            desc=desc+st.nextToken()+"\002";
          }
          out.write(desc);out.newLine();
          out.write(celldata.cellTT);out.newLine();
/* Fix up SRC for clean writing-change \n to \002 */
          src="";
          st=new StringTokenizer(celldata.src,"\r\n");
          while (st.hasMoreTokens()){
            src=src+st.nextToken()+"\002";
          }
          out.write(src);out.newLine();
          out.write(celldata.iconbytes);out.newLine();
        } /* Else don't write it */
      }
      out.close();	/* Done with file */
    } catch (Exception e){
      parent.msg("Failure inside writecelllibrary():"+e);
    }
  }


/* File dialog routine to accept a filename from user */
  class get_write_name_class extends Frame /*implements ActionListener*/
  {
    String last_filename;

    get_write_name_class(String default_name)	/* constructor */
    {
      super("Select Output File");	/* Parent's constructor */
      FileDialog fd=new FileDialog(this,"File Selection",FileDialog.SAVE);
      if (default_name.length() > 0){
        fd.setFile(default_name);
      }
      fd.setFont(new Font("Serif",Font.PLAIN,14));
      fd.setBackground(Color.gray);
      fd.setForeground(Color.black);
      fd.show();

      if (fd.getDirectory() == null) last_filename=""; else
      last_filename=fd.getDirectory() +  fd.getFile();
    }

    String get_name()
    {
      return(last_filename);
    }
  }


/* File dialog routine to accept a filename from user */
  class get_name_class extends Frame /*implements ActionListener*/
  {
    String last_filename;

    get_name_class(String default_name)	/* constructor */
    {
      super("Select file");	/* Parent's constructor */
      FileDialog fd=new FileDialog(this,"File Selection",FileDialog.LOAD);
      if (default_name.length() > 0) {
        fd.setFile(default_name);
      }
      fd.setFont(new Font("Serif",Font.PLAIN,14));
      fd.setBackground(Color.gray);
      fd.setForeground(Color.black);
      fd.show();
      if (fd.getDirectory() == null) last_filename=""; else
      last_filename=fd.getDirectory() +fd.getFile();
    }

    String get_name()
    {
      return(last_filename);
    }
  }


  public void writegrid(String name){
    int r,c,i;

    try{
      FileWriter fw=new FileWriter(name);
      BufferedWriter out=new BufferedWriter(fw);
      String temp;

      out.write("GRID_FILE\n");
      out.write(ROWS+"\n");
      out.write(COLS+"\n");

      for (r=0;r<ROWS;r++){
        for(c=0;c<COLS;c++){
/* In grid_data, library and cell are split. We need to interleave them */
          temp="";
          StringTokenizer library=new StringTokenizer(grid_data[r][c].library,"\002");
          StringTokenizer cell=new StringTokenizer(grid_data[r][c].cell,"\002");
          while (library.hasMoreTokens()){
            if (temp.length() > 0) {temp=temp+"\002";} /* Add inter-cell separator */
            temp=temp+library.nextToken()+"\002"+cell.nextToken()+"\002"+cell.nextToken();
          }
          out.write(temp+"\n");
        }
      }
      out.close();	/* Done with file */
    } catch (Exception e){
      parent.msg("Failure inside writegrid():"+e);
    }
  }


  public void writebinarygrid(String name)
  {
    int r,c,i;
    Vector cell_vector;
    Enumeration en;
    singlecellclass singlecell;
    char inputs,output;
    String desc,cellTT,key;
    String compdesc; /* composite of multiple descriptions */

    try{
      FileWriter fw=new FileWriter(name);
      BufferedWriter out=new BufferedWriter(fw);
      String temp;

/* Write header */
      temp="Generate by Java Loader V2.0                                                                        ";
      for (i=0;i<80;i++) writechar(out,temp.charAt(i));

      writeint(out,ROWS);
      writeint(out,COLS);
  
/* Write cells */
      for (r=0;r<ROWS;r++){
        for(c=0;c<COLS;c++){
          cellTT="00000000000000000000000000000000";
          desc=""; /* Default NOP  values */
          compdesc="";

          if (grid_data[r][c].library.length() > 0){ /* Not blank cell spec */
            cell_vector=get_cell_vector(grid_data[r][c].library,grid_data[r][c].cell);       
            en=cell_vector.elements();
            while (en.hasMoreElements()){

              singlecell=(singlecellclass) en.nextElement();
              key=singlecell.library+"\002"+singlecell.cell; /* standard hash key format */
              if (!parent.cellHash.containsKey(key)){
                if (!key.equals("\002")) parent.msg("Cell <" + singlecell.cell + ">, Library <" + singlecell.library+"> not found in hash.");
              } else { /* Write the cell here */
                celldataclass celldata=(celldataclass)parent.cellHash.get(key);
                desc=celldata.desc;
                for (i=0;i<desc.length();i++)
                  if (desc.substring(i,i+1).equals("\002"))
                    desc=desc.substring(0,i) + "\n" + desc.substring(i+1);
                compdesc=compdesc + ". " + desc;
                cellTT=OR(cellTT,ROTATE(celldata.cellTT,singlecell.rot),r,c);
              } /* Done with this piece of (composite) cell */
            } /* Done with enumeration */
          } /* Donw with [r][c] */
          writecell(out,'\000','\000',cellTT,compdesc);
        } /* End of c */
      } /* End of r */
      out.close();	/* Done with file */
    } catch (Exception e){
      parent.msg("Failure inside writegrid():"+e);
    }

  }

/* Truth Table Manipulation Routines */
  String ROTATE(String TT,int num)
  {
    int i,row;
    String NEWTT;
    int rowperm[]={0,8,4,12,1,9,5,13,2,10,6,14,3,11,7,15}; /* Huh? */
    int colperm[]={0,4,8,12,2,6,10,14,1,5,9,13,3,7,11,15}; /* Whatever */

    if (num==0) return(TT);

/* Convert TT to integer array */
    int tt[]=new int[32];
    int newtt[]=new int[32];

    for (i=0;i<32;i++)
      tt[i]=Integer.parseInt(TT.substring(i,i+1),16); /* Convert to number */

    for (i=0;i<num;i++){ /* do whole rotation scheme here */
      for (row=0;row<16;row++){
        newtt[2*row]=colperm[tt[2*rowperm[row]]];
        newtt[1+2*row]=colperm[tt[1+2*rowperm[row]]];
      }
      for (row=0;row<32;row++) tt[row]=newtt[row];
    }

/* Now convert back to string */
    NEWTT="";
    for (i=0;i<32;i++)
      NEWTT=NEWTT+Integer.toHexString(newtt[i]);

    return(NEWTT); /* Return the result */
  } /* End of ROTATE() */

  String OR(String TT1,String TT2,int r, int c)
  {
    int i,t1,t2;
    String NEWTT;

    NEWTT="";
    for (i=0;i<32;i++){
      t1=Integer.parseInt(TT1.substring(i,i+1),16); /* Integer value */
      t2=Integer.parseInt(TT2.substring(i,i+1),16); /* Integer value */
      if ((t1&t2) != 0) parent.msg("WARNING: Possible cell contention at [" + r + "," + c + "]");
      NEWTT=NEWTT+Integer.toHexString(t1|t2);
    }
    return(NEWTT);
  }

/* I/O routines */

  void writeint(BufferedWriter out,int i)
  {
    writechar(out,(char) (i%256)); /* LSB */
    writechar(out,(char) (i/256)); /* MSB */
  }

  void writechar(BufferedWriter out,char c)
  {
    try{
      out.write((char)(32+(int)(c/16))); /* MSN */
      out.write((char)(32+(int)(c%16))); /* LSN */
    } catch (Exception e){
      parent.msg("Failure inside writechar():"+e);
    }
  }

  void writecell(BufferedWriter out,char in,char outs,String tt, String desc)
  {
    int i;

    writechar(out,in);
    writechar(out,outs);
    for (i=0;i<32;i+=2) writechar(out,(char)Integer.parseInt(tt.substring(i,i+2),16));
    writeint(out,desc.length());
    for (i=0;i<desc.length();i++) writechar(out,desc.charAt(i));
  }

/* save_undo copies grid_data[0:ROWS-1,0:COLS-1] to undo[][] */
  public void save_undo()
  {
    int r,c,i;

    for (r=0;r<ROWS;r++){
      for(c=0;c<COLS;c++){
        for (i=4;i>0;i--){
          undo[i][r][c].library=undo[i-1][r][c].library;
          undo[i][r][c].cell=undo[i-1][r][c].cell;
        }
        undo[0][r][c].library=grid_data[r][c].library;
        undo[0][r][c].cell=grid_data[r][c].cell;
      }
    }
  }

/* Flush cache and reload from disk */
  public void do_flush_cache()
  {
    try {

      Hashtable liblist=new Hashtable(); /* Store current library list */

      System.out.print("Flushing: ");
/* Record name of currently-loaded libraries */
      Enumeration e=parent.lib_loaded_hash.keys();
      while (e.hasMoreElements()){
        Object key=e.nextElement();
        System.out.print(key + " ");
          liblist.put(key,"T");
      }
      System.out.print("\n\nReloading cache from, disk: ");

/* Now we know the name of all the libraries that have been loaded */
/* Next we flush the cache */

      e=liblist.keys(); /* We'll use the list we just made */
      while (e.hasMoreElements()){
        parent.lib_loaded_hash.remove(e.nextElement());
      }
/*
 * Now parent's lib_load_hash is empty. That should be sufficient to force
 * a reload of each library in its entirity. So let's trigger that now...
 */

      e=liblist.keys(); /* We'll use the list we just made */
      while (e.hasMoreElements()){
        loadcelllibrary((String) e.nextElement());
      }
    
      System.out.println("Done.");
    } catch(Exception ex){
      System.out.println("Something bad happened - returned hash key is NULL");
    }
    parent.update_all();
  }

/* do_undo restores grid_data */
  public void do_undo()
  {
    int r,c,i;

    for (r=0;r<ROWS;r++){
      for(c=0;c<COLS;c++){
        grid_data[r][c].library=undo[0][r][c].library;
        grid_data[r][c].cell=undo[0][r][c].cell;
        for(i=0;i<4;i++){
          undo[i][r][c].library=undo[i+1][r][c].library;
          undo[i][r][c].cell=undo[i+1][r][c].cell;
        }
      }
    }
    repaint();
  }

/* Pick out end of a pathname */
  String tail(String s)
  {
    int i;

    while (-1 != (i=s.indexOf(File.separatorChar))){
      s=s.substring(i+1);
    }
    return(s);
  }
}

/* window_event_handler class here */
class window_event_handler extends WindowAdapter
{
  main_window calling_window;

/* Constructor */
  public window_event_handler(main_window arg)
  {
    calling_window=arg;
  }

  public void windowClosing(WindowEvent we)
  {
    --calling_window.parent.numWindows;
    if (0==calling_window.parent.numWindows) System.exit(0);
    calling_window.setVisible(false);

  }

  public void windowClosed(WindowEvent we)
  {
    calling_window.dispose();
  }


}
