import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;

/*
 * iconeditor is a class for editing our icons
 */

public class iconeditor extends Frame 
implements ActionListener /* Fot buttons/etc */
{

  public Color currentcolor=Color.red;
  public int intcolor=1;
  public int icon[][]=new int[32][32];
  MyCanvas mycanvas;
  public int scale=8;

  public static void main(String args[])
  {
    iconeditor ie=new iconeditor(); /* Normally called from celleditor */
  }



/* Default constructor for this class-no initial icon */
  iconeditor()
  {
    super("Icon Editor");

    int x,y;

    for (x=0;x<32;x++) for(y=0;y<32;y++) icon[x][y]=0;

    setLayout(new BorderLayout());

    Panel p1=new Panel(); /* Command buttons */
    p1.setLayout(new GridLayout(5,1));
/* Add buttons */
    Button b1=new Button("Point");
    b1.addActionListener(this);
    p1.add(b1);

    Button b2=new Button("Line");
    b2.addActionListener(this);
    p1.add(b2);

    Button b3=new Button("Rect");
    b3.addActionListener(this);
    p1.add(b3);

    Button b4=new Button("Erase");
    b4.addActionListener(this);
    p1.add(b4);

    Button b5=new Button("Exit");
    b5.addActionListener(this);
    p1.add(b5);



    Panel p2=new Panel(); /* Drawing Area */
    mycanvas=new MyCanvas(this);
    mycanvas.setSize(32*scale,32*scale);
    p2.add(mycanvas);


    Panel p3=new Panel();
    p3.setLayout(new GridLayout(1,8));

    Button c1=new Button("White");
    c1.addActionListener(this);
    p3.add(c1);

    Button c2=new Button("Red");
    c2.addActionListener(this);
    p3.add(c2);

    Button c3=new Button("Yellow");
    c3.addActionListener(this);
    p3.add(c3);

    Button c4=new Button("Green");
    c4.addActionListener(this);
    p3.add(c4);

    Button c5=new Button("Blue");
    c5.addActionListener(this);
    p3.add(c5);

    Button c6=new Button("Violet");
    c6.addActionListener(this);
    p3.add(c6);

    Button c7=new Button("Orange");
    c7.addActionListener(this);
    p3.add(c7);

    Button c8=new Button("Black");
    c8.addActionListener(this);
    p3.add(c8);


    add(p1,"West");
    add(p2,"Center");
    add(p3,"South");

    this.setSize(400,300);

    repaint();
    show();

/* Add mouse listeners */
    mycanvas.addMouseListener(new myMouseAdapter(this));
    mycanvas.addMouseMotionListener(new myMouseMotionAdapter(this));

    addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent we){
        System.exit(0);
      }
    });
  } /* End of constructor */

  public void actionPerformed(ActionEvent ae)
  {
    //System.out.println("Command=" + ae.getActionCommand());
    if (ae.getActionCommand().equals("Exit")){
      System.exit(0);
    }
  }

  public void draw(int x,int y)
  {
    Graphics g=mycanvas.getGraphics();
    g.setColor(currentcolor);
    g.setXORMode(Color.black);
    g.fillRect(x*scale,y*scale,scale,scale);
    icon[x][y]=intcolor;
  }

  public int geticon(int x,int y)
  {
    return(icon[x][y]);
  }

} /* End of iconeditor class*/

class MyCanvas extends Canvas
{
  iconeditor parent;

  public MyCanvas(iconeditor ie)
  {
    super();
    parent=ie;
  }

  public void paint(Graphics g)
  {
    int x,y;

    g.setXORMode(Color.black);
    for (x=0;x<32;x++){
      for(y=0;y<32;y++){
        if (parent.geticon(x,y) == 0) g.setColor(Color.white); else g.setColor(Color.red);
        g.fillRect(x*parent.scale,y*parent.scale,parent.scale,parent.scale);
      }
    }
  }

  public void update()
  {
    repaint();
  }
}

class myMouseAdapter extends MouseAdapter
{
  iconeditor parent;

/* Constructor */
  public myMouseAdapter(iconeditor ie)
  {
    parent=ie; /* Remember parent */
  }

  public void mousePressed(MouseEvent me)
  {
    System.out.println("Press");
  }

  public void mouseReleased (MouseEvent me)
  {
    System.out.println("Released");
  }
}


class myMouseMotionAdapter extends MouseMotionAdapter
{
  iconeditor parent;

/* Constructor */
  public myMouseMotionAdapter(iconeditor ie)
  {
    parent=ie; /* Remember parent */
  }

  public void mouseDragged(MouseEvent me)
  {
    System.out.println("Dragged");
    parent.draw(me.getX()/parent.scale,me.getY()/parent.scale);
  }

  public void mouseMoved (MouseEvent me)
  {
    /*System.out.println("Move");*/
  }
}
