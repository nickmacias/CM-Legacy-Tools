import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;

class cellselectdialog extends Dialog
implements ActionListener /* for buttons */
{
  java.awt.List list; /* Main list of cells */
  cellselectclass parent;

/*** CONSTRUCTOR ***/
  cellselectdialog(cellselectclass p,boolean mode)
  {
    super(p.parent,"Cell Selection",mode); /* Make a dialog */

    parent=p; /* So this code and the event handlers can R/W parent vars */

    this.setLocation(parent.parent.getLocation().x+50,parent.parent.getLocation().y+50);

    list=new java.awt.List();
    make_cell_list(parent); /* Make a list out of the cells in parent.library */

/* Now we add the elements to the cell-selector window */
    setLayout(new BorderLayout());

    Panel p1=new Panel();
/* Add buttons */
    Button b1=new Button("OK");
    b1.setActionCommand("OK\001"); /* Wierd command, not likely to be in list */
    b1.addActionListener(this);
    p1.add(b1);

    Button b2=new Button("Cancel");
    b2.setActionCommand("Cancel\001"); /* Wierd command, not likely to be in list */
    b2.addActionListener(this);
    p1.add(b2);

    Panel p2=new Panel();
    Button b3=new Button("New Library");
    b3.setActionCommand("New Library\001");
    b3.addActionListener(this);
    p2.add(b3);

/* Now place these in window */
    add(p1,"North");
    add(p2,"South");
    add(list,"Center");
    list.addActionListener(this);
    this.setSize(300,400);

    repaint();

    addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent we){
        dispose();
      }
    });

  } /* End of constructor */

/* Read parent.library and add cells to list */
  void make_cell_list(cellselectclass parent)
  {
    int i,j;
    String names[];

    list.removeAll();
 /* Scan the hash for cells in this library */
    Enumeration e=parent.rootparent.cellHash.keys();
    while (e.hasMoreElements()){
      Object k=e.nextElement();
      celldataclass celldata=(celldataclass) parent.rootparent.cellHash.get(k);
/* See if this cell belongs to library */
      if (celldata.library.equals(parent.library)){
        list.add(celldata.name);
      }
    }
/* Now sort the list */
    names=new String[list.getItemCount()];
    for (i=0;i<list.getItemCount();i++) names[i]=list.getItem(i);

/* Bubble sort... */
    for (i=0;i<list.getItemCount();i++){
      for (j=i;j<list.getItemCount();j++){
        if (names[j].toUpperCase().compareTo(names[i].toUpperCase()) < 0){
          String temp=names[i];names[i]=names[j];names[j]=temp;
        }
      }
    }
    for (i=0;i<list.getItemCount();i++)
      list.replaceItem(names[i],i);

    list.select(0);
  }

  public void actionPerformed(ActionEvent ae){
    if (ae.getActionCommand() == "Cancel\001"){ /* Cancel */
      parent.cell="";
      dispose();
    } else if (ae.getActionCommand() == "New Library\001"){
      parent.select_lib_dialog(); /* Sets "library" in parent */
      make_cell_list(parent); /* update displayed list of cells */
    } else { /* Anything else is a cell selection */
      parent.cell=list.getSelectedItem();
      dispose();
    }
  }
} /* End of cellselectdialog */
