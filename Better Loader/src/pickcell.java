import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;

/*
 * pickcell class accepts a pair of library and cell strings from a grid
 * and displays each library/cell in a list.
 * User can select one library/cell from the list, which is returned
 * to the caller.
 *
 * Constructor is: pickcell(lib,cell)
 *
 */

class pickcell /* extends Frame*/
{
  public String library; /* Selected library */
  public String cell; /* Selected cell */
  public boolean selected; /* Set of user selects an item */
  FileDialog fd; /* Main dialog */
  loader rootparent;
  main_window parent;

/*
 * Constructor. User gets a dropdown list
 * of the cells in that library
 */
  pickcell(loader par,main_window par2,String l,String c)
  {
    rootparent=par;
    parent=par2;
    selected=false; // Initially, no selection has been made
    pickcelldialog d=new pickcelldialog(this,true,l,c);
    d.show(); /* Modal-won't return until user has made selection */

/* Now the user has made their selection...all the variables should be set */
  }
}


/* pickcelldialog displays the actual listbox and let the user select or cancel */

class pickcelldialog extends Dialog
implements ActionListener /* for buttons */
{
  java.awt.List list; /* Main list of cells */
  pickcell parent;
  String picked_library,picked_cell; // Selection by user

/*** CONSTRUCTOR ***/
  pickcelldialog(pickcell p,boolean mode,String library_in,String cell_in)
  {
    super(p.parent,"Single Cell Selection",mode); /* Make a dialog */

    parent=p; /* So this code and the event handlers can R/W parent vars */

    this.setLocation(parent.parent.getLocation().x+50,parent.parent.getLocation().y+50);

    list=new java.awt.List();
    make_cell_list(this,library_in,cell_in); /* Make a list out of the cells in parent.library */
/* Now we add the elements to the cell-selector window */
    setLayout(new BorderLayout());

    Panel p1=new Panel();
/* Add buttons */
    Button b1=new Button("OK");
    b1.setActionCommand("OK\001"); /* Wierd command, not likely to be in list */
    b1.addActionListener(this);
    p1.add(b1);

    Button b2=new Button("Cancel");
    b2.setActionCommand("Cancel\001"); /* Wierd command, not likely to be in list */
    b2.addActionListener(this);
    p1.add(b2);

    Panel p2=new Panel();
    Button b3=new Button("New Library");
    b3.setActionCommand("New Library\001");
    b3.addActionListener(this);
    p2.add(b3);

/* Now place these in window */
    add(p1,"North");
    add(p2,"South");
    add(list,"Center");
    list.addActionListener(this);
    this.setSize(300,400);

    repaint();

    addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent we){
        dispose();
      }
    });

  } /* End of constructor */

/* Read parent.library and add cells to list */
  void make_cell_list(pickcelldialog parent,String library_in,String cell_in)
  {
    int i,j;
    String libs[];

    list.removeAll();
/* Break lib/par composite structure into single pairs */
    Vector v=get_cell_vector(library_in,cell_in);

 /* Scan the hash for cells in this library */
    Enumeration e=parent.rootparent.cellHash.keys();
    while (e.hasMoreElements()){
      Object k=e.nextElement();
      celldataclass celldata=(celldataclass) parent.rootparent.cellHash.get(k);
/* See if this cell belongs to library */
      if (celldata.library.equals(parent.library)){
        list.add(celldata.name);
      }
    }

    list.select(0);
  }

  public void actionPerformed(ActionEvent ae){
    if (ae.getActionCommand() == "Cancel\001"){ /* Cancel */
      parent.selected=false;
      dispose();
    } else { /* Anything else is a cell selection */
      parent.selected=true;
      parent.result=list.getSelectedItem();
      dispose();
    }
  }
} /* End of pickcelldialog */
