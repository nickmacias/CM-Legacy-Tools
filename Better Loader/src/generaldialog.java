import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;

class generaldialog extends Dialog
implements ActionListener /* for buttons */
{
  boolean OKstatus=false; /* No OK */

/*** CONSTRUCTOR ***/
  generaldialog(String title,String prompt)
  {
    super(new Frame(),title,true); /* Make a dialog */

/* Now we add the elements to the cell-selector window */
    setLayout(new BorderLayout());

    Panel p1=new Panel();
/* Add buttons */
    Button b1=new Button("Yes");
    b1.addActionListener(this);
    p1.add(b1);

    Button b2=new Button("No");
    b2.addActionListener(this);
    p1.add(b2);


    Panel p3=new Panel();p3.setLayout(new BorderLayout());
    p3.add(new Label(prompt),"Center");

/* Now place these in window */
    add(p3,"North");
    add(p1,"South");

    this.setSize(220,125);

    repaint();

    addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent we){
        dispose();
      }
    });

  } /* End of constructor */

  public void actionPerformed(ActionEvent ae){
    if (ae.getActionCommand() == "Yes"){ /* Cancel */
      OKstatus=true;
    } else {
      OKstatus=false;
    }
    dispose();
  }


  public boolean ok()
  {
    return(OKstatus);
  }

} /* End of generaldialog */
