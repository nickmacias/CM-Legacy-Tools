import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;

class newcellselectdialog extends Dialog
implements ActionListener,ItemListener
{
  java.awt.List list; /* Main list of cells */
  newcellselectclass parent;
  TextField tf; /* Holds user's entry of new cell name */
  Label liblabel;

/*** CONSTRUCTOR ***/
  newcellselectdialog(newcellselectclass p,boolean mode)
  {
    super(p.parent,"Save Cell As...",mode); /* Make a dialog */

    parent=p; /* So this code and the event handlers can R/W parent vars */


/* Now we add the elements to the cell-selector window */
    setLayout(new BorderLayout());

    Panel p1=new Panel();
/* Add buttons */
    Button b1=new Button("OK");
    b1.setActionCommand("OK\001"); /* Wierd command, not likely to be in list */
    b1.addActionListener(this);
    p1.add(b1);

    Button b2=new Button("Cancel");
    b2.setActionCommand("Cancel\001"); /* Wierd command, not likely to be in list */
    b2.addActionListener(this);
    p1.add(b2);

/* Make dropdown list with names of cells */
    list=new java.awt.List();
    list.addActionListener(this);
    list.addItemListener(this);
    p1.add(list);
    make_cell_list(parent.rootparent,list,parent.library);

    Panel p2=new Panel();p2.setLayout(new BorderLayout());
    Panel p3=new Panel();p3.setLayout(new BorderLayout());

    Button b3=new Button("New Library");
    b3.setActionCommand("New Library\001");
    b3.addActionListener(this);
    p3.add(b3,"West");
    liblabel=new Label("Library=" + parent.library);
    p3.add(liblabel,"Center");

    p2.add(new Label("Enter cell name:"),"Center");

    tf=new TextField(30);
    tf.addActionListener(this);
    p2.add(tf,"South");


/* Now place these in window */
    add(p3,"North");
    add(p2,"Center");
    add(p1,"South");

    this.setSize(300,200);

    pack();
    repaint();

    addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent we){
        dispose();
      }
    });

  } /* End of constructor */

  public void itemStateChanged(ItemEvent ie)
  {
    java.awt.List l=(java.awt.List)ie.getItemSelectable();
    tf.setText(l.getSelectedItem());
  }

  public void actionPerformed(ActionEvent ae){
    if (ae.getActionCommand() == "Cancel\001"){ /* Cancel */
      parent.cell="";parent.library="";
      dispose();

    } else if (ae.getActionCommand() == "New Library\001"){
      parent.select_lib_dialog(); /* Sets "library" in parent */
      liblabel.setText("Current Library=" + parent.library);
      make_cell_list(parent.rootparent,list,parent.library);
      pack();
      repaint();

    } else { /* Anything else is a cell selection */
      parent.cell=tf.getText();
/* See if this cell already exists. If so, confirm choice; if not confirmed, skip dispose */
      if (parent.rootparent.cellHash.containsKey(parent.library+"\002"+parent.cell)){
        generaldialog gd=new generaldialog("File Exists","Cell " + parent.cell + " Exists: Overwrite?");
        gd.show();
        if (!gd.ok()){ /* Don't overwrite cell! */
          parent.library="";parent.cell="";
        } else {
          dispose();
        }
      }
      dispose();
    }
  }

/* Read parent.library and add cells to list */
  void make_cell_list(loader root,java.awt.List celllist,String lib)
  {
    int i,j;
    String names[];

System.out.println("Inside make_cell_list");
    celllist.removeAll();
 /* Scan the hash for cells in this library */
    Enumeration e=root.cellHash.keys();
    while (e.hasMoreElements()){
      Object k=e.nextElement();
      celldataclass celldata=(celldataclass) root.cellHash.get(k);
/* See if this cell belongs to library */
      if (celldata.library.equals(lib)){
        celllist.add(celldata.name);
      }
    }

/* Sort the list */
    names=new String[celllist.getItemCount()];
    for (i=0;i<celllist.getItemCount();i++) names[i]=celllist.getItem(i);

/* Bubble sort... */
    for (i=0;i<celllist.getItemCount();i++){
      for (j=i;j<celllist.getItemCount();j++){
        if (names[j].toUpperCase().compareTo(names[i].toUpperCase()) < 0){
          String temp=names[i];names[i]=names[j];names[j]=temp;
        }
      }
    }
    for (i=0;i<celllist.getItemCount();i++)
      celllist.replaceItem(names[i],i);

System.out.println("Leaving make_cell_list");

  }
} /* End of newcellselectdialog */
