import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;

/*
 * loader is the main class
 * It sets up some global vars, and creates the first window
 */

public class loader extends Frame 
{
/* Hash tables for storing cell information from libraries */
  public Hashtable cellHash=new Hashtable();
  public Hashtable lib_loaded_hash = new Hashtable(); /* Remembers which libs are loaded */
  public String libroot; /* Base directory for libraries */
  public griddataclass clipboard[][];
  public int clipboard_r=-1,clipboard_c=-1;
  public int numWindows=1; /* Start with one window */
  public createIcon ie;  /* Icon editor */
  public boolean ieup=false; /* Set when LD's superlicious icon editor is displayed */
  msgwin mwin;

/* Miscellaneous variables for tracking # of windows, etc. */

/* Main program starts here-instantiate the first main window */

  public static void main(String args[])
  {
 
    loader self=new loader(); /* create an instance of loader class */
/* This is necessary since main() is abstract and thus doesn't have "this" */
  }

/* Constructor for this class */
  loader()
  {
    libroot="C:\\NICK'S STUFF\\JAVA\\LOADER\\LIBRARIES\\";
    /* System.out.println("libroot=<"+libroot+">"); */
/* Specify LIBROOT when running loader.class via the "-DLIBROOT=..." switch */
    libroot=System.getProperty("LIBROOT");

/* See if the license file exists */
  try {
    FileReader fr=new FileReader(libroot+"LICENSE.TXT");
    BufferedReader in=new BufferedReader(fr);
    String temp=in.readLine();
    in.close(); /* License must exist! */
  } catch (Exception e){ /* NO LICENSE... */
    license license_obj=new license();
    while (license.done() == 0);
    if (license.done()!=2){
      System.out.println("You must accept the license terms to run this software.\n");
      System.exit(0);
    }
/* User has accepted the license-create license.txt to record this fact */
    try{
      FileWriter fw=new FileWriter(libroot+"LICENSE.TXT");
      BufferedWriter out=new BufferedWriter(fw);
      out.write(license.text());
      out.close();
    } catch (Exception e2){
      System.out.println("Unknown error " + e2 + "creating " + libroot + "LICENSE.TXT file");
      System.exit(0);
    }
  }

    mwin=new msgwin(); /* Create message window */
    msg("Beginning loader at " + new Date());
    msg("libroot=<"+libroot+">");

    main_window mw=new main_window(this,32,32); /* Create initial window with empty grid */
    mw.setSize(400,400);
 
    mw.show(); /* This seems to cause layout manager to repack components */
    mw.setLocation(mwin.getLocation().x+mwin.getSize().width,mwin.getLocation().y);

  }

  public void msg(String t)
  {
    mwin.addtext(t);
  }


/* Code for updating windws as icons change */

  Vector windows=new Vector();

/* other windows call register() when they are created and de-register() when they are closed */

  public void register(main_window w)
  {
    windows.addElement((Object) w);
  }

  public void deregister(main_window w)
  {
    boolean ignore=windows.removeElement(w);
  }

  public void update_all()
  {
    Enumeration e=windows.elements();
    while (e.hasMoreElements()){
      main_window mw=(main_window) e.nextElement();
      mw.repaint();
    }
  }

}

class license extends Frame
implements ActionListener  /* For ACCEPT/DECLINE buttons */
{
  static TextArea ta;
  static int doneflag=0;

  license()
  {
    super("Licensing Agreement-Read Carefully");
/* Now we add the elements to the cell-selector window */
    setLayout(new BorderLayout());

    Panel p1=new Panel();
/* Add buttons */
    Button b1=new Button("Accept");
    b1.addActionListener(this);
    p1.add(b1);

    Button b2=new Button("Decline");
    b2.addActionListener(this);
    p1.add(b2);

    Panel p3=new Panel();
    p3.setLayout(new BorderLayout());
    ta=new TextArea();
    p3.add(ta,"Center");

/* Now place these in window */
    add(p3,"Center");
    add(p1,"South");

    this.setSize(600,700);
    ta.setText("");
ta.setText(ta.getText()+"SOFTWARE LICENSE AGREEMENT AND LIMITED WARRANTY \n");
ta.setText(ta.getText()+" \n");
ta.setText(ta.getText()+"READ THE TERMS AND CONDITIONS OF THIS LICENSE AGREEMENT CAREFULLY\n");
ta.setText(ta.getText()+"BEFORE CONTINUING WITH THE INSTALLATION OR USE OF THE CELL MATRIX\n");
ta.setText(ta.getText()+"LOADER OR ANY ASSOCIATED FILES, LIBRARIES, JAVA CLASSES, OR\n");
ta.setText(ta.getText()+"DOCUMENTATION (COLLECTIVELY REFERRED TO HEREAFTER AS \"THE PROGRAM\").\n");
ta.setText(ta.getText()+"THE PROGRAM IS COPYRIGHTED AND LICENSED, NOT SOLD. BY PRESSING THE\n");
ta.setText(ta.getText()+"\"ACCEPT\" BUTTON, YOU INDICATE YOUR ACCEPTENCE OF THE TERMS AND\n");
ta.setText(ta.getText()+"CONDITIONS BELOW. \n");
ta.setText(ta.getText()+" \n");
ta.setText(ta.getText()+"1. License. Cell Matrix Corporation (\"Licensor\") hereby grants you\n");
ta.setText(ta.getText()+"a nonexclusive license to use The Program, solely for your own use\n");
ta.setText(ta.getText()+"in developing designs for Cell Matrix programmable devices or for\n");
ta.setText(ta.getText()+"your own research purposes. Your use of any intellectual\n");
ta.setText(ta.getText()+"property contained in The Program, in accordance with the terms of\n");
ta.setText(ta.getText()+"this License Agreement, shall not constitute a license of that\n");
ta.setText(ta.getText()+"intellectual property outside the context of The Program. Cell\n");
ta.setText(ta.getText()+"Matrix Corporation and/or it's directors retain title to The Program\n");
ta.setText(ta.getText()+"and to any patents, copyrights, trade secrets and other intellectual\n");
ta.setText(ta.getText()+"property rights therein. This License allows you to use The Program\n");
ta.setText(ta.getText()+"on a single computer. You may NOT distribute any part of The Program.\n");
ta.setText(ta.getText()+"This License Agreement does not convey to you an interest in The\n");
ta.setText(ta.getText()+"Program, nor to any intellectual property contained therein, but\n");
ta.setText(ta.getText()+"only a limited right of use, revocable in accordance with the terms\n");
ta.setText(ta.getText()+"of this License Agreement \n");
ta.setText(ta.getText()+"\nUnder no circumstances are you to use The Program for any\n");
ta.setText(ta.getText()+"commercial purposes whatsoever.\n");
ta.setText(ta.getText()+" \n");
ta.setText(ta.getText()+"2. Confidentiality. The Program is confidential and proprietary\n");
ta.setText(ta.getText()+"information of Cell Matrix Corporation and/or it's directors. You\n");
ta.setText(ta.getText()+"agree to take reasonable steps to protect The Program from\n");
ta.setText(ta.getText()+"unauthorized disclosure or use. You agree that you shall not\n");
ta.setText(ta.getText()+"(and shall not permit other persons or entities to) distribute,\n");
ta.setText(ta.getText()+"reverse-engineer, decompile, disassemble, merge, modify, create\n");
ta.setText(ta.getText()+"derivative works of (in whole or in part), or translate any part\n");
ta.setText(ta.getText()+"of The Program except as allowed in this license agreement.\n");
ta.setText(ta.getText()+" \n");
ta.setText(ta.getText()+"3. Term. This License Agreement is effective upon your pressing\n");
ta.setText(ta.getText()+"the \"Accept\" button and shall continue until terminated. You may\n");
ta.setText(ta.getText()+"terminate this License Agreement at any time by destroying all\n");
ta.setText(ta.getText()+"copies of The Program. Licensor may terminate this License Agreement\n");
ta.setText(ta.getText()+"at any time and without notice, upon breach by you of any term\n");
ta.setText(ta.getText()+"hereof. Upon termination, you must destroy all copies of The Program.\n");
ta.setText(ta.getText()+"\n");
ta.setText(ta.getText()+"4. Limitation of Liability. In no event will Cell Matrix Corporation\n");
ta.setText(ta.getText()+"nor its directors be liable for any losses, including but not limited\n");
ta.setText(ta.getText()+"to lost revenue, profit or data, or for direct, special, indirect,\n");
ta.setText(ta.getText()+"consequential, incidental or punitive damages, however caused,\n");
ta.setText(ta.getText()+"arising out of the installation, use of or inability to use The\n");
ta.setText(ta.getText()+"Program, even if Cell Matrix Corporation or its directors have been\n");
ta.setText(ta.getText()+"advised of the possibility of such damages. Some states do not allow\n");
ta.setText(ta.getText()+"the limitation or exclusion of liability for incidental or\n");
ta.setText(ta.getText()+"consequential damages, so the above limitation or exclusion may\n");
ta.setText(ta.getText()+"not apply to you. \n");
ta.setText(ta.getText()+" \n");
ta.setText(ta.getText()+"5. Disclaimer. Cell Matrix Corporation makes and you receive no\n");
ta.setText(ta.getText()+"warranties or conditions, express, implied, statutory or otherwise,\n");
ta.setText(ta.getText()+"and Cell Matrix Corporation specifically disclaims any implied\n");
ta.setText(ta.getText()+"warranties of merchantability, noninfringement, or fitness for a\n");
ta.setText(ta.getText()+"particular purpose. Cell Matrix Corporation does not warrant that\n");
ta.setText(ta.getText()+"the functions contained in The program will meet your requirements,\n");
ta.setText(ta.getText()+"or that the operation of The Program will be uninterrupted or error\n");
ta.setText(ta.getText()+"free, or that defects in The Program will be corrected. Furthermore,\n");
ta.setText(ta.getText()+"Cell Matrix Corporation does not warrant or make any representations\n");
ta.setText(ta.getText()+"regarding use or the results of the use of The Program in terms of\n");
ta.setText(ta.getText()+"correctness, accuracy, reliability or otherwise. \n");
ta.setText(ta.getText()+" \n");
ta.setText(ta.getText()+"6. High Risk Activities. The Program is not fault-tolerant and is\n");
ta.setText(ta.getText()+"not designed, manufactured or intended for use as on-line control\n");
ta.setText(ta.getText()+"equipment in hazardous environments requiring fail-safe performance,\n");
ta.setText(ta.getText()+"such as in the operation of nuclear facilities, aircraft navigation\n");
ta.setText(ta.getText()+"or communication systems, or air traffic control, in which failure\n");
ta.setText(ta.getText()+"of The Program could lead directly to death, personal injury, or\n");
ta.setText(ta.getText()+"severe physical or environmental damage (\"High Risk Activities\").\n");
ta.setText(ta.getText()+"Cell Matrix Corporation specifically disclaims any express or implied\n");
ta.setText(ta.getText()+"warranty of fitness for High Risk Activities. \n");
ta.setText(ta.getText()+" \n");
ta.setText(ta.getText()+"7. Miscellaneous. Should any term of this License Agreement be\n");
ta.setText(ta.getText()+"declared void or unenforceable by any court of competent jurisdiction,\n");
ta.setText(ta.getText()+"such declaration shall have on effect on the remaining terms hereof.\n");
ta.setText(ta.getText()+"The failure of either party to enforce any rights granted hereunder\n");
ta.setText(ta.getText()+"or to take action against the other party in the event of any breach\n");
ta.setText(ta.getText()+"hereunder shall not be deemed a waiver by that party as to subsequent\n");
ta.setText(ta.getText()+"actions in the event of future breaches. \n");
ta.setText(ta.getText()+"\n");
ta.setText(ta.getText()+"Upon accepting the terms of this License Agreement,a copy of it will\n");
ta.setText(ta.getText()+"be placed in your selected library root.\n");
 

    repaint();
    show();

    addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent we){
        doneflag=1;  /* NOT accepted */
        dispose();
      }
    });
  } /* End of constructor */

  public void actionPerformed(ActionEvent ae)
  {
    if (ae.getActionCommand().equals("Accept")){
      doneflag=2;
      dispose();
    } else if (ae.getActionCommand().equals("Decline")){
      doneflag=1;
      dispose();
    } else {
      doneflag=1;
      dispose();
    }
  }

  public static String text()
  {
    return(ta.getText());
  }

  public static int done()
  {
    return (doneflag);
  }
} /* End of msgwin */

class msgwin extends Frame
implements ActionListener /* for CLEAR button */
{
  TextArea ta;

/*** CONSTRUCTOR ***/
  msgwin()
  {
    super("");

/* Now we add the elements to the cell-selector window */
    setLayout(new BorderLayout());

    Panel p1=new Panel();
/* Add buttons */
    Button b1=new Button("Clear");
    b1.addActionListener(this);
    p1.add(b1);

    Panel p3=new Panel();
    p3.setLayout(new BorderLayout());
    ta=new TextArea();
    p3.add(ta,"Center");

/* Now place these in window */
    add(p3,"Center");
    add(p1,"South");

    this.setSize(200,150);

    repaint();
    show();

    addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent we){
        dispose();
      }
    });
  } /* End of constructor */

  public void actionPerformed(ActionEvent ae)
  {
    if (ae.getActionCommand().equals("Clear")){
      ta.setText("");
    } else dispose();
  }

  public void addtext(String t)
  {
    ta.setText(ta.getText() + t + "\n");
    ta.select((ta.getText()).length()-1,ta.getText().length()-1);
  }

} /* End of msgwin */
