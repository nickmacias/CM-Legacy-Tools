import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

/* simulation of the transmittance of signals through a small cell matrix 
    that implements a particular DES decrypter layout. Layout should be
    near this file on an html page,  DESdecryptor.gif
*/
public class createIcon extends Frame
implements ActionListener
{
/***
  public static void main(String args[]){
    createIcon mci=new createIcon();
  }
***/

  ClickInfo clickInfo = new ClickInfo(); // screen (frame) coordinates of the place where the user clicked the mouse. Used to pass info from mouse handling to this class.
  String drawMode; // how clicks are interpreted to set pixel colors: "pixel", "straightLine", or "rectangleOutline", or "filledRectangle".    
  Color drawColor; // current color 
  int borderPixels = 8; 
  int iconPixels = 32;
  int menuPixels = 2;
  int totalHeightPixels = borderPixels*2 + iconPixels + menuPixels; 
  int totalWidthPixels = borderPixels*2 + iconPixels;
  int Cin = borderPixels + 5;  // pixel where center of Cin arrow abuts icon.
  int Cout = borderPixels + iconPixels - 6;
  int Din = Cin + 5;
  int Dout = Cout - 5;
  loader rootparent;  /* Holds base loader object */
  Pixel Pixels[][] = new Pixel[totalWidthPixels][totalHeightPixels];
  Vector palette;
  // semaphores for synchronizing events (repaint and mouse handling)
  boolean semClicked; 
  boolean stopFlicker = true; // default is double buffering. No double buffering draws slightly quicker, but flickers annoyingly.
  // draw on an image instead of the screen: double buffering of screen image avoids annoying flicker.
  Image imageBuffer;
  Graphics g_buffer;
  
  createIcon(loader root)
  {
    super("LD's Superlicious Icon Editor");
    rootparent=root;
    rootparent.ieup=true;
    setSize(400,400);
    drawColor = Color.black; // default drawing color.
    drawMode = "pixel";
    semClicked = false;
    createColorPalette();
    initPixels();
    show();
    addMouseListener(new my_mouse_handling_class(this));
    addKeyListener(new MyKeyAdapter(this));
    addWindowListener(new Iwindow_event_handler(this));
   }

 void createColorPalette()
  {
   palette = new Vector(16);
  // primary colors, where r, g, and b are either 0 or 255
   palette.addElement(new Color(255,255,255));
   palette.addElement(new Color(0,0,0));
   palette.addElement(new Color(255,0,0));
   palette.addElement(new Color(0,255,0));
   palette.addElement(new Color(0,0,255));
   palette.addElement(new Color(255,255,0));
   palette.addElement(new Color(255, 0,255));
   palette.addElement(new Color(0,255,255));
   // secondary colors of my choosing. All of these are websafe colors. 
   palette.addElement(new Color(0,51,153));  // dark blue
   palette.addElement(new Color(204,255,255));  // light blue
   palette.addElement(new Color(0,153,51));  // medium green
   palette.addElement(new Color(102,0,0)); // dark red/brown
   palette.addElement(new Color(255,255,204)); // light yellow
   palette.addElement(new Color(255,153,0));  // orange
   palette.addElement(new Color(153,153,153)); // medium grey
   palette.addElement(new Color(153,51,204));  // purple
 }

// set -- interface to outside world
  public void setIconPixelColors(Color Colors[])
  {
   int i = 0;	
   for (int w = 0; w < totalWidthPixels; w++)
      for (int h = 0; h < totalHeightPixels; h++)
          if (Pixels[w][h].type == "icon")
             { 
             	Pixels[w][h].setColor(Colors[i]);
             	i++;
             	}
    repaint();
  }
 
// get -- interface to outside world
  public Color[] getIconPixelColors()
  {
  Color Colors[] = new Color[iconPixels*iconPixels];
  
   int i = 0;	
   for (int w = 0; w < totalWidthPixels; w++)
      for (int h = 0; h < totalHeightPixels; h++)
          if (Pixels[w][h].type == "icon")
             { 
             	Colors[i] = Pixels[w][h].getColor();
             	i++;
             	}
  return Colors;
  }
 

  public void actionPerformed(ActionEvent ae)
   {
/*      repaint();	*/
    }
  
  public void initPixels()
  {
  int w, h;
  // create instance of each Pixel
  for (w = 0; w < totalWidthPixels; w++)
    for (h = 0; h < totalHeightPixels; h++)
       {
       Pixels[w][h] = new Pixel();
   }
  // create four borders with registration marks for I/O lines
  // lefthand border
  for (w=0; w < borderPixels; w++)
    for (h=borderPixels; h < borderPixels + iconPixels; h++)
        {
       Pixels[w][h].type = "border";
       Pixels[w][h].setColor(Color.white); 
       }
  // righthand border
  for (w=borderPixels + iconPixels; w < totalWidthPixels; w++)
    for (h=borderPixels; h < borderPixels + iconPixels; h++)
        {
       Pixels[w][h].type = "border";
       Pixels[w][h].setColor(Color.white); 
        }
  // top border
  for (w=borderPixels; w < borderPixels + iconPixels; w++)
    for (h=0; h < borderPixels; h++)
        {
       Pixels[w][h].type = "border";
       Pixels[w][h].setColor(Color.white); 
        }
  // bottom border
  for (w=borderPixels; w < borderPixels + iconPixels; w++)
    for (h=iconPixels + borderPixels; h < totalHeightPixels; h++)
        {
       Pixels[w][h].type = "border";
       Pixels[w][h].setColor(Color.white); 
        }

// fill in registration marks, i.e., colored arrows representing Cin, Cout, Din, Dout. 
  // left border
  int arrowWidth = 4; // how many pixels it takes to draw an arrow, widthwise.
                                                  // (w, h, color)
  drawHorizontalArrowPointingRight(borderPixels-1,Cin,Color.red); // Cin left
  drawHorizontalArrowPointingRight(borderPixels-1,Din,Color.black); // Din left
  drawHorizontalArrowPointingLeft(borderPixels-1,Dout,Color.black); // Dout left
  drawHorizontalArrowPointingLeft(borderPixels-1,Cout,Color.red); // Cout left
  // top border
  drawVerticalArrowPointingUp(Cin, borderPixels-arrowWidth, Color.red); // Cin top
  drawVerticalArrowPointingUp(Din,borderPixels-arrowWidth,Color.black); // Din top
  drawVerticalArrowPointingDown(Dout,borderPixels-1,Color.black); // Dout top
  drawVerticalArrowPointingDown(Cout,borderPixels-1,Color.red); // Cout top
  // right border
  drawHorizontalArrowPointingRight(borderPixels+iconPixels-1+arrowWidth, Cin, Color.red); // Cin right
  drawHorizontalArrowPointingRight(borderPixels+iconPixels-1+arrowWidth,Din,Color.black); // Din right
  drawHorizontalArrowPointingLeft(borderPixels+iconPixels-1+arrowWidth,Dout,Color.black); // Dout right
  drawHorizontalArrowPointingLeft(borderPixels+iconPixels-1+arrowWidth,Cout,Color.red); // Cout right
  // bottom border
  drawVerticalArrowPointingUp(Cin, borderPixels+iconPixels, Color.red); // Cin bottom
  drawVerticalArrowPointingUp(Din,borderPixels+iconPixels,Color.black); // Din bottom
  drawVerticalArrowPointingDown(Dout,borderPixels+iconPixels-1+arrowWidth,Color.black); // Dout bottom
  drawVerticalArrowPointingDown(Cout,borderPixels+iconPixels-1+arrowWidth,Color.red); // Cout bottom

   // create color palette region beneath icon and on the left. 
   for (h = borderPixels*2 + iconPixels; h < totalHeightPixels; h++)
      { // repeat for several rows so that the colors are easier to see.
      Pixels[0][h].setColor(Color.black); 
      Enumeration pal = palette.elements();
      for (w = 1; w < totalWidthPixels; w++)
       {
       Pixels[w][h].type = "palette";
       if (pal.hasMoreElements())
          Pixels[w][h].setColor((Color) pal.nextElement());
       else
          Pixels[w][h].setColor(Color.black); 
        }// for w
      } // for h
   
/*   // drawing mode region beneath icon and on the right.
   for (w = totalWidthPixels/2; w < totalWidthPixels; w++)
     for (h = borderPixels*2 + iconPixels; h < totalHeightPixels; h++)
       {
       Pixels[w][h].type = "drawMode";
       Pixels[w][h].setColor(Color.yellow); // later create a palette vector. See p. 294
        }
 */
  // icon region starts off blank. Default pixel color is white.
  for (w = borderPixels; w < totalWidthPixels - borderPixels; w++)
    for (h = borderPixels; h < totalHeightPixels - borderPixels - menuPixels; h++)
       {
       Pixels[w][h].type = "icon";
       Pixels[w][h].setColor(Color.white);;
       }

  }
  
void drawHorizontalArrowPointingRight(int w,int h,Color c)
   {
   Pixels[w][h].setColor( c);
   Pixels[w-1][h].setColor(c);
   Pixels[w-1][h-1].setColor(c);
   Pixels[w-1][h+1].setColor(c);
   Pixels[w-2][h].setColor(c);
   Pixels[w-2][h-2].setColor(c);
   Pixels[w-2][h+2].setColor(c);
   Pixels[w-3][h].setColor(c);  
   } 

void drawHorizontalArrowPointingLeft(int w,int h,Color c)
   {
   Pixels[w][h].setColor(c);
   Pixels[w-1][h].setColor(c);
   Pixels[w-1][h-2].setColor(c);
   Pixels[w-1][h+2].setColor(c);
   Pixels[w-2][h].setColor(c);
   Pixels[w-2][h-1].setColor(c);
   Pixels[w-2][h+1].setColor(c);
   Pixels[w-3][h].setColor(c);  
   } 

void drawVerticalArrowPointingUp(int w,int h,Color c)
   {
   Pixels[w][h].setColor(c);
   Pixels[w-1][h+1].setColor(c);
   Pixels[w][h+1].setColor(c);
   Pixels[w+1][h+1].setColor(c);
   Pixels[w-2][h+2].setColor(c);
   Pixels[w][h+2].setColor(c);
   Pixels[w+2][h+2].setColor(c);
   Pixels[w][h+3].setColor(c);  
    } 

void drawVerticalArrowPointingDown(int w,int h,Color c)
   {
   Pixels[w][h].setColor(c);
   Pixels[w-1][h-1].setColor(c);
   Pixels[w][h-1].setColor(c);
   Pixels[w+1][h-1].setColor(c);
   Pixels[w-2][h-2].setColor(c);
   Pixels[w][h-2].setColor(c);
   Pixels[w+2][h-2].setColor(c);
   Pixels[w][h-3].setColor(c);  
    } 


public void setColorPalette(Vector v)
{  
  if  (v.size() > 256) System.out.println("only the first 256 colors of this palette will be used.");
  palette = v;  // from now on use the passed-in palette.
} // end createColorPalette

// do I need to override the update() method?
public void update(Graphics g) 
{
 paint(g);	
}
  public void paint(Graphics g) 
  {
     	
   Dimension area = getSize();  // get current size of drawing area. May not be square, but the user can make it look square to him/her.

   // create a buffer and draw on that
   Graphics gg;
   if (stopFlicker)
     {
      imageBuffer = createImage(area.width, area.height); // to avoid flickering image, draw on a buffer, then show it.
      g_buffer = imageBuffer.getGraphics(); // Get graphics object for buffer
      gg = g_buffer;
     }
   else
      gg = g;
      
   int pixelHeight = area.height/totalHeightPixels;
   int pixelWidth = area.width/totalWidthPixels;
   int w,h;
   int x1, x2, y1, y2;
   int startX, startY;
   int stopX, stopY;
   boolean outsideDrawableArea = false; // used as a control statement
   
   if (semClicked)  // semaphore avoids recycling of old clicks
     {
     // figure out which pixel the person clicked on
      w = clickInfo.x/pixelWidth;   
      h = clickInfo.y/pixelHeight; 
      if (w > totalWidthPixels || h > totalHeightPixels) outsideDrawableArea = true; // not inside drawable area.
      if (outsideDrawableArea == false)
         {
         //  System.out.println("pixel coords: (" + w + ", " + h  + ")");
         if (Pixels[w][h].type == "icon")
            {
            // draw it immediately, to give the user immediate feedback in the event of double buffering, which pauses until all pixels are drawn.
            Pixels[w][h].setColor(drawColor);
            g.setColor(drawColor);
            g.fillRect(w*pixelWidth, h*pixelHeight, pixelWidth, pixelHeight);  
            g.setColor(Color.lightGray);
            g.drawRect(w*pixelWidth, h*pixelHeight, pixelWidth, pixelHeight); 
            }
         else // if (Pixels[w][h].type == "palette" || "border")
            { // pick up that pixel's color. It's okay if it's an I/O line rather than a palette color.
            drawColor = Pixels[w][h].getColor();
          }
      }
    semClicked = false;
     } 

   if (outsideDrawableArea == false) // don't bother if outside drawable area.
     {
     if (drawMode == "rectangle")
        { // ready to draw a rectangle filling in the pixels from (x1, y1) to (x2, y2)
        // figure out which pixel the person clicked on
        x2 = clickInfo.x/pixelWidth;   
        y2 = clickInfo.y/pixelHeight; 
        x1 = clickInfo.oldX/pixelWidth;   
        y1 = clickInfo.oldY/pixelHeight; 
        startX = Math.min(x1,x2);
        stopX = Math.max(x1,x2);
        startY = Math.min(y1,y2);
        stopY = Math.max(y1,y2);
        for (w = startX; w <= stopX; w++)
           for (h = startY; h <= stopY; h++)
             {
             if (Pixels[w][h].type == "icon")
                Pixels[w][h].setColor(drawColor);
             }
        drawMode = "pixel";  // this keypress has been used up, so revert back to default setting.
        }
 
     else if (drawMode == "rectangleOutline" )
        { // ready to draw a rectangle outline whose extent is defined by the pixels from (x1, y1) to (x2, y2)
        // figure out which pixel the person clicked on
        x2 = clickInfo.x/pixelWidth;   
        y2 = clickInfo.y/pixelHeight; 
        x1 = clickInfo.oldX/pixelWidth;   
        y1 = clickInfo.oldY/pixelHeight; 
        startX = Math.min(x1,x2);
        stopX = Math.max(x1,x2);
        startY = Math.min(y1,y2);
        stopY = Math.max(y1,y2);
        // top edge
        for (w = startX; w <= stopX; w++)
           for (h = startY; h < startY+1; h++)
             {
             if (Pixels[w][h].type == "icon")
                Pixels[w][h].setColor(drawColor);
             }
        // bottom edge
        for (w = startX; w <= stopX; w++)
           for (h = stopY; h < stopY+1; h++)
             {
             if (Pixels[w][h].type == "icon")
                Pixels[w][h].setColor(drawColor);
             }
        // left edge
        for (h = startY; h <= stopY; h++)
           for (w = startX; w < startX+1; w++)
             {
             if (Pixels[w][h].type == "icon")
                Pixels[w][h].setColor(drawColor);
             }
        // right edge
        for (h = startY; h <= stopY; h++)
           for (w = stopX; w < stopX+1; w++)
             {
             if (Pixels[w][h].type == "icon")
                Pixels[w][h].setColor(drawColor);
             }
        drawMode = "pixel";  // this keypress has been used up, so revert back to default setting.
        } // end rectangleOutline

     else if (drawMode == "straightLine" )
        { // this is the only non-literal drawing routine. It corrects lines to multiples of 45 degrees.
        // figure out which pixel the person clicked on
        x2 = clickInfo.x/pixelWidth;   
        y2 = clickInfo.y/pixelHeight; 
        x1 = clickInfo.oldX/pixelWidth;   
        y1 = clickInfo.oldY/pixelHeight; 

        // the pixels that the user used to indicate this line may not be on it, so set their colors back to what they were before.
        if (Pixels[x1][y1].type == "icon")
           Pixels[x1][y1].setColor(Pixels[x1][y1].getPreviousColor());
        if (Pixels[x2][y2].type == "icon")
           Pixels[x2][y2].setColor(Pixels[x2][y2].getPreviousColor());
       
        int run = Math.abs(x2-x1);
        int rise =  Math.abs(y2-y1);
        float dy = (float) rise;
        float dx = (float) run;
//        System.out.println("rise/run = " + dy + "/" + dx);
        // look at rise/run. But don't divide, since denominator could be zero. Subset instead.
        if (dy < 0.5f * dx) // straighten line to a horizontal line.
           {
//            System.out.println("horizontal line");
            // draw a horizontal line from (x1,x2). Stop at x2, which might be negative direction.
            h = y1;
            if (x2 < x1) // draw leftward instead of rightward.
               for (w = x1; w >= x2; w--)
                  {
                  if (Pixels[w][h].type == "icon")
                     Pixels[w][h].setColor(drawColor);
                  }
            else 
               for (w = x1; w <= x2; w++)
                 {
                  if (Pixels[w][h].type == "icon")
                     Pixels[w][h].setColor(drawColor);
                 }
            }
        else if (dy > 1.5f * dx) // straighten line to a vertical line.
           {
//            System.out.println("vertical line");
            // draw a vertical line from (x1, y1). Stop at y2, which might be negative direction.
            w = x1;
            if (y2 < y1)  // draw upward instead of downward.
               for (h = y1; h >= y2; h--)
                   {
                  if (Pixels[w][h].type == "icon")
                     Pixels[w][h].setColor(drawColor);
                  }
            else
               for (h = y1; h <= y2; h++)
                  {
                  if (Pixels[w][h].type == "icon")
                     Pixels[w][h].setColor(drawColor);
                  }
            }
        else // straighten line to a 45 degree diagonal.
           {
//            System.out.println("diagonal line");
            // draw a line that rises and runs equally, starting at (x1,y1) and heading toward (x2,y2)
            if ((y1 < y2) && (x1 < x2))
               for (w = x1, h = y1; w <= x2; w++, h++)
                  {
                   if (Pixels[w][h].type == "icon")
                     Pixels[w][h].setColor(drawColor);
                   }
            else if ((y1 < y2) && (x1 > x2))
               for (w = x1, h = y1; w >= x2; w--, h++)
                  {
                   if (Pixels[w][h].type == "icon")
                     Pixels[w][h].setColor(drawColor);
                   }
            else if ((y1 > y2) && (x1 > x2))
               for (w = x1, h = y1; w >= x2; w--, h--)
                  {
                   if (Pixels[w][h].type == "icon")
                     Pixels[w][h].setColor(drawColor);
                   }
            else //if (y1 > y2 && x1 < x2)
               for (w = x1, h = y1; w <= x2; w++, h--)
                  {
                   if (Pixels[w][h].type == "icon")
                     Pixels[w][h].setColor(drawColor);
                   }
            }
     
       drawMode = "pixel";  // this keypress has been used up, so revert back to default setting.
       } // end straightLine
   } // end if insideDrawableArea
 
   // draw the contents of the window. The window basically contains "pixels," whose extent can cross multiple screen pixels.
   for (w = 0; w < totalWidthPixels; w++)
      for (h = 0; h < totalHeightPixels; h++)
         {
          gg.setColor(Pixels[w][h].getColor());
          gg.fillRect(w*pixelWidth, h*pixelHeight, pixelWidth, pixelHeight);  
        }
   // put outlines around certain cells to make it easier to draw and select cells.
   for (w = borderPixels; w < iconPixels + borderPixels; w++)
      for (h = borderPixels; h < iconPixels + borderPixels; h++)
         {
            gg.setColor(Color.lightGray);
            gg.drawRect(w*pixelWidth, h*pixelHeight, pixelWidth, pixelHeight); 
         }
  
   if (stopFlicker) // now that it's done, post the image to the screen
     {
     g.drawImage(imageBuffer, 0, 0, this);
}

   } // end paint


} // end class createIcon

class my_mouse_handling_class extends MouseAdapter
{
  createIcon main_class;

  public my_mouse_handling_class(createIcon arg)
  {
    main_class=arg;
  }

//  public void mouseClicked(MouseEvent e) {}
//  public void mouseEntered(MouseEvent e){}
//  public void mouseExited(MouseEvent e){}
//  public void mouseReleased(MouseEvent e){}
  public void mousePressed(MouseEvent e)
    {
    
    main_class.clickInfo.oldX = main_class.clickInfo.x;
    main_class.clickInfo.oldY = main_class.clickInfo.y;
    main_class.clickInfo.x=e.getX();
    main_class.clickInfo.y=e.getY();
    main_class.semClicked = true;
//    System.out.println("Pressed ");
    main_class.repaint();
    }
 
}

class MyKeyAdapter extends KeyAdapter
{
  createIcon main_class;

  public MyKeyAdapter(createIcon arg)
  {
    main_class=arg;
  }
  
  public void keyPressed(KeyEvent e)
  {
   switch (e.getKeyChar()) 
      {
      case 'f':
         main_class.drawMode = "rectangle";
         System.out.println("filled rectangle");
         break;
      case 'r':
         main_class.drawMode = "rectangleOutline";
         System.out.println("rectangle outline");
         break;
      case 'l':
         main_class.drawMode = "straightLine";
         System.out.println("straightLine");
         break;
      case 'p': 
         main_class.drawMode = "pixel";
         System.out.println("pixel");
         break;
      case 'b': 
         if (main_class.stopFlicker == true)
            {
            main_class.stopFlicker = false;
            System.out.println("turned off double buffering");
            }
         else
            {
            main_class.stopFlicker = true;
            System.out.println("turned on double buffering");
            }
         break;
      default:
         System.out.println("filled rectangle: f     rectangle outline: r     one pixel: p     straight line: l    toggle double buffering: b");
         break;
      }
  main_class.repaint();
   } // end keyPressed        

} // end class MyKeyAdapter

/* Iwindow_event_handler class here */
class Iwindow_event_handler extends WindowAdapter
{
  createIcon calling_window;

/* Constructor */
  public Iwindow_event_handler(createIcon arg)
  {
    calling_window=arg;
  }

  public void windowClosing(WindowEvent we)
  {
    calling_window.rootparent.ieup=false;
    calling_window.dispose(); /* %%% */
  }
}

/* structure for keeping more than one click around 
*/
class ClickInfo
{
  int x, y; // used if mode is "pixel"
  int oldX, oldY; // needed if mode is "rectangle" or "line" to determine its extent
  
  ClickInfo() 
  {
   this.x = -1;
   this.y = -1;
   this.oldX = -1;
   this.oldY = -1;
  }; // constructor
  
} // end class ClickInfo

/* represents one square region of a viewable area divided into 32 by 32 parts.
*/
class Pixel 
{
  String type; // membership: icon, border, palette, drawMode
  Color colorNow;
  Color colorOld;

  Pixel() 
  {
   this.type = "none";
   this.colorNow = Color.blue;
   this.colorOld = Color.blue;
  }; // constructor
  
  Pixel(String s, Color c) 
  {
   this.type = s;
   this.setColor(c);
   this.colorOld = Color.white;
  }; // constructor
  
  public void setColor(Color c)
  {
  this.colorOld = this.colorNow;
  this.colorNow = c;	
  }

  public Color getColor()
  {
   return this.colorNow;
  }

  public Color getPreviousColor()
  {
   return this.colorOld;
  }

} // end class pixel




