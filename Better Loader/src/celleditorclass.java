import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;

/*
 * celleditorclass - Display cell's icon and source code
 * User can modify source, then re-compile it.
 * SAVE replaces exsiting cell, SAVE AS write new cell/library
 * Editing a blank cell allowed too!
 */

class celleditorclass extends Frame
implements ActionListener
{
  loader rootparent; /* Main loader object */
  main_window parent; /* Window which called this edit window */
  iconclass iconcanvas;
  Image iconimg;
  TextArea ta;
  String loaded_cell="",loaded_library=""; /* Remember... */
  String loaded_iconbytes; /* Save/update this */
  int i;
  FontMetrics fm; // font metrics for this window

/* Constructor for empty editor window */
  celleditorclass(loader par,main_window par2)
  {
    super("");
    Font font=new Font("Serif",Font.PLAIN,14);
    setFont(font);
    fm = getFontMetrics(font);
    setBackground(Color.gray);
    setForeground(Color.black);
    rootparent=par;
    parent=par2;
    make_ce_window();
    setup_empty_cell();
  }

  void setup_empty_cell()
  {
    loaded_library="";loaded_cell="";
    ta.setText("");
/* Set loaded_iconbytes to blank icon */
    loaded_iconbytes="";
    for (i=0;i<128;i++){
      loaded_iconbytes=loaded_iconbytes+"00000000";
    }

/* Add image of icon to iconcanvas */
    int i,j,iarray[];
    iarray=new int[1024];

    for (i=0;i<1024;i++){
      j=((int) loaded_iconbytes.charAt(i))-'0';
      iarray[i]=0xff000000;
      if(0!=(j&4)) iarray[i]|=0xff;
      if(0!=(j&2)) iarray[i]|=0xff00;
      if(0!=(j&1)) iarray[i]|=0xff0000;
    }
    iconimg=createImage(new MemoryImageSource(32,32,iarray,0,32));
    iconcanvas.seticon(iconimg);
    iconcanvas.repaint();
    setTitle("[New Cell]");
  }

/* Constructor for editing initial cell */
  celleditorclass(loader par,main_window par2,String library,String cell)
  {
    super(); /* load_cell will set titlebar */
    setFont(new Font("Serif",Font.PLAIN,14));
    setBackground(Color.gray);
    setForeground(Color.black);
    rootparent=par;
    parent=par2;
    make_ce_window();
    load_cell(library,cell);
  } /* End of constructor */

/* Method to load a cell into the edit window */
  void load_cell(String library,String c)
  {

    String srot;
    String src,cell;
    int i,j;

    i=c.indexOf("\002");
    srot=c.substring(i+1); /* String form of rotation */
    cell=c.substring(0,i);

/* Set title bar */
    setTitle(library+"/"+cell);

/* Retrieve the cell data for this cell */
    celldataclass cd=(celldataclass) rootparent.cellHash.get(library + "\002" + cell);
    loaded_iconbytes=cd.iconbytes;

/* Add image of icon to iconcanvas */
    int iarray[];
    iarray=new int[1024];

    for (i=0;i<1024;i++){
      j=((int) cd.iconbytes.charAt(i))-'0';
      iarray[i]=0xff000000;
      if(0!=(j&4)) iarray[i]|=0xff;
      if(0!=(j&2)) iarray[i]|=0xff00;
      if(0!=(j&1)) iarray[i]|=0xff0000;
    }
    iconimg=createImage(new MemoryImageSource(32,32,iarray,0,32));
    iconcanvas.seticon(iconimg);
    iconcanvas.repaint();


/* Now add the text */
    src=cd.src; /* Source text */
/* Convert \002 to newlines */
    while ((i=src.indexOf("\002")) >= 0){
      src=src.substring(0,i)+"\n"+src.substring(i+1);
    }
    ta.setText(src);

// position cursor at beginning
    ta.setSelectionStart(0);
    ta.setSelectionEnd(0);
    ta.setCaretPosition(0);

    loaded_cell=cell;loaded_library=library; /* Remember these */
  }

/* Method to place panels/etc. in window */
  void make_ce_window()
  {
/* Window listener for closing */
    addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent we){
        dispose();
      }
    }); /* End of window listener */    

  setLocation(parent.getLocation().x+50,parent.getLocation().y+50);

/* Panel for icon */
    Panel iconpanel=new Panel();
    iconcanvas=new iconclass(rootparent);
    iconcanvas.setSize(33,33);
    iconpanel.add(iconcanvas);

/* Panel for text */
    Panel textpanel=new Panel();
    ta=new TextArea(10,40);  // rows and columns!
    textpanel.add(ta);


/* Now place these in window */
    add(iconpanel,"West");
    add(textpanel,"Center");

/* Add menus to this window */
    MenuBar mb=new MenuBar();
    setMenuBar(mb);
    mb.setFont(new Font("Serif",Font.PLAIN,14));

    Menu mfile=new Menu("File");
    mb.add(mfile);	/* Add menu to menubar */

    MenuItem main_new=new MenuItem("New",new MenuShortcut(KeyEvent.VK_N));
    main_new.addActionListener(this);
    mfile.add(main_new);

    MenuItem main_open=new MenuItem("Open Cell",new MenuShortcut(KeyEvent.VK_O));
    main_open.addActionListener(this);
    mfile.add(main_open);

    MenuItem main_save=new MenuItem("Save Cell",new MenuShortcut(KeyEvent.VK_S));
    main_save.addActionListener(this);
    mfile.add(main_save);

    MenuItem main_saveas=new MenuItem("Save Cell As",new MenuShortcut(KeyEvent.VK_A));
    main_saveas.addActionListener(this);
    mfile.add(main_saveas);

    MenuItem main_delete=new MenuItem("Delete Current Cell");
    main_delete.addActionListener(this);
    mfile.add(main_delete);

    MenuItem main_ie=new MenuItem("Icon Editor");
    main_ie.addActionListener(this);
    mfile.add(main_ie);

    MenuItem main_load=new MenuItem("Load Icon",new MenuShortcut(KeyEvent.VK_E));
    main_load.addActionListener(this);
    mfile.add(main_load);

    MenuItem main_saveicon=new MenuItem("Save Icon",new MenuShortcut(KeyEvent.VK_I));
    main_saveicon.addActionListener(this);
    mfile.add(main_saveicon);

    MenuItem main_close=new MenuItem("Close");
    main_close.addActionListener(this);
    mfile.add(main_close);

    pack();  // make it all purdy...
  }

/* Menu action handlers */
  public void actionPerformed(ActionEvent ae){
    String cell,key;
    int i,j;

    parent.parent.msg("Sel="+ae.getActionCommand());

    if (ae.getActionCommand() == "New") {
      setup_empty_cell();
    }

    if (ae.getActionCommand() == "Open Cell") {
      cellselectclass csc=new cellselectclass(rootparent,parent,rootparent.libroot);
      if ((csc.getlib().length() > 0) && (csc.getcell().length() > 0)){
/* Add rotation to cell */
        cell=csc.getcell()+"\002"+"0";
        load_cell(csc.getlib(),cell);
        requestFocus();
      }
    }

    if ((ae.getActionCommand() == "Save Cell") ||
        (ae.getActionCommand() == "Save Cell As")) {
      if ((ae.getActionCommand() == "Save Cell As") || (loaded_library.length() <= 0)){
/* Get a library and cell here */
        newcellselectclass ncsc=new newcellselectclass(rootparent,parent,rootparent.libroot);
        loaded_cell=ncsc.getcell();
        loaded_library=ncsc.getlib();
        if ((loaded_cell.length() <= 0) || (loaded_library.length() <= 0)){
          loaded_cell="";loaded_library="";
        }
        requestFocus();
      }

/* Save current cell under loaded_library/loaded_cell */
      if ((loaded_cell.length() > 0) && (loaded_library.length() > 0)){
        key=loaded_library+"\002"+loaded_cell;
        if (!rootparent.lib_loaded_hash.containsKey(loaded_library)){ /* First load this library into hash */
           parent.loadcelllibrary(loaded_library); /* Load into cache */
        }
        setTitle(loaded_library+"/"+loaded_cell);

/* Now store the (new) cell in the hash */      
        celldataclass celldata=new celldataclass();
        celldata.name=loaded_cell;
        celldata.library=loaded_library;
        celldata.desc="";
        celldata.cellTT="00000000000000000000000000000000";
        celldata.src=ta.getText();
        celldata.iconbytes=loaded_iconbytes;

/* Now compile the code */
        if (1==celldata.compile()){
          rootparent.cellHash.put(key,celldata);
/* This could be a re-write or a new cell. Either way, re-write the .LIB file */
          parent.writecelllibrary(loaded_library);
          parent.parent.update_all();
        } else {
          new msgbox("Compile error-cell not saved").show();
        }
      } /* else no cell was selected! */
    }

    if ((loaded_library.length() >= 0) && (loaded_cell.length() >= 0) && 
        (ae.getActionCommand() == "Delete Current Cell")){ /* Remove from hash and writethru */
      generaldialog gddel=new generaldialog("Deleting Cell","Are you sure you want to delete " + loaded_cell + "?");
      gddel.show();
      if (gddel.ok()){ /* Delete it */
        rootparent.cellHash.remove(loaded_library+"\002"+loaded_cell);
        parent.writecelllibrary(loaded_library);
        parent.parent.update_all();
      }
    }

    if (ae.getActionCommand() == "Save Icon") {
      if (!rootparent.ieup){
        rootparent.ie=new createIcon(rootparent);
      }

      Color c[]=new Color[1024]; // Output color array

      int rr,cc,iout;
      int rval,gval,bval;
      iout=0;
      for (rr=0;rr<32;rr++){
        for (cc=0;cc<32;cc++){
          i=32*cc+rr;  // Index into iconbytes[] (Row/Col swap just happened)
          j=Integer.parseInt(loaded_iconbytes.substring(i,i+1)); // "0"-"7"
          rval=((j&1)==0)?0:255;
          gval=((j&2)==0)?0:255;
          bval=((j&4)==0)?0:255;
          c[iout++]=new Color(rval,gval,bval);
        }
      }
      rootparent.ie.setIconPixelColors(c);
    }


    if (ae.getActionCommand() == "Icon Editor") {
      if (!rootparent.ieup){
        rootparent.ie=new createIcon(rootparent);
      }
    }

    if (ae.getActionCommand() == "Load Icon") {
      if (!rootparent.ieup){
        rootparent.ie=new createIcon(rootparent);
      }
      Color c[]=rootparent.ie.getIconPixelColors();
      String newib; // New icon byte string
      newib="";

      int rr,cc;
      for (rr=0;rr<32;rr++){
        for (cc=0;cc<32;cc++){
          i=32*cc+rr;
          j=(c[i].getRed() > 127)?1:0;
          j+=(c[i].getGreen() > 127)?2:0;
          j+=(c[i].getBlue() > 127)?4:0;
          newib=newib+j;
        }
      }
      loaded_iconbytes=newib;

/* Now re-draw icon in editor window */

/* Add image of icon to iconcanvas */
      int iarray[];
      iarray=new int[1024];

      for (i=0;i<1024;i++){
        j=((int) loaded_iconbytes.charAt(i))-'0';
        iarray[i]=0xff000000;
        if(0!=(j&4)) iarray[i]|=0xff;
        if(0!=(j&2)) iarray[i]|=0xff00;
        if(0!=(j&1)) iarray[i]|=0xff0000;
      }
      iconimg=createImage(new MemoryImageSource(32,32,iarray,0,32));
      iconcanvas.seticon(iconimg);
      iconcanvas.repaint();
    }

    if (ae.getActionCommand() == "Close") {
      setVisible(false);
    }
  }
}
