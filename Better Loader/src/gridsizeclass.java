import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;

class gridsizeclass extends Dialog /* show/change grid size */
implements ActionListener
{
  int NEWR,NEWC; /* Selected # of rows and columns */
  TextField rtext,ctext;

/*
 * Constructor. User selects a library, then gets a dropdown list
 * of the cells in that library
 */
  gridsizeclass(main_window parent,int rows,int cols) /* Pass in initial size */
  {
    super(parent,"Enter grid size",true);

    setFont(new Font("Serif",Font.PLAIN,14));
    setBackground(Color.gray);
    setForeground(Color.black);

    NEWR=rows;NEWC=cols;

    setLayout(new BorderLayout());

    setLocation(parent.getLocation().x+50,parent.getLocation().y+50);

/* Window management */
    addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent we){
        dispose();
      }
    });

    Panel p1=new Panel();
    Button b1=new Button("OK");
    b1.addActionListener(this);
    p1.add(b1);

    Panel p2=new Panel();
    p2.add(new Label("Rows"));
    rtext=new TextField(""+NEWR,4);
    p2.add(rtext);
    rtext.addActionListener(this);

    Panel p3=new Panel();
    p3.add(new Label("Cols"));
    ctext=new TextField(""+NEWC,4);
    p3.add(ctext);
    ctext.addActionListener(this);

    add(p2,"North");
    add(p3,"Center");
    add(p1,"South");
    
    this.pack();
    this.show(); /* Modal-won't return until user has made selection */

  }

  public void actionPerformed(ActionEvent ae)
  {
    NEWR=Integer.parseInt(rtext.getText());
    NEWC=Integer.parseInt(ctext.getText());
    dispose();
  }

  public int getNEWR()
  {
    return(NEWR);
  }

  public int getNEWC()
  {
    return(NEWC);
  }
}
