import java.util.*;

class celldataclass
{
  public String name;
  public String library;
  public String desc;
  public String cellTT;
  public String iconbytes;
  public String src;

/*
 * compile() method compiles src into cellTT and desc.
 * Returns 1 for success, 0 for failure
 */

  public int compile()
  {
    String tmp_desc=""; /* Stores description string */
    String cd,dir; /* C/D and N/S/W/E */
    String temp;
    boolean doing_desc=false; /* Set when we're parsing a .DESC ... .END block */
    int i,ind;
    int program[]; /* Ordered same as characters in cellTT */
    int result[]; /* Result of tabulate() call */
    int cnum;

    tmp_desc="";

    program=new int[32];
    result=new int[16];

/* Initialize outputs */
    for (i=0;i<32;i++) program[i]=0;

/* Tokenize the src input and parse each line */
    StringTokenizer st=new StringTokenizer(src,"\n");
    while (st.hasMoreElements()) {
      String line=st.nextToken();
      if (line.indexOf(";") >= 0) line=line.substring(0,line.indexOf(";"));
      line=line.toUpperCase();
      String newline="";
      for (i=0;i<line.length();i++) {
        if (!Character.isWhitespace(line.charAt(i)))
          newline=newline+line.substring(i,i+1);
      } /* line is conditioned now */

      if (newline.equals(".DESC")) {
        doing_desc=true;
      } else if (newline.equals(".END")) {
        doing_desc=false;
      } else if (doing_desc) {
        tmp_desc=tmp_desc+newline+"\n";
      } else if (newline.length() == 36){

        if (newline.substring(0,4).equals(".H3X")){ // V3 HEX!
          temp=newline.substring(4,36);
          cellTT="";
          for (i=0;i<31;i+=2){
            cellTT=cellTT+temp.charAt(i)+temp.charAt(i+1);
          }
          desc=tmp_desc+"\n<<<PRESERVEORDER>>>\n";;

// We've specified a .HEX directive that will be used by a V3 simulator
// But since this loader program outputs V2 TTs, a V3 simulator
// will do some bit twiddling, which wrecks the bit pattern we
// specified in the .HEX statement.
// So we do 2 things:
// (1) avoid all bit manipulations - store the straight pattern in the
//     binary file; and
// (2) store the special tag <<<PRESERVEORDER>>> in the cells' description
//     which V3 (and future) simulators intercept and interpret as
//     "Leave the bit order as it is."

          return(1);
        } else if (newline.substring(0,4).equals(".HEX")){
          temp=newline.substring(4,36);
// Want to change ordering so .HEX abcd means bits shifted out are abcd
// Set cellTT[i]=temp[30-i] (i even)
// cellTT[i]=temp[32-i] (i odd)
          cellTT="";
          for (i=0;i<31;i+=2){
            cellTT=cellTT+temp.charAt(30-i)+temp.charAt(31-i);
          }
          desc=tmp_desc;
          return(1);
        }

      } else if (newline.length() >= 4) { /* Compile line */
        if (!newline.substring(2,3).equals("=")) return(0);
        cd=newline.substring(0,1);
        dir=newline.substring(1,2);
        newline=newline.substring(3);

/* find index of column */
        if (dir.equals("E")) ind=0; else if (dir.equals("W")) ind=1;
        else if (dir.equals("S")) ind=2; else if (dir.equals("N")) ind=3;
        else return(0);

        if (cd.equals("C")) ind+=4; else if (!cd.equals("D")) return(0);

/* get TT column */
        if (0==tabulate(newline,result)) return(0); /* Return on fail */

/* store this row in program[] */
        for (i=0;i<16;i++) {
          /* Character is at position i*2, +1 if ind<4 */
          /* Bit position is 1<<(ind%4) */
          /* Set the bit if result[i]=1 */
          if (result[i]==1) {
            cnum=i*2;if (ind < 4) cnum=cnum+1; /* Character number */
            program[cnum]|=(1<<(ind%4));
          } /* end of SET BIT code */
        } /* End of for loop */
      } /* end of compile line */
      else if (newline.length() > 0) { /* Short line but not blank */
        return(0);
      } /* End of massive IF on conditioned line */
    } /* End of WHILE loop */

/* Save program[] to cellTT */
    cellTT="";
    for (i=0;i<32;i++){
      cellTT=cellTT+Integer.toHexString(program[i]);
    }
    desc=tmp_desc;
    return(1);
  } /* End of compile() */




  int val[]; /* Stack */
  int sp; /* Stack pointer */
  boolean fail; /* Set if we fail somewhere */

/* tabulate() returns an array of 16 TT values */
  int tabulate(String str, int[] result)
  {

/* ljkd's TABULATE.C                                                     */
/*     -  REVERSE POLISH BITWISE CALCULATOR FOR NICK'S TRUTH TABLES      */
/* Polish calculator from K&R modified to do bitwise operations          */
/* Reverse Notation required: write DN := ~E N|S  as DN := E! NS|        */
/*     ( or (E!)(NS|) ). Any spaces are ignored.                         */
/* INPUT: takes the symbolic RHS of the statement above -----^           */
/* as subprocedure, takes this RHS as a one-line argument "input"        */
/* OUTPUT: the boolean value of the RHS for all possible values of E, N, &S */
/*         in the form of an array called result.                        */
/* Program can be used interactively. To see partial results, put parens */
/* around subexpressions.                                                */
/* Modified by NJM on Oct 11, 2000 for Java                              */

    int type, i, j;
    val=new int[100];
    fail=false; /* OK for now */

    for (i = 0; i < 16; ++i) {  
      sp=0; /* Empty stack */
      for(j=0;j<str.length();j++){ /* Character by character */
        switch(str.charAt(j)){
          case 'W':if ((i&1) == 0) push(0); else push(1);break;
          case 'E':if ((i&2) == 0) push(0); else push(1);break;
          case 'N':if ((i&4) == 0) push(0); else push(1);break;
          case 'S':if ((i&8) == 0) push(0); else push(1);break;

          case '&':push(pop() & pop());break;

          case '(':break;
          case ')':break;

          case '+':
          case '|':push(pop() | pop());break;

          case '!':
          case '~':push(1-pop());break;

          case'^':push(pop() ^ pop());break;

          case '0':push(0);break;
          case '1':push(1);break;

          default:System.out.println("Parse error");
          return(0);
        }
      } /* Entire line parsed */
      result[i]=(int) (pop()); /* Final expression value */
      if (sp != 0) return(0); /* bad expression */
    } /* End of all 16 rows */
    if (fail) return(0); /* Failure */
    return(1); /* Success! */
  } /* End of tabulate() */

  void push(int i)
  {
    if (sp > 99) fail=true;
    if (fail) return;
    val[sp++]=i;
  }

  int pop()
  {
    if (sp == 0) fail=true;
    if (fail) return(0);
    return(val[--sp]);
  }


} /* End of class celldataclass */
