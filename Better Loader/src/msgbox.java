import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;

class msgbox extends Dialog
implements ActionListener /* for button */
{

/*** CONSTRUCTOR ***/
  msgbox(String title)
  {
    super(new Frame(),"Attention!",true); /* Make a modal dialog */

/* Now we add the elements to the cell-selector window */
    setLayout(new BorderLayout());

    Panel p1=new Panel();
/* Add buttons */
    Button b1=new Button("OK");
    b1.addActionListener(this);
    p1.add(b1);

    Panel p3=new Panel();p3.setLayout(new BorderLayout());
    p3.add(new Label(title),"Center");

/* Now place these in window */
    add(p3,"North");
    add(p1,"South");

    this.setSize(220,125);

    repaint();
    show();

    addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent we){
        dispose();
      }
    });

  } /* End of constructor */

  public void actionPerformed(ActionEvent ae){
    dispose();
  }

} /* End of msgbox */
