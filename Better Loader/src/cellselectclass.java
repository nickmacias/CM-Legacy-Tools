import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;

class cellselectclass /* extends Frame*/
{
  boolean DOS=false;
  String root; /* Root of all cell libraries */
  String library; /* Selected library */
  String cell; /* Selected cell */
  FileDialog fd; /* Main dialog */
  loader rootparent;
  main_window parent;

/*
 * Constructor. User selects a library, then gets a dropdown list
 * of the cells in that library
 */
  cellselectclass(loader par,main_window par2,String r)
  {
    rootparent=par;
    parent=par2;
    root=r;
    cell="";
    if (parent.last_cell_lib.length()>0){
      library=parent.last_cell_lib;
    } else {
      select_lib_dialog();
    }
    if (library.length() <= 0) return;

 /* Next we need a dropdown box of the cells in the given library */
 /* Make a modal dialog for displaying the list and buttons */

    cellselectdialog d=new cellselectdialog(this,true);
    d.show(); /* Modal-won't return until user has made selection */

/* Now the user has made their selection...all the variables should be set */
  }

  void select_lib_dialog()
  {
    library="";
    fd=new FileDialog(rootparent,"Library Select",FileDialog.LOAD);
    fd.setDirectory(root);

    fd.setLocation(parent.getLocation().x+50,parent.getLocation().y+50);
    fd.show();
    library=fd.getFile();
    if (library == null) {library="";return;} /* Cancel? */
    library=fd.getDirectory() + library;
    if (DOS) library=library.toUpperCase();

/* Pick out end of pathname to get actual library */
    if (!rootparent.libroot.equals(library.substring(0,rootparent.libroot.length()))){
      System.out.println("Can not move above root!");
      return;
    }
    library=library.substring(rootparent.libroot.length());
    parent.last_cell_lib=library;

    parent.loadcelllibrary(library); /* Load into hash if not already there */
  }

  public String getlib()
  {
    return(library);
  }

  public String getcell()
  {
    return(cell);
  }
}
