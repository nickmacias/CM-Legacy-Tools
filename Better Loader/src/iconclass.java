import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;


/*
 * Make an iconcanvas class here for drawing an icon in the cell editor
 */

class iconclass extends Canvas
{
  Image iconimage;
  loader rootparent;
  boolean goticon=false;

  iconclass(loader par) /* Constructor-saves image */
  {
    goticon=false;
  }

  public void seticon(Image i)
  {
    iconimage=i;
    goticon=true;
  }

  public void paint(Graphics g)
  {
    if (goticon){
      g.drawImage(iconimage,0,0,rootparent);
    } else {

    }
  }
}
