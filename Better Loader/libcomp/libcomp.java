import java.util.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Graphics;

/*
 * libcomp is the main class
 */

public class libcomp extends Frame 
{
  String libroot;

  public static void main(String args[])
  {
    if (args.length != 1){
      System.out.println("Usage: libcomp inputfilename");
      return;
    }
    libcomp c=new libcomp(args[0]);
/* pause here */
    System.out.print("Hit enter to finish: ");
    BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    try{br.readLine();}catch(Exception e){};
  }

/* A few global variables */

  String library_path,library_name; /* Remember these */
  int ROWS,COLS; /* These too */
  int bg_color,border_color,text_color; /* 3 bits - WEAK! */
  celldataclass lib[][];
  int current_r,current_c;

  int BGCOLOR=0,BORDERCOLOR=(-1),TEXTCOLOR=0;


/* Constructor for this class */
  libcomp(String name)
  {
    String buffer,cbuf;

/* make flags */
    boolean doing_cell=false; /* SET when we're inside a cell def */
    ROWS=0;
    library_name="";

/* Remember next cell to compile */
    current_r=0;current_c=(-1);

libroot="C:\\NICK'S STUFF\\JAVA\\LOADER\\LIBRARIES\\"; /* GET FROM PARENT */

    if (!myopen(name)) return;
    while (more()){
      buffer=next();
      cbuf=condition(buffer); /* Cleaned up */
      //System.out.println("> " + cbuf);

/* Check for .START directive */
      if (check_start(cbuf)){
        doing_cell=false;
        System.out.print("Compiling " + library_path + library_name + "...");
        continue;
      }

/* Check text/color directrives */
      if (check_tc(cbuf)){
        continue;
      }

/* check for .IO */
      if (check_io(cbuf)){
        continue;
      }

/* Check for .SIZE directive */
      if (check_size(cbuf)){
        doing_cell=false;
        make_empty_lib();
        continue;
      }

/* .FINISH? */
      if (check_finish(cbuf)){
        doing_cell=false;
        do_finish();
        continue;
      }

/* Check for .CELL directive */
      if (check_cell(cbuf)){ /* Inside a cell here */
        doing_cell=true; /* start storing source code */
        if (++current_c >= COLS){
          current_c=0;
          if (++current_r >= ROWS){
            System.out.println("ERROR: Too many cell definitions for" +
                               ROWS + " x " + COLS + " grid.");
            continue;
          }
        } /* current_* are updated now */
        set_bg(); /* Set the cell's background using *current* BGCOLOR */
        continue;
      }

/* Not a recognized directive here. Is it source code? */
      if (doing_cell){
        lib[current_r][current_c].src=lib[current_r][current_c].src+
                                      buffer+"\n";
        continue;
      }
      if (cbuf.length() > 0) System.out.println("Syntax error: " + buffer);
    }
    myclose();
    //do_finish();
  }

/* Create empty library[][] structure */
  void make_empty_lib()
  {
    int r,c,k;
    lib=new celldataclass[ROWS][COLS];

    for (r=0;r<ROWS;r++){
      for (c=0;c<COLS;c++){ /* This initialization should be in
                               class instantiator (but it isn't) */
        lib[r][c]=new celldataclass();
        lib[r][c].name="CELL_"+r+"_"+c;
        lib[r][c].library=""; /* Not needed */
        lib[r][c].desc="";
        lib[r][c].cellTT="0000000000000000";
        lib[r][c].iconbytes="";
        lib[r][c].src="";
        for (k=0;k<64;k++) lib[r][c].iconbytes=
                             lib[r][c].iconbytes+"0000000000000000";
      }
    }
  }

/* Check for .CELL */
  boolean check_cell(String cbuf)
  {
    int r,c;

    if (cbuf.length() >= 5){
      if (cbuf.substring(0,5).equals(".CELL")){
        cbuf=cbuf.substring(5);
        if (cbuf.length()==0) return(true);
        try {
          r=Integer.parseInt(cbuf.substring(0,cbuf.indexOf(",")));
          c=Integer.parseInt(cbuf.substring(1+cbuf.indexOf(",")));
          current_r=r;current_c=c-1; /* We'll increment this soon */
          return(true);
        } catch (Exception e){ /* Not specifying [r,c] */
          System.out.println("Syntax error in .CELL: " + e);
          return(true);
        }
      }
    }
    return(false);
  }

/* Check for .FINISH */
  boolean check_finish(String cbuf)
  {
    if (ROWS <= 0) return(false); /* No size set */
    if (cbuf.length() >= 7){
      if (cbuf.substring(0,7).equals(".FINISH")) return(true);
    }
    return(false);
  }

/* Check for .SIZE */
  boolean check_size(String cbuf)
  {
    if (cbuf.length() > 5){
      if (cbuf.substring(0,5).equals(".SIZE")){
        cbuf=cbuf.substring(5);
        if (ROWS > 0){
          System.out.println("Size has already been set");
          return(true);
        }
        try {
          ROWS=Integer.parseInt(cbuf.substring(0,cbuf.indexOf(",")));
          COLS=Integer.parseInt(cbuf.substring(1+cbuf.indexOf(",")));
          return(true);
        } catch (Exception e){
          System.out.println("Parse error on " + cbuf + ": " + e);
          return(false);
        }
      }
    }
    return(false);
  }

/* Routine to check for .START directive */
  boolean check_start(String cbuf)
  {
    String path,name; /* Build into these */

    if (cbuf.length() > 6){
      if (cbuf.substring(0,6).equals(".START")){
/* Break line into pieces */
        cbuf=cbuf.substring(6);
        path="";
        while (cbuf.indexOf("/") >= 0){
          path=path+cbuf.substring(0,cbuf.indexOf("/"))+File.separatorChar;
          cbuf=cbuf.substring(cbuf.indexOf("/")+1);
        }
/* Parse cbuf into name,rows,cols */
        name=cbuf;
        library_path=path;
        library_name=name;
        return(true);
      }
    }
    return(false);
  }


/*
 * My file input routines
 */
BufferedReader br;
  boolean myopen(String fname) /* 0=success */
  {
    try {
      FileReader fr=new FileReader(fname);
      br=new BufferedReader(fr);
      return(true);
    } catch (Exception e){
      System.out.println("File open error:" + e);
      return(false);
    }
  }

  String nextline;

  boolean more() /* true if more input available */
  {
    int i;

    try {
      nextline=br.readLine();
      return(nextline != null);
    } catch (Exception e){
      System.out.println("File read error:" + e);
      return(false);
    }
  }

  String next() /* Return next string (up to \n) */
  {
    return(nextline);
  }

  void myclose() /* Close the input stream */
  {
    try {
      br.close();
    } catch (Exception e){
    }
  }

/*
 * Input string conditioner
 */
  String condition(String in)
  {
    int i;
    String out;

    in=in.toUpperCase();
    out="";
    for (i=0;i<in.length();i++){
      if (!Character.isWhitespace(in.charAt(i))){
        if (in.substring(i,i+1).equals(";")) return(out);
        out=out+in.substring(i,i+1);
      }
    }
    return(out);
  }

/* Routine to process data at end of input */
  void do_finish()
  {
    int r,c,i,k;
    int loop; /* Write library twice! */
    FileWriter fw;
    boolean syn_err=false;
    int badr,badc;
    String news;

    current_c=(-1);current_r=0; /* Regardless of what follows */

    badr=badc=0;
    for (r=0;r<ROWS;r++){
      for (c=0;c<COLS;c++){
        if (0==lib[r][c].compile()){
          if (!syn_err){badr=r;badc=c;syn_err=true;}
        }
      }
    }
    if (syn_err){ /* Give up here */
      System.out.print("One or more compile errors (first one at ["
                          + badr + "," + badc + "]): ");
      System.out.println("Will not update library");
      ROWS=0;
      return;
    }

/* Now set the border */
    if (BORDERCOLOR >= 0){
      for (r=0;r<ROWS;r++){
        for (i=0;i<32;i++){
          lib[r][0].iconbytes=
            lib[r][0].iconbytes.substring(0,32*i)+BORDERCOLOR+
            lib[r][0].iconbytes.substring(32*i+1);
          lib[r][COLS-1].iconbytes=
            lib[r][COLS-1].iconbytes.substring(0,32*i+31)+BORDERCOLOR+
            lib[r][COLS-1].iconbytes.substring(32*i+32);
        }
      }

      news=""; /* Just to make the compiler happy! */
      for (c=0;c<COLS;c++){
        for (i=0;i<32;i++){
          news="";
          for (k=0;k<32;k++) news=news+BORDERCOLOR;
          news=news+lib[0][c].iconbytes.substring(32);
        }
        lib[0][c].iconbytes=news;
      }

      for (c=0;c<COLS;c++){
        for (i=0;i<32;i++){
          news="";
          for (k=0;k<32;k++) news=news+BORDERCOLOR;
          news=lib[ROWS-1][c].iconbytes.substring(0,32*31) + news;
        }
        lib[ROWS-1][c].iconbytes=news;
      }
    } /* but totally skip border setup if BORDERCOLOR<0 */

/* Now create the library */
for (loop=0;loop<2;loop++){
    try
    {
      if (loop==0){
        fw=new FileWriter(libroot+library_path + library_name + ".LIB");
      } else {
        fw=new FileWriter(library_name + ".LIB");
      }
      BufferedWriter out=new BufferedWriter(fw);
      String line;
      String name,desc,tt,src,iconbytes;

      for (r=0;r<ROWS;r++){
        for(c=0;c<COLS;c++){
          celldataclass celldata=lib[r][c];
            out.write(celldata.name);out.newLine();
    
            desc="";
            StringTokenizer st=new StringTokenizer(celldata.desc,"\n");
            while (st.hasMoreTokens()){
              desc=desc+st.nextToken()+"\002";
            }
            out.write(desc);out.newLine();
            out.write(celldata.cellTT);out.newLine();
    /* Fix up SRC for clean writing-change \n to \002 */
            src="";
            st=new StringTokenizer(celldata.src,"\n");
            while (st.hasMoreTokens()){
              src=src+st.nextToken()+"\002";
            }
            out.write(src);out.newLine();
            out.write(celldata.iconbytes);out.newLine();
          }
        }
        out.close();	/* Done with file */
    } catch (Exception e){
      System.out.println("Failure while creating library: "+e);
    }
} /* End of two-spot */


/* and create the GRID file */
    try
    {
      fw=new FileWriter(library_name + ".GRD");
      BufferedWriter out=new BufferedWriter(fw);

      out.write("GRID_FILE\n" + ROWS + "\n" + COLS + "\n");

      for (r=0;r<ROWS;r++){
        for(c=0;c<COLS;c++){
          out.write(library_path + library_name + ".LIB" + "\002" +
                    "CELL_" + r + "_" + c + "\002" + "0\n"); 
          }
        }
        out.close();	/* Done with file */
    } catch (Exception e){
      System.out.println("Failure while creating grid file: "+e);
    }

    ROWS=0;
    System.out.println("Done\n");
  }

/* See if we've got a text or color directive */
  boolean check_tc(String cbuf)
  {
    cbuf=cbuf+"              "; /* Simplifies checks */
    try {
      if (cbuf.substring(0,3).equals(".BG")){
        BGCOLOR=7&Integer.parseInt(condition(cbuf.substring(3)));
        return(true);
      }

      if (cbuf.substring(0,7).equals(".BORDER")){
        BORDERCOLOR=7&Integer.parseInt(condition(cbuf.substring(7)));
        return(true);
      }

      if (cbuf.substring(0,5).equals(".TEXT")){ /* May specify color or text */
        cbuf=cbuf.substring(5);
        if (cbuf.indexOf(",") >= 0){ /* Two args: color,text */
          TEXTCOLOR=7&Integer.parseInt(condition(cbuf.substring(0,cbuf.indexOf(","))));
          do_text(cbuf.substring(cbuf.indexOf(",")+1));
          return(true);
        } /* Only one argument here */
        if (cbuf.substring(0,1).equals("\"")){ /* Text */
          do_text(cbuf);
          return(true);
        }
/* Here, just a number */
        TEXTCOLOR=7&Integer.parseInt(condition(cbuf));
        return(true);
      }
    } catch (Exception e){
      System.out.println("Syntax error: " + e);
      return(false);
    }
    return(false);
  }

/* I/O indicator routine */
  void do_io(String side, String s) /* Side is e.g. DWI */
  {
    int r,c,i,maxl;

    r=(-1); /* Error */
    c=0;
    maxl=0;

    if (side.equals("CWI")){r=7;c=1;maxl=7;}
    if (side.equals("DWI")){r=12;c=1;maxl=7;}
    if (side.equals("DWO")){r=23;c=1;maxl=7;}
    if (side.equals("CWO")){r=28;c=1;maxl=7;}

    if (side.equals("CNO")){r=5;c=5;maxl=6;}
    if (side.equals("DNO")){r=5;c=9;maxl=5;}
    if (side.equals("DNI")){r=5;c=20;maxl=3;}
    if (side.equals("CNI")){r=5;c=24;maxl=2;}

    if (side.equals("CSI")){r=30;c=5;maxl=6;}
    if (side.equals("DSI")){r=30;c=9;maxl=5;}
    if (side.equals("DSO")){r=30;c=20;maxl=3;}
    if (side.equals("CSO")){r=30;c=24;maxl=2;}

    if (side.equals("CEO")){r=7;c=28;maxl=7;}
    if (side.equals("DEO")){r=12;c=28;maxl=7;}
    if (side.equals("DEI")){r=23;c=28;maxl=7;}
    if (side.equals("CEI")){r=28;c=28;maxl=7;}

    if (r == (-1)){
      System.out.println("ERROR: \"" + side + "\" is an unknown side.");
      return;
    }

/* Remove spaces and quotes */
    s=condition(s);
    while (s.indexOf("\"") >= 0){
      s=s.substring(0,s.indexOf("\"")) + s.substring(s.indexOf("\"")+1);
    }

/* Truncate s as necessary */
    if (s.length() > maxl) s=s.substring(0,maxl);

/* EAST is special - we want to back up */
    if (side.substring(1,2).equals("E")) c=c-4*(s.length()-1);

/* Now draw the text! */
    for (i=0;i<s.length();i++){
      do_char(r,c,s.substring(i,i+1));
      c=c+4;
    }

    return;
  }

/* Text output routine */
  void do_text(String s)
  {
    int r,c,l,i;

/* Remove spaces and quotes */
    s=condition(s);
    while (s.indexOf("\"") >= 0){
      s=s.substring(0,s.indexOf("\"")) + s.substring(s.indexOf("\"")+1);
    }

    if (s.length() > 15) s=s.substring(0,15);

    l=s.length();
/* Can do up to 15 characters! */

    if (l <= 5){
      r=17;
      c=16-2*l;
      for (i=0;i<l;i++){
        do_char(r,c,s.substring(i,i+1));
        c=c+4;
      }
      return;

    }
    if (l <= 10){ /* Need to go over 2 rows */
      r=11;
      c=6;
      for (i=0;i<5;i++){
        do_char(r,c,s.substring(i,i+1));
        c=c+4;
      }

      s=s.substring(5);
      l=l-5;
      r=17;
      c=16-2*l;
      for (i=0;i<l;i++){
        do_char(r,c,s.substring(i,i+1));
        c=c+4;
      }
      return;
    }

/* 11-15 chars here */
    r=11;
    c=6;
    for (i=0;i<5;i++){
      do_char(r,c,s.substring(i,i+1));
      c=c+4;
    }

    s=s.substring(5);
    l=l-5;
    r=17;
    c=6;
    for (i=0;i<5;i++){
      do_char(r,c,s.substring(i,i+1));
      c=c+4;
    }

    s=s.substring(5);
    l=l-5;
    r=23;
    c=16-2*l;
    for (i=0;i<l;i++){
      do_char(r,c,s.substring(i,i+1));
      c=c+4;
    }
    return;

  }

  static int pix[]={5,5,7,5,7, 7,5,7,5,7, 7,4,4,4,7, /* A-Z */
                    6,5,5,5,6, 7,4,7,4,7, 4,4,7,4,7,
                    7,5,5,4,7, 5,5,7,5,5, 7,2,2,2,7, 
                    7,5,1,1,1, 5,6,4,6,5, 7,4,4,4,4,
                    5,5,5,7,5,
                    5,5,7,7,5, 7,5,5,5,7, 4,4,7,5,7,
                    7,7,5,5,7, 5,6,7,5,7, 7,1,7,4,7,
                    2,2,2,2,7, 7,5,5,5,5, 2,2,5,5,5,
                    5,7,7,5,5, 5,5,7,5,5, 2,2,7,5,5,
                    7,4,2,1,7, /* Now the numbers */
                    7,5,5,5,7, 7,2,2,2,6, 7,4,7,1,7,
                    7,1,3,1,7, 1,1,7,5,5, 7,1,7,4,7,
                    7,5,7,4,7, 2,2,1,1,7, 7,5,7,5,7,
                    1,1,7,5,7, /* Now some punctuation */
                    0,7,0,7,0, /* = */
                    0,2,7,2,0, /* + */
                    0,0,7,0,0, /* - */
                    4,4,2,1,1, /* / */
                    6,6,0,0,0,  /* . */
                    2,0,2,1,6,	/* ? */
                    2,0,2,2,2,	/* ! */
                    0,2,0,2,0	/* : */
                   };


  void do_char(int rr,int cc,String ch)
  {
    int ind,r,c,p;

    ind="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789=+-/.?!:".indexOf(ch);
    if (ind < 0){
      //System.out.println("Error: Don't know how to draw " + ch);
      return;
    }
    for (r=0;r<5;r++){
      for (c=0;c<3;c++){
        p=pix[5*ind+r] & (4>>c);
        if (p != 0) do_pixel(rr-r,cc+c,TEXTCOLOR);
      }
    }
  }

  void do_pixel(int r,int c,int color)
  {
    color=color&7;
    lib[current_r][current_c].iconbytes=
    lib[current_r][current_c].iconbytes.substring(0,32*r+c) + color +
    lib[current_r][current_c].iconbytes.substring(32*r+c+1);
  }

  void set_bg()
  {
    int i;
    lib[current_r][current_c].iconbytes="";
    for (i=0;i<1024;i++){
      lib[current_r][current_c].iconbytes=
      lib[current_r][current_c].iconbytes+BGCOLOR;
    }
  }

  boolean check_io(String s)
  {
    if (s.length() < 10) return(false);
    if (!s.substring(0,3).equals(".IO")) return(false);
    if (!s.substring(6,7).equals(",")){
      System.out.println("ERROR: Expecting .IO side,\"text\"");
      return(false);
    }
    do_io(s.substring(3,6),s.substring(7));
    return(true);
  }
}
