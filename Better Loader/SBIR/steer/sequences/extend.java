/*
 * Copyright (C) 1992-2001 Cell Matrix Corporation. All Rights Reserved.
 * No part of this code may be copied, stored, transmitted, conveyed,
 * or in any way used by any unauthorized person or persons. This code
 * contains intellectual property belonging to Cell Matrix Corporation
 * and/or its Directors.
 */

class extend{
  public static void main(String args[]){
    run("e",0,false);
    run("s",1,false);
    run("w",2,false);
    run("n",3,false);
    run("ep",0,true);
    run("sp",1,true);
    run("wp",2,true);
    run("np",3,true);
  }

  static void run(String fname,int rotate,boolean preclear)
  {
    int i;
    cell_matrix_interface cmi;  // Main interface object
    wire_build w;

    fname="X//"+fname;

    cmi=new cell_matrix_interface(fname,"Wire extension");
    w=new wire_build(cmi,"e",64,0,"CC,PC");
    if (preclear) w.preclear(true);
    for (i=0;i<rotate;i++) cmi.rotate("cw");
    w.extend();
    cmi.close();
  }
}
