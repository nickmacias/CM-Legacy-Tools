;
; Static and Dynamic ID assignment test
; 
; First load up a network of supercells
;
l 0 0 data/super0.bin
r 1
l 0 68 data/super.bin
r 1
l 0 136 data/super.bin
r 1
l 0 204 data/super.bin
r 1
l 0 272 data/super.bin
r 1
l 0 340 data/super.bin
r 1
l 68 0 data/super.bin
r 1
l 68 68 data/super.bin
r 1
l 68 136 data/super.bin
r 1
l 68 204 data/super.bin
r 1
l 68 272 data/super.bin
r 1
l 68 340 data/super.bin
r 1
l 136 0 data/super.bin
r 1
l 136 68 data/super.bin
r 1
l 136 136 data/super.bin
r 1
l 136 204 data/super.bin
r 1
l 136 272 data/super.bin
r 1
l 136 340 data/super.bin
r 1
l 204 0 data/super.bin
r 1
l 204 68 data/super.bin
r 1
l 204 136 data/super.bin
r 1
;***
;*** Exclude supercell [3,3]
;***
;***l 204 204 data/super.bin
;***
r 1
l 204 272 data/super.bin
r 1
l 204 340 data/super.bin
r 1
l 272 0 data/super.bin
r 1
l 272 68 data/super.bin
r 1
l 272 136 data/super.bin
r 1
l 272 204 data/super.bin
r 1
l 272 272 data/super.bin
r 1
l 272 340 data/super.bin
r 1
l 340 0 data/super.bin
r 1
l 340 68 data/super.bin
r 1
l 340 136 data/super.bin
r 1
l 340 204 data/super.bin
r 1
l 340 272 data/super.bin
r 1
l 340 340 data/super.bin
r 1
;
; Create the L2 network
s 28 0 dw 1
r 1
;
; Set DW inputs where we want edge cells
s 85 0 dw 1
s 153 0 dw 1
s 221 0 dw 1
;
; Start the state machines
s 37 0 dw 1
s 37 0 dw 0
;
r 40
;
H 15 57
H 15 125
H 15 193
H 15 261
H 15 329
H 15 397
;
H 83 57
H 83 125
H 83 193
H 83 261
H 83 329
H 83 397
;
H 151 57
H 151 125
H 151 193
H 151 261
H 151 329
H 151 397
;
H 219 57
H 219 125
H 219 193
H 219 261
H 219 329
H 219 397
;
H 287 57
H 287 125
H 287 193
H 287 261
H 287 329
H 287 397
;
H 355 57
H 355 125
H 355 193
H 355 261
H 355 329
H 355 397
;
