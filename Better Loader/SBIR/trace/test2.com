;
; Static and Dynamic ID assignment test
; 
;
; Load build network
;
l 0 0 build.bin
r 1
l 0 50 mid.bin
r 1
; Now set indexing for relative offset
is 55 55
i 0 1
; Load in supercell network
;
l 0 0 fulltrc.bin
r 1
;
; Set jumper for [0,0]
c 59 41
DE=1
.
c 41 62
DE=1
.
;
r 1
;
l 0 250 fulltrc.bin
b 0 250
r 1
l 0 500 fulltrc.bin
r 1
l 250 0 fulltrc.bin
r 1
l 250 250 fulltrc.bin
r 1
l 250 500 fulltrc.bin
;
; reset index to point to build circuit
is 250 250 ; Real index size
i 0 0
;
; Enable the build circuit
;
s 0 0 dw 1
;
; Make first path from W->E
s 8 0 dw 1
s 2 0 dw 1
r 1
s 2 0 dw 0
r 128   ; Ample time!
;
; Now set W->S path
s 16 0 dw 1
s 17 0 dw 1
s 2 0 dw 1
r 1
s 2 0 dw 0
r 128   ; Build that path
;
; Now set terminal node from North
s 7 0 dw 1
s 8 0 dw 0
s 16 0 dw 1
s 17 0 dw 0
;
; Show we have no inverter yet
o 14 0
s 14 0 dw 1
o 14 0
s 14 0 dw 0
o 14 0
;
; Now load final build
s 2 0 dw 1
r 1
s 2 0 dw 0
r 128   ; Do the final build
;
; Now check for inverter again
o 14 0
s 14 0 dw 1
o 14 0
s 14 0 dw 0
o 14 0
